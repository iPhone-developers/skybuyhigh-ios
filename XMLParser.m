//
//  XMLParser.m
//  XML
//
//  Created by Thapovan InfoSystems on 07/16/09.
//  Copyright 2009 Thapovan Info Systems
//

#import "XMLParser.h"
#import "SkyBuyHighAppDelegate.h"
#import "MenCategoryViewController.h"
#import "Products.h"

@implementation XMLParser
@synthesize categoryId,ownerId;

- (XMLParser *) initXMLParser {
	
	[super init];
	
	appDelegate = (SkyBuyHighAppDelegate *)[[UIApplication sharedApplication] delegate];
	menCategoryViewController =appDelegate.menTableViewController;
	womenCategoryViewController = appDelegate.womenTableViewController;
	giftCategoryViewController = appDelegate.giftTableViewController;
	privateJetCategoryViewController = appDelegate.privatejetTableViewController;
	menCategoryViewController.productInfo=	[[NSMutableArray alloc] init];
	womenCategoryViewController.productInfo=	[[NSMutableArray alloc] init];
	giftCategoryViewController.productInfo = [[NSMutableArray alloc] init];
	privateJetCategoryViewController.productInfo = [[NSMutableArray alloc] init];
	
	return self;
}

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName 
  namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qualifiedName 
	attributes:(NSDictionary *)attributeDict {
	
	if([elementName isEqualToString:@"Category"]) {
		//Initialize the array.
		//menCategoryViewController.productInfo=	[[NSMutableArray alloc] init];
		//womenCategoryViewController.productInfo=	[[NSMutableArray alloc] init];
		categoryId = [[attributeDict objectForKey:@"id"] integerValue];
		//NSLog(@"categoryId: %@", categoryId);
	}
	else if([elementName isEqualToString:@"Product"]) {
		
		//Initialize the book.
		prod = [[Products alloc] init];
		prod.prodImagesURL = [[NSMutableArray alloc] init];
		ownerId = [[attributeDict objectForKey:@"OwnerId"] integerValue];
		if([[attributeDict objectForKey:@"Adv"] isEqualToString:@"Y"])
			prod.isAdv = YES;
		else 
		    prod.isAdv = NO;		
		[prod setOwnerId:ownerId];
		[prod setCategoryId:categoryId];	
		//Extract the attribute here.
		//aBook.bookID = [[attributeDict objectForKey:@"id"] integerValue];
		
		
		//NSLog(@"Reading id value :%i", aBook.bookID);
	}
	
	//NSLog(@"Processing Element: %@", elementName);
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string { 
	
	if(!currentElementValue) 
		currentElementValue = [[NSMutableString alloc] initWithString:string];
	else
		[currentElementValue appendString:string];
	
	[currentElementValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
	//[currentElementValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
	//NSLog(@"Processing Value: %@", currentElementValue);
	
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName 
  namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
	
	if([elementName isEqualToString:@"Category"])
		return;
	
	//There is nothing to do if we encounter the Books element here.
	//If we encounter the Book element howevere, we want to add the book object to the array
	// and release the object.
	if([elementName isEqualToString:@"Product"]) {
		prod.totalImagesCount = [prod.prodImagesURL count];
		if(categoryId == 10)
			[menCategoryViewController.productInfo addObject:prod];
		else if(categoryId == 12)
			[womenCategoryViewController.productInfo addObject:prod];
		else if (categoryId == 14) 
			[giftCategoryViewController.productInfo addObject:prod];
		else if (categoryId == 20)
			[privateJetCategoryViewController.productInfo addObject:prod];
		
		[prod release];
		prod = nil;
	}
	else {
		if ([elementName isEqualToString:@"BRAND"]) {
			[prod setBRAND:[currentElementValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]];
		}
		else if ([elementName isEqualToString:@"ProductId"]) {
			[prod setProdId:[currentElementValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]];
			
		}
		else if ([elementName isEqualToString:@"ProductCode"]) {
			[prod setProdCode:[currentElementValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]];
		}		
		else if ([elementName isEqualToString:@"TITLE"]) {
			[prod setTITLE:[currentElementValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]];
		}
		else if ([elementName isEqualToString:@"MainLargeView"]) {
			
			[prod setMainImageURL:[currentElementValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]];
			
			if(!([[currentElementValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]	isEqualToString:@""]))
				[prod.prodImagesURL addObject: [currentElementValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]];
			
		}
		else if ([elementName isEqualToString:@"ViewOneLargeView"]) {
			if(!([[currentElementValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]	isEqualToString:@""]))
				[prod.prodImagesURL addObject: [currentElementValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]];
		}
		else if ([elementName isEqualToString:@"ViewTwoLargeView"]) {
			if(!([[currentElementValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]	isEqualToString:@""]))
				[prod.prodImagesURL addObject: [currentElementValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]];
		}
		else if ([elementName isEqualToString:@"ViewThreeLargeView"]) {
			if(!([[currentElementValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]	isEqualToString:@""]))
				[prod.prodImagesURL addObject: [currentElementValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]];
		}	
		else if ([elementName isEqualToString:@"SkyBuyPrice"]) {
			[prod setPrice:[currentElementValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]];
		}
		else if ([elementName isEqualToString:@"ShortDiscription"]) {
			[prod setShortDiscription:						 
			 [[currentElementValue stringByTrimmingCharactersInSet:
			   [NSCharacterSet whitespaceAndNewlineCharacterSet]] 
			  stringByReplacingOccurrencesOfString:@"<br>" withString:@" "]];
		}	
		else if ([elementName isEqualToString:@"LongDiscription"]) {
			NSMutableString *tempString = [[NSMutableString alloc] initWithString:[[currentElementValue stringByTrimmingCharactersInSet:
																					[NSCharacterSet whitespaceAndNewlineCharacterSet]] 
																				   stringByReplacingOccurrencesOfString:@"<br>" withString:@"\n"]];
			[tempString replaceOccurrencesOfString:@"<br/>" withString:@"\n" options:NSNumericSearch range:NSMakeRange(0, [tempString length])];
			[prod setLongDiscription: tempString];
			[tempString release];
		}
		else if ([elementName isEqualToString:@"MainThumbURL"]) {
			[prod setThumbnailURL:[currentElementValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]];
		}
		else if ([elementName isEqualToString:@"Price"]) {
			[prod setInStorePrice:[currentElementValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]];
			if ([[prod inStorePrice] isEqualToString:@"$0.00"]) {
				[prod setInStorePrice:@"-"];
			}
		}
		else if ([elementName isEqualToString:@"size"]) {
			//NSLog(@"%@",[currentElementValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]);
			if([[currentElementValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""])
				prod.prodSize =nil;
			else{	
				//currentElementValue.text = [@"Size," stringByAppendingString:currentElementValue];
				[prod setProdSize:(NSMutableArray *)[[@"Size,"  stringByAppendingString:[currentElementValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]] componentsSeparatedByString:@","]];
			
			}
			prod.size = @"Size";
		}
		else if ([elementName isEqualToString:@"color"]) {
			if([[currentElementValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""])
				prod.prodColor = nil;
			else{
				[prod setProdColor:(NSMutableArray *)[[@"Color," stringByAppendingString:[currentElementValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]] componentsSeparatedByString:@","]];
			}
			prod.color = @"Color";
		}
		
	}
	//[prod setValue:currentElementValue forKey:elementName];
	
	[currentElementValue release];
	currentElementValue = nil;
}

- (void)parser:(NSXMLParser *)parser parseErrorOccurred:(NSError *)parseError {
	NSLog(@"Error occured %@", parseError);
}


- (void) dealloc {
	
	[prod release];
	[currentElementValue release];
	[super dealloc];
}

@end
