//
//  MenCategoryController.m
//  SkyBuyHigh
//
//  Created by Thapovan InfoSystems on 02/07/09.
//  Copyright 2009 Thapovan. All rights reserved.
//

#import "MenCategoryController.h"
#import "Products.h"




@implementation MenCategoryController
@synthesize productInfo;

/*
 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        // Custom initialization
    }
    return self;
}
*/

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView {
}
*/

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
	
	Products *prod1=[[Products alloc] initWithProductBrand:@"BELLARI" prodctTitle:@"Mosaic Collection Bangle" productImage:@"1.jpg"];
	Products *prod2=[[Products alloc]initWithProductBrand:@"Adrienne Landau" prodctTitle:@"Cashmere and Fox Cape"  productImage:@"2.JPG"];
	Products *prod3=[[Products alloc]initWithProductBrand:@"Dior Watches" prodctTitle:@"CHIFFRE ROUGE 41mm Watch"  productImage:@"3.jpg"];
	Products *prod4=[[Products alloc]initWithProductBrand:@"Mauboussin" prodctTitle:@"Forever Fountain Pen" productImage:@"4.jpg"];
	Products *prod5=[[Products alloc]initWithProductBrand:@"DavidOff" prodctTitle:@"Large Humidor Collection" productImage:@"5.jpg"];
	// Configure and show the window
	self.productInfo=[[NSMutableArray alloc]initWithObjects:prod1,prod2,prod3,prod4,prod5,nil];

}


/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [super dealloc];
}


@end
