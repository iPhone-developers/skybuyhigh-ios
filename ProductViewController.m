//
//  ProductViewController.m
//  SampleProject
//
//  Created by Thapovan Info Systems on 16/06/09.
//  Copyright 2009 Thapovan. All rights reserved.
//

#import "ProductViewController.h"
#import "Products.h"
#import "SkyBuyHighAppDelegate.h"
#import "MenCategoryViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "ImageManipulations.h"
#import "ProductDetailsDAO.h"
#import "DeviceRegViewController.h"
#import "DataCache.h"

@implementation ProductViewController

@synthesize productDesc,productBrand,productTitle,productImage,productPrice;
@synthesize productData;
@synthesize checkoutBarButton;
@synthesize window;
@synthesize addToBagImage;
@synthesize toolbar;
@synthesize buyNowBarButton, productInStorePrice;
@synthesize imageController;
@synthesize personalShopper;
@synthesize personalShopperViewController;
@synthesize pickerView;
@synthesize pickerToolbar;
@synthesize productSize,productColor;
@synthesize addtoBagProductData;
@synthesize buttonTitle;
@synthesize sizeButton,colorButton;
@synthesize isSizeandColor;
@synthesize toolbarItems;
@synthesize checkoutViewController;
@synthesize pickeToolbarItems;
@synthesize selectedPickerColor,selectedPickerSize;


- (void)viewDidLoad { 
	//NSLog(@"Product viewDidLoad entered");
	[super viewDidLoad];
	
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        // Custom initialization
    }
    return self;
}

-(IBAction) showPersonalShopper:(id)sender {
	//UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Test" message:@"Testing" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
	//[alert show];
	//[alert release];
	if (personalShopperViewController == nil) {
		personalShopperViewController = [[PersonalShopperViewController alloc] initWithNibName:@"PersonalShopperViewController" bundle:nil];
		
	}

	[self.navigationController pushViewController:personalShopperViewController animated:YES];
}

- (void)showSizeandColorLabel
{
	//NSLog(@"show picker");
	[[self.view viewWithTag:991] removeFromSuperview];
	[[self.view viewWithTag:992] removeFromSuperview];
	[[self.view viewWithTag:993] removeFromSuperview];
	[[self.view viewWithTag:994] removeFromSuperview];
	
	isSizeandColor = NO;
	[self setButtonTitle:@""];
	if([self.productData.prodSize count]>0 && [self.productData.prodColor count]>0){		
		isSizeandColor = YES;
	}
	if(isSizeandColor){
		sizeButton = [UIButton buttonWithType:UIButtonTypeCustom];
		sizeButton.frame = CGRectMake(118, 126, 100, 19); 
		[sizeButton setTitle:@"SizeButton" forState:UIControlStateNormal];
		[sizeButton setImage:[UIImage imageNamed:@"addicon1.png"] forState:UIControlStateNormal];
		sizeButton.adjustsImageWhenHighlighted = YES;	
		sizeButton.tag = 991;
		[sizeButton addTarget:self action:@selector(showProductSizeandColorPicker:) forControlEvents:UIControlEventTouchUpInside];
		
		productSize = [[UILabel alloc] initWithFrame:CGRectMake(140, 123, 80, 21)];
		[productSize setBackgroundColor:[UIColor clearColor]];
		[productSize setText:@"Size"];
		productSize.tag = 992;
		[productSize setAdjustsFontSizeToFitWidth:YES];
		
		
		colorButton = [UIButton buttonWithType:UIButtonTypeCustom];
		colorButton.frame = CGRectMake(218, 126, 100, 19); 
		[colorButton setTitle:@"ColorButton" forState:UIControlStateNormal];
		[colorButton setImage:[UIImage imageNamed:@"addicon1.png"] forState:UIControlStateNormal];
		colorButton.adjustsImageWhenHighlighted = YES;	
		colorButton.tag = 993;
		[colorButton addTarget:self action:@selector(showProductSizeandColorPicker:) forControlEvents:UIControlEventTouchUpInside];
		
		productColor = [[UILabel alloc] initWithFrame:CGRectMake(240, 123, 80, 21)];
		[productColor setBackgroundColor:[UIColor clearColor]];
		[productColor setText:@"Color"];
		productColor.tag = 994;
		[productColor setAdjustsFontSizeToFitWidth:YES];
		
		[self.view addSubview:sizeButton];
		[self.view addSubview:productSize];		
		[self.view addSubview:colorButton];
		[self.view addSubview:productColor];		
		
	}
	else if((!isSizeandColor) && [self.productData.prodSize count]>0){
		
		sizeButton = [UIButton buttonWithType:UIButtonTypeCustom];
		sizeButton.frame = CGRectMake(118, 126, 100, 19); // size and position of button
		[sizeButton setTitle:@"SizeButton" forState:UIControlStateNormal];
		[sizeButton setImage:[UIImage imageNamed:@"addicon1.png"] forState:UIControlStateNormal];
		sizeButton.adjustsImageWhenHighlighted = YES;	
		sizeButton.tag = 991;
		[sizeButton addTarget:self action:@selector(showProductSizeandColorPicker:) forControlEvents:UIControlEventTouchUpInside];
		
		productSize = [[UILabel alloc] initWithFrame:CGRectMake(140, 123, 100, 21)];
		[productSize setBackgroundColor:[UIColor clearColor]];
		[productSize setText:@"Size"];
		[productSize setAdjustsFontSizeToFitWidth:YES];
		productSize.tag = 992;
		
		[self.view addSubview:sizeButton];
		[self.view addSubview:productSize];
	}	
	else if((!isSizeandColor) && [self.productData.prodColor count]>0){
		
		colorButton = [UIButton buttonWithType:UIButtonTypeCustom];
		colorButton.frame = CGRectMake(118, 126, 100, 19); // size and position of button
		[colorButton setTitle:@"ColorButton" forState:UIControlStateNormal];
		[colorButton setImage:[UIImage imageNamed:@"addicon1.png"] forState:UIControlStateNormal];
		colorButton.adjustsImageWhenHighlighted = YES;	
		colorButton.tag = 991;
		[colorButton addTarget:self action:@selector(showProductSizeandColorPicker:) forControlEvents:UIControlEventTouchUpInside];
		
		productColor = [[UILabel alloc] initWithFrame:CGRectMake(140, 123, 100, 21)];
		[productColor setBackgroundColor:[UIColor clearColor]];
		[productColor setText:@"Color"];
		productColor.tag = 992;
		[productColor setAdjustsFontSizeToFitWidth:YES];
		
		[self.view addSubview:colorButton];
		[self.view addSubview:productColor];
	}	
	
	
}

- (void)viewDidAppear:(BOOL)animated {
	
	isSizeandColor = NO;
	if(toolbar ==nil){
		toolbar =[[UIToolbar alloc] initWithFrame:CGRectMake(0,372,320,44)];
	}	
	if(toolbarItems == nil){
		toolbarItems = [NSArray arrayWithObjects:
						[[UIBarButtonItem alloc] initWithImage:[[UIImage imageNamed:@"photos-icon.png"]retain] style:UIBarButtonItemStylePlain target:self action:@selector(showProductImage:)],
						[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:NULL action:NULL],
						[[UIBarButtonItem alloc] initWithImage:[[UIImage imageNamed:@"prev-icon.png"]retain] style:UIBarButtonItemStylePlain target:self action:@selector(previousProduct:)],
						[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:NULL action:NULL],
						[[UIBarButtonItem alloc] initWithImage:[[UIImage imageNamed:@"next-icon.png"]retain] style:UIBarButtonItemStylePlain target:self action:@selector(nextProduct:)],
						[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:NULL action:NULL],
						[[UIBarButtonItem alloc] initWithImage:[[UIImage imageNamed:@"cart-icon.png"]retain] style:UIBarButtonItemStylePlain target:self action:@selector(checkoutAction:)],
						nil];
		
		toolbar.barStyle =UIBarStyleBlack;		
		toolbar.items = toolbarItems;
	}
	// Since only single instance of ProductViewController is created, enable all the buttons when view appears
	// so that if it is set to disabled in one category, it is not disabled in other categories as well.
	for (UIBarButtonItem *item in toolbarItems) {
		[item setEnabled:YES];
	}
	if (self.productData.thumbnailURL != nil) { 
		NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
		NSString *documentsDirectory = [paths objectAtIndex:0];
		NSString *fileName = [documentsDirectory stringByAppendingPathComponent:self.productData.thumbnailURL];
		if ([[DataCache tableViewImageCache] objectForKey:fileName] == nil) {
			UIImage *fileImage = [[UIImage alloc] initWithContentsOfFile:fileName];
			[ImageManipulations roundedCornersOfImage:fileImage withClipRadius:20 forKey:fileName];
			[fileImage release];
		}
		[productImage setImage:[UIImage imageWithCGImage:(CGImageRef)[[DataCache tableViewImageCache] objectForKey:fileName]]];
		productImage.tag = 999;
	}
	[productTitle setText:self.productData.TITLE];
	[productBrand setText:self.productData.BRAND];
	[productDesc setText:self.productData.LongDiscription];
	[productSize setText:@"Size"];
	[productColor setText:@"Color"];
	[productPrice setText:self.productData.Price];
	[productInStorePrice setText:self.productData.inStorePrice];
	/*buyNowBarButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"button-buynow.png"]
													   style:UIBarButtonItemStyleBordered
													  target:self
													  action:@selector(addToBagProducts:)];	*/
	if (buyNowBarButton == nil){
		UIButton *buyNowButton = [UIButton buttonWithType:UIButtonTypeCustom];
		[buyNowButton setFrame:CGRectMake(0, 0, 71, 30)];
		[buyNowButton setImage:[UIImage imageNamed:@"button-buynow.png"] forState:UIControlStateNormal];
		[buyNowButton addTarget:self action:@selector(addToBagProducts:) forControlEvents:UIControlEventTouchUpInside];
		buyNowBarButton = [[UIBarButtonItem alloc] initWithCustomView:buyNowButton];
	}
	
	self.navigationItem.rightBarButtonItem =buyNowBarButton;
	self.navigationItem.hidesBackButton = YES;
	self.navigationItem.leftBarButtonItem =	[[UIBarButtonItem alloc] initWithTitle:@"More Products"
																			 style:UIBarButtonItemStyleBordered
																			target:self
																			action:@selector(handleBack:)];
	//show product size and color label
	/*float height = 216.0f;
	pickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0.0f, 416.0f - height, 320.0f, height)];
	
	pickerToolbar =[[UIToolbar alloc] initWithFrame:CGRectMake(0,165,320,35)];
	pickeToolbarItems = [NSArray arrayWithObjects:							
						 [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:NULL action:NULL],
						 [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleBordered target:self action:@selector(hidePickerView:)],
						 nil];	
		
	*/
	[self showSizeandColorLabel];
	if ([[(SkyBuyHighAppDelegate *)[[UIApplication sharedApplication] delegate] addToBagProducts] count] > 0) {
		[[[toolbar items] objectAtIndex:6] setTitle:[NSString stringWithFormat:@"%d",[[(SkyBuyHighAppDelegate *)[[UIApplication sharedApplication] delegate] addToBagProducts] count]]];
	}else{
		[[[toolbar items] objectAtIndex:6] setTitle:@""];
	}	
	if(![(SkyBuyHighAppDelegate *)[[UIApplication sharedApplication] delegate] deviceRegStatus]){	
		[[[toolbar items] objectAtIndex:6] setEnabled:NO];
	}	
	
	
	id categoryViewController ;
	NSMutableArray *productInformation ;
	categoryViewController = [[self.navigationController viewControllers] objectAtIndex:0];
	productInformation = [categoryViewController productInfo];
	NSUInteger rowCount = [categoryViewController nsiCurrentRow];
	if(rowCount ==0){
		[[[toolbar items] objectAtIndex:2] setEnabled:NO];

	}else if((rowCount+1) == productInformation.count){
		[[[toolbar items] objectAtIndex:4] setEnabled:NO];
	}
	
	
	[self.view addSubview:toolbar];
	[super viewDidAppear:animated];
}



- (void) handleBack:(id)sender {
	[[self.view viewWithTag:10070] removeFromSuperview];
	[[self.view viewWithTag:10080] removeFromSuperview];
	[self.navigationController popToRootViewControllerAnimated:YES];
}



- (void)checkoutAction:(id)sender {
	
	SkyBuyHighAppDelegate *delegate=(SkyBuyHighAppDelegate *)[[UIApplication sharedApplication]delegate];	
	if([delegate.addToBagProducts count] >0){
		if (checkoutViewController) {
			[checkoutViewController release];
		}
		checkoutViewController = [[CheckOutProductsController alloc] init];
		 [self.navigationController pushViewController:checkoutViewController animated:YES];
		
		/*[UIView beginAnimations:nil context:NULL];
		[UIView setAnimationDuration:0.75];
		[UIView setAnimationTransition:UIViewAnimationTransitionFlipFromLeft forView:delegate.menNavController.view cache:YES];
		[self.navigationController pushViewController:checkoutViewController animated:YES];
		[UIView commitAnimations]; */
	}
	else {
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"SkyBuyHigh" message:@"You have no items to checkout" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
		[alert show];
		[alert release];
	}
}


- (void)viewDidDisappear:(BOOL)animated {
	selectedPickerSize = 0;
	selectedPickerColor = 0;
	[[self.view viewWithTag:10070] removeFromSuperview];
	[[self.view viewWithTag:10080] removeFromSuperview];
	[super viewDidDisappear:animated];
}

-(void)addToBagProducts:(id)sender
{	
	
	if([(SkyBuyHighAppDelegate *)[[UIApplication sharedApplication] delegate] deviceRegStatus])
	{	
		NSMutableArray *productInfomation=nil ;
		id categoryViewController=nil;
		SkyBuyHighAppDelegate *delegate=(SkyBuyHighAppDelegate *)[[UIApplication sharedApplication]delegate];
		categoryViewController = [[self.navigationController viewControllers] objectAtIndex:0];
		productInfomation = [categoryViewController productInfo];
		NSUInteger rowCount = [categoryViewController nsiCurrentRow];
		Products *prod= [productInfomation objectAtIndex:rowCount];
		NSString *alertMsg=[[NSString alloc] init];
		if ([prod.prodSize count]>0 && [[productSize text] isEqualToString:@"Size"] && [prod.prodColor count]>0 && [[productColor text] isEqualToString:@"Color"] ){
			alertMsg = @"Please select Product Size and Color.";
		}else if([prod.prodSize count]>0 && [[productSize text] isEqualToString:@"Size"]){
			alertMsg = @"Please select Product Size.";
		}else if([prod.prodColor count]>0 && [[productColor text] isEqualToString:@"Color"]){
			alertMsg = @"Please select Product Color.";
		}
		else{
		
			[self hidePickerView:nil];
			
			Products *addToBagProductsDetails = [[Products alloc] initWithProductData:prod];
			[delegate.addToBagProducts addObject:addToBagProductsDetails];
			//[(UILabel *)[[[[toolbar items] objectAtIndex:6] customView] viewWithTag:88] setText:[NSString stringWithFormat:@"%d",[delegate.addToBagProducts count]]];
			[addToBagProductsDetails release]; addToBagProductsDetails=nil;
			
			addToBagImage=[[UIImageView alloc]init];
			//UIImage *img=self.productImage.image;
			addToBagImage.frame=CGRectMake(10, 62, 103, 100);
			addToBagImage.image=self.productImage.image;
			[self.view addSubview:addToBagImage];
			CGPoint checkOutLocation = CGPointMake(335,455);
			[UIView beginAnimations:@"center" context:nil];
			[UIView setAnimationDuration:0.75];
			addToBagImage.transform = CGAffineTransformRotate(CGAffineTransformIdentity, 90);
			addToBagImage.center=checkOutLocation;
			addToBagImage.alpha=0;	
			addToBagImage.bounds = CGRectMake(335,455, 0, 0);
			[UIView commitAnimations];
			[addToBagImage release];
			[[[toolbar items] objectAtIndex:6] setTitle:[NSString stringWithFormat:@"%d",[delegate.addToBagProducts count]]];
		}
		if(![alertMsg isEqualToString:@""]){
			UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"SkyBuyHigh" message:alertMsg delegate:nil cancelButtonTitle:@"OK"
												  otherButtonTitles:nil];	
			[alert show];
			[alert release];
		} 
	}else {
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"SkyBuyHigh" message:@"In order to make purchases, this device must be registered with SkyBuyHigh. Please contact register@skybuyhigh.com for more details on how to register." delegate:self cancelButtonTitle:@"OK"
											  otherButtonTitles:nil];	
		[alert show];
		[alert release];
	}
	
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
	if(buttonIndex  == 0){
			
		/*if (!([[Reachability sharedReachability] internetConnectionStatus] == NotReachable)) {
			DeviceRegViewController *deviceRegViewController = [[DeviceRegViewController alloc] initWithNibName:@"DeviceRegView" bundle:nil];
			deviceRegViewController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
			[self presentModalViewController:deviceRegViewController animated:YES];
		}	
		else{
			UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"SkyBuyHigh" message:@"You need to activate Wi-Fi (or) 3G in order to register this device." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];		
			[alert show];
			[alert release];
		}	*/
		
			
	}	
	//[alertView release];
}

-(void)nextProduct:(id)sender
{
	NSMutableArray *productInfomation ;
	id categoryViewController ;
	categoryViewController = [[self.navigationController viewControllers] objectAtIndex:0]; 
	
	productInfomation = [categoryViewController productInfo];
	NSUInteger rowCount = [categoryViewController nsiCurrentRow];
	
	if((rowCount + 1) == productInfomation.count){
		//rowCount =rowCount - 1;
		[[[toolbar items] objectAtIndex:4] setEnabled:NO];
	}
	else if (rowCount == 0) {
		[[[toolbar items] objectAtIndex:2] setEnabled:YES];
	}	
	if ([[[toolbar items] objectAtIndex:4] isEnabled]) {
		Products *prod= [productInfomation objectAtIndex:rowCount+1];	
		[categoryViewController setNsiCurrentRow:rowCount+1];
	
		if(prod != nil) { 
			selectedPickerSize = 0;
			selectedPickerColor = 0;
			self.productData = prod;
			if (prod.thumbnailURL != nil) { 
				NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
				NSString *documentsDirectory = [paths objectAtIndex:0];
				NSString *fileName = [documentsDirectory stringByAppendingPathComponent:prod.thumbnailURL];
			
				if ([[DataCache tableViewImageCache] objectForKey:fileName] == nil) {
					UIImage *fileImage = [[UIImage alloc] initWithContentsOfFile:fileName];
					[ImageManipulations roundedCornersOfImage:fileImage withClipRadius:20 forKey:fileName];
					[fileImage release];
				}
				[productImage setImage:[UIImage imageWithCGImage:(CGImageRef)[[DataCache tableViewImageCache] objectForKey:fileName]]];
				self.productImage.tag = 999;
			}
			[self.productTitle setText:prod.TITLE];
			[self.productBrand setText:prod.BRAND];	
			[self.productDesc setText:prod.LongDiscription];
			[self.productPrice setText:prod.Price];
			[self.productInStorePrice setText:prod.inStorePrice];
		}	
		[self showSizeandColorLabel];
		CATransition *animation = [CATransition animation];
		//animation.type = @"oglFlip";
		animation.type = kCATransitionReveal;
		animation.subtype = kCATransitionFromRight;
		animation.duration = 0.75;  //Or whatever
		//animation.speed = 0.75;
		animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionDefault]; //Or whatever
		//animation.startProgress = 0.25;  //Set this as needed
		//animation.endProgress = 0.75;  //Set this as needed
		animation.fillMode = kCAFillModeForwards;
		[[[self view] layer] addAnimation:animation forKey:kCATransition];
		
	}
	if((rowCount+2) == productInfomation.count){
		[[[toolbar items] objectAtIndex:4] setEnabled:NO];
	}
}

-(void)previousProduct:(id)sender
{
	id categoryViewController ;
	NSMutableArray *productInfomation ;
	categoryViewController = [[self.navigationController viewControllers] objectAtIndex:0];
	productInfomation = [categoryViewController productInfo];
	NSUInteger rowCount = [categoryViewController nsiCurrentRow];
	
	if(rowCount == 0){
		//rowCount =rowCount + 1;
		[[[toolbar items] objectAtIndex:2] setEnabled:NO];
	}		
	else if(rowCount + 1 == productInfomation.count) {
		[[[toolbar items] objectAtIndex:4] setEnabled:YES];
	}	
	if ([[[toolbar items] objectAtIndex:2] isEnabled]) { 
		Products *prod= [productInfomation objectAtIndex:rowCount-1];	
		[categoryViewController setNsiCurrentRow:rowCount-1];
	
		if(prod != nil) { 	
			self.productData = prod;
			if (prod.thumbnailURL != nil) { 
				selectedPickerSize = 0;
				selectedPickerColor = 0;
				NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
				NSString *documentsDirectory = [paths objectAtIndex:0];
				NSString *fileName = [documentsDirectory stringByAppendingPathComponent:prod.thumbnailURL];
				if ([[DataCache tableViewImageCache] objectForKey:fileName] == nil) {
					UIImage *fileImage = [[UIImage alloc] initWithContentsOfFile:fileName];
					[ImageManipulations roundedCornersOfImage:fileImage withClipRadius:20 forKey:fileName];
					[fileImage release];
				}
				[productImage setImage:[UIImage imageWithCGImage:(CGImageRef)[[DataCache tableViewImageCache] objectForKey:fileName]]];
				self.productImage.tag = 999;
			}
			[self.productTitle setText:prod.TITLE];
			[self.productBrand setText:prod.BRAND];
			[self.productDesc setText:prod.LongDiscription];
			[self.productPrice setText:prod.Price];
			[self.productInStorePrice setText:prod.inStorePrice];
		}
		[self showSizeandColorLabel];
		CATransition *animation = [CATransition animation];
		//animation.type = @"oglFlip";
		animation.type = kCATransitionReveal;
		animation.subtype = kCATransitionFromLeft;
		animation.duration = 0.75;  //Or whatever
		//animation.speed = 0.75;
		animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionDefault]; //Or whatever
		//animation.startProgress = 0.25;  //Set this as needed
		//animation.endProgress = 0.75;  //Set this as needed
		animation.fillMode = kCAFillModeForwards;
		[[[self view] layer] addAnimation:animation forKey:kCATransition];
		
	}
	
	if(rowCount == 1){
		[[[toolbar items] objectAtIndex:2] setEnabled:NO];
	}
	
}


-(void)showProductImage:(id)sender
{
	
	id categoryViewController ;
	id selectedNavController;
	NSMutableArray *productInformation ;
	selectedNavController = self.navigationController;
	categoryViewController = [[selectedNavController viewControllers] objectAtIndex:0];
	imageController = [[selectedNavController viewControllers] objectAtIndex:1];

	imageController.hidesBottomBarWhenPushed = YES;	
	productInformation = [categoryViewController productInfo];
	NSUInteger rowCount = [categoryViewController nsiCurrentRow];
	Products *product= [productInformation objectAtIndex:rowCount];	
	imageController.currentProduct = product;
	imageController.productImage =  product.MainImageURL;
	
	[UIView beginAnimations:@"imageZoom" context:NULL];
	[UIView setAnimationDuration:0.65];
	[UIView setAnimationTransition:UIViewAnimationTransitionFlipFromRight forView:[selectedNavController view] cache:YES];
	//[delegate.menNavController pushViewController:imageController animated:NO];
	[selectedNavController popViewControllerAnimated:NO];
	[UIView commitAnimations];
}


#pragma mark -
#pragma mark pickerView Controls

-(void)hidePickerView:(id)sender
{
		
	/*pickerView.hidden = YES;
	 pickerToolbar.hidden = YES;
	 [pickerView release]; pickerView = nil;
	 [pickerToolbar release]; pickerToolbar = nil;
	 [pickeToolbarItems release]; pickeToolbarItems =nil;*/
	
	[[self.view viewWithTag:10070] removeFromSuperview];
	[[self.view viewWithTag:10080] removeFromSuperview];
	
	if(isSizeandColor){
		[sizeButton setEnabled:YES];	
		[colorButton setEnabled:YES];
	}else{
		if([buttonTitle isEqualToString:@"SizeButton"])
			[sizeButton setEnabled:YES];
		else if([buttonTitle isEqualToString:@"ColorButton"])
			[colorButton setEnabled:YES];
	}
}


-(void)showProductSizeandColorPicker:(id)sender
{
	
	//NSLog(@"Inside showProductSizeandColorPicker");
	
	/*pickerView.hidden = YES;
	pickerToolbar.hidden = YES;
	[pickerView release]; pickerView = nil;
	[pickerToolbar release]; pickerToolbar = nil;
	[pickeToolbarItems release]; pickeToolbarItems =nil;*/
	
	buttonTitle = [sender titleForState:UIControlStateNormal] ;
	if(isSizeandColor){
		[sizeButton setEnabled:NO];	
		[colorButton setEnabled:NO];
	}else{
		if([buttonTitle isEqualToString:@"SizeButton"]){
			[sizeButton setEnabled:NO];		
		}	
		else if([buttonTitle isEqualToString:@"ColorButton"]){
			[colorButton setEnabled:NO];		
		}
	}	
	// Add the picker
	float height = 216.0f;
	pickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0.0f, 416.0f - height, 320.0f, height)];
	pickerView.delegate = self;
	pickerView.tag = 10070;
	pickerView.showsSelectionIndicator = YES;	
	if([buttonTitle isEqualToString:@"SizeButton"]){
		[pickerView selectRow:selectedPickerSize inComponent:0 animated:YES];
	}	
	else if([buttonTitle isEqualToString:@"ColorButton"]){
		[pickerView selectRow:selectedPickerColor inComponent:0 animated:YES];	
	}
	
	[self.view addSubview:pickerView];
	[pickerView release];
	pickerToolbar =[[UIToolbar alloc] initWithFrame:CGRectMake(0,165,320,35)];
	pickeToolbarItems = [NSArray arrayWithObjects:							
							 [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:NULL action:NULL],
							 [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleBordered target:self action:@selector(hidePickerView:)],
							 nil];	
	pickerToolbar.barStyle =UIBarStyleBlack;
	pickerToolbar.items = pickeToolbarItems;
	pickerToolbar.tag = 10080;
	
	[self.view addSubview:pickerToolbar];	
	//[pickerToolbar release];
	
	// Manage re-sizing and rotation
	self.view.autoresizesSubviews = YES;
	self.view.autoresizingMask = (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight);	
	//[pickeToolbarItems release];
	
	
}	

// Number of wheels
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView { return 1; }

// Number of rows per wheel
- (NSInteger)pickerView: (UIPickerView *)pView numberOfRowsInComponent: (NSInteger) component 
{ 
	//NSLog(@"%d",[productData.prodSize count]);
	if([buttonTitle isEqualToString:@"SizeButton"]) {
		return ([self.productData.prodSize count]); 
	}
	else {
		return ([self.productData.prodColor count]); 
	}
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
	//[self.productSize objectAtIndex:row];
	if([buttonTitle isEqualToString:@"SizeButton"]){
		return [self.productData.prodSize objectAtIndex:row];
	}	
	else  {
		return [self.productData.prodColor objectAtIndex:row];
	}	
}



// Respond to user selection
- (void)pickerView:(UIPickerView *)aPickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
	//NSLog(@"didSelectRow entered");
	//NSLog(@"row %d",row);
	
	if([buttonTitle isEqualToString:@"SizeButton"]){
		[productSize setText:[productData.prodSize objectAtIndex:row]];
		self.productData.size = [productData.prodSize objectAtIndex:row];
		selectedPickerSize = row;
	}	
	else{ 
		[productColor setText:[productData.prodColor objectAtIndex:row]];
		self.productData.color = [productData.prodColor objectAtIndex:row];	
		selectedPickerColor = row;
	}	
	
}	


// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
   // return (interfaceOrientation == UIInterfaceOrientationPortrait);
	return YES;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning]; // Releases the view if it doesn't have a superview
    // Release anything that's not essential, such as cached data
}

- (void)dealloc {
	[productDesc release],productDesc = nil;
	[productBrand release],productBrand = nil;
	[productTitle release],productTitle = nil;
	[productImage release],productImage = nil;
	[productPrice release],productPrice = nil;
	[productData release],productData = nil;
	[checkoutBarButton release],checkoutBarButton = nil;
	[window release],window = nil;
	[addToBagImage release],addToBagImage = nil;
	[toolbar release],toolbar = nil;
	[buyNowBarButton release],buyNowBarButton = nil;
	[productInStorePrice release],productInStorePrice = nil;
	[imageController release],imageController = nil;
	[personalShopper release],personalShopper = nil;
	[personalShopperViewController release],personalShopperViewController = nil;
	[pickerView release],pickerView=nil;
	[pickerToolbar release],pickerToolbar = nil;
	[productSize release],productSize=nil;
	[productColor release],productColor = nil;
	[addtoBagProductData release],addtoBagProductData = nil;
	[buttonTitle release],buttonTitle=nil;
	[sizeButton release],sizeButton = nil;
	[colorButton release],colorButton = nil;
	[toolbarItems release],toolbarItems = nil;
	[checkoutViewController release],checkoutViewController = nil;
	[pickeToolbarItems release], pickeToolbarItems = nil;
	[super dealloc];
}


#pragma mark -
#pragma mark Additional Touch Events

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event { 
	
	//Get all the touches.
	NSSet *allTouches = [event allTouches];
	switch ([allTouches count])
	{
		case 1:
		{
			UITouch *touch = [[allTouches allObjects] objectAtIndex:0];
			CGPoint pt = [touch locationInView:self.view];
			if (pt.x >= 13 && pt.x <= 113 && pt.y >= 62 && pt.y <= 162)
				switch ([touch tapCount]) {
					case 1: [self showProductImage:nil]; break;
						
				}
		}
	}
}


@end
