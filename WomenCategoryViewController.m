//
//  WomenCategoryViewController.m
//  SkyBuyHigh
//
//  Created by Thapovan InfoSystems on 02/07/09.
//  Copyright 2009 Thapovan. All rights reserved.
//

#import "WomenCategoryViewController.h"
#import "Products.h"
#import "SkyBuyHighAppDelegate.h"
#import "MenCategoryViewController.h"
#import "GiftCategoryViewController.h"
#import "PrivateJetCategoryViewController.h"
#import "CheckOutProductsController.h"
#import "ProductImageZoomController.h"
#import "ImageManipulations.h"
#import "DataCache.h"
#import "ControllerManager.h"

@implementation WomenCategoryViewController
@synthesize productInfo;
//@synthesize productViewController;
@synthesize nsiCurrentRow, imageController;
@synthesize myCustomCell;

- (id)initWithStyle:(UITableViewStyle)style {
    // Override initWithStyle: if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
    if (self = [super initWithStyle:style]) {
    }
    return self;
}

- (void)viewDidLoad { 
    
	[super viewDidLoad];
}



- (void)viewWillAppear:(BOOL)animated {

	[super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated {
	[DataCache clearThumbnailCache];
	[DataCache clear];
    [super viewDidAppear:animated];
}


- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
	[DataCache clear];
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}


#pragma mark Table view methods

// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return productInfo.count;
}

- (CGFloat)tableView:(UITableView *)tableView
heightForRowAtIndexPath:(NSIndexPath *) indexPath
{
	return 85;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
		 cellForRowAtIndexPath:(NSIndexPath *) indexPath {
	
	static NSString *CellIdentifier = @"CellIdentifier";
	UITableViewCell *cell = (UITableViewCell *) [tableView dequeueReusableCellWithIdentifier:CellIdentifier];	
	
	if (cell == nil) {
		//cell = [[[ProductTableView alloc] initWithFrame:CGRectZero reuseIdentifier:CellIdentifier] autorelease];
		[[NSBundle mainBundle] loadNibNamed:@"ProductViewCell" owner:self options:nil];
		cell = myCustomCell;
		myCustomCell = nil;
		NSString *backgroundImagePath = [[NSBundle mainBundle] pathForResource:@"rowbg-85-gray" ofType:@"png"];
		UIImage *backgroundImage = [[UIImage alloc] initWithContentsOfFile:backgroundImagePath];
		cell.backgroundView = [[[UIImageView alloc] initWithImage:backgroundImage] autorelease];
		[backgroundImage release];
		//cell.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"rowbg.png"]];
		cell.accessoryType = UITableViewCellAccessoryDetailDisclosureButton;
	}
	
	NSUInteger row = [indexPath row];
	Products *rowData = [self.productInfo objectAtIndex:row];
	//[cell setData:rowData];
	[(UILabel *)[cell viewWithTag:2] setText:[rowData BRAND]];
	[(UILabel *)[cell viewWithTag:3] setText:[rowData TITLE]];
	[(UILabel *)[cell viewWithTag:4] setText:[rowData Price]];
	
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	NSString *fileName = [documentsDirectory stringByAppendingPathComponent:rowData.thumbnailURL];
	//NSLog(@"Path to image = %@", fileName);
	[[cell.contentView viewWithTag:1010] removeFromSuperview];
	//image = [[DataCache tableViewImageCache] objectForKey:fileName];
	if ([[DataCache tableViewImageCache] objectForKey:fileName] == nil) {
		UIImage *fileImage = [[UIImage alloc] initWithContentsOfFile:fileName];
		//image = [[UIImage alloc] initWithCGImage:
		[ImageManipulations roundedCornersOfImage:fileImage withClipRadius:20 forKey:fileName];
		//[[DataCache tableViewImageCache] setObject:image forKey:fileName];
		//[image release];
		[fileImage release];
	}
	[(UIImageView *) [cell viewWithTag:1] setImage:[UIImage imageWithCGImage:(CGImageRef)[[DataCache tableViewImageCache] objectForKey:fileName]]];
	return cell;
}

- (void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath {
	[self tableView:tableView didSelectRowAtIndexPath:indexPath];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	if (imageController == nil) {
		imageController = (ProductImageZoomController *)[[ControllerManager instance] controllerWithName:@"ProductImageZoomController" andNib:@"ProductImageView"];
	}
	imageController.hidesBottomBarWhenPushed = YES;
	NSUInteger row = [indexPath row];
	Products *product = [productInfo objectAtIndex:row];
	//[imageController.title setText:[product TITLE]];
	
	imageController.currentProduct = product;
	imageController.productImage =  product.MainImageURL;
	//[imageController.scrollView setZoomScale:1.0];
	//[imageController.scrollView setContentSize:CGSizeMake(280,350)];
	
	SkyBuyHighAppDelegate *delegate = [[UIApplication sharedApplication] delegate];
	//[UIView beginAnimations:nil context:UIGraphicsGetCurrentContext()];
	//[UIView setAnimationDuration:0.75];
	//[UIView setAnimationTransition:UIViewAnimationTransitionFlipFromLeft forView:delegate.womenNavController.view cache:NO];
	
	[delegate.womenNavController pushViewController:imageController animated:YES];
	//[UIView commitAnimations];
	
	nsiCurrentRow = row;
	
}


- (void)dealloc {

    [super dealloc];
}


@end

