//
//  PrivateJetCategoryViewController.h
//  SkyBuyHigh
//
//  Created by Thapovan InfoSystems on 02/07/09.
//  Copyright 2009 Thapovan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProductViewController.h"
#import "ProductImageZoomController.h"

@interface PrivateJetCategoryViewController : UITableViewController
<UITableViewDelegate, UITableViewDataSource> {
	
	NSMutableArray *productInfo;
	ProductViewController *productViewController;
	NSUInteger nsiCurrentRow;
	ProductImageZoomController *imageController;
	UITableViewCell *myCustomCell;
	UIImage *image;
}

@property(nonatomic,retain) NSMutableArray *productInfo;
//@property(nonatomic,retain) ProductViewController *productViewController;
@property (nonatomic) NSUInteger nsiCurrentRow;
@property (nonatomic, retain) ProductImageZoomController *imageController;
@property (nonatomic, retain) IBOutlet UITableViewCell *myCustomCell;

@end
