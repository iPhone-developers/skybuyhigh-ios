//
//  CustomerShippingDetailsController.m
//  SkyBuyHigh
//
//  Created by Thapovan InfoSystems on 30/07/09.
//  Copyright 2009 Thapovan. All rights reserved.
//

#import "CustomerShippingDetailsController.h"
#import "OrderConfirmationViewController.h"
#import "SkyBuyHighAppDelegate.h"
#import "TailNumberViewController.h"


@implementation CustomerShippingDetailsController
@synthesize webView;


- (void)viewWillAppear:(BOOL)animated {
	/*NSString *htmlFile = [[NSBundle mainBundle] pathForResource:@"CustomerShippingDetails" ofType:@"html"];
	NSData *htmlData = [NSData dataWithContentsOfFile:htmlFile];
	webView.delegate = self;
	[webView loadData:htmlData MIMEType:@"text/html" textEncodingName:@"UTF-8" baseURL:[NSURL URLWithString:@""]];*/
	
}


- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidLoad {
	NSString *htmlFile = [[NSBundle mainBundle] pathForResource:@"CustomerShippingDetails" ofType:@"html"];
	NSData *htmlData = [NSData dataWithContentsOfFile:htmlFile];
	webView.delegate = self;
	[webView loadData:htmlData MIMEType:@"text/html" textEncodingName:@"UTF-8" baseURL:[NSURL URLWithString:@""]];
	[super viewDidLoad];
}	
- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}


- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request  navigationType:(UIWebViewNavigationType)navigationType {
   
	SkyBuyHighAppDelegate *delegate=(SkyBuyHighAppDelegate *)[[UIApplication sharedApplication]delegate];
	CustomerDetails *customerShippingDetails =(CustomerDetails *) delegate.customerShippingDetails;
	NSString *requestString = [[request URL] absoluteString];
	NSArray *components = [requestString componentsSeparatedByString:@":"];
	if ([components count] > 1 && 
		[(NSString *)[components objectAtIndex:0] isEqualToString:@"http"]) {
		if([(NSString *)[components objectAtIndex:2] isEqualToString:@"FirstName"]) 
		{
			[customerShippingDetails setCustFirstName:[[components objectAtIndex:3] stringByReplacingOccurrencesOfString:@"%20" withString:@" "]];
		}
		if([(NSString *)[components objectAtIndex:4] isEqualToString:@"LastName"]) 
		{
			[customerShippingDetails setCusLastName:[[components objectAtIndex:5] stringByReplacingOccurrencesOfString:@"%20" withString:@" "]];
		}
		if([(NSString *)[components objectAtIndex:6] isEqualToString:@"Address1"]) 
		{
			[customerShippingDetails setAddress1:[[components objectAtIndex:7] stringByReplacingOccurrencesOfString:@"%20" withString:@" "]];
		}
		if([(NSString *)[components objectAtIndex:8] isEqualToString:@"Address2"]) 
		{
			[customerShippingDetails setAddress2:[[components objectAtIndex:9] stringByReplacingOccurrencesOfString:@"%20" withString:@" "]];
		}
		if([(NSString *)[components objectAtIndex:10] isEqualToString:@"City"]) 
		{
			[customerShippingDetails setCity:[[components objectAtIndex:11] stringByReplacingOccurrencesOfString:@"%20" withString:@" "]];
		}
		if([(NSString *)[components objectAtIndex:12] isEqualToString:@"State"]) 
		{
			[customerShippingDetails setState:[[components objectAtIndex:13] stringByReplacingOccurrencesOfString:@"%20" withString:@" "]];
		}
		if([(NSString *)[components objectAtIndex:14] isEqualToString:@"Country"]) 
		{
			[customerShippingDetails setCountry:[[components objectAtIndex:15] stringByReplacingOccurrencesOfString:@"%20" withString:@" "]];
		}
		if([(NSString *)[components objectAtIndex:16] isEqualToString:@"Zip"]) 
		{
			[customerShippingDetails setZip:[[components objectAtIndex:17] stringByReplacingOccurrencesOfString:@"%20" withString:@" "]];
		}
		
		if([(NSString *)[components objectAtIndex:18] isEqualToString:@"Phone"]) 
		{
			[customerShippingDetails setPhone:[[components objectAtIndex:19] stringByReplacingOccurrencesOfString:@"%20" withString:@" "]];
		}
		if([(NSString *)[components objectAtIndex:20] isEqualToString:@"Email"]) 
		{
			[customerShippingDetails setEmail:[[components objectAtIndex:21] stringByReplacingOccurrencesOfString:@"%20" withString:@" "]];			
		}
		
	}	
	
	/*OrderConfirmationViewController *orderConfirmationViewController = [[OrderConfirmationViewController alloc] initWithNibName:@"OrderConfirmationView" bundle:nil];
	[self.navigationController pushViewController:orderConfirmationViewController animated:NO];*/
	TailNumberViewController *tailNumberViewController = [[TailNumberViewController alloc] initWithNibName:@"TailNumberView" bundle:nil];
	[self.navigationController pushViewController:tailNumberViewController animated:NO];
	
	return YES; // Return YES to make sure regular navigation works as expected.
}

-(void)webViewDidFinishLoad:(UIWebView *)webView1 {
	SkyBuyHighAppDelegate *delegate=(SkyBuyHighAppDelegate *)[[UIApplication sharedApplication]delegate];
	CustomerDetails *customerShippingDetails =(CustomerDetails *) delegate.customerShippingDetails;
	
	NSString *firstName = nil;
	NSString *lastName = nil;
	NSString *address1 = nil;
	NSString *address2 = nil;
	NSString *city = nil;
	NSString *state = nil;
	NSString *country= nil;
	NSString *zip = nil;
	NSString *phone = nil;
	NSString *email = nil;
	
	if ([customerShippingDetails custFirstName] != nil) {
		firstName = [NSString stringWithFormat:@"%@",[customerShippingDetails custFirstName]];
	}
	else {
		firstName =[[NSString alloc] initWithString:@""];
	}
	
	if ([customerShippingDetails cusLastName] != nil) {
		lastName = [NSString stringWithFormat:@"%@",[customerShippingDetails cusLastName]];
	}
	else {
		lastName =[[NSString alloc] initWithString:@""];
	}
	
	if ([customerShippingDetails address1] != nil) {
		address1 = [NSString stringWithFormat:@"%@",[customerShippingDetails address1]];
	}
	else {
		address1 =[[NSString alloc] initWithString:@""];
	}
	
	if ([customerShippingDetails address2] != nil) {
		address2 = [NSString stringWithFormat:@"%@",[customerShippingDetails address2]];
	}
	else {
		address2 =[[NSString alloc] initWithString:@""];
	}
	
	if ([customerShippingDetails city] != nil) {
		city = [NSString stringWithFormat:@"%@",[customerShippingDetails city]];
	}
	else {
		city =[[NSString alloc] initWithString:@""];
	}
	
	if ([customerShippingDetails state] != nil) {
		state = [NSString stringWithFormat:@"%@",[customerShippingDetails state]];
	}
	else {
		state =[[NSString alloc] initWithString:@""];
	}
	
	if ([customerShippingDetails country] != nil) {
		country = [NSString stringWithFormat:@"%@",[customerShippingDetails country]];
	}
	else {
		country =[[NSString alloc] initWithString:@""];
	}
	
	if ([customerShippingDetails zip] != nil) {
		zip = [NSString stringWithFormat:@"%@",[customerShippingDetails zip]];
	} 
	else {
		zip =[[NSString alloc] initWithString:@""];
	}
	
	if ([customerShippingDetails phone] != nil) {
		phone = [NSString stringWithFormat:@"%@",[customerShippingDetails phone]];
	}
	else {
		phone =[[NSString alloc] initWithString:@""];
	}
	
	if ([customerShippingDetails email] != nil) {
		email = [NSString stringWithFormat:@"%@",[customerShippingDetails email]];
	}
	else {
		email =[[NSString alloc] initWithString:@""];
	}
	
	NSString *jsCommand = [NSString stringWithFormat:@"setFormValues('%@','%@','%@','%@','%@','%@','%@','%@','%@','%@')",firstName,lastName,address1,address2,city,state,country,zip,phone,email];
	
    [webView stringByEvaluatingJavaScriptFromString:jsCommand];
}

- (void)dealloc {
	[webView release], webView = nil;
	[super dealloc];
}

@end
