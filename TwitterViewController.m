//
//  TwitterViewController.m
//  SkyBuyHigh
//
//  Created by Thapovan Info Systems on 20/11/09.
//  Copyright 2009 Thapovan. All rights reserved.
//

#import "TwitterViewController.h"
#import "Reachability.h"


@implementation TwitterViewController
@synthesize webView;
/*
 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        // Custom initialization
    }
    return self;
}
*/

- (void)viewWillAppear:(BOOL)animated {
	if (!([[Reachability sharedReachability] internetConnectionStatus] == NotReachable)) {
		//NSString *htmlFile = [[NSBundle mainBundle] pathForResource:@"CustomerBillingDetails" ofType:@"html"];
		//NSData *htmlData = [NSData dataWithContentsOfFile:htmlFile];
		webView.delegate = self;
		[webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://twitter.com/skybuyhigh"]]];
		//[webView loadData:htmlData MIMEType:@"text/html" textEncodingName:@"UTF-8" baseURL:[NSURL URLWithString:@""]];
	}
	else {
		NSString *resourcePath = [[[[NSBundle mainBundle] resourcePath]
								   stringByReplacingOccurrencesOfString:@"/" withString:@"//"]
								  stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
		NSString *imgPath = [NSString stringWithFormat:@"file:/%@//logo_black.png", resourcePath];
		NSString *htmlString = [NSString stringWithFormat:@"<html><body style='background-color:#ebebeb'><br /><br /><center><img src=%@ width=165 height=86 /><br /><br /><h3>Cannot load the SkyBuyHigh Twitter page.<br /></h3><h4>You must connect to a Wi-Fi or cellular network to access this section.</h4></center></body></html>", imgPath];
		[webView loadHTMLString:htmlString baseURL:nil];
	}
	
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request  navigationType:(UIWebViewNavigationType)navigationType {
	if(![[[request URL] absoluteString] isEqualToString:@"http://twitter.com/skybuyhigh"]){
		[[UIApplication sharedApplication] openURL:[NSURL URLWithString:[[request URL] absoluteString]]];
		return NO;
	}else{
        return YES;
	}
}	



- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
	NSString *resourcePath = [[[[NSBundle mainBundle] resourcePath]
							   stringByReplacingOccurrencesOfString:@"/" withString:@"//"]
							  stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
	NSString *imgPath = [NSString stringWithFormat:@"file:/%@//logo_black.png", resourcePath];
	NSString *htmlString = [NSString stringWithFormat:@"<html><body style='background-color:#ebebeb'><br /><br /><center><img src=%@ width=165 height=86 /><br /><br /><h3>Cannot load the SkyBuyHigh Twitter page.<br /></h3><h4>You must connect to a Wi-Fi or cellular network to access this section.</h4></center></body></html>", imgPath];
	[self.webView loadHTMLString:htmlString baseURL:nil]; 
}
/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView {
}
*/

/*
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
}
*/

/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [super dealloc];
}


@end
