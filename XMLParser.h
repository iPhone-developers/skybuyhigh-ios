//
//  XMLParser.h
//  XML
//
//  Created by Thapovan InfoSystems on 07/16/09.
//  Copyright 2009 Thapovan Info Systems
//

#import <UIKit/UIKit.h>

@class SkyBuyHighAppDelegate,MenCategoryViewController, WomenCategoryViewController,Products;
@class GiftCategoryViewController, PrivateJetCategoryViewController;

@interface XMLParser : NSObject {

	NSMutableString *currentElementValue;
	
	SkyBuyHighAppDelegate *appDelegate;
	MenCategoryViewController *menCategoryViewController;
	WomenCategoryViewController *womenCategoryViewController;
	GiftCategoryViewController *giftCategoryViewController;
	PrivateJetCategoryViewController *privateJetCategoryViewController;
	Products *prod; 
	NSUInteger categoryId;
	NSUInteger ownerId;
}


@property(nonatomic) NSUInteger categoryId;
@property(nonatomic) NSUInteger ownerId;
- (XMLParser *) initXMLParser;

@end
