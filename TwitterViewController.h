//
//  TwitterViewController.h
//  SkyBuyHigh
//
//  Created by Thapovan Info Systems on 20/11/09.
//  Copyright 2009 Thapovan. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface TwitterViewController : UIViewController<UIWebViewDelegate> {
	UIWebView *webView;
}

@property (nonatomic, retain) IBOutlet UIWebView *webView;

@end
