//
//  WelcomePageViewController.m
//  SkyBuyHigh
//
//  Created by SkyBuyHigh on 02/11/09.
//  Copyright 2009 Thapovan Info Systems. All rights reserved.
//

#import "WelcomePageViewController.h"
#import "SkyBuyHighAppDelegate.h"
#import "Reachability.h"

@implementation WelcomePageViewController

@synthesize skipButton;
@synthesize scrollView, imageView;
@synthesize activityIndicator, loading;
/*
 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        // Custom initialization
    }
    return self;
}
*/

- (void)viewWillAppear:(BOOL)animated {
	
	   
}
-(void) showDesignersList {
	[self.scrollView setFrame:CGRectMake(0, 20, 320, 421)];
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationDuration: 1.25];
	[UIView setAnimationTransition:UIViewAnimationTransitionCurlUp forView:self.view cache:YES];
	
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	NSMutableString *welcomeTextPath = [[NSMutableString alloc] initWithString:[documentsDirectory stringByAppendingPathComponent:@"WelcomeText.png"]];
	UIImage *welcomeImage = [UIImage imageWithContentsOfFile:welcomeTextPath];
	[self.imageView setImage:welcomeImage];
	
	[self.scrollView setContentSize:CGSizeMake(320.0, welcomeImage.size.height)];
	[skipButton setHidden:NO];
	[UIView commitAnimations];
	//[self.scrollView setMaximumZoomScale:2.0];
}


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
	[self.scrollView setFrame:CGRectMake(0, 20, 320, 460)];
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	NSMutableString *welcomePagePath = [[NSMutableString alloc] initWithString:[documentsDirectory stringByAppendingPathComponent:@"WelcomePage.png"]];
    //NSLog(@"Welcome Page Path = %@",welcomePagePath);
	[self.imageView setImage:[UIImage imageWithContentsOfFile:welcomePagePath]];
	
	NSFileManager *fileManager = [NSFileManager defaultManager];
	if (![fileManager fileExistsAtPath:[documentsDirectory stringByAppendingPathComponent:@"WelcomeText.png"]]) {
		if (!([[Reachability sharedReachability] internetConnectionStatus] == NotReachable)) {
			[self performSelector:@selector(skipIntro) withObject:nil afterDelay:3.0];
		} else {
			[self performSelector:@selector(skipIntro) withObject:nil afterDelay:0.0];
		}
	} else {
		[self performSelector:@selector(showDesignersList) withObject:nil afterDelay:2.0];
	}
    [super viewDidLoad];
}


/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}

-(void) callApplication {
	SkyBuyHighAppDelegate *delegate = (SkyBuyHighAppDelegate *) [[UIApplication sharedApplication] delegate];
	[delegate mainApplicationFlow];
}


-(IBAction) skipIntro {
	
	
	NSArray *paths= NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentDirectory = [paths objectAtIndex:0];
	NSFileManager *fileManager = [NSFileManager defaultManager];
	if ([fileManager fileExistsAtPath:[documentDirectory stringByAppendingPathComponent:@"WelcomeText.png"]]) {
		[skipButton setEnabled:NO];
		[activityIndicator startAnimating];
		[loading setHidden:NO];
	}
	[self performSelector:@selector(callApplication) withObject:nil afterDelay:0.2];
}

- (void)dealloc {
	[self.skipButton release], self.skipButton = nil;
	[self.scrollView release], self.scrollView = nil;
	[self.imageView release], self.imageView = nil;
	[self.activityIndicator release], self.activityIndicator = nil;
	[self.loading release], self.loading = nil;
    [super dealloc];
}


@end
