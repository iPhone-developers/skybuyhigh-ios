//
//  PlaceOrder.m
//  SkyBuyHigh
//
//  Created by Thapovan Info Systems on 17/08/09.
//  Copyright 2009 Thapovan. All rights reserved.
//

#import "PlaceOrder.h"
#import "PlaceOrderServiceSvc.h"
#import "ProductDetailsDAO.h"

@implementation PlaceOrder

+(NSString *) sendRequestWithParams:(NSString *)orderXMLString customerId:(NSInteger)customerId {
	NSString *orderResponse=nil;
	PlaceOrderPortBinding *ref = [PlaceOrderServiceSvc PlaceOrderPortBinding];
	ref.logXMLInOut = YES;
	[PlaceOrderServiceSvc PlaceOrderPortBinding].logXMLInOut = YES;
	PlaceOrderServiceSvc_placeOrder *params = [[PlaceOrderServiceSvc_placeOrder new] autorelease];
	//NSLog(@"Place Order Request URL = %@", [[ref address] absoluteString]);
	params.arg0 = [NSString stringWithCString:"Prod_Det\0" encoding:NSUTF8StringEncoding];
	params.arg1 = orderXMLString;
	
	PlaceOrderPortBindingResponse *resp = [ref placeOrderUsingParameters:params];
	for (id bodyPart in resp.bodyParts) {
		if ([bodyPart isKindOfClass:[PlaceOrderServiceSvc_placeOrderResponse class]]) {
			PlaceOrderServiceSvc_placeOrderResponse *orderResp = (PlaceOrderServiceSvc_placeOrderResponse*)bodyPart;
			NSLog(@"Order Response = \n%@",[orderResp serializedFormUsingElementName:@"TEST"]);	
			orderResponse = [orderResp return_];
			if ([[orderResp return_] isEqualToString:@"OPS"]) {		
				[[ProductDetailsDAO instance] deleteOrderDetails:customerId];
				[UIApplication sharedApplication].applicationIconBadgeNumber = [UIApplication sharedApplication].applicationIconBadgeNumber - 1;
			} else {
				[[ProductDetailsDAO instance] updateDeliverAttempt:customerId];
			}
		}
	}
	return orderResponse;
}

+(NSString *) sendRequestWithParams:(NSString *)airlinePackageXMLString
{
	NSString *orderResponse=nil;
	NSUInteger airPackageItemsCount = [[ProductDetailsDAO instance] getAirPackageItemsCount];
	PlaceOrderPortBinding *ref = [PlaceOrderServiceSvc PlaceOrderPortBinding];
	ref.logXMLInOut = YES;
	//NSLog(@"Place Order Request URL = %@", [[ref address] absoluteString]);
	[PlaceOrderServiceSvc PlaceOrderPortBinding].logXMLInOut = YES;
	PlaceOrderServiceSvc_placeOrder *params = [[PlaceOrderServiceSvc_placeOrder new] autorelease];
	
	params.arg0 = [NSString stringWithCString:"Air_Det\0" encoding:NSUTF8StringEncoding];
	params.arg1 = airlinePackageXMLString;
	
	PlaceOrderPortBindingResponse *resp = [ref placeOrderUsingParameters:params];
	for (id bodyPart in resp.bodyParts) {
		if ([bodyPart isKindOfClass:[PlaceOrderServiceSvc_placeOrderResponse class]]) {
			PlaceOrderServiceSvc_placeOrderResponse *orderResp = (PlaceOrderServiceSvc_placeOrderResponse*)bodyPart;
			//NSLog(@"Airline Package Response = \n%@",[orderResp serializedFormUsingElementName:@"TEST"]);		
			orderResponse = [orderResp return_];
			if ([[orderResp return_] isEqualToString:@"OPS"]) {		
				[[ProductDetailsDAO instance] deleteAirPackageDetails];
				[UIApplication sharedApplication].applicationIconBadgeNumber = [UIApplication sharedApplication].applicationIconBadgeNumber - airPackageItemsCount;
			} else {
				[[ProductDetailsDAO instance] updateAirPackageDeliverAttempt];
			}
		}
	}
	return orderResponse;
}


@end
