//
//  PlaceOrderViewController.h
//  SkyBuyHigh
//
//  Created by Thapovan InfoSystems on 21/08/09.
//  Copyright 2009 Thapovan. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface PlaceOrderViewController : UIViewController {
	
	UILabel *OrderDetailsMsg;
	//UITextField *flightNo;
	UIProgressView *progressView;
	UILabel *successOrderMsg;
	UILabel *failureOrderMsg;
	UIButton *submitButton;
	UILabel *errorMsg;
	UIActivityIndicatorView *activityIndicator;
	UIButton *continueButton;
}

@property (nonatomic, retain) IBOutlet UILabel *OrderDetailsMsg;
//@property (nonatomic, retain) IBOutlet UITextField *flightNo;
@property (nonatomic, retain) IBOutlet UIProgressView *progressView;
@property (nonatomic, retain) IBOutlet UILabel *successOrderMsg;
@property (nonatomic, retain) IBOutlet UILabel *failureOrderMsg;
@property (nonatomic, retain) IBOutlet UIButton *submitButton;
@property (nonatomic, retain) IBOutlet UILabel *errorMsg;
@property (nonatomic, retain) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (nonatomic, retain) IBOutlet UIButton *continueButton;

-(IBAction)textFieldDoneEditing:(id)sender;
-(void)continueShopping:(id)sender;
-(void)submitOrderDetails:(id)sender;
-(void)placeOrderDetails;
-(NSString *) documentsPath;

@end
