//
//  OrderConfirmationViewController.h
//  SkyBuyHigh
//
//  Created by Thapovan InfoSystems on 25/07/09.
//  Copyright 2009 Thapovan. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface OrderConfirmationViewController : UIViewController {
	UILabel *orderConfirmationNumber;
	UILabel *emailText;
	NSString *orderConfirmationNo;
	NSString *emailTextMessage;
	
}
@property(nonatomic,retain) IBOutlet UILabel *orderConfirmationNumber;
@property(nonatomic,retain) IBOutlet UILabel *emailText;
@property(nonatomic,retain) NSString *orderConfirmationNo;
@property(nonatomic,retain) NSString *emailTextMessage;

//-(void)suggestionBox:(id)sender;
@end