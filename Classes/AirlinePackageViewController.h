//
//  AirlinePackageViewController.h
//  SkyBuyHigh
//
//  Created by Thapovan Info Systems on 08/09/09.
//  Copyright 2009 Thapovan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Products.h"
#import "AirlineProductViewController.h"
@class ProductImageZoomController,AirlineProductViewController;

@interface AirlinePackageViewController : UIViewController {
	Products *productData;
	UITextView *productDesc;
	UILabel *productTitle;

	UIImageView *addToBagImage;
	
	UIToolbar *toolbar;
	UIWindow *window;
	ProductImageZoomController *imageController;
	//NSString *buttonTitle;
	UIButton *personalShopper;

	Products *addtoBagProductData;
	BOOL  isSizeandColor;
	NSArray* toolbarItems;
	AirlineProductViewController *airlineProductViewController;
}
@property(nonatomic,retain) IBOutlet UITextView *productDesc;
@property(nonatomic,retain) IBOutlet UILabel *productTitle;


@property(nonatomic,retain) IBOutlet UIImageView *addToBagImage;
@property(nonatomic,retain) Products *productData;
@property(nonatomic,retain) IBOutlet UIToolbar *toolbar;
@property(nonatomic,retain) UIWindow *window;
//@property (nonatomic, retain) NSString *buttonTitle;
@property (nonatomic, retain) ProductImageZoomController *imageController;
@property (nonatomic, retain) IBOutlet UIButton *personalShopper;

@property(nonatomic,retain) Products *addtoBagProductData;
@property (nonatomic, retain) NSArray *toolbarItems;
@property(nonatomic, retain) AirlineProductViewController *airlineProductViewController;


-(IBAction) showPersonalShopper:(id)sender;

-(void)nextProduct:(id)sender;
-(void)previousProduct:(id)sender;


@end