//
//  OnlinePlaceOrderViewController.h
//  SkyBuyHigh
//
//  Created by Thapovan Info Systems on 14/12/09.
//  Copyright 2009 Thapovan. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface OnlinePlaceOrderViewController : UIViewController {
	
	UIActivityIndicatorView *activityIndicator;
	UITextView *placeOrderStatus;
	NSString *orderConfirmationNo;
	NSString *emailTextMessage;
}

@property (nonatomic, retain) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (nonatomic,retain ) IBOutlet UITextView *placeOrderStatus;
@property (nonatomic,retain ) NSString *orderConfirmationNo;
@property (nonatomic,retain ) NSString *emailTextMessage;


-(void)placeOrder;

@end
