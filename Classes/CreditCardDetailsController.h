//
//  CreditCardDetailsController.h
//  SkyBuyHigh
//
//  Created by Thapovan InfoSystems on 30/07/09.
//  Copyright 2009 Thapovan. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface CreditCardDetailsController : UIViewController <UIWebViewDelegate> {
	UIWebView *webView;
}

@property (nonatomic, retain) IBOutlet UIWebView *webView;
@end