//
//  UnzipViewController.m
//  SkyBuyHigh
//
//  Created by Thapovan Info Systems on 07/09/09.
//  Copyright 2009 Thapovan. All rights reserved.
//

#import "UnzipViewController.h"
#import "SkyBuyHighAppDelegate.h"
#import "ZipArchive.h"

@implementation UnzipViewController


-(void) unzipCatalogue {
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	NSMutableString *ZipPath = [[NSMutableString alloc] initWithString:[[NSBundle mainBundle] pathForResource:@"SkyBuyPics" ofType:@"zip"]];
	
	[ZipPath replaceOccurrencesOfString:@" " withString:@"\\ " options:NSNumericSearch range:NSMakeRange(0, [ZipPath length])];
	NSFileManager *fileManager = [NSFileManager defaultManager];
	NSMutableString *checkDir = [[NSMutableString alloc] initWithString:documentsDirectory];
	[checkDir appendString:@"/SkyBuyPics"];
	NSError *error;
	BOOL unzipSuccess = YES;
	if (([fileManager contentsOfDirectoryAtPath:checkDir error:&error] == nil) || ![fileManager fileExistsAtPath:[documentsDirectory stringByAppendingString:@"/ZipFileExtracted"]]) {
		//First Time Run - Unzip the bundled zip file
		ZipArchive* za = [[ZipArchive alloc] init];
		if( [za UnzipOpenFile:ZipPath] )
		{
			BOOL ret = [za UnzipFileTo:documentsDirectory overWrite:YES];
			if( NO==ret )
			{
				unzipSuccess = NO;
				NSLog(@"Unzip failed");
			}
			[za UnzipCloseFile];
		}else{
			unzipSuccess = NO;
		}
		[za release];
		[fileManager copyItemAtPath:[[NSBundle mainBundle] pathForResource:@"eCatalogue" ofType:@"xml"] toPath:[documentsDirectory stringByAppendingPathComponent:@"eCatalogue.xml"] error:&error];
	}
	[ZipPath release];
	[checkDir release];
	
	
	if (unzipSuccess){
		[[NSFileManager defaultManager] createFileAtPath:[documentsDirectory stringByAppendingString:@"/ZipFileExtracted"] contents:nil attributes:nil];
		//NSLog(@"UnZip Success - ZipFileExtracted");
	}
	//[(SkyBuyHighAppDelegate *)[[UIApplication sharedApplication] delegate] initializeTabBar];
	//[(SkyBuyHighAppDelegate *)[[UIApplication sharedApplication] delegate] setDownloadAirPackage:YES];
	[(SkyBuyHighAppDelegate *)[[UIApplication sharedApplication] delegate] downloadCatalogue:YES];
}


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
	[self performSelector:@selector(unzipCatalogue) withObject:nil afterDelay:0.2];
    [super viewDidLoad];
}


/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [super dealloc];
}


@end
