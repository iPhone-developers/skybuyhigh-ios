//
//  ProductViewController.m
//  SampleProject
//
//  Created by Thapovan Info Systems on 16/06/09.
//  Copyright 2009 Thapovan. All rights reserved.
//

#import "AirlineProductViewController.h"
#import "Products.h"
#import "SkyBuyHighAppDelegate.h"
#import "MenCategoryViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "ImageManipulations.h"
#import "ProductDetailsDAO.h"
#import "DeviceRegViewController.h"
#import "DataCache.h"

@implementation AirlineProductViewController

@synthesize productDesc,productBrand,productTitle,productImage,productPrice;
@synthesize productData;
@synthesize checkoutBarButton;
@synthesize window;
@synthesize addToBagImage;
@synthesize toolbar;
@synthesize buyNowBarButton, productInStorePrice;
@synthesize imageController;
@synthesize personalShopper;
@synthesize personalShopperViewController;
@synthesize pickerView;
@synthesize addtoBagProductData;
@synthesize toolbarItems;
@synthesize checkoutViewController;
@synthesize airlineViewController;
@synthesize travelDateButton;
@synthesize travelDate;
@synthesize pickerToolbar;
@synthesize pickeToolbarItems;
@synthesize selectedTravelDate;

- (void)viewDidLoad { 
	//NSLog(@"Product viewDidLoad entered");
	[super viewDidLoad];
	
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        // Custom initialization
    }
    return self;
}

-(IBAction) showPersonalShopper:(id)sender {
	if (personalShopperViewController == nil) {
		personalShopperViewController = [[PersonalShopperViewController alloc] initWithNibName:@"PersonalShopperViewController" bundle:nil];
		
	}
	
	[self.navigationController pushViewController:personalShopperViewController animated:YES];
}

- (void)showTravelDate
{
	//NSLog(@"show picker");
	[[self.view viewWithTag:991] removeFromSuperview];
	[[self.view viewWithTag:992] removeFromSuperview];
	
	
	travelDateButton = [UIButton buttonWithType:UIButtonTypeCustom];
	travelDateButton.frame = CGRectMake(118, 126, 100, 19); // size and position of button
	[travelDateButton setTitle:@"TravelDateButton" forState:UIControlStateNormal];
	[travelDateButton setImage:[UIImage imageNamed:@"addicon1.png"] forState:UIControlStateNormal];
	travelDateButton.adjustsImageWhenHighlighted = YES;	
	travelDateButton.tag = 991;
	[travelDateButton addTarget:self action:@selector(showTraveldatePicker:)  forControlEvents:UIControlEventTouchUpInside];
	
	travelDate  = [[UILabel alloc] initWithFrame:CGRectMake(140, 123, 100, 21)];
	[travelDate setBackgroundColor:[UIColor clearColor]];
	[travelDate setText:@"Travel Date"];
	[travelDate setAdjustsFontSizeToFitWidth:YES];
	travelDate.tag = 992;
	
	[self.view addSubview:travelDateButton];
	[self.view addSubview:travelDate];
	
	
	
	
}



- (void)viewDidAppear:(BOOL)animated {
	
	if (toolbar == nil) {
		toolbar =[[UIToolbar alloc] initWithFrame:CGRectMake(0,372,320,44)];
	}
	if (toolbarItems == nil) {
		toolbarItems = [NSArray arrayWithObjects:
						[[UIBarButtonItem alloc] initWithImage:[[UIImage imageNamed:@"photos-icon.png"]retain] style:UIBarButtonItemStylePlain target:self action:@selector(showProductImage:)],
						[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:NULL action:NULL],
						[[UIBarButtonItem alloc] initWithImage:[[UIImage imageNamed:@"prev-icon.png"]retain] style:UIBarButtonItemStylePlain target:self action:@selector(previousProduct:)],
						[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:NULL action:NULL],
						[[UIBarButtonItem alloc] initWithImage:[[UIImage imageNamed:@"next-icon.png"]retain] style:UIBarButtonItemStylePlain target:self action:@selector(nextProduct:)],
						[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:NULL action:NULL],
						[[UIBarButtonItem alloc] initWithImage:[[UIImage imageNamed:@"cart-icon.png"]retain] style:UIBarButtonItemStylePlain target:self action:@selector(checkoutAction:)],
						nil];
		
		toolbar.barStyle =UIBarStyleBlack;
		toolbar.items = toolbarItems;
	}
	for (UIBarButtonItem *item in toolbarItems) {
		[item setEnabled:YES];
	}
	if (self.productData.thumbnailURL != nil) { 
		NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
		NSString *documentsDirectory = [paths objectAtIndex:0];
		NSString *fileName = [documentsDirectory stringByAppendingPathComponent:self.productData.thumbnailURL];
		if ([[DataCache tableViewImageCache] objectForKey:fileName] == nil) {
			UIImage *fileImage = [[UIImage alloc] initWithContentsOfFile:fileName];
			[ImageManipulations roundedCornersOfImage:fileImage withClipRadius:20 forKey:fileName];
			[fileImage release];
		}
		[productImage setImage:[UIImage imageWithCGImage:(CGImageRef)[[DataCache tableViewImageCache] objectForKey:fileName]]];
		productImage.tag = 999;
	}
	[productTitle setText:self.productData.TITLE];
	[productBrand setText:self.productData.BRAND];
	[productDesc setText:self.productData.LongDiscription];
	[productPrice setText:self.productData.Price];
	[productInStorePrice setText:self.productData.inStorePrice];
	/*buyNowBarButton = [[UIBarButtonItem alloc] initWithTitle:@"Buy Now"
													   style:UIBarButtonItemStyleBordered
													  target:self
													  action:@selector(addToBagProducts:)];	*/
	
	if (buyNowBarButton == nil){
		UIButton *buyNowButton = [UIButton buttonWithType:UIButtonTypeCustom];
		[buyNowButton setFrame:CGRectMake(0, 0, 71, 30)];
		[buyNowButton setImage:[UIImage imageNamed:@"button-buynow.png"] forState:UIControlStateNormal];
		[buyNowButton addTarget:self action:@selector(addToBagProducts:) forControlEvents:UIControlEventTouchUpInside];
		buyNowBarButton = [[UIBarButtonItem alloc] initWithCustomView:buyNowButton];
	}
	
	self.navigationItem.rightBarButtonItem =buyNowBarButton;
	self.navigationItem.hidesBackButton = YES;
	self.navigationItem.leftBarButtonItem =	[[UIBarButtonItem alloc] initWithTitle:@"More Products"
																			 style:UIBarButtonItemStyleBordered
																			target:self
																			action:@selector(handleBack:)];
	
	[self showTravelDate];
	
	if ([[(SkyBuyHighAppDelegate *)[[UIApplication sharedApplication] delegate] addToBagProducts] count] > 0) {
		[[[toolbar items] objectAtIndex:6] setTitle:[NSString stringWithFormat:@"%d",[[(SkyBuyHighAppDelegate *)[[UIApplication sharedApplication] delegate] addToBagProducts] count]]];
	}else{
		[[[toolbar items] objectAtIndex:6] setTitle:@""];
	}
	
	if(![(SkyBuyHighAppDelegate *)[[UIApplication sharedApplication] delegate] deviceRegStatus]){	
		[[[toolbar items] objectAtIndex:6] setEnabled:NO];
	}	
	
	id categoryViewController ;
	NSMutableArray *productInformation ;
	categoryViewController = [[self.navigationController viewControllers] objectAtIndex:0];
	productInformation = [categoryViewController productInfo];
	NSUInteger rowCount = [categoryViewController nsiCurrentRow];
	if(rowCount ==0){
		[[[toolbar items] objectAtIndex:2] setEnabled:NO];
		
	}else if((rowCount+1) == productInformation.count){
		[[[toolbar items] objectAtIndex:4] setEnabled:NO];
	}
	
	[self.view addSubview:toolbar];
	[super viewDidAppear:animated];
}



- (void) handleBack:(id)sender {
	[self.navigationController popToRootViewControllerAnimated:YES];
}



- (void)checkoutAction:(id)sender {
	
	SkyBuyHighAppDelegate *delegate=(SkyBuyHighAppDelegate *)[[UIApplication sharedApplication]delegate];	
	if([delegate.addToBagProducts count] >0){
		if (checkoutViewController) {
			[checkoutViewController release];
		}
		checkoutViewController = [[CheckOutProductsController alloc] init];
		[self.navigationController pushViewController:checkoutViewController animated:YES];
	}
	else {
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"SkyBuyHigh" message:@"You have no items to checkout" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
		[alert show];
		[alert release];
	}
}




- (void)viewDidDisappear:(BOOL)animated {
	[self setSelectedTravelDate:[NSDate date]];
	[[self.view viewWithTag:10050] removeFromSuperview];
	[[self.view viewWithTag:10060] removeFromSuperview];
	
	[super viewDidDisappear:animated];
}

-(void)addToBagProducts:(id)sender
{	
	if([(SkyBuyHighAppDelegate *)[[UIApplication sharedApplication] delegate] deviceRegStatus])
	{	
		NSMutableArray *productInfomation=nil ;
		id categoryViewController=nil;
		SkyBuyHighAppDelegate *delegate=(SkyBuyHighAppDelegate *)[[UIApplication sharedApplication]delegate];
		categoryViewController = [[self.navigationController viewControllers] objectAtIndex:0];
		productInfomation = [categoryViewController productInfo];
		NSUInteger rowCount = [categoryViewController nsiCurrentRow];
		Products *prod= [productInfomation objectAtIndex:rowCount];
		/*NSDate *currenDate = [NSDate date];		
		NSDateFormatter *dateFormatter = [[[NSDateFormatter alloc] init] autorelease];		
		[dateFormatter setDateFormat:@"MM/dd/yyyy"];
		NSLog(@"Current Date = %@",[[[NSDate date]description]substringToIndex: 10]);
		NSLog(@"Selected Travel Date = %@",[[selectedTravelDate description]substringToIndex: 10]);
		NSLog(@"Difference = %d",[[[[NSDate date]description]substringToIndex: 10] compare:[[selectedTravelDate description]substringToIndex: 10]]);*/
		if([self.travelDate.text isEqualToString:@"Travel Date"]){
			UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"SkyBuyHigh" message:@"Please select Travel Date." delegate:nil cancelButtonTitle:@"OK"
												  otherButtonTitles:nil];	
			[alert show];
			[alert release];
		}else if ([[[[NSDate date]description]substringToIndex: 10] compare:[[selectedTravelDate description]substringToIndex: 10]]==1){
			UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"SkyBuyHigh" message:@"Invalid Travel Date." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];		
			[alert show];
			[alert release];
		}		
		else{
			[[self.view viewWithTag:10050] removeFromSuperview];
			[[self.view viewWithTag:10060] removeFromSuperview];
			[travelDateButton setEnabled:YES];
			
			[prod setIsAirlineProduct:YES];
			Products *addToBagProductsDetails = [[Products alloc] initWithProductData:prod];
			[delegate.addToBagProducts addObject:addToBagProductsDetails];
			//[(UILabel *)[[[[toolbar items] objectAtIndex:6] customView] viewWithTag:88] setText:[NSString stringWithFormat:@"%d",[delegate.addToBagProducts count]]];
			[addToBagProductsDetails release]; addToBagProductsDetails=nil;
			
			addToBagImage=[[UIImageView alloc]init];
			//UIImage *img=self.productImage.image;
			addToBagImage.frame=CGRectMake(25, 20, 100, 100);
			addToBagImage.image=self.productImage.image;
			[self.view addSubview:addToBagImage];
			CGPoint checkOutLocation = CGPointMake(335,455);
			[UIView beginAnimations:@"center" context:nil];
			[UIView setAnimationDuration:0.75];
			addToBagImage.transform = CGAffineTransformRotate(CGAffineTransformIdentity, 90);
			addToBagImage.center=checkOutLocation;
			addToBagImage.alpha=0;	
			addToBagImage.bounds = CGRectMake(335,455, 0, 0);
			[UIView commitAnimations];
			[addToBagImage release];
			[[[toolbar items] objectAtIndex:6] setTitle:[NSString stringWithFormat:@"%d",[delegate.addToBagProducts count]]];
		}	
	}else {
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"SkyBuyHigh" message:@"In order to make purchases, this device must be registered with SkyBuyHigh. Touch OK to register." delegate:self cancelButtonTitle:nil
											  otherButtonTitles:@"OK", @"Cancel", nil];	
		[alert show];
		[alert release];
	}
	
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
	if(buttonIndex  == 0){
		//NSLog(@"OK BUTTON PRESSED");
		
		if (!([[Reachability sharedReachability] internetConnectionStatus] == NotReachable)) {
			DeviceRegViewController *deviceRegViewController = [[DeviceRegViewController alloc] initWithNibName:@"DeviceRegView" bundle:nil];
			//[(SkyBuyHighAppDelegate *)[[UIApplication sharedApplication] delegate] setCallDownloadCatalogue:NO];
			deviceRegViewController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
			[self presentModalViewController:deviceRegViewController animated:YES];
		}	
		else{
			UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"SkyBuyHigh" message:@"You need to activate Wi-Fi (or) 3G in order to register this device." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];		
			[alert show];
			[alert release];
		}	
	}	
	//[alertView release];
}

-(void)nextProduct:(id)sender
{
	NSMutableArray *productInfomation ;
	id categoryViewController ;
	Products *prod=nil;
	categoryViewController = [[self.navigationController viewControllers] objectAtIndex:0]; 
	productInfomation = [categoryViewController productInfo];
	NSUInteger rowCount = [categoryViewController nsiCurrentRow];
	
	
	
	if((rowCount + 1) == productInfomation.count){
		//rowCount =rowCount - 1;
		[[[toolbar items] objectAtIndex:4] setEnabled:NO];
	}
	else if (rowCount == 0) {
		[[[toolbar items] objectAtIndex:2] setEnabled:YES];
	}	
	if ([[[toolbar items] objectAtIndex:4] isEnabled]) {
		prod  = [productInfomation objectAtIndex:rowCount+1];	
		[categoryViewController setNsiCurrentRow:rowCount+1];
		
		if(prod != nil) { 		
			self.productData = prod;
			if (prod.thumbnailURL != nil) { 
				[self setSelectedTravelDate:[NSDate date]];
				NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
				NSString *documentsDirectory = [paths objectAtIndex:0];
				NSString *fileName = [documentsDirectory stringByAppendingPathComponent:prod.thumbnailURL];
				
				if ([[DataCache tableViewImageCache] objectForKey:fileName] == nil) {
					UIImage *fileImage = [[UIImage alloc] initWithContentsOfFile:fileName];
					[ImageManipulations roundedCornersOfImage:fileImage withClipRadius:20 forKey:fileName];
					[fileImage release];
				}
				[productImage setImage:[UIImage imageWithCGImage:(CGImageRef)[[DataCache tableViewImageCache] objectForKey:fileName]]];
				self.productImage.tag = 999;
			}
			[self.productTitle setText:prod.TITLE];
			[self.productBrand setText:prod.BRAND];	
			[self.productDesc setText:prod.LongDiscription];
			[self.productPrice setText:prod.Price];
			[self.productInStorePrice setText:prod.inStorePrice];
		}	
		
		[self showTravelDate];
		
		CATransition *animation = [CATransition animation];
		//animation.type = @"oglFlip";
		animation.type = kCATransitionReveal;
		animation.subtype = kCATransitionFromRight;
		animation.duration = 0.75;  //Or whatever
		//animation.speed = 0.75;
		animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionDefault]; //Or whatever
		//animation.startProgress = 0.25;  //Set this as needed
		//animation.endProgress = 0.75;  //Set this as needed
		animation.fillMode = kCAFillModeForwards;
		[[[self view] layer] addAnimation:animation forKey:kCATransition];
		
		if ([prod isAdv]) {		
			imageController = [[self.navigationController viewControllers] objectAtIndex:1];	
			imageController.currentProduct = prod;
			imageController.showAirlineDetailView = @"PackageView";
			imageController.prodView =@"next";
			[self.navigationController popViewControllerAnimated:NO];
		}
	}
	
	if((rowCount+2) == productInfomation.count){
		[[[toolbar items] objectAtIndex:4] setEnabled:NO];
	}
	
	
}

-(void)previousProduct:(id)sender
{
	id categoryViewController ;
	NSMutableArray *productInfomation ;
	Products *prod= nil;
	categoryViewController = [[self.navigationController viewControllers] objectAtIndex:0];
	
	productInfomation = [categoryViewController productInfo];
	NSUInteger rowCount = [categoryViewController nsiCurrentRow];
	
	if(rowCount == 0){
		//rowCount =rowCount + 1;
		[[[toolbar items] objectAtIndex:2] setEnabled:NO];
	}		
	else if(rowCount + 1 == productInfomation.count) {
		[[[toolbar items] objectAtIndex:4] setEnabled:YES];
	}	
	if ([[[toolbar items] objectAtIndex:2] isEnabled]) { 
		prod= [productInfomation objectAtIndex:rowCount-1];	
		[categoryViewController setNsiCurrentRow:rowCount-1];
		
		if(prod != nil) { 	
			self.productData = prod;
			if (prod.thumbnailURL != nil) { 
				[self setSelectedTravelDate:[NSDate date]];
				NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
				NSString *documentsDirectory = [paths objectAtIndex:0];
				NSString *fileName = [documentsDirectory stringByAppendingPathComponent:prod.thumbnailURL];
				if ([[DataCache tableViewImageCache] objectForKey:fileName] == nil) {
					UIImage *fileImage = [[UIImage alloc] initWithContentsOfFile:fileName];
					[ImageManipulations roundedCornersOfImage:fileImage withClipRadius:20 forKey:fileName];
					[fileImage release];
				}
				[productImage setImage:[UIImage imageWithCGImage:(CGImageRef)[[DataCache tableViewImageCache] objectForKey:fileName]]];
				self.productImage.tag = 999;
			}
			[self.productTitle setText:prod.TITLE];
			[self.productBrand setText:prod.BRAND];
			[self.productDesc setText:prod.LongDiscription];
			[self.productPrice setText:prod.Price];
			[self.productInStorePrice setText:prod.inStorePrice];
		}
		[self showTravelDate];
		
		CATransition *animation = [CATransition animation];
		//animation.type = @"oglFlip";
		animation.type = kCATransitionReveal;
		animation.subtype = kCATransitionFromLeft;
		animation.duration = 0.75;  //Or whatever
		//animation.speed = 0.75;
		animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionDefault]; //Or whatever
		//animation.startProgress = 0.25;  //Set this as needed
		//animation.endProgress = 0.75;  //Set this as needed
		animation.fillMode = kCAFillModeForwards;
		[[[self view] layer] addAnimation:animation forKey:kCATransition];
		
		if ([prod isAdv]) {		
			imageController = [[self.navigationController viewControllers] objectAtIndex:1];	
			imageController.currentProduct = prod;
			imageController.showAirlineDetailView = @"PackageView";
			imageController.prodView =@"previous";
			[self.navigationController popViewControllerAnimated:NO];
		}
	}
	
	if(rowCount == 1){
		[[[toolbar items] objectAtIndex:2] setEnabled:NO];
	}
	
}


-(void)showProductImage:(id)sender
{
	
	id categoryViewController ;
	id selectedNavController;
	NSMutableArray *productInformation ;
	selectedNavController = self.navigationController;
	categoryViewController = [[selectedNavController viewControllers] objectAtIndex:0];
	imageController = [[selectedNavController viewControllers] objectAtIndex:1];
	
	imageController.hidesBottomBarWhenPushed = YES;	
	productInformation = [categoryViewController productInfo];
	NSUInteger rowCount = [categoryViewController nsiCurrentRow];
	Products *product= [productInformation objectAtIndex:rowCount];	
	imageController.currentProduct = product;
	imageController.productImage =  product.MainImageURL;
	
	[UIView beginAnimations:@"imageZoom" context:NULL];
	[UIView setAnimationDuration:0.65];
	[UIView setAnimationTransition:UIViewAnimationTransitionFlipFromRight forView:[selectedNavController view] cache:YES];
	//[delegate.menNavController pushViewController:imageController animated:NO];
	[selectedNavController popViewControllerAnimated:NO];
	[UIView commitAnimations];
}


#pragma mark -
#pragma mark pickerView Controls

-(void)hidePickerView:(id)sender
{
	
	/*NSDate *currenDate = [NSDate date];	
	NSDateFormatter *dateFormatter = [[[NSDateFormatter alloc] init] autorelease];		
	[dateFormatter setDateFormat:@"MM/dd/yyyy"];*/
	//NSLog(@"Date difference=%d",[[dateFormatter stringFromDate:currenDate] compare:[dateFormatter stringFromDate:selectedTravelDate]]);	
	if ([[[[NSDate date]description]substringToIndex: 10] compare:[[selectedTravelDate description]substringToIndex: 10]]==1){
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"SkyBuyHigh" message:@"Invalid Travel Date." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];		
		[alert show];
		[alert release];
	}else{
		[[self.view viewWithTag:10050] removeFromSuperview];
	    [[self.view viewWithTag:10060] removeFromSuperview];
	    [travelDateButton setEnabled:YES];
	}
	//pickerView.hidden = YES;	
	//pickerToolbar.hidden = YES;	
}


-(void)showTraveldatePicker:(id)sender
{
	//[[self.view viewWithTag:10050] removeFromSuperview];
	//[[self.view viewWithTag:10060] removeFromSuperview];
	//pickerView.hidden = YES;
	//pickerToolbar.hidden = YES;
	[travelDateButton setEnabled:NO];
	
	// Add the picker
	float height = 216.0f;
	pickerView = [[UIDatePicker alloc] initWithFrame:CGRectMake(0.0f, 416.0f - height, 320.0f, height)];
	pickerView.datePickerMode = UIDatePickerModeDate;
    [pickerView setMinimumDate:[NSDate date]];
	if(selectedTravelDate!=nil)
		[pickerView setDate:selectedTravelDate animated:NO];
	[pickerView addTarget:self action:@selector(selectedTravelDate:) forControlEvents:UIControlEventValueChanged];
	pickerView.tag = 10050;
	
	[self.view addSubview:pickerView];
	[pickerView release];	
	
	pickerToolbar =[[UIToolbar alloc] initWithFrame:CGRectMake(0,165,320,35)];
	pickeToolbarItems = [NSArray arrayWithObjects:							
						 [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:NULL action:NULL],
						 [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleBordered target:self action:@selector(hidePickerView:)],
						 nil];	
	pickerToolbar.barStyle =UIBarStyleBlack;
	pickerToolbar.items = pickeToolbarItems;
	pickerToolbar.tag = 10060;
	[self.view addSubview:pickerToolbar];	
	//[pickerToolbar release];
	
	// Manage re-sizing and rotation
	self.view.autoresizesSubviews = YES;
	self.view.autoresizingMask = (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight);	
	
	
}	

- (void) selectedTravelDate: (UIDatePicker *) picker
{
	[self setSelectedTravelDate:[picker date]];
	
	NSDateFormatter *dateFormatter = [[[NSDateFormatter alloc] init] autorelease];
	[dateFormatter setDateFormat:@"MMM d yyyy"];
	travelDate.text = [dateFormatter stringFromDate:selectedTravelDate];
	
	[dateFormatter setDateFormat:@"MM/dd/yyyy"];	
	self.productData.travelDate = [dateFormatter stringFromDate:selectedTravelDate];		
}

// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
	// return (interfaceOrientation == UIInterfaceOrientationPortrait);
	return YES;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning]; // Releases the view if it doesn't have a superview
    // Release anything that's not essential, such as cached data
}

- (void)dealloc {
	[productDesc release],productDesc = nil;
	[productBrand release],productBrand = nil;
	[productTitle release],productTitle = nil;
	[productImage release],productImage = nil;
	[productPrice release],productPrice = nil;
	[productData release],productData = nil;
	[checkoutBarButton release],checkoutBarButton = nil;
	[window release],window = nil;
	[addToBagImage release],addToBagImage = nil;
	[toolbar release],toolbar = nil;
	[buyNowBarButton release],buyNowBarButton = nil;
	[productInStorePrice release],productInStorePrice = nil;
	[imageController release],imageController = nil;
	[personalShopper release],personalShopper = nil;
	[personalShopperViewController release],personalShopperViewController = nil;
	[pickerView release],pickerView=nil;
	[pickerToolbar release],pickerToolbar = nil;
	[pickeToolbarItems release],pickeToolbarItems = nil;
	[addtoBagProductData release],addtoBagProductData = nil;
	[toolbarItems release],toolbarItems = nil;
	[checkoutViewController release],checkoutViewController = nil;
	
    [super dealloc];
}


#pragma mark -
#pragma mark Additional Touch Events

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event { 
	
	//Get all the touches.
	NSSet *allTouches = [event allTouches];
	switch ([allTouches count])
	{
		case 1:
		{
			UITouch *touch = [[allTouches allObjects] objectAtIndex:0];
			CGPoint pt = [touch locationInView:self.view];
			if (pt.x >= 13 && pt.x <= 113 && pt.y >= 62 && pt.y <= 162)
				switch ([touch tapCount]) {
					case 1: [self showProductImage:nil]; break;
						
				}
		}
	}
}


@end
