//
//  CustomerDetails.m
//  SkyBuyHigh
//
//  Created by Thapovan InfoSystems on 27/07/09.
//  Copyright 2009 Thapovan. All rights reserved.
//

#import "CustomerDetails.h"


@implementation CustomerDetails
@synthesize custTitle,custFirstName,cusLastName,address1,address2,city,state,country,zip,phone,email,sameAddress,specialIns,personalMsg,feedback,suggestions,tailNumber;


-(id)initWithCustomerDetails:(CustomerDetails *) data{
	self.custTitle = data.custTitle;
	self.custFirstName = data.custFirstName;
	self.cusLastName = data.cusLastName;
	self.address1 = data.address1;
	self.address2 = data.address2;
	self.city = data.city;
	self.state = data.state;
	self.country = data.country;
	self.zip = data.zip;
	self.phone = data.phone;
	self.email = data.email;
	self.sameAddress = data.sameAddress;
	return self;
}

@end
