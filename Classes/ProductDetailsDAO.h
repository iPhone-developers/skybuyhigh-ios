//
//  ProductDetailsDAO.h
//  SkyBuyHigh
//
//  Created by Thapovan InfoSystems on 11/08/09.
//  Copyright 2009 Thapovan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>
#import "CustomerDetails.h"
#import "CreditCardDetails.h"
#import "Products.h"
#import "DeviceDetails.h"


@interface ProductDetailsDAO : NSObject {
	sqlite3 *database;
	
}
-(void)getDatabaseConnection;
-(void) closeDatabaseConnection;
- (void)insertProductDetails:(Products *)products setCustomerId:(NSInteger)customerId setOrderConfirmationNo:(NSString *)orderConfirmationNo setTailNumber:(NSString *)tailNumber;
- (NSInteger)insertCustomerBillingDetails:(CustomerDetails *)customerBillingDetails customerShippingDetails:(CustomerDetails *)customerShippingDetails customerCreditCatddetails:(CreditCardDetails *)creditCardDetails deviceRegDetails:(DeviceDetails *)deviceDetails orderConfirmationNumber:(NSString *)orderConfirmationNo;
- (NSInteger)getOrderItemsCount;
- (NSMutableArray *)generateOrderItemDetailsXML;
- (NSString *)getEncryptedData:(NSString *)value;
-(void)deleteOrderDetails:(NSInteger)customerId;
-(void)updateCatalogueDate:(NSInteger)deviceId macAddress:(NSString *)macAddr withCatalogueDate:(NSString *)catalogueDate;
-(NSString *)getCatalogueDate:(NSInteger)deviceId macAddress:(NSString *)macAddr;
-(NSString *)getCatalogueDetailsURL;
-(NSString *)getValidDeviceURL;
-(void)updateDeliverAttempt:(NSInteger)customerId;
- (void)insertDeviceRegDetails:(NSArray *)deviceRegDetails  withProductKey:(NSString *)productKey;
-(DeviceDetails *)getDeviceRegDetails;
-(void)deleteAllOrderDetails;
- (BOOL)getDeviceRegStatus;
- (NSString *)getMaskCreditCardNo:(NSString *)creditCardNO;
- (void)insertAirPakcageDetails:(CustomerDetails *)customerAirPackageDetails  productDetails:(Products *)products deviceRegDetails:(DeviceDetails *)deviceDetails;
- (NSString *)generateAirlinePackageDetailsXML;
-(void)deleteAirPackageDetails;
-(void)updateAirPackageDeliverAttempt;
- (NSInteger)getAirPackageItemsCount ;
-(void)updateServerAccountInfo:(NSString *)accountInfo;
- (NSMutableArray *)getServerAccountInfo;
-(void)updateDeviceDetails;
-(void)updateDeviceId:(NSInteger)deviceId macAddress:(NSString *)macAddr;
-(void)updateDeviceStatus;
-(void)updateAirCode:(NSString *)airCode airName:(NSString *)airName;
-(void)updateSuggestions:(NSString *)suggestions feedBack:(NSString *)feedBack orderConfirmationNumber:(NSString *)orderConfirmationNo;
-(NSString *)getCatalogueDate;
+ (ProductDetailsDAO *) instance;

+ (NSString *)getEData:(NSString *)value;
@end
