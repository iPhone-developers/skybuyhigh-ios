//
//  CheckOutProductTableView.h
//  SkyBuyHigh
//
//  Created by Thapovan InfoSystems on 24/07/09.
//  Copyright 2009 Thapovan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Products.h"

@interface CheckOutProductTableView : UITableViewCell {
	
	UILabel *prodBrand;
	UILabel *prodTitle;
	NSString *prodImage;
	UILabel  *prodPrice;
	UILabel *prodSize;
	UILabel *prodColor;
	UILabel *quantity;
}
@property (nonatomic, retain) UILabel *prodBrand;
@property (nonatomic, retain) UILabel *prodTitle;
@property (nonatomic, retain) UILabel *prodPrice;
@property (nonatomic, retain) NSString *prodImage;
@property (nonatomic, retain) UILabel *prodSize;
@property (nonatomic, retain) UILabel *prodColor;
@property (nonatomic, retain) UILabel *quantity;

- (void) setData:(Products*) data; 

// internal function to ease setting up label text
-(UILabel *)newLabelWithPrimaryColor:(UIColor *)primaryColor selectedColor:(UIColor *)selectedColor fontSize:(CGFloat)fontSize bold:(BOOL)bold;


@end