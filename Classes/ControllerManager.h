//
//  ControllerManager.h
//  SkyBuyHigh
//
//  Created by Thapovan Info Systems on 11/09/09.
//  Copyright 2009 Thapovan. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface ControllerManager : NSObject {
	NSMutableDictionary *controllers;
}

+(ControllerManager *) instance;
- (UIViewController *) controllerWithName:(NSString *) name andNib:(NSString *) nibName;

@end
