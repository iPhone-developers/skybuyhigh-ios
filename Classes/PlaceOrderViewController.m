//
//  PlaceOrderViewController.m
//  SkyBuyHigh
//
//  Created by Thapovan InfoSystems on 21/08/09.
//  Copyright 2009 Thapovan. All rights reserved.
//

#import "PlaceOrderViewController.h"
#import "SkyBuyHighAppDelegate.h"
#import "ProductDetailsDAO.h"
#import "PlaceOrder.h"
#import "OrderDetails.h"

@implementation PlaceOrderViewController
@synthesize OrderDetailsMsg;//flightNo;
@synthesize progressView;
@synthesize successOrderMsg,failureOrderMsg;
@synthesize submitButton;
@synthesize errorMsg;
@synthesize activityIndicator;
@synthesize continueButton;


-(NSString *) documentsPath {
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	return documentsDirectory;
}

-(void)continueShopping:(id)sender{
	[self dismissModalViewControllerAnimated:YES];
}
-(IBAction)textFieldDoneEditing:(id)sender {
	[sender resignFirstResponder];
}

-(void)submitOrderDetails:(id)sender{
	//[flightNo resignFirstResponder];
	//if(![[[flightNo text] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] isEqualToString:@""]){
		[activityIndicator startAnimating];
		[self.progressView setProgress:0.0];
		[submitButton setEnabled:NO];
		[continueButton setEnabled:NO];
		[self performSelector:@selector(placeOrderDetails) withObject:nil afterDelay:0.0f];
	/*}else{
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"SkyBuyHigh" message:@"Please enter the flight number. " delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
		[alert show];
		[alert release];
	}*/	
}	


-(void)placeOrderDetails{

	NSUInteger placeOrderSuccessCount=0,placeOrderFailureCount=0,orderItemsCount=0,airPackageItemsCount;
	NSString *orderResponse=nil;
	orderItemsCount=[[ProductDetailsDAO instance] getOrderItemsCount];
	airPackageItemsCount = [[ProductDetailsDAO instance] getAirPackageItemsCount];
	OrderDetailsMsg.text=[[NSString alloc] initWithFormat:@"Total pending orders         : %d",orderItemsCount+airPackageItemsCount];
	
	if([[ProductDetailsDAO instance] getOrderItemsCount]>0){
		NSMutableArray *ordersArray = [[ProductDetailsDAO instance] generateOrderItemDetailsXML];
		for (int i = 0; i < [ordersArray count]; i++) {
			NSString *orderXML=[[ordersArray objectAtIndex:i] orderDetailsXml];		
			//	orderXML=[orderXML stringByReplacingOccurrencesOfString:@"FLIGHTNUMREPLACE" withString:[flightNo text]];
			orderResponse = [PlaceOrder sendRequestWithParams:orderXML customerId:[[ordersArray objectAtIndex:i] customerId]];
			if(orderResponse!=nil && [orderResponse isEqualToString:@"OPS"]) {
				placeOrderSuccessCount=placeOrderSuccessCount+1;
			}
			else {
				placeOrderFailureCount = placeOrderFailureCount+1;
			}
			successOrderMsg.text = [[NSString alloc] initWithFormat:@"Successfully placed order : %d",placeOrderSuccessCount];
			
			if(placeOrderFailureCount > 0) {						  
				failureOrderMsg.text = [[NSString alloc] initWithFormat:@"Failed order                          : %d",placeOrderFailureCount];
			}
			else {
				failureOrderMsg.text =@"";
			}
			
			[self.progressView setProgress: ((float)i+1) / (orderItemsCount + airPackageItemsCount)];
		}
	}	
	if ([[ProductDetailsDAO instance] getAirPackageItemsCount] > 0) {
		NSString *airlinePackageDetailsXML = [[ProductDetailsDAO instance] generateAirlinePackageDetailsXML];
		//airlinePackageDetailsXML=[airlinePackageDetailsXML stringByReplacingOccurrencesOfString:@"FLIGHTNUMREPLACE" withString:[flightNo text]];
		if(airlinePackageDetailsXML!=nil){
			orderResponse = [PlaceOrder sendRequestWithParams:airlinePackageDetailsXML];	
			if (orderResponse != nil && [orderResponse isEqualToString:@"OPS"]) {
				placeOrderSuccessCount=placeOrderSuccessCount+airPackageItemsCount;
			}
			else {
				placeOrderFailureCount = placeOrderFailureCount+airPackageItemsCount;
			}
			successOrderMsg.text = [[NSString alloc] initWithFormat:@"Successfully placed order : %d",placeOrderSuccessCount];
																		
																	
			if (placeOrderFailureCount > 0) {						
				failureOrderMsg.text = [[NSString alloc] initWithFormat:@"Failed order                          : %d",placeOrderFailureCount];
			}
			else {
				failureOrderMsg.text =@"";
			}
			
			[self.progressView setProgress: 1.0];
		}
	}
	if (placeOrderFailureCount == 0) {
		[submitButton setEnabled:NO];
		[errorMsg setHidden:NO];
		errorMsg.text=@"Order placed Successfully. Touch Continue Shopping to proceed.";
		[submitButton setTitle:@"Submit" forState:UIControlStateNormal];
	} else if (placeOrderFailureCount > 0) {
	    [submitButton setEnabled:YES];
		[submitButton setTitle:@"Retry" forState:UIControlStateNormal];
		[errorMsg setHidden:NO];
		if(orderResponse!=nil && [orderResponse isEqualToString:@"OPF"]){
			errorMsg.text=@"Order placement failed. Touch Retry to place orders.";
		}else if(orderResponse!=nil && [orderResponse isEqualToString:@"IOV"]){
			errorMsg.text=@"Invalid Total Order Value. Touch Retry to place orders.";
		}else if(orderResponse!=nil && [orderResponse isEqualToString:@"DID"]){
			errorMsg.text=@"Device is deleted. Contact SkyBuyHigh Admin.";
			[[ProductDetailsDAO instance] updateDeviceStatus];
			[(SkyBuyHighAppDelegate *)[[UIApplication sharedApplication] delegate] setDeviceRegStatus:[[ProductDetailsDAO instance] getDeviceRegStatus]];
			NSError *error=nil;
			[[NSFileManager defaultManager] removeItemAtPath:[[self documentsPath] stringByAppendingString:@"/AirlinePackage"] error:&error];
		}else if(orderResponse!=nil && [orderResponse isEqualToString:@"DIN"]){
			errorMsg.text=@"Device is inactive. Contact SkyBuyHigh Admin.";
		}else if(orderResponse!=nil && [orderResponse isEqualToString:@"DNA"]){
			errorMsg.text=@"Device is not allocated. Contact SkyBuyHigh Admin.";
		}else if(orderResponse!=nil && [orderResponse isEqualToString:@"DNC"]){
			errorMsg.text=@"Device is not activated. Contact SkyBuyHigh Admin.";
		}else if(orderResponse!=nil && [orderResponse isEqualToString:@"DNR"]){
			errorMsg.text=@"Device is not Registered with SkyBuyHigh. Contact SkyBuyHigh Admin.";
		}else if(orderResponse!=nil && [orderResponse isEqualToString:@"DRD"]){
			errorMsg.text=@"Device registered with this air charter is Deleted. Contact SkyBuyHigh Admin.";
		}else if(orderResponse!=nil && [orderResponse isEqualToString:@"EPX"]){
			errorMsg.text=@"Error while parsing XML. Contact SkyBuyHigh Admin.";
		}else if(orderResponse!=nil && [orderResponse isEqualToString:@"INA"]){
			errorMsg.text=@"Air Charter is inactive. Contact SkyBuyHigh Admin.";
		}else if(orderResponse!=nil && [orderResponse isEqualToString:@"IPK"]){
			errorMsg.text=@"Invalid Product Key. Contact SkyBuyHigh Admin.";
		}else if(orderResponse!=nil && [orderResponse isEqualToString:@"IAC"]){
			errorMsg.text=@"Invalid Air charter code. Contact SkyBuyHigh Admin.";
		}else if(orderResponse!=nil && [orderResponse isEqualToString:@"DEX"]){
			errorMsg.text=@"Problem in device authentication. Contact SkyBuyHigh Admin.";
		}else if(orderResponse!=nil && [orderResponse isEqualToString:@"IND"]){
			errorMsg.text=@"Invalid Device. Contact SkyBuyHigh Admin.";
		}else if(orderResponse!=nil && [orderResponse isEqualToString:@"IAP"]){
			errorMsg.text=@"Invalid Airline Code/Product key. Contact SkyBuyHigh Admin.";
		}else{
			errorMsg.text=@"Order placement failed. Touch Retry to place orders.";
		}
	}	
	[continueButton setEnabled:YES];
	[self.activityIndicator stopAnimating];
}	

- (void)viewDidAppear:(BOOL)animated {
	NSUInteger  orderItemsCount=[[ProductDetailsDAO instance] getOrderItemsCount];
	NSUInteger airPackageItemsCount = [[ProductDetailsDAO instance] getAirPackageItemsCount];
	OrderDetailsMsg.text=[[NSString alloc] initWithFormat:@"Total pending orders         : %d",orderItemsCount+airPackageItemsCount];
	successOrderMsg.text=@"";
	failureOrderMsg.text =@"";
	
	[errorMsg setText:@"Touch submit to place the order(s)."];
	[submitButton setTitle:@"Submit" forState:UIControlStateNormal];
}	

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}


- (void)dealloc {
	[OrderDetailsMsg release],OrderDetailsMsg = nil;
	//[flightNo release],flightNo = nil;
	[successOrderMsg release],successOrderMsg = nil;
	[failureOrderMsg release],failureOrderMsg = nil;
	[submitButton release],submitButton =nil;
	[continueButton release],continueButton = nil;
	[errorMsg release],errorMsg = nil;
	[progressView release],progressView =nil;
	[activityIndicator release],activityIndicator = nil;
    [super dealloc];
}


@end
