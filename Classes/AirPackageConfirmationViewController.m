//
//  AirPackageConfirmationViewController.m
//  SkyBuyHigh
//
//  Created by Thapovan InfoSystems on 08/09/09.
//  Copyright 2009 Thapovan. All rights reserved.
//

#import "AirPackageConfirmationViewController.h"
#import "DeviceDetails.h"
#import "ProductDetailsDAO.h"
#import "PlaceOrder.h"
#import "OrderDetails.h"



@implementation AirPackageConfirmationViewController


- (void)viewWillAppear:(BOOL)animated {
	
	self.navigationItem.hidesBackButton = YES;
	self.navigationItem.leftBarButtonItem =	[[UIBarButtonItem alloc] initWithTitle:@"More Products"
																			 style:UIBarButtonItemStyleBordered
																			target:self
																			action:@selector(handleBack:)];		
	[UIApplication sharedApplication].applicationIconBadgeNumber = [[ProductDetailsDAO instance] getOrderItemsCount]+[[ProductDetailsDAO instance] getAirPackageItemsCount];
	/*SkyBuyHighAppDelegate *delegate=(SkyBuyHighAppDelegate *)[[UIApplication sharedApplication]delegate];
	NSMutableArray *productInfomation=nil ;
	id categoryViewController=nil;
	categoryViewController = [[self.navigationController viewControllers] objectAtIndex:0];
	productInfomation = [categoryViewController productInfo];
	NSUInteger rowCount = [categoryViewController nsiCurrentRow];
	Products *prod= [productInfomation objectAtIndex:rowCount];
	DeviceDetails *deviceDetails=[[ProductDetailsDAO instance] getDeviceRegDetails];
	[[ProductDetailsDAO instance] insertAirPakcageDetails:delegate.customerAirPackageDetails productDetails:prod deviceRegDetails:deviceDetails];
	
	if (!([[Reachability sharedReachability] internetConnectionStatus] == NotReachable)) {
		NSString *orderResponse=nil;
		//Place airline packages
		NSString *airlinePackageDetailsXML = [[ProductDetailsDAO instance] generateAirlinePackageDetailsXML];
		if(airlinePackageDetailsXML!=nil){
			orderResponse = [PlaceOrder sendRequestWithParams:airlinePackageDetailsXML];	
			if (orderResponse != nil && [orderResponse isEqualToString:@"OPS"]) {
				NSLog(@"Airline Package has been placed successfully");
			}
		}
		
		//Place product details
		NSMutableArray *ordersArray = [[ProductDetailsDAO instance] generateOrderItemDetailsXML];
		for (int i = 0; i < [ordersArray count]; i++) {
			NSLog(@"Inside placing Product details");
			NSString *orderXML=[[ordersArray objectAtIndex:i] orderDetailsXml];		
			orderResponse = [PlaceOrder sendRequestWithParams:orderXML customerId:[[ordersArray objectAtIndex:i] customerId]];
			if(orderResponse!=nil && [orderResponse isEqualToString:@"OPS"]) {
				//placeOrderSuccessCount=placeOrderSuccessCount+1;
				NSLog(@"Order has been successfully placed.");
			}				
		}
	}

	[UIApplication sharedApplication].applicationIconBadgeNumber = [[ProductDetailsDAO instance] getOrderItemsCount]+[[ProductDetailsDAO instance] getAirPackageItemsCount];
	delegate.customerAirPackageDetails = [[CustomerDetails alloc] init];	*/
}	
	
- (void) handleBack:(id)sender
{
   /* SkyBuyHighAppDelegate *delegate=(SkyBuyHighAppDelegate *)[[UIApplication sharedApplication]delegate];
	UITabBarController *tabBarController = delegate.tabBarController;
	NSUInteger tabControllerIndex = tabBarController.selectedIndex;
	if(tabControllerIndex == 0){
		[delegate.menNavController popToViewController:delegate.menTableViewController animated:YES];
	}else if(tabControllerIndex == 1){
		[delegate.womenNavController popToViewController:delegate.womenTableViewController animated:YES];
	}else if(tabControllerIndex == 2){		
		[delegate.giftNavController popToViewController:delegate.giftTableViewController animated:YES];
	}else if(tabControllerIndex == 3){
		[delegate.privatejetNavController popToViewController:delegate.privatejetTableViewController animated:YES];
	}*/
	[self.navigationController popToRootViewControllerAnimated:YES];
}


- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [super dealloc];
}


@end
