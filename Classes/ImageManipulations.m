//
//  ImageManipulations.m
//  PalTalk
//
//  Created by Thapovan Info Systems on 30/06/09.
//  Copyright 2009 Thapovan. All rights reserved.
//

#import "ImageManipulations.h"
#import <QuartzCore/QuartzCore.h>
#import "DataCache.h"

@implementation ImageManipulations

/*
- (void)drawRect:(CGRect)rect 
{
    CGContextRef currentContext = UIGraphicsGetCurrentContext();
	
    CGGradientRef glossGradient;
    CGColorSpaceRef rgbColorspace;
    size_t num_locations = 2;
    CGFloat locations[2] = { 0.0, 1.0 };
    CGFloat components[8] = { 1.0, 1.0, 1.0, 0.35,  // Start color
	1.0, 1.0, 1.0, 0.06 }; // End color
	
    rgbColorspace = CGColorSpaceCreateDeviceRGB();
    glossGradient = CGGradientCreateWithColorComponents(rgbColorspace, components, locations, num_locations);
	
    CGRect currentBounds = self.bounds;
    CGPoint topCenter = CGPointMake(CGRectGetMidX(currentBounds), 0.0f);
    CGPoint midCenter = CGPointMake(CGRectGetMidX(currentBounds), CGRectGetMidY(currentBounds));
    CGContextDrawLinearGradient(currentContext, glossGradient, topCenter, midCenter, 0);
	
    CGGradientRelease(glossGradient);
    CGColorSpaceRelease(rgbColorspace); 
}
*/

void addRoundedRectToPath(CGContextRef context, CGRect rect, float ovalWidth, float ovalHeight)
{
    float fw, fh;
    if (ovalWidth == 0 || ovalHeight == 0) {
        CGContextAddRect(context, rect);
        return;
    }
    CGContextSaveGState(context);
    CGContextTranslateCTM (context, CGRectGetMinX(rect), CGRectGetMinY(rect));
    CGContextScaleCTM (context, ovalWidth, ovalHeight);
    fw = CGRectGetWidth (rect) / ovalWidth;
    fh = CGRectGetHeight (rect) / ovalHeight;
    CGContextMoveToPoint(context, fw, fh/2);
    CGContextAddArcToPoint(context, fw, fh, fw/2, fh, 1);
    CGContextAddArcToPoint(context, 0, fh, 0, fh/2, 1);
    CGContextAddArcToPoint(context, 0, 0, fw/2, 0, 1);
    CGContextAddArcToPoint(context, fw, 0, fw, fh/2, 1);
    CGContextClosePath(context);
    CGContextRestoreGState(context);
}

+ (UIImage *)roundCornersOfImage:(UIImage *)source withClipRadius:(int) clipRadius{
    int w = source.size.width;
    int h = source.size.height;
    
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGContextRef context = CGBitmapContextCreate(NULL, w, h, 8, 4 * w, colorSpace, kCGImageAlphaPremultipliedFirst);
    CGContextBeginPath(context);
    CGRect rect = CGRectMake(0, 0, w, h);
    addRoundedRectToPath(context, rect, clipRadius, clipRadius);
    CGContextClosePath(context);
    CGContextClip(context);
	
    CGContextDrawImage(context, CGRectMake(0, 0, w, h), source.CGImage);
    CGImageRef imageMasked = CGBitmapContextCreateImage(context);
	CGContextRelease(context);
    CGColorSpaceRelease(colorSpace);
    
    return [UIImage imageWithCGImage:imageMasked];    
}


+ (void)roundedCornersOfImage:(UIImage *)source withClipRadius:(int) clipRadius forKey:(NSString *)key {
    int w = source.size.width;
    int h = source.size.height;
    
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGContextRef context = CGBitmapContextCreate(NULL, w, h, 8, 4 * w, colorSpace, kCGImageAlphaPremultipliedFirst);
    CGContextBeginPath(context);
    CGRect rect = CGRectMake(0, 0, w, h);
    addRoundedRectToPath(context, rect, clipRadius, clipRadius);
    CGContextClosePath(context);
    CGContextClip(context);
	
    CGContextDrawImage(context, CGRectMake(0, 0, w, h), source.CGImage);
    CGImageRef imageMasked = CGBitmapContextCreateImage(context);
	[[DataCache tableViewImageCache] setObject:(id)imageMasked forKey:key];
	CGImageRelease(imageMasked);
	CGContextRelease(context);
    CGColorSpaceRelease(colorSpace);
}


#pragma mark -
#pragma mark Reflection code

 
 CGImageRef CreateGradientImage(int pixelsWide, int pixelsHigh)
 {
 CGImageRef theCGImage = NULL;
 
 // gradient is always black-white and the mask must be in the gray colorspace
 CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceGray();
 
 // create the bitmap context
 CGContextRef gradientBitmapContext = CGBitmapContextCreate(nil, pixelsWide, pixelsHigh,
 8, 0, colorSpace, kCGImageAlphaNone);
 
 // define the start and end grayscale values (with the alpha, even though
 // our bitmap context doesn't support alpha the gradient requires it)
 CGFloat colors[] = {0.0, 1.0, 1.0, 1.0};
 
 // create the CGGradient and then release the gray color space
 CGGradientRef grayScaleGradient = CGGradientCreateWithColorComponents(colorSpace, colors, NULL, 2);
 CGColorSpaceRelease(colorSpace);
 
 // create the start and end points for the gradient vector (straight down)
 CGPoint gradientStartPoint = CGPointZero;
 CGPoint gradientEndPoint = CGPointMake(0, pixelsHigh);
 
 // draw the gradient into the gray bitmap context
 CGContextDrawLinearGradient(gradientBitmapContext, grayScaleGradient, gradientStartPoint,
 gradientEndPoint, kCGGradientDrawsAfterEndLocation);
 
 // convert the context into a CGImageRef and release the context
 theCGImage = CGBitmapContextCreateImage(gradientBitmapContext);
 CGContextRelease(gradientBitmapContext);
 
 // return the imageref containing the gradient
 return theCGImage;
 }
 
 CGContextRef MyCreateBitmapContext(int pixelsWide, int pixelsHigh)
 {
 CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
 
 // create the bitmap context
 CGContextRef bitmapContext = CGBitmapContextCreate (nil, pixelsWide, pixelsHigh, 8,
 0, colorSpace,
 // this will give us an optimal BGRA format for the device:
 (kCGBitmapByteOrder32Little | kCGImageAlphaPremultipliedFirst));
 CGColorSpaceRelease(colorSpace);
 
 return bitmapContext;
 }
 
 + (UIImage *)reflectedImage:(UIImageView *)fromImage withHeight:(NSUInteger)height
 {
 if (!height) return nil;
 
 // create a bitmap graphics context the size of the image
 CGContextRef mainViewContentContext = MyCreateBitmapContext(fromImage.bounds.size.width, height);
 
 // offset the context -
 // This is necessary because, by default, the layer created by a view for caching its content is flipped.
 // But when you actually access the layer content and have it rendered it is inverted.  Since we're only creating
 // a context the size of our reflection view (a fraction of the size of the main view) we have to translate the
 // context the delta in size, and render it.
 //
 CGFloat translateVertical= fromImage.bounds.size.height - height;
 CGContextTranslateCTM(mainViewContentContext, 0, -translateVertical);
 
 // render the layer into the bitmap context
 CALayer *layer = fromImage.layer;
 [layer renderInContext:mainViewContentContext];
 
 // create CGImageRef of the main view bitmap content, and then release that bitmap context
 CGImageRef mainViewContentBitmapContext = CGBitmapContextCreateImage(mainViewContentContext);
 CGContextRelease(mainViewContentContext);
 
 // create a 2 bit CGImage containing a gradient that will be used for masking the 
 // main view content to create the 'fade' of the reflection.  The CGImageCreateWithMask
 // function will stretch the bitmap image as required, so we can create a 1 pixel wide gradient
 CGImageRef gradientMaskImage = CreateGradientImage(1, height);
 
 // create an image by masking the bitmap of the mainView content with the gradient view
 // then release the  pre-masked content bitmap and the gradient bitmap
 CGImageRef reflectionImage = CGImageCreateWithMask(mainViewContentBitmapContext, gradientMaskImage);
 CGImageRelease(mainViewContentBitmapContext);
 CGImageRelease(gradientMaskImage);
 
 // convert the finished reflection image to a UIImage 
 UIImage *theImage = [UIImage imageWithCGImage:reflectionImage];
 
 // image is retained by the property setting above, so we can release the original
 CGImageRelease(reflectionImage);
 
 return theImage;
 }


+ (UIImageView *)getStarsOfType:(NSString *)starType withPosition:(int)i {
	UIImage *starImage = [UIImage imageNamed:starType];
	UIImageView *starView = [[UIImageView alloc] initWithFrame:CGRectMake(i*13,0,10,15)];
	[starView setImage:starImage];
	//[ratingView addSubview:starView];
	[starImage release];
	return starView;
}

@end
