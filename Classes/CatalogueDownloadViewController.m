//
//  CatalogueDownloadViewController.m
//  CatalogueDownload
//
//  Created by Thapovan Info Systems on 27/08/09.
//  Copyright Thapovan 2009. All rights reserved.
//

#import "CatalogueDownloadViewController.h"
#import "ZipArchive.h"
#import "SkyBuyHighAppDelegate.h"
#import "ProductDetailsDAO.h"
#import "DeviceDetails.h"

@implementation CatalogueDownloadViewController

@synthesize progressView, totalBytes, completedBytes, fileSize, theConnection;
@synthesize cancelButton, completedFileSize;
@synthesize activityIndicator, activityLabel;
@synthesize catalogueDate;
@synthesize continueButton;
@synthesize airName, airCode;
@synthesize errorMsg;
@synthesize totalBytesLabel,totalSizeLabel,downloadedLabel;

-(void)showCatalogue {
	[(SkyBuyHighAppDelegate *)[[UIApplication sharedApplication] delegate] initializeTabBar];
}


-(void)waitForContinue {
	[cancelButton setHidden:YES];
	[continueButton setHidden:NO];
}

-(NSString *) documentsPath {
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	return documentsDirectory;
}

-(IBAction)cancelUpdate:(id)sender {
	[activityLabel setText:@"Cancelling Update..."];
	NSError *error = nil;
	[zipHandle release];
	[[NSFileManager defaultManager] removeItemAtPath:[[self documentsPath] stringByAppendingString:@"/SkyBuy.zip"] error:&error];
	if (error) {
		NSLog(@"Failed to delete after update was cancelled");
	}
	[self.theConnection cancel];
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
	[cancelButton setEnabled:NO];
	[self performSelector:@selector(showCatalogue) withObject:nil afterDelay:0.2f];
}


-(IBAction)continueShopping:(id)sender {
	[activityLabel setText:@"Loading Catalogue..."];
	[activityIndicator startAnimating];
	[continueButton setEnabled:NO];
	[self performSelector:@selector(showCatalogue) withObject:nil afterDelay:0.2f];
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {

	NSMutableURLRequest *theRequest = nil;
	[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
	if (![[NSFileManager defaultManager] fileExistsAtPath:[[self documentsPath] stringByAppendingString:@"/AirlinePackage"]] && [(SkyBuyHighAppDelegate *)[[UIApplication sharedApplication] delegate] deviceRegStatus]) {
		[(SkyBuyHighAppDelegate *)[[UIApplication sharedApplication] delegate] setDownloadAirPackage:YES];
	}

	if([(SkyBuyHighAppDelegate *)[[UIApplication sharedApplication] delegate] deviceRegStatus])
		theRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[[ProductDetailsDAO instance] getCatalogueDetailsURL]] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:15.0];
	else{
		NSDictionary *connections = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Connections" ofType:@"plist"]];
		NSString *catalogueDetailsURL=[[NSString alloc] initWithFormat:[connections valueForKey:@"UnregisteredCatalogueDownloadURL"],[[UIDevice currentDevice] uniqueIdentifier],[[ProductDetailsDAO instance] getCatalogueDate]]; 
		theRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:catalogueDetailsURL] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:15.0];
	}

	[activityIndicator startAnimating];
	[activityLabel setText:@"Checking for Update"];
	
	theConnection = [[NSURLConnection alloc] initWithRequest:theRequest delegate:self];
	if (theConnection) {
		//NSLog(@"Connection successfully made");
		[activityLabel setText:@"Checking for catalogue updates..."];
	}
	else {
		NSLog(@"Connection could not be made");
	}
	
	[super viewDidLoad];
}


-(void)connection:(NSURLConnection *)connection
didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge {	
    if ([challenge previousFailureCount] == 0) {		
		//NSLog(@"Inside didReceiveAuthentication Challenge");
        NSURLCredential *newCredential;		
		NSArray *accountInfo = [[ProductDetailsDAO instance] getServerAccountInfo];
		if(accountInfo!=nil){
			//NSLog(@"Username = %@", [accountInfo objectAtIndex:0]);
			//NSLog(@"Password = %@", [accountInfo objectAtIndex:1]);
			newCredential=[NSURLCredential credentialWithUser:[accountInfo objectAtIndex:0]
													 password:[accountInfo objectAtIndex:1]
												  persistence:NSURLCredentialPersistenceNone];
		}
        [[challenge sender] useCredential:newCredential
               forAuthenticationChallenge:challenge];
		
		[accountInfo release];accountInfo = nil;
		
    } else {
		//NSLog(@"Authentication failed");
        [[challenge sender] cancelAuthenticationChallenge:challenge];
        // inform the user that the user name and password
        // in the preferences are incorrect
        //[self showPreferencesCredentialsAreIncorrectPanel:self];
    }
}


- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    // this method is called when the server has determined that it	
    // has enough information to create the NSURLResponse
    // it can be called multiple times, for example in the case of a
    // redirect, so each time we reset the data.
    // receivedData is declared as a method instance elsewhere
	[self.cancelButton setHidden:NO];
	[self.cancelButton setEnabled:YES];
    if ([response isKindOfClass:[NSHTTPURLResponse class]]) {
		[UIView beginAnimations:@"updateFinished" context:nil];
		[UIView setAnimationDuration: 0.65];
		[UIView setAnimationTransition:UIViewAnimationTransitionFlipFromLeft forView:activityLabel cache:YES];
		[activityLabel setText:@"Downloading catalogue..."];
		[UIView commitAnimations];
		NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
	    //NSLog(@"Response Header = \n%@",[httpResponse allHeaderFields]);
		NSString *accountinfo = [[httpResponse allHeaderFields] objectForKey:@"Accountinfo"];
		if(accountinfo!=nil && ![accountinfo isEqualToString:@""]){
			[[ProductDetailsDAO instance] updateServerAccountInfo:accountinfo];
		}
		airCode = [[httpResponse allHeaderFields] objectForKey:@"Aircode"];
		airName = [[httpResponse allHeaderFields] objectForKey:@"Airname"];
			
		errorMsg = [[httpResponse allHeaderFields] objectForKey:@"Errormsg"];
		
		if (errorMsg == nil) {
			if (![[[httpResponse allHeaderFields] objectForKey:@"Filesize"] isEqualToString:@"0"]) {
				[downloadedLabel setHidden:NO];
				[totalSizeLabel setHidden:NO];
				[totalBytesLabel setHidden:NO];
				totalBytes.text =[[httpResponse allHeaderFields] objectForKey:@"Filesize"];
				fileSize = [totalBytes.text longLongValue];
				NSError *error = nil;
				NSString *filePath = [[self documentsPath] stringByAppendingString:@"/SkyBuy.zip"];
				if ([[NSFileManager defaultManager] fileExistsAtPath:filePath]) {
					[[NSFileManager defaultManager] removeItemAtPath:filePath error:&error];
					if (error) {
						NSLog(@"Error deleting file - %@",[error localizedDescription]);
                    }
				}
				[[NSFileManager defaultManager] createFileAtPath:filePath contents:nil attributes:nil];
				zipHandle = [NSFileHandle fileHandleForWritingAtPath:filePath];
				[zipHandle retain];
				completedFileSize = 0;
				catalogueDate = [[httpResponse allHeaderFields] objectForKey:@"Downloaddate"];
			}
			else {
				[activityLabel setText:@"No updates available. Loading catalogue..."];
				totalBytes.text=@"";
				
			}
		}
		else {
			[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
			[activityIndicator stopAnimating];
			[activityLabel setText:[NSString stringWithFormat:@"Error - %@", errorMsg]];
			[self waitForContinue];
		}
	}
}


- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    // append the new data to the file on disk using the zipHandle
	completedFileSize = completedFileSize + [data length];
	[zipHandle writeData:data];
	completedBytes.text = [NSString stringWithFormat:@"%lu", completedFileSize];
	[progressView setProgress:completedFileSize / (float)fileSize];
	if (completedFileSize == fileSize) {
		[activityLabel setText:@"Extracting catalogue..."];
	}
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    // Extract the received zip file
	// Update catalogue XML file, also update catalogue update date in DB
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
	if (completedFileSize != 0 && catalogueDate != nil) {
		BOOL unzipSuccess = YES;// fileCopySuccess = YES;
		NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
		NSString *documentsDirectory = [paths objectAtIndex:0];
		NSFileManager *fileManager = [NSFileManager defaultManager];
		if ([fileManager fileExistsAtPath:[[self documentsPath] stringByAppendingString:@"/SkyBuy.zip"]]) {
			//NSLog(@"File written successfully");
		}
		NSMutableString *ZipPath = [[NSMutableString alloc] initWithFormat:@"%@/SkyBuy.zip",documentsDirectory];
		ZipArchive* za = [[ZipArchive alloc] init];
		if( [za UnzipOpenFile:ZipPath] )
		{
			BOOL ret = [za UnzipFileTo:documentsDirectory overWrite:YES];
			if( NO==ret )
			{
				unzipSuccess = NO;
				NSLog(@"Unzip failed");
			}
			[za UnzipCloseFile];
		}
		else {
			unzipSuccess = NO;
			NSLog(@"File Open failed");
		}
		[za release];
		[ZipPath release];
		[connection release];
		[zipHandle release];
		
		DeviceDetails *thisDevice = nil;
		if([(SkyBuyHighAppDelegate *)[[UIApplication sharedApplication] delegate] deviceRegStatus])
			thisDevice = [[ProductDetailsDAO instance] getDeviceRegDetails];
		
		if (unzipSuccess && catalogueDate != nil) {
			if(airCode!=nil && airName!=nil && ![airCode isEqualToString:@""] && ![airName isEqualToString:@""]){
				[[ProductDetailsDAO instance] updateAirCode:airCode airName:airName];
			}	
			if ([[totalBytes text] isEqualToString:@"0"]) {
				[totalBytes setText:@""];
			}
			if([(SkyBuyHighAppDelegate *)[[UIApplication sharedApplication] delegate] deviceRegStatus])
				[[ProductDetailsDAO instance] updateCatalogueDate:[thisDevice deviceId] macAddress:[thisDevice macAddr] withCatalogueDate:catalogueDate];
			else
				[[ProductDetailsDAO instance] updateCatalogueDate:0 macAddress:[[UIDevice currentDevice] uniqueIdentifier] withCatalogueDate:catalogueDate];
			
			if([(SkyBuyHighAppDelegate *)[[UIApplication sharedApplication] delegate] downloadAirPackage]) {			
				[[NSFileManager defaultManager] createFileAtPath:[documentsDirectory stringByAppendingString:@"/AirlinePackage"] contents:nil attributes:nil];
				[(SkyBuyHighAppDelegate *)[[UIApplication sharedApplication] delegate] setDownloadAirPackage:NO];
			}	
			//NSLog(@"Catalogue successfully updated after auto update");
			[UIView beginAnimations:@"updateFinished" context:nil];
			[UIView setAnimationDuration: 0.65];
			[UIView setAnimationTransition:UIViewAnimationTransitionFlipFromLeft forView:activityLabel cache:YES];
			[activityLabel setText:@"Update successful. Touch Continue to proceed."];
			[UIView commitAnimations];
			[activityIndicator stopAnimating];
		}
		else {
			//NSLog(@"Not updating catalogue date");
			[UIView beginAnimations:@"updateFinished" context:nil];
			[UIView setAnimationDuration: 0.65];
			[UIView setAnimationTransition:UIViewAnimationTransitionFlipFromLeft forView:activityLabel cache:YES];
			[activityLabel setText:@"Error downloading catalogue. Please try again later."];
			[UIView commitAnimations];
			[activityIndicator stopAnimating];
		}
	}
	else {
		[UIView beginAnimations:@"updateFinished" context:nil];
		[UIView setAnimationDuration: 0.65];
		[UIView setAnimationTransition:UIViewAnimationTransitionFlipFromLeft forView:activityLabel cache:YES];
		
		if (errorMsg == nil) {
			[activityLabel setText:@"No update available. Touch Continue to proceed."];
		} else {
			if (errorMsg!=nil && [errorMsg isEqualToString:@"DIN"]) {
				[activityLabel setText:@"Device is inactive. Contact SkyBuyHigh Admin."];
			} else if(errorMsg!=nil && [errorMsg isEqualToString:@"DNA"]) {
				[activityLabel setText:@"Device is not allocated. Contact SkyBuyHigh Admin."];
			} else if(errorMsg!=nil && [errorMsg isEqualToString:@"DNC"]) {
				[activityLabel setText:@"Device is not activated. Contact SkyBuyHigh Admin."];
			} else if(errorMsg!=nil && [errorMsg isEqualToString:@"DNR"]) {
				[activityLabel setText:@"Device is not Registered with SkyBuyHigh. Contact SkyBuyHigh Admin."];
			} else if(errorMsg!=nil && [errorMsg isEqualToString:@"DRD"]) {
				[activityLabel setText:@"Device registered with this air charter is Deleted. Contact SkyBuyHigh Admin."];
			} else if(errorMsg!=nil && [errorMsg isEqualToString:@"INA"]) {
				[activityLabel setText:@"Air Charter is inactive. Contact SkyBuyHigh Admin."];
			} else if(errorMsg!=nil && [errorMsg isEqualToString:@"IPK"]) {
				[activityLabel setText:@"Invalid Product Key. Contact SkyBuyHigh Admin."];
			} else if(errorMsg!=nil && [errorMsg isEqualToString:@"IAC"]) {
				[activityLabel setText:@"Invalid Air charter code. Contact SkyBuyHigh Admin."];
			} else if(errorMsg!=nil && [errorMsg isEqualToString:@"DEX"]) {
					[activityLabel setText:@"Problem in device authentication. Contact SkyBuyHigh Admin."];
			} else if(errorMsg!=nil && [errorMsg isEqualToString:@"IND"]) {
				[activityLabel setText:@"Invalid Device. Contact SkyBuyHigh Admin."];
			} else if(errorMsg!=nil && [errorMsg isEqualToString:@"IAP"]) {
				[activityLabel setText:@"Invalid Airline Code/Product key. Contact SkyBuyHigh Admin."];
			} else if(errorMsg!=nil && [errorMsg isEqualToString:@"DID"]) {
                [activityLabel setText:@"Device is deleted. Contact SkyBuyHigh Admin."];
                [[ProductDetailsDAO instance] updateDeviceStatus];
				[(SkyBuyHighAppDelegate *)[[UIApplication sharedApplication] delegate] setDeviceRegStatus:[[ProductDetailsDAO instance] getDeviceRegStatus]];
				NSError *error=nil;
				[[NSFileManager defaultManager] removeItemAtPath:[[self documentsPath] stringByAppendingString:@"/AirlinePackage"] error:&error];
				if (!error) {
					NSLog(@"Airline Package file is deleted.");
				}
            }else{
				[activityLabel setText:@"No update available. Touch Continue to proceed."];
			}
					
		}	
		[self.progressView setProgress: 1.0];
		[UIView commitAnimations];
		[activityIndicator stopAnimating];
	}
	[self waitForContinue];
}


- (void)connection:(NSURLConnection *)connection
  didFailWithError:(NSError *)error {	
    // release the connection, and the data object	
    [connection release];	
	if (zipHandle)
		[zipHandle release];
    // inform the user	
		
	activityLabel.text = @"Please check your connectivity and try again later.";
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
	[activityIndicator stopAnimating];
	[self waitForContinue];
}



- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
}


- (void)dealloc {
	[progressView release], progressView = nil;
	[totalBytes release], totalBytes = nil;
	[completedBytes release], completedBytes = nil;
	[cancelButton release], cancelButton = nil;
	[continueButton release],continueButton = nil;
	if (theConnection)
		[theConnection release], theConnection = nil;
	[catalogueDate release];
	if (zipHandle)
		[zipHandle release], zipHandle = nil;
	[activityIndicator release], activityIndicator = nil;
	[activityLabel release], activityLabel = nil;
	[totalBytesLabel release], totalBytesLabel =nil;
	[totalSizeLabel release], totalSizeLabel = nil;
	[downloadedLabel release],downloadedLabel =nil;
	[airCode release],airCode = nil;
	[airName release],airName = nil;	
	[errorMsg release],errorMsg = nil;
    [super dealloc];
}

@end

