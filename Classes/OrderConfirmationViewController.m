//
//  OrderConfirmationViewController.m
//  SkyBuyHigh
//
//  Created by Thapovan InfoSystems on 25/07/09.
//  Copyright 2009 Thapovan. All rights reserved.
//

#import "OrderConfirmationViewController.h"
#import "SkyBuyHighAppDelegate.h"
#import "ProductDetailsDAO.h"
#import "SuggestionBoxViewController.h"
#import "OrderDetails.h"
#import "PlaceOrder.h"

@implementation OrderConfirmationViewController
@synthesize orderConfirmationNumber;
@synthesize emailText;
@synthesize orderConfirmationNo;
@synthesize emailTextMessage;

- (void)viewWillAppear:(BOOL)animated {
	self.navigationItem.hidesBackButton = YES;
	self.navigationItem.leftBarButtonItem =	[[UIBarButtonItem alloc] initWithTitle:@"More Products"
																			 style:UIBarButtonItemStyleBordered
																			target:self
																			action:@selector(handleBack:)];	
	[orderConfirmationNumber setText:orderConfirmationNo];
	[emailText setText:emailTextMessage];
	[UIApplication sharedApplication].applicationIconBadgeNumber = [[ProductDetailsDAO instance] getOrderItemsCount]+[[ProductDetailsDAO instance] getAirPackageItemsCount];
	/*SkyBuyHighAppDelegate *delegate=(SkyBuyHighAppDelegate *)[[UIApplication sharedApplication]delegate];
	
	if([delegate.addToBagProducts count]>0){
		NSString *orderResponse=nil;
		DeviceDetails *deviceDetails=[[ProductDetailsDAO instance] getDeviceRegDetails];
		[[ProductDetailsDAO instance] getDatabaseConnection];
		NSDateFormatter *dateFormatter = [[[NSDateFormatter alloc] init] autorelease];
		[dateFormatter setDateFormat:@"MMddyy-HHmmss"];
		NSString *currentDatetime = [dateFormatter stringFromDate:[NSDate date]];
		
		orderConfirmationNo =[[NSString alloc] initWithFormat:@"%@-%@",delegate.customerBillingDetails.phone,currentDatetime];
		NSInteger customerId=[[ProductDetailsDAO instance] insertCustomerBillingDetails:delegate.customerBillingDetails customerShippingDetails:delegate.customerShippingDetails customerCreditCatddetails:delegate.creditCardDetails deviceRegDetails:deviceDetails orderConfirmationNumber:orderConfirmationNo];
		
		
		for(int i=0;i<[delegate.addToBagProducts count];i++){		
			Products *prod = [delegate.addToBagProducts objectAtIndex:i];
			[[ProductDetailsDAO instance] insertProductDetails:prod setCustomerId:customerId setOrderConfirmationNo:orderConfirmationNo setTailNumber:delegate.customerBillingDetails.tailNumber];
			[prod release];		
		}	
		[[ProductDetailsDAO instance] closeDatabaseConnection];
	    NSLog(@"Order Confirmation product Count =%d",[[ProductDetailsDAO instance] getOrderItemsCount]);
		if (!([[Reachability sharedReachability] internetConnectionStatus] == NotReachable)) {
		    NSLog(@"Net reachable");
			//Place product details
			NSMutableArray *ordersArray = [[ProductDetailsDAO instance] generateOrderItemDetailsXML];
			for (int i = 0; i < [ordersArray count]; i++) {
				NSString *orderXML=[[ordersArray objectAtIndex:i] orderDetailsXml];		
			    orderResponse = [PlaceOrder sendRequestWithParams:orderXML customerId:[[ordersArray objectAtIndex:i] customerId]];
				if(orderResponse!=nil && [orderResponse isEqualToString:@"OPS"]) {
					//placeOrderSuccessCount=placeOrderSuccessCount+1;
					NSLog(@"Order has been successfully placed.");
				}				
			}
			
			//Place airline packages
			NSString *airlinePackageDetailsXML = [[ProductDetailsDAO instance] generateAirlinePackageDetailsXML];
			if(airlinePackageDetailsXML!=nil){
				orderResponse = [PlaceOrder sendRequestWithParams:airlinePackageDetailsXML];	
				if (orderResponse != nil && [orderResponse isEqualToString:@"OPS"]) {
					NSLog(@"Airline Package has been placed successfully");
				}
			}
		}
		[UIApplication sharedApplication].applicationIconBadgeNumber = [[ProductDetailsDAO instance] getOrderItemsCount]+[[ProductDetailsDAO instance] getAirPackageItemsCount];
		orderConfirmationNumber.text = orderConfirmationNo;
		emailText.text=[[NSString alloc] initWithFormat:@"Please note the order confirmation number which will also be e-mailed to %@",[delegate.customerBillingDetails	email]];
		
	}
	
	delegate.creditCardDetails = [[CreditCardDetails alloc] init];	
	delegate.customerBillingDetails = [[CustomerDetails alloc] init];	
	delegate.customerShippingDetails = [[CustomerDetails alloc] init];
	delegate.addToBagProducts = [[NSMutableArray alloc] init];
	NSLog(@"Order Confirmation product Count =%d",[[ProductDetailsDAO instance] getOrderItemsCount]);*/

	[super viewWillAppear:NO];
	
}




- (void) handleBack:(id)sender
{
   /* SkyBuyHighAppDelegate *delegate=(SkyBuyHighAppDelegate *)[[UIApplication sharedApplication]delegate];
	UITabBarController *tabBarController = delegate.tabBarController;
	NSUInteger tabControllerIndex = tabBarController.selectedIndex;
	if(tabControllerIndex == 0){
		[delegate.menNavController popToViewController:delegate.menTableViewController animated:YES];
	}else if(tabControllerIndex == 1){
		[delegate.womenNavController popToViewController:delegate.womenTableViewController animated:YES];
	}else if(tabControllerIndex == 2){		
		[delegate.giftNavController popToViewController:delegate.giftTableViewController animated:YES];
	}else if(tabControllerIndex == 3){
		[delegate.privatejetNavController popToViewController:delegate.privatejetTableViewController animated:YES];
	}*/
	[self.navigationController popToRootViewControllerAnimated:YES];
 }

/*
-(void)suggestionBox:(id)sender{
	SuggestionBoxViewController *suggestionBoxViewController=[[SuggestionBoxViewController alloc] initWithNibName:@"SuggestionBoxView" bundle:nil];
	[suggestionBoxViewController setOrderConfirmationNo:orderConfirmationNo];
	//[self.navigationController presentModalViewController:suggestionBoxViewController animated:YES];
	[self.navigationController pushViewController:suggestionBoxViewController animated:NO];
}

*/
- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}


- (void)dealloc {
	[emailText release],emailText = nil;
    [super dealloc];
}


@end
