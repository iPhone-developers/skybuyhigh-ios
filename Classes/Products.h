//
//  Products.h
//  SkyBuyHigh
//
//  Created by Thapovan InfoSystems on 02/07/09.
//  Copyright 2009 Thapovan. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface Products : NSObject

 {
	
	NSString *BRAND;
	NSString *TITLE;
	NSString *MainImageURL;	
	NSString *LongDiscription;
	NSString *Price;
	NSString *thumbnailURL;
	NSString *inStorePrice;
	NSString *size;
	NSString *color;
	NSMutableArray *prodSize;
	NSMutableArray *prodColor;
	NSString *prodId;
	NSString *prodCode;
	NSUInteger ownerId;
	NSUInteger categoryId;
	NSString *shortDiscription;
	NSMutableArray *prodImagesURL;
	NSUInteger totalImagesCount;
	BOOL isAdv; 
	NSString *travelDate; 
	BOOL isAirlineProduct; 
}

@property(nonatomic,retain) NSString *BRAND;
@property(nonatomic,retain) NSString *TITLE;
@property(nonatomic,retain) NSString *MainImageURL;
@property(nonatomic,retain) NSString *shortDiscription;
@property(nonatomic,retain) NSString *LongDiscription;
@property(nonatomic, retain) NSString *Price;
@property (nonatomic, retain) NSString *thumbnailURL;
@property (nonatomic, retain) NSString *inStorePrice;
@property (nonatomic, retain) NSString *size;
@property (nonatomic, retain) NSString *color;
@property (nonatomic, retain) NSMutableArray *prodSize;
@property (nonatomic, retain) NSMutableArray *prodColor;
@property (nonatomic, retain) NSString *prodId;
@property (nonatomic, retain) NSString *prodCode;
@property (nonatomic) NSUInteger ownerId;
@property (nonatomic) NSUInteger categoryId;
@property (nonatomic, retain) NSMutableArray *prodImagesURL;
@property (nonatomic) NSUInteger totalImagesCount;
@property (nonatomic) BOOL isAdv;
@property (nonatomic, retain) NSString *travelDate;
@property (nonatomic) BOOL isAirlineProduct;



//-(id)initWithProductBrand:(NSString *)brand prodctTitle:(NSString *)title productImage:(NSString *)image producDescription:(NSString *)desc productPrice:(NSString *)price productSize:(NSString *)Size productColor:(NSString *)color prodThumbnailURL:(NSString *)thumbnailURL;
-(id)initWithProductData:(Products *)products;
@end
