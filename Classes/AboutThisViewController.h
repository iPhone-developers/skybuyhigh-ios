//
//  AboutThisViewController.h
//  SkyBuyHigh
//
//  Created by SkyBuyHigh on 21/12/09.
//  Copyright 2009 Thapovan Info Systems. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface AboutThisViewController : UIViewController {
	UITextView *textView;
}

@property (nonatomic, retain) IBOutlet UITextView *textView;
@end
