//
//  ProductTableView.m
//  SkyBuyHigh
//
//  Created by Thapovan InfoSystems on 07/07/09.
//  Copyright 2009 Thapovan. All rights reserved.
//

#import "CheckOutProductTableView.h"
#import "SkyBuyHighAppDelegate.h"



@implementation CheckOutProductTableView


@synthesize prodBrand,prodTitle,prodImage,prodPrice,prodSize,prodColor,quantity;

-(void) setData: (Products *)prodData {
	//NSLog(@"prodData Image:%@",prodData.MainImageURL);
	prodBrand.text = prodData.BRAND;
	prodTitle.text = prodData.TITLE;
	prodImage = prodData.MainImageURL;
	prodPrice.text =prodData.Price;
	
	if([prodData.size isEqualToString:@"Size"])
		prodSize.text = @"Size       :  -";
	else				
		prodSize.text =[@"Size       : " stringByAppendingString:prodData.size];
	
	if([prodData.color isEqualToString:@"Color"])
		prodColor.text =@"Color	     :  -";
	else
		prodColor.text=[@"Color	     : " stringByAppendingString:prodData.color];
	//quantity.text=	@"Quantity :  1";
}

- (UILabel *)newLabelWithPrimaryColor:(UIColor *)primaryColor selectedColor:(UIColor *)selectedColor fontSize:(CGFloat)fontSize bold:(BOOL)bold
{
	/*
	 Create and configure a label.
	 */
	
    UIFont *font;
    if (bold) {
        font = [UIFont boldSystemFontOfSize:fontSize];
    } else {
        font = [UIFont systemFontOfSize:fontSize];
    }
	
    /*
	 Views are drawn most efficiently when they are opaque and do not have a clear background, so set these defaults.  To show selection properly, however, the views need to be transparent (so that the selection color shows through).  This is handled in setSelected:animated:.
	 */
	UILabel *newLabel = [[UILabel alloc] initWithFrame:CGRectZero];
	newLabel.backgroundColor = [UIColor clearColor];
	newLabel.opaque = YES;
	newLabel.textColor = primaryColor;
	newLabel.highlightedTextColor = selectedColor;
	newLabel.font = font;
	
	return newLabel;
}

- (id)initWithFrame:(CGRect)frame reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithFrame:frame reuseIdentifier:reuseIdentifier]) {
		//self.frame = CGRectMake(0, 0, 320, 100);
        // Initialization code
		// we need a view to place our labels on.
		UIView *myContentView = self.contentView;
		//self.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"rowbg.png"]];
		/*
		 init the title label.
		 set the text alignment to align on the left
		 add the label to the subview
		 release the memory
		 */
		
		self.prodBrand = [self newLabelWithPrimaryColor:[UIColor blackColor] selectedColor:[UIColor whiteColor] fontSize:16.0 bold:YES];
		self.prodBrand.textAlignment = UITextAlignmentLeft; // default
		[myContentView addSubview:self.prodBrand];
		[self.prodBrand release];
		
		/*
		 init the url label. (you will see a difference in the font color and size here!
		 set the text alignment to align on the left
		 add the label to the subview
		 release the memory
		 */
        self.prodTitle = [self newLabelWithPrimaryColor:[UIColor blackColor] selectedColor:[UIColor whiteColor] fontSize:13.0 bold:NO];
		self.prodTitle.textAlignment = UITextAlignmentLeft; // default
		[myContentView addSubview:self.prodTitle];
		[self.prodTitle release];
		
		self.prodPrice = [self newLabelWithPrimaryColor:[UIColor blackColor] selectedColor:[UIColor whiteColor] fontSize:12.0 bold:YES];
		self.prodPrice.textAlignment = UITextAlignmentCenter; // default
		[myContentView addSubview:self.prodPrice];
		[self.prodPrice release];
		
		self.prodSize = [self newLabelWithPrimaryColor:[UIColor blackColor] selectedColor:[UIColor whiteColor] fontSize:10.0 bold:NO];
		self.prodSize.textAlignment = UITextAlignmentLeft; // default
		[myContentView addSubview:self.prodSize];
		[self.prodSize release];	
		
		self.prodColor = [self newLabelWithPrimaryColor:[UIColor blackColor] selectedColor:[UIColor whiteColor] fontSize:10.0 bold:NO];
		self.prodColor.textAlignment = UITextAlignmentLeft; // default
		[myContentView addSubview:self.prodColor];
		[self.prodColor release];	
		
		self.quantity = [self newLabelWithPrimaryColor:[UIColor blackColor] selectedColor:[UIColor whiteColor] fontSize:10.0 bold:NO];
		self.quantity.textAlignment = UITextAlignmentLeft; // default
		[myContentView addSubview:self.quantity];
		[self.quantity release];
		
    }
    return self;
}

// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
	return (interfaceOrientation == UIDeviceOrientationPortrait);
	//return YES;
}
- (void)layoutSubviews {
	
    [super layoutSubviews];
	
	// getting the cell size
    CGRect contentRect = self.contentView.bounds;
	
	// In this example we will never be editing, but this illustrates the appropriate pattern
    if (!self.editing) {
		
		// get the X pixel spot
        CGFloat boundsX = contentRect.origin.x;
		CGRect frame;
		
        /*
		 Place the title label.
		 place the label whatever the current X is plus 10 pixels from the left
		 place the label 4 pixels from the top
		 make the label 200 pixels wide
		 make the label 20 pixels high
		 */
		frame = CGRectMake(boundsX + 85, 6, 230, 20);
		self.prodBrand.frame = frame;
		
		// place the url label
		frame = CGRectMake(boundsX + 85, 30, 230, 14);
		self.prodTitle.frame = frame;
		
		// place the url label
		frame = CGRectMake(boundsX -70 , 80, 230, 14);
		self.prodPrice.frame = frame;
		
		// place the url label
		frame = CGRectMake(boundsX + 85 , 55, 230, 14);
		self.prodColor.frame = frame;
		
		// place the url label
		frame = CGRectMake(boundsX + 85 , 72, 230, 14);
		self.prodSize.frame = frame;
		
		// place the url label
		frame = CGRectMake(boundsX + 85, 80, 230, 14);
		self.quantity.frame = frame;
		
		//place the imageView
		/*frame = CGRectMake(boundsX + 10, 4, 50, 50);
		 UIImageView *imageView = [[UIImageView alloc] initWithFrame:frame];
		 [imageView setImage:self.prodImage];*/
		
	}
}



- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
	
    [super setSelected:selected animated:animated];
	
    // Configure the view for the selected state
}


- (void)dealloc {
    [super dealloc];
}

@end
