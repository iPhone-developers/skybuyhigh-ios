//
//  CreditCardDetails.h
//  SkyBuyHigh
//
//  Created by Thapovan InfoSystems on 27/07/09.
//  Copyright 2009 Thapovan. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface CreditCardDetails : NSObject {
	
	NSString *cardHolderName;
	NSString *creditcardNo;
	NSString *creditCardType;	
	NSString *securityNo;
	NSString *expiryDate;	

	}

@property (nonatomic,retain) NSString *cardHolderName;
@property (nonatomic,retain) NSString *creditcardNo;
@property (nonatomic,retain) NSString *creditCardType;	
@property (nonatomic,retain) NSString *securityNo;
@property (nonatomic,retain) NSString *expiryDate;

@end
