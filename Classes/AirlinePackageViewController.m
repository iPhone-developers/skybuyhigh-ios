//
//  AirlinePackageViewController.m
//  SkyBuyHigh
//
//  Created by Thapovan Info Systems on 08/09/09.
//  Copyright 2009 Thapovan. All rights reserved.
//
#import "AirlinePackageViewController.h"
#import <QuartzCore/QuartzCore.h>

#import "Products.h"
#import "SkyBuyHighAppDelegate.h"
#import "ImageManipulations.h"
#import "ProductDetailsDAO.h"
#import "CheckOutProductsController.h"
#import "CustomerAirPackageController.h"

@implementation AirlinePackageViewController

@synthesize productDesc,productTitle;
@synthesize productData;
@synthesize window;
@synthesize addToBagImage;
@synthesize toolbar;
@synthesize imageController;
@synthesize personalShopper;
@synthesize addtoBagProductData;
@synthesize toolbarItems;
@synthesize airlineProductViewController;

- (void)viewDidLoad { 
	//NSLog(@"Product viewDidLoad entered");
	[super viewDidLoad];
	
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        // Custom initialization
    }
    return self;
}

-(IBAction) showPersonalShopper:(id)sender {	
	CustomerAirPackageController *customerAirPackageController = [[CustomerAirPackageController alloc] initWithNibName:@"CustomerAirPackageView" bundle:nil];
	[self.navigationController pushViewController:customerAirPackageController animated:YES];		
}



- (void)viewDidAppear:(BOOL)animated {
	if (toolbar == nil) {
		toolbar =[[UIToolbar alloc] initWithFrame:CGRectMake(0,372,320,44)];
	}
	if (toolbarItems == nil) {
		toolbarItems = [NSArray arrayWithObjects:
						[[UIBarButtonItem alloc] initWithImage:[[UIImage imageNamed:@"photos-icon.png"]retain] style:UIBarButtonItemStylePlain target:self action:@selector(showProductImage:)],
						[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:NULL action:NULL],
						[[UIBarButtonItem alloc] initWithImage:[[UIImage imageNamed:@"prev-icon.png"]retain] style:UIBarButtonItemStylePlain target:self action:@selector(previousProduct:)],
						[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:NULL action:NULL],
						[[UIBarButtonItem alloc] initWithImage:[[UIImage imageNamed:@"next-icon.png"]retain] style:UIBarButtonItemStylePlain target:self action:@selector(nextProduct:)],
						[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:NULL action:NULL],
						[[UIBarButtonItem alloc] initWithImage:[[UIImage imageNamed:@"cart-icon.png"]retain] style:UIBarButtonItemStylePlain target:self action:@selector(checkoutAction:)],
						nil];
		
		toolbar.barStyle =UIBarStyleBlack;
		toolbar.items = toolbarItems;
	}
	// Since only single instance of AirlineProductViewController is created, enable all the buttons when view appears
	// so that if it is set to disabled in one category, it is not disabled in other categories as well.
	for (UIBarButtonItem *item in toolbarItems) {
		[item setEnabled:YES];
	}
	[productTitle setText:self.productData.TITLE];
	[productDesc setText:self.productData.LongDiscription];
	
	self.navigationItem.leftBarButtonItem =	[[UIBarButtonItem alloc] initWithTitle:@"More Products"
																			 style:UIBarButtonItemStyleBordered
																			target:self
																			action:@selector(handleBack:)];
	
	if ([[(SkyBuyHighAppDelegate *)[[UIApplication sharedApplication] delegate] addToBagProducts] count] > 0) {
		[[[toolbar items] objectAtIndex:6] setTitle:[NSString stringWithFormat:@"%d",[[(SkyBuyHighAppDelegate *)[[UIApplication sharedApplication] delegate] addToBagProducts] count]]];
	}
	
	if(![(SkyBuyHighAppDelegate *)[[UIApplication sharedApplication] delegate] deviceRegStatus]){	
		[[[toolbar items] objectAtIndex:6] setEnabled:NO];
	}	
	
	id categoryViewController ;
	NSMutableArray *productInformation ;
	categoryViewController = [[self.navigationController viewControllers] objectAtIndex:0];
	productInformation = [categoryViewController productInfo];
	NSUInteger rowCount = [categoryViewController nsiCurrentRow];
	if(rowCount ==0){
		[[[toolbar items] objectAtIndex:2] setEnabled:NO];
		
	}else if((rowCount+1) == productInformation.count){
		[[[toolbar items] objectAtIndex:4] setEnabled:NO];
	}
	
	[self.view addSubview:toolbar];
	[super viewDidAppear:animated];
}



- (void) handleBack:(id)sender {
	[self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)checkoutAction:(id)sender { 
	SkyBuyHighAppDelegate *delegate=(SkyBuyHighAppDelegate *)[[UIApplication sharedApplication]delegate];
	if([delegate.addToBagProducts count] >0){
		CheckOutProductsController	*checkoutViewController = [[CheckOutProductsController alloc] init];
		[self.navigationController pushViewController:checkoutViewController animated:NO];
	}
	else {
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"SkyBuyHigh" message:@"You have no items to checkout" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
		[alert show];
		[alert release];
	}
}

- (void)viewDidDisappear:(BOOL)animated {
	
	[super viewDidDisappear:animated];
}


-(void)nextProduct:(id)sender
{
	NSMutableArray *productInfomation ;
	id categoryViewController ;
	Products *prod = nil;
	categoryViewController = [[self.navigationController viewControllers] objectAtIndex:0]; 
	productInfomation = [categoryViewController productInfo];
	NSUInteger rowCount = [categoryViewController nsiCurrentRow];
	
	if (rowCount + 1 == productInfomation.count) {
		[[[toolbar items] objectAtIndex:4] setEnabled:NO];
	}
	else if (rowCount == 0) {
		[[[toolbar items] objectAtIndex:2] setEnabled:YES];
	}	
	if ([[[toolbar items] objectAtIndex:4] isEnabled]) {
		prod= [productInfomation objectAtIndex:rowCount+1];	
		[categoryViewController setNsiCurrentRow:rowCount+1];
		
		if (prod != nil) { 		
			self.productData = prod;
			
			[self.productTitle setText:prod.TITLE];
			[self.productDesc setText:prod.LongDiscription];
		}	
		CATransition *animation = [CATransition animation];
		animation.type = kCATransitionReveal;
		animation.subtype = kCATransitionFromRight;
		animation.duration = 0.75;
		animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionDefault];
		animation.fillMode = kCAFillModeForwards;
		[[[self view] layer] addAnimation:animation forKey:kCATransition];
		
		if (![prod isAdv]) {			
			imageController = [[self.navigationController viewControllers] objectAtIndex:1];	
			imageController.currentProduct = prod;
			imageController.showAirlineDetailView = @"ProductView";
			imageController.prodView =@"next";
			[self.navigationController popViewControllerAnimated:NO];
		}
	}
	
	if((rowCount+2) == productInfomation.count){
		[[[toolbar items] objectAtIndex:4] setEnabled:NO];
	}
}

-(void)previousProduct:(id)sender
{
	id categoryViewController ;
	NSMutableArray *productInfomation ;
	Products *prod =nil;
	categoryViewController = [[self.navigationController viewControllers] objectAtIndex:0];
	productInfomation = [categoryViewController productInfo];
	NSUInteger rowCount = [categoryViewController nsiCurrentRow];
	
	if (rowCount == 0) {
		[[[toolbar items] objectAtIndex:2] setEnabled:NO];
	}		
	else if (rowCount + 1 == productInfomation.count) {
		[[[toolbar items] objectAtIndex:4] setEnabled:YES];
	}	
	if ([[[toolbar items] objectAtIndex:2] isEnabled]) { 
		prod= [productInfomation objectAtIndex:rowCount-1];	
		[categoryViewController setNsiCurrentRow:rowCount-1];
		
		if (prod != nil) { 	
			self.productData = prod;
			
			[self.productTitle setText:prod.TITLE];
			[self.productDesc setText:prod.LongDiscription];
		}
		CATransition *animation = [CATransition animation];
		animation.type = kCATransitionReveal;
		animation.subtype = kCATransitionFromLeft;
		animation.duration = 0.75;
		animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionDefault];
		animation.fillMode = kCAFillModeForwards;
		[[[self view] layer] addAnimation:animation forKey:kCATransition];
		
		if (![prod isAdv]) {			
			imageController = [[self.navigationController viewControllers] objectAtIndex:1];	
			imageController.currentProduct = prod;
			imageController.showAirlineDetailView = @"ProductView";
			imageController.prodView =@"previous";
			[self.navigationController popViewControllerAnimated:NO];
		}
	}
	
	if(rowCount == 1){
		[[[toolbar items] objectAtIndex:2] setEnabled:NO];
	}
	
}


-(void)showProductImage:(id)sender {
	id categoryViewController ;
	id selectedNavController;
	NSMutableArray *productInformation ;
	selectedNavController = self.navigationController;
	categoryViewController = [[selectedNavController viewControllers] objectAtIndex:0];
	imageController = [[selectedNavController viewControllers] objectAtIndex:1];
	
	
	imageController.hidesBottomBarWhenPushed = YES;	
	productInformation = [categoryViewController productInfo];
	NSUInteger rowCount = [categoryViewController nsiCurrentRow];
	Products *product= [productInformation objectAtIndex:rowCount];	
	imageController.currentProduct = product;
	imageController.productImage =  product.MainImageURL;
	
	[UIView beginAnimations:@"imageZoom" context:NULL];
	[UIView setAnimationDuration:0.65];
	[UIView setAnimationTransition:UIViewAnimationTransitionFlipFromRight forView:[selectedNavController view] cache:YES];
	[selectedNavController popViewControllerAnimated:NO];
	[UIView commitAnimations];
}



// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
	// return (interfaceOrientation == UIInterfaceOrientationPortrait);
	return YES;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning]; // Releases the view if it doesn't have a superview
    // Release anything that's not essential, such as cached data
}
- (void)dealloc {
	[productDesc release],productDesc=nil;
	[productTitle release],productTitle = nil;
	[productData release],productTitle = nil;
	[window release],window = nil;
	[addToBagImage release],addToBagImage = nil;
	[toolbar release],toolbar = nil;
	[imageController release],imageController = nil;
	[personalShopper release],personalShopper = nil;
	[addtoBagProductData release],addtoBagProductData = nil;
	[toolbarItems release],toolbarItems = nil;	
    [super dealloc];
}




@end
