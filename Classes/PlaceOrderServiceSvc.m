#import "PlaceOrderServiceSvc.h"
#import <libxml/xmlstring.h>
#if TARGET_OS_IPHONE
#import <CFNetwork/CFNetwork.h>
#endif
#import "ProductDetailsDAO.h"
@implementation PlaceOrderServiceSvc_placeOrder
- (id)init
{
	if((self = [super init])) {
		arg0 = 0;
		arg1 = 0;
	}
	
	return self;
}
- (void)dealloc
{
	if(arg0 != nil) [arg0 release];
	if(arg1 != nil) [arg1 release];
	
	[super dealloc];
}
- (NSString *)nsPrefix
{
	return @"PlaceOrderServiceSvc";
}
- (NSString *)serializedFormUsingElementName:(NSString *)elName
{
	NSMutableString *serializedForm = [NSMutableString string];
	[serializedForm appendFormat:@"<PlaceOrderServiceSvc:%@", elName];
	[serializedForm appendString:[self serializedAttributeString]];
	[serializedForm appendFormat:@">"];
	
	[serializedForm appendString:[self serializedElementString]];
	
	[serializedForm appendFormat:@"\n</PlaceOrderServiceSvc:%@>", elName];
	
	return serializedForm;
}
- (NSString *)serializedAttributeString
{
	NSMutableString *serializedForm = [NSMutableString string];
	
	
	
	return serializedForm;
}
- (NSString *)serializedElementString
{
	NSMutableString *serializedForm = [NSMutableString string];
	
	
	if(self.arg0 != 0) {
		[serializedForm appendFormat:@"\n%@", [self.arg0 serializedFormUsingElementName:@"arg0"]];
	}
	if(self.arg1 != 0) {
		[serializedForm appendFormat:@"\n%@", [self.arg1 serializedFormUsingElementName:@"arg1"]];
	}
	
	return serializedForm;
}
/* elements */
@synthesize arg0;
@synthesize arg1;
/* attributes */
- (NSDictionary *)attributes
{
	NSMutableDictionary *attributes = [NSMutableDictionary dictionary];
	
	return attributes;
}
+ (PlaceOrderServiceSvc_placeOrder *)deserializeNode:(xmlNodePtr)cur
{
	PlaceOrderServiceSvc_placeOrder *newObject = [[PlaceOrderServiceSvc_placeOrder new] autorelease];
	
	[newObject deserializeAttributesFromNode:cur];
	[newObject deserializeElementsFromNode:cur];
	
	return newObject;
}
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur
{
}
- (void)deserializeElementsFromNode:(xmlNodePtr)cur
{
	
	
	for( cur = cur->children ; cur != NULL ; cur = cur->next ) {
		if(cur->type == XML_ELEMENT_NODE) {
			xmlChar *elementText = xmlNodeListGetString(cur->doc, cur->children, 1);
			NSString *elementString = nil;
			
			if(elementText != NULL) {
				elementString = [NSString stringWithCString:(char*)elementText encoding:NSUTF8StringEncoding];
				xmlFree(elementText);
			}
			if(xmlStrEqual(cur->name, (const xmlChar *) "arg0")) {
				
				Class elementClass = nil;
				xmlChar *instanceType = xmlGetNsProp(cur, (const xmlChar *) "type", (const xmlChar *) "http://www.w3.org/2001/XMLSchema-instance");
				if(instanceType == NULL) {
					elementClass = [NSString  class];
				} else {
					NSString *elementTypeString = [NSString stringWithCString:(char*)instanceType encoding:NSUTF8StringEncoding];
					
					NSArray *elementTypeArray = [elementTypeString componentsSeparatedByString:@":"];
					
					NSString *elementClassString = nil;
					if([elementTypeArray count] > 1) {
						NSString *prefix = [elementTypeArray objectAtIndex:0];
						NSString *localName = [elementTypeArray objectAtIndex:1];
						
						xmlNsPtr elementNamespace = xmlSearchNs(cur->doc, cur, (const xmlChar *)[prefix UTF8String]);
						
						NSString *standardPrefix = [[USGlobals sharedInstance].wsdlStandardNamespaces objectForKey:[NSString stringWithCString:(char*)elementNamespace->href]];
						
						elementClassString = [NSString stringWithFormat:@"%@_%@", standardPrefix, localName];
					} else {
						elementClassString = [elementTypeString stringByReplacingOccurrencesOfString:@":" withString:@"_" options:0 range:NSMakeRange(0, [elementTypeString length])];
					}
					
					elementClass = NSClassFromString(elementClassString);
					xmlFree(instanceType);
				}
				
				id newChild = [elementClass deserializeNode:cur];
				
				self.arg0 = newChild;
			}
			if(xmlStrEqual(cur->name, (const xmlChar *) "arg1")) {
				
				Class elementClass = nil;
				xmlChar *instanceType = xmlGetNsProp(cur, (const xmlChar *) "type", (const xmlChar *) "http://www.w3.org/2001/XMLSchema-instance");
				if(instanceType == NULL) {
					elementClass = [NSString  class];
				} else {
					NSString *elementTypeString = [NSString stringWithCString:(char*)instanceType encoding:NSUTF8StringEncoding];
					
					NSArray *elementTypeArray = [elementTypeString componentsSeparatedByString:@":"];
					
					NSString *elementClassString = nil;
					if([elementTypeArray count] > 1) {
						NSString *prefix = [elementTypeArray objectAtIndex:0];
						NSString *localName = [elementTypeArray objectAtIndex:1];
						
						xmlNsPtr elementNamespace = xmlSearchNs(cur->doc, cur, (const xmlChar *)[prefix UTF8String]);
						
						NSString *standardPrefix = [[USGlobals sharedInstance].wsdlStandardNamespaces objectForKey:[NSString stringWithCString:(char*)elementNamespace->href]];
						
						elementClassString = [NSString stringWithFormat:@"%@_%@", standardPrefix, localName];
					} else {
						elementClassString = [elementTypeString stringByReplacingOccurrencesOfString:@":" withString:@"_" options:0 range:NSMakeRange(0, [elementTypeString length])];
					}
					
					elementClass = NSClassFromString(elementClassString);
					xmlFree(instanceType);
				}
				
				id newChild = [elementClass deserializeNode:cur];
				
				self.arg1 = newChild;
			}
		}
	}
}
@end
@implementation PlaceOrderServiceSvc_placeOrderResponse
- (id)init
{
	if((self = [super init])) {
		return_ = 0;
	}
	
	return self;
}
- (void)dealloc
{
	if(return_ != nil) [return_ release];
	
	[super dealloc];
}
- (NSString *)nsPrefix
{
	return @"PlaceOrderServiceSvc";
}
- (NSString *)serializedFormUsingElementName:(NSString *)elName
{
	NSMutableString *serializedForm = [NSMutableString string];
	[serializedForm appendFormat:@"<%@ xmlns=\"http://webservice.sbh.com/\"", elName];
	[serializedForm appendString:[self serializedAttributeString]];
	[serializedForm appendFormat:@">"];
	
	[serializedForm appendString:[self serializedElementString]];
	
	[serializedForm appendFormat:@"\n</%@>", elName];
	
	return serializedForm;
}
- (NSString *)serializedAttributeString
{
	NSMutableString *serializedForm = [NSMutableString string];
	
	
	
	return serializedForm;
}
- (NSString *)serializedElementString
{
	NSMutableString *serializedForm = [NSMutableString string];
	
	
	if(self.return_ != 0) {
		[serializedForm appendFormat:@"\n%@", [self.return_ serializedFormUsingElementName:@"return"]];
	}
	
	return serializedForm;
}
/* elements */
@synthesize return_;
/* attributes */
- (NSDictionary *)attributes
{
	NSMutableDictionary *attributes = [NSMutableDictionary dictionary];
	
	return attributes;
}
+ (PlaceOrderServiceSvc_placeOrderResponse *)deserializeNode:(xmlNodePtr)cur
{
	PlaceOrderServiceSvc_placeOrderResponse *newObject = [[PlaceOrderServiceSvc_placeOrderResponse new] autorelease];
	
	[newObject deserializeAttributesFromNode:cur];
	[newObject deserializeElementsFromNode:cur];
	
	return newObject;
}
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur
{
}
- (void)deserializeElementsFromNode:(xmlNodePtr)cur
{
	
	
	for( cur = cur->children ; cur != NULL ; cur = cur->next ) {
		if(cur->type == XML_ELEMENT_NODE) {
			xmlChar *elementText = xmlNodeListGetString(cur->doc, cur->children, 1);
			NSString *elementString = nil;
			
			if(elementText != NULL) {
				elementString = [NSString stringWithCString:(char*)elementText encoding:NSUTF8StringEncoding];
				xmlFree(elementText);
			}
			if(xmlStrEqual(cur->name, (const xmlChar *) "return")) {
				
				Class elementClass = nil;
				xmlChar *instanceType = xmlGetNsProp(cur, (const xmlChar *) "type", (const xmlChar *) "http://www.w3.org/2001/XMLSchema-instance");
				if(instanceType == NULL) {
					elementClass = [NSString  class];
				} else {
					NSString *elementTypeString = [NSString stringWithCString:(char*)instanceType encoding:NSUTF8StringEncoding];
					
					NSArray *elementTypeArray = [elementTypeString componentsSeparatedByString:@":"];
					
					NSString *elementClassString = nil;
					if([elementTypeArray count] > 1) {
						NSString *prefix = [elementTypeArray objectAtIndex:0];
						NSString *localName = [elementTypeArray objectAtIndex:1];
						
						xmlNsPtr elementNamespace = xmlSearchNs(cur->doc, cur, (const xmlChar *)[prefix UTF8String]);
						
						NSString *standardPrefix = [[USGlobals sharedInstance].wsdlStandardNamespaces objectForKey:[NSString stringWithCString:(char*)elementNamespace->href]];
						
						elementClassString = [NSString stringWithFormat:@"%@_%@", standardPrefix, localName];
					} else {
						elementClassString = [elementTypeString stringByReplacingOccurrencesOfString:@":" withString:@"_" options:0 range:NSMakeRange(0, [elementTypeString length])];
					}
					
					elementClass = NSClassFromString(elementClassString);
					xmlFree(instanceType);
				}
				
				id newChild = [elementClass deserializeNode:cur];
				
				self.return_ = newChild;
			}
		}
	}
}
@end
@implementation PlaceOrderServiceSvc
+ (void)initialize
{
	[[USGlobals sharedInstance].wsdlStandardNamespaces setObject:@"xs" forKey:@"http://www.w3.org/2001/XMLSchema"];
	[[USGlobals sharedInstance].wsdlStandardNamespaces setObject:@"PlaceOrderServiceSvc" forKey:@"http://webservice.sbh.com/"];
}
+ (PlaceOrderPortBinding *)PlaceOrderPortBinding
{	
	NSDictionary *connections = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Connections" ofType:@"plist"]];
	return [[[PlaceOrderPortBinding alloc] initWithAddress:[connections valueForKey:@"PlaceOrderURL"]] autorelease];
}
@end
@implementation PlaceOrderPortBinding
@synthesize address;
@synthesize defaultTimeout;
@synthesize logXMLInOut;
@synthesize cookies;
- (id)init
{
	if((self = [super init])) {
		address = nil;
		cookies = nil;
		defaultTimeout = 10;//seconds
		logXMLInOut = NO;
		synchronousOperationComplete = NO;
	}
	
	return self;
}
- (id)initWithAddress:(NSString *)anAddress
{
	if((self = [self init])) {
		self.address = [NSURL URLWithString:anAddress];
	}
	
	return self;
}
- (void)addCookie:(NSHTTPCookie *)toAdd
{
	if(toAdd != nil) {
		if(cookies == nil) cookies = [[NSMutableArray alloc] init];
		[cookies addObject:toAdd];
	}
}
- (PlaceOrderPortBindingResponse *)performSynchronousOperation:(PlaceOrderPortBindingOperation *)operation
{
	synchronousOperationComplete = NO;
	[operation start];
	
	// Now wait for response
	NSRunLoop *theRL = [NSRunLoop currentRunLoop];
	
	while (!synchronousOperationComplete && [theRL runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]]);
	return operation.response;
}
- (void)performAsynchronousOperation:(PlaceOrderPortBindingOperation *)operation
{
	[operation start];
}
- (void) operation:(PlaceOrderPortBindingOperation *)operation completedWithResponse:(PlaceOrderPortBindingResponse *)response
{
	synchronousOperationComplete = YES;
}
- (PlaceOrderPortBindingResponse *)placeOrderUsingParameters:(PlaceOrderServiceSvc_placeOrder *)aParameters 
{
	return [self performSynchronousOperation:[[(PlaceOrderPortBinding_placeOrder*)[PlaceOrderPortBinding_placeOrder alloc] initWithBinding:self delegate:self
																							parameters:aParameters
																							] autorelease]];
}
- (void)placeOrderAsyncUsingParameters:(PlaceOrderServiceSvc_placeOrder *)aParameters  delegate:(id<PlaceOrderPortBindingResponseDelegate>)responseDelegate
{
	[self performAsynchronousOperation: [[(PlaceOrderPortBinding_placeOrder*)[PlaceOrderPortBinding_placeOrder alloc] initWithBinding:self delegate:responseDelegate
																							 parameters:aParameters
																							 ] autorelease]];
}
- (void)sendHTTPCallUsingBody:(NSString *)outputBody soapAction:(NSString *)soapAction forOperation:(PlaceOrderPortBindingOperation *)operation
{
	NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:self.address 
																												 cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData
																										 timeoutInterval:self.defaultTimeout];
	NSData *bodyData = [outputBody dataUsingEncoding:NSUTF8StringEncoding];
	
	if(cookies != nil) {
		[request setAllHTTPHeaderFields:[NSHTTPCookie requestHeaderFieldsWithCookies:cookies]];
	}
	[request setValue:@"wsdl2objc" forHTTPHeaderField:@"User-Agent"];
	[request setValue:soapAction forHTTPHeaderField:@"SOAPAction"];
	[request setValue:@"text/xml; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
	[request setValue:[NSString stringWithFormat:@"%u", [bodyData length]] forHTTPHeaderField:@"Content-Length"];
	[request setValue:self.address.host forHTTPHeaderField:@"Host"];
	[request setHTTPMethod: @"POST"];
	// set version 1.1 - how?
	[request setHTTPBody: bodyData];
		
	if(self.logXMLInOut) {
		//NSLog(@"OutputHeaders:\n%@", [request allHTTPHeaderFields]);
		//NSLog(@"OutputBody:\n%@", outputBody);
	}
	
	NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:operation];
	
	operation.urlConnection = connection;
	[connection release];
}
- (void) dealloc
{
	[address release];
	[cookies release];
	[super dealloc];
}
@end
@implementation PlaceOrderPortBindingOperation
@synthesize binding;
@synthesize response;
@synthesize delegate;
@synthesize responseData;
@synthesize urlConnection;
- (id)initWithBinding:(PlaceOrderPortBinding *)aBinding delegate:(id<PlaceOrderPortBindingResponseDelegate>)aDelegate
{
	if (self = [super init]) {
		self.binding = aBinding;
		response = nil;
		self.delegate = aDelegate;
		self.responseData = nil;
		self.urlConnection = nil;
	}
	
	return self;
}
-(void)connection:(NSURLConnection *)connection didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge
{	
    if ([challenge previousFailureCount] == 0) {		
        NSURLCredential *newCredential;		
		NSArray *accountInfo = [[ProductDetailsDAO instance] getServerAccountInfo];
		if(accountInfo!=nil){
			newCredential=[NSURLCredential credentialWithUser:[accountInfo objectAtIndex:0]
													 password:[accountInfo objectAtIndex:1]
												  persistence:NSURLCredentialPersistenceNone];
		}
        [[challenge sender] useCredential:newCredential
               forAuthenticationChallenge:challenge];
		
		[accountInfo release];accountInfo = nil;
		
    } else {
        [[challenge sender] cancelAuthenticationChallenge:challenge];
	}
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)urlResponse
{
	NSHTTPURLResponse *httpResponse;
	if ([urlResponse isKindOfClass:[NSHTTPURLResponse class]]) {
		httpResponse = (NSHTTPURLResponse *) urlResponse;
	} else {
		httpResponse = nil;
	}
	
	if(binding.logXMLInOut) {
		//NSLog(@"ResponseStatus: %u\n", [httpResponse statusCode]);
		//NSLog(@"ResponseHeaders:\n%@", [httpResponse allHeaderFields]);
	}
	
	NSMutableArray *cookies = [[NSHTTPCookie cookiesWithResponseHeaderFields:[httpResponse allHeaderFields] forURL:binding.address] mutableCopy];
	
	binding.cookies = cookies;
	[cookies release];
	
  if ([urlResponse.MIMEType rangeOfString:@"text/xml"].length == 0) {
		NSError *error = nil;
		[connection cancel];
		if ([httpResponse statusCode] >= 400) {
			NSDictionary *userInfo = [NSDictionary dictionaryWithObject:[NSHTTPURLResponse localizedStringForStatusCode:[httpResponse statusCode]] forKey:NSLocalizedDescriptionKey];
				
			error = [NSError errorWithDomain:@"PlaceOrderPortBindingResponseHTTP" code:[httpResponse statusCode] userInfo:userInfo];
		} else {
			NSDictionary *userInfo = [NSDictionary dictionaryWithObject:
																[NSString stringWithFormat: @"Unexpected response MIME type to SOAP call:%@", urlResponse.MIMEType]
																													 forKey:NSLocalizedDescriptionKey];
			error = [NSError errorWithDomain:@"PlaceOrderPortBindingResponseHTTP" code:1 userInfo:userInfo];
		}
				
		[self connection:connection didFailWithError:error];
  }
}
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
  if (responseData == nil) {
		responseData = [data mutableCopy];
	} else {
		[responseData appendData:data];
	}
}
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
	if (binding.logXMLInOut) {
		//NSLog(@"ResponseError:\n%@", error);
	}
	response.error = error;
	[delegate operation:self completedWithResponse:response];
}
- (void)dealloc
{
	[binding release];
	[response release];
	[delegate release];
	[responseData release];
	[urlConnection release];
	
	[super dealloc];
}
@end
@implementation PlaceOrderPortBinding_placeOrder
@synthesize parameters;
- (id)initWithBinding:(PlaceOrderPortBinding *)aBinding delegate:(id<PlaceOrderPortBindingResponseDelegate>)responseDelegate
parameters:(PlaceOrderServiceSvc_placeOrder *)aParameters
{
	if((self = [super initWithBinding:aBinding delegate:responseDelegate])) {
		self.parameters = aParameters;
	}
	
	return self;
}
- (void)dealloc
{
	if(parameters != nil) [parameters release];
	
	[super dealloc];
}
- (void)main
{
	[response autorelease];
	response = [PlaceOrderPortBindingResponse new];
	
	PlaceOrderPortBinding_envelope *envelope = [PlaceOrderPortBinding_envelope sharedInstance];
	
	NSMutableDictionary *headerElements = nil;
	headerElements = [NSMutableDictionary dictionary];
	
	NSMutableDictionary *bodyElements = nil;
	bodyElements = [NSMutableDictionary dictionary];
	if(parameters != nil) [bodyElements setObject:parameters forKey:@"placeOrder"];
	
	NSString *operationXMLString = [envelope serializedFormUsingHeaderElements:headerElements bodyElements:bodyElements];
	
	[binding sendHTTPCallUsingBody:operationXMLString soapAction:@"" forOperation:self];
}
- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
	if (responseData != nil && delegate != nil)
	{
		xmlDocPtr doc;
		xmlNodePtr cur;
		
		doc = xmlParseMemory([responseData bytes], [responseData length]);
		
		if (doc == NULL) {
			NSDictionary *userInfo = [NSDictionary dictionaryWithObject:@"Errors while parsing returned XML" forKey:NSLocalizedDescriptionKey];
			
			response.error = [NSError errorWithDomain:@"PlaceOrderPortBindingResponseXML" code:1 userInfo:userInfo];
			[delegate operation:self completedWithResponse:response];
		} else {
			
			cur = xmlDocGetRootElement(doc);
			cur = cur->children;
			
			for( ; cur != NULL ; cur = cur->next) {
				if(cur->type == XML_ELEMENT_NODE) {
					
					if(xmlStrEqual(cur->name, (const xmlChar *) "Body")) {
						NSMutableArray *responseBodyParts = [NSMutableArray array];
						
						xmlNodePtr bodyNode;
						for(bodyNode=cur->children ; bodyNode != NULL ; bodyNode = bodyNode->next) {
							if(cur->type == XML_ELEMENT_NODE) {
								if(xmlStrEqual(bodyNode->name, (const xmlChar *) "placeOrderResponse")) {
									PlaceOrderServiceSvc_placeOrderResponse *bodyObject = [PlaceOrderServiceSvc_placeOrderResponse deserializeNode:bodyNode];
									//NSAssert1(bodyObject != nil, @"Errors while parsing body %s", bodyNode->name);
									if (bodyObject != nil) [responseBodyParts addObject:bodyObject];
								}
                                                                else if (xmlStrEqual(bodyNode->ns->prefix, (const xmlChar *) "soap") && 
                                                                         xmlStrEqual(bodyNode->name, (const xmlChar *) "Fault")) {
									SOAPFault *bodyObject = [SOAPFault deserializeNode:bodyNode];
									//NSAssert1(bodyObject != nil, @"Errors while parsing body %s", bodyNode->name);
									if (bodyObject != nil) [responseBodyParts addObject:bodyObject];
                                                                }
							}
						}
						
						response.bodyParts = responseBodyParts;
					}
				}
			}
			
			xmlFreeDoc(doc);
		}
		
		xmlCleanupParser();
		[delegate operation:self completedWithResponse:response];
	}
}
@end
static PlaceOrderPortBinding_envelope *PlaceOrderPortBindingSharedEnvelopeInstance = nil;
@implementation PlaceOrderPortBinding_envelope
+ (PlaceOrderPortBinding_envelope *)sharedInstance
{
	if(PlaceOrderPortBindingSharedEnvelopeInstance == nil) {
		PlaceOrderPortBindingSharedEnvelopeInstance = [PlaceOrderPortBinding_envelope new];
	}
	
	return PlaceOrderPortBindingSharedEnvelopeInstance;
}
- (NSString *)serializedFormUsingHeaderElements:(NSDictionary *)headerElements bodyElements:(NSDictionary *)bodyElements
{
	NSMutableString *serializedForm = [NSMutableString string];
	
	[serializedForm appendFormat:@"<?xml version=\"1.0\" encoding=\"utf-8\"?>\n"];
	[serializedForm appendFormat:@"<soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\"\n"];
	[serializedForm appendFormat:@"xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\" xsl:version=\"1.0\"\n"];
	[serializedForm appendFormat:@"xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\n"];
	[serializedForm appendFormat:@"xmlns:xs=\"http://www.w3.org/2001/XMLSchema\"\n"];
	[serializedForm appendFormat:@"xmlns:PlaceOrderServiceSvc=\"http://webservice.sbh.com/\"\n"];
	[serializedForm appendFormat:@">\n"];
	
	if(headerElements != nil) {
		[serializedForm appendFormat:@"<soap:Header>\n"];
		
		for(NSString *key in [headerElements allKeys]) {
			id header = [headerElements objectForKey:key];
			[serializedForm appendFormat:@"%@\n", [header serializedFormUsingElementName:key]];
		}
		
		[serializedForm appendFormat:@"</soap:Header>"];
	}
	
	if(bodyElements != nil) {
		[serializedForm appendFormat:@"<soap:Body>\n"];
		
		for(NSString *key in [bodyElements allKeys]) {
			id body = [bodyElements objectForKey:key];
			[serializedForm appendFormat:@"%@\n", [body serializedFormUsingElementName:key]];
		}
		
		[serializedForm appendFormat:@"</soap:Body>\n"];
	}
	
	[serializedForm appendFormat:@"</soap:Envelope>"];
	
	return serializedForm;
}
@end
@implementation PlaceOrderPortBindingResponse
@synthesize headers;
@synthesize bodyParts;
@synthesize error;
- (id)init
{
	if((self = [super init])) {
		headers = nil;
		bodyParts = nil;
		error = nil;
	}
	
	return self;
}
@end
