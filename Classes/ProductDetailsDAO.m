//
//  ProductDetailsDAO.m
//  SkyBuyHigh
//
//  Created by Thapovan InfoSystems on 11/08/09.
//  Copyright 2009 Thapovan. All rights reserved.
//

#import "ProductDetailsDAO.h"
#import "CustomerDetails.h"
#import "CreditCardDetails.h"
#import "OrderDetails.h"
#import "EncryptUtil.h"
#import "SkyBuyHighAppDelegate.h"

static sqlite3_stmt *insert_customer_statement = nil;
static sqlite3_stmt *insert_product_statement = nil;
static sqlite3_stmt *select_orderitemcount_statement = nil;
static sqlite3_stmt *select_orderitem_statement = nil;
static sqlite3_stmt *delete_orderdetails_statement = nil;
static sqlite3_stmt *update_cataloguedate_statement = nil;
static sqlite3_stmt *select_cataloguedate_statement = nil;
static sqlite3_stmt *select_cataloguedetails_statement = nil;
static sqlite3_stmt *select_devicekey_statement = nil;
static sqlite3_stmt *update_deliverattempt_statement = nil;
static sqlite3_stmt *insert_devicereg_statement = nil;
static sqlite3_stmt *select_devicereg_statement = nil;
static sqlite3_stmt *select_deviceregstatus_statement = nil;
static sqlite3_stmt *insert_airpackage_statement = nil;
static sqlite3_stmt *select_airpackage_statement = nil;
static sqlite3_stmt *delete_airpackagedetails_statement = nil;
static sqlite3_stmt *update_airpackage_deliverattempt_statement = nil;
static sqlite3_stmt *select_airpackageitemcount_statement = nil;
static sqlite3_stmt *update_accountInfo_statement = nil;
static sqlite3_stmt *select_accountinfo_statement = nil;
static sqlite3_stmt *update_deviceid_statement = nil;
static sqlite3_stmt *update_devicestatus_statement = nil;
static sqlite3_stmt *update_aircode_statement = nil;
static sqlite3_stmt *update_suggestions_statement = nil;

@implementation ProductDetailsDAO


static ProductDetailsDAO *singletonInstance;
+ (ProductDetailsDAO *) instance {
	
	@synchronized (self) {
		if (singletonInstance == NULL) {
			singletonInstance = [[ProductDetailsDAO alloc] init];
		}
	}
	return singletonInstance;		
}


-(void)getDatabaseConnection{
	NSArray *paths= NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentDirectory = [paths objectAtIndex:0];
	NSString *path = [documentDirectory stringByAppendingPathComponent:@"skybuyhigh.sqlite"];
	
	if(!(sqlite3_open([path	UTF8String], &database) == SQLITE_OK)){
		NSLog(@"Error connecting to the Database");	
	}	
	
}

-(void) closeDatabaseConnection {
	sqlite3_close(database);
}

- (NSInteger)insertCustomerBillingDetails:(CustomerDetails *)customerBillingDetails customerShippingDetails:(CustomerDetails *)customerShippingDetails customerCreditCatddetails:(CreditCardDetails *)creditCardDetails deviceRegDetails:(DeviceDetails *)deviceDetails orderConfirmationNumber:(NSString *)orderConfirmationNo
{
	//NSLog(@"Customer name:%@",customerBillingDetails.custFirstName);
	//[self getDatabseConnection];
	//if(insert_customer_statement == nil){
	const char *sql="INSERT INTO sbh_customer(order_confirmation_no,cust_bill_fname,cust_bill_lname,cust_bill_addr1,cust_bill_addr2,cust_bill_city ,cust_bill_state,cust_bill_zip,cust_bill_phone,cust_bill_email,cust_ship_fname,cust_ship_lname,cust_ship_addr1,cust_ship_addr2,cust_ship_city,cust_ship_state,cust_ship_zip,cust_ship_phone,cust_ship_email,cust_spl_inst,cust_personal_msg,payment_name,payment_type,payment_number,payment_code,payment_exp_date,payment_lfd,device_key,air_id,air_name,device_id,device_code,cust_newservices_feedback,cust_suggestion,sbh_create_dt,sbh_create_id,sbh_update_dt,sbh_update_id) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	
	if(sqlite3_prepare_v2(database, sql, -1, &insert_customer_statement, NULL)!=SQLITE_OK){
		NSAssert1(0,@"Error: failed to prepare statement with messgae '%s'.",sqlite3_errmsg(database));
	}
	//}
	/*NSDateFormatter *dateFormatter = [[[NSDateFormatter alloc] init] autorelease];
	[dateFormatter setDateFormat:@"MMddyy-HHmmss"];
	NSString *currentDatetime = [dateFormatter stringFromDate:[NSDate date]];	
	NSString *orderConfirmationNo =[[NSString alloc] initWithFormat:@"%@-%@",customerBillingDetails.phone,currentDatetime];*/
	creditCardDetails.expiryDate = [[NSString alloc] initWithFormat:@"%@/%@",[creditCardDetails.expiryDate substringWithRange:NSMakeRange(0,2)],[creditCardDetails.expiryDate substringWithRange:NSMakeRange(2,4)]];
	//NSLog(@"orderConfirmationNo:%@",orderConfirmationNo);
	
	
	sqlite3_bind_text(insert_customer_statement, 1, [orderConfirmationNo UTF8String], -1,SQLITE_TRANSIENT);	
	sqlite3_bind_text(insert_customer_statement, 2, [[self getEncryptedData:customerBillingDetails.custFirstName] UTF8String], -1,SQLITE_TRANSIENT);
	sqlite3_bind_text(insert_customer_statement, 3, [[self getEncryptedData:customerBillingDetails.cusLastName] UTF8String], -1,SQLITE_TRANSIENT);
	sqlite3_bind_text(insert_customer_statement, 4, [[self getEncryptedData:customerBillingDetails.address1] UTF8String], -1,SQLITE_TRANSIENT);
	sqlite3_bind_text(insert_customer_statement, 5, [[self getEncryptedData:customerBillingDetails.address2] UTF8String], -1,SQLITE_TRANSIENT);
	sqlite3_bind_text(insert_customer_statement, 6, [[self getEncryptedData:customerBillingDetails.city] UTF8String], -1,SQLITE_TRANSIENT);
	sqlite3_bind_text(insert_customer_statement, 7, [[self getEncryptedData:customerBillingDetails.state] UTF8String], -1,SQLITE_TRANSIENT);
	sqlite3_bind_text(insert_customer_statement, 8, [[self getEncryptedData:customerBillingDetails.zip] UTF8String], -1,SQLITE_TRANSIENT);
	sqlite3_bind_text(insert_customer_statement, 9, [[self getEncryptedData:customerBillingDetails.phone] UTF8String], -1,SQLITE_TRANSIENT);
	sqlite3_bind_text(insert_customer_statement, 10, [[self getEncryptedData:customerBillingDetails.email] UTF8String], -1,SQLITE_TRANSIENT);
	sqlite3_bind_text(insert_customer_statement, 11, [[self getEncryptedData:customerShippingDetails.custFirstName] UTF8String], -1,SQLITE_TRANSIENT);
	sqlite3_bind_text(insert_customer_statement, 12, [[self getEncryptedData:customerShippingDetails.cusLastName] UTF8String], -1,SQLITE_TRANSIENT);
	sqlite3_bind_text(insert_customer_statement, 13, [[self getEncryptedData:customerShippingDetails.address1] UTF8String], -1,SQLITE_TRANSIENT);
	sqlite3_bind_text(insert_customer_statement, 14, [[self getEncryptedData:customerShippingDetails.address2] UTF8String], -1,SQLITE_TRANSIENT);
	sqlite3_bind_text(insert_customer_statement, 15, [[self getEncryptedData:customerShippingDetails.city] UTF8String], -1,SQLITE_TRANSIENT);
	sqlite3_bind_text(insert_customer_statement, 16, [[self getEncryptedData:customerShippingDetails.state] UTF8String], -1,SQLITE_TRANSIENT);
	sqlite3_bind_text(insert_customer_statement, 17, [[self getEncryptedData:customerShippingDetails.zip] UTF8String], -1,SQLITE_TRANSIENT);
	sqlite3_bind_text(insert_customer_statement, 18, [[self getEncryptedData:customerShippingDetails.phone] UTF8String], -1,SQLITE_TRANSIENT);
	sqlite3_bind_text(insert_customer_statement,19,  [[self getEncryptedData:customerShippingDetails.email] UTF8String], -1,SQLITE_TRANSIENT);
	sqlite3_bind_text(insert_customer_statement, 20, [customerBillingDetails.specialIns UTF8String], -1,SQLITE_TRANSIENT);
	sqlite3_bind_text(insert_customer_statement, 21, [customerBillingDetails.personalMsg UTF8String], -1,SQLITE_TRANSIENT);
	sqlite3_bind_text(insert_customer_statement, 22, [[self getEncryptedData:creditCardDetails.cardHolderName] UTF8String], -1,SQLITE_TRANSIENT);
	sqlite3_bind_text(insert_customer_statement, 23, [[self getEncryptedData:creditCardDetails.creditCardType] UTF8String], -1,SQLITE_TRANSIENT);
	sqlite3_bind_text(insert_customer_statement, 24, [[self getEncryptedData:creditCardDetails.creditcardNo] UTF8String], -1,SQLITE_TRANSIENT);
	sqlite3_bind_text(insert_customer_statement,25,  [[self getEncryptedData:creditCardDetails.securityNo] UTF8String], -1,SQLITE_TRANSIENT);
	sqlite3_bind_text(insert_customer_statement, 26, [[self getEncryptedData:creditCardDetails.expiryDate] UTF8String], -1,SQLITE_TRANSIENT);
	sqlite3_bind_text(insert_customer_statement, 27, [[self getEncryptedData:[self getMaskCreditCardNo:creditCardDetails.creditcardNo]] UTF8String], -1,SQLITE_TRANSIENT);
	sqlite3_bind_text(insert_customer_statement, 28, [deviceDetails.productKey UTF8String], -1,SQLITE_TRANSIENT);
	sqlite3_bind_int(insert_customer_statement, 29,  [deviceDetails airCode]);
	sqlite3_bind_text(insert_customer_statement, 30, [deviceDetails.airName UTF8String], -1,SQLITE_TRANSIENT);
	sqlite3_bind_int(insert_customer_statement, 31,  [deviceDetails deviceId]) ;
	sqlite3_bind_text(insert_customer_statement, 32, [@"iPhone" UTF8String], -1,SQLITE_TRANSIENT);
	if(customerBillingDetails.feedback!=nil)
		sqlite3_bind_text(insert_customer_statement, 33, [customerBillingDetails.feedback UTF8String], -1,SQLITE_TRANSIENT);
	else
		sqlite3_bind_text(insert_customer_statement, 33, [@"" UTF8String], -1,SQLITE_TRANSIENT);
	if(customerBillingDetails.suggestions!=nil)
		sqlite3_bind_text(insert_customer_statement, 34, [customerBillingDetails.suggestions UTF8String], -1,SQLITE_TRANSIENT);
	else
		sqlite3_bind_text(insert_customer_statement, 34, [@"" UTF8String], -1,SQLITE_TRANSIENT);
	sqlite3_bind_text(insert_customer_statement, 35, [[[[NSDate date]description]substringToIndex: 19] UTF8String], -1,SQLITE_TRANSIENT);
	sqlite3_bind_text(insert_customer_statement, 36, [@"Admin" UTF8String], -1,SQLITE_TRANSIENT);
	sqlite3_bind_text(insert_customer_statement, 37, [[[[NSDate date]description]substringToIndex: 19] UTF8String], -1,SQLITE_TRANSIENT);
	sqlite3_bind_text(insert_customer_statement, 38, [@"Admin" UTF8String], -1,SQLITE_TRANSIENT);
	
	int success = sqlite3_step(insert_customer_statement);
	NSInteger customerId;
	if(success!=SQLITE_ERROR){
		customerId = sqlite3_last_insert_rowid(database);
		//NSLog(@"Inserted Customer Id : %d",customerId);
	}	
	sqlite3_reset(insert_customer_statement);	
	//sqlite3_close(database);
	//[currentDatetime release];
	//[dateFormatter release];
	//[orderConfirmationNo release];
	return customerId;
}





- (void)insertProductDetails:(Products *)products setCustomerId:(NSInteger)customerId setOrderConfirmationNo:(NSString *)orderConfirmationNo setTailNumber:(NSString *)tailNumber
{
	//[self getDatabseConnection];
	//NSLog(@"Customer id:%d",customerId);
	//if(insert_product_statement == nil){
	const char *sql="INSERT INTO sbh_orderdetail(order_confirmation_no,cust_id,order_seq_no,vend_id,catg_id,prod_code,prod_id,prod_name,prod_short_desc,prod_color,prod_size,skybuy_price,vend_price,shop_qty,travel_date,flight_no,deliver_attempt,deliver_status,sbh_create_dt,sbh_create_id,sbh_update_dt,sbh_update_id)  VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	
	if(sqlite3_prepare_v2(database, sql, -1, &insert_product_statement, NULL)!=SQLITE_OK){
		NSAssert1(0,@"Error: failed to prepare statement with messgae '%s'.",sqlite3_errmsg(database));
	}
	//}
	
	NSString *ownerId = [[NSString alloc] initWithFormat:@"%d",products.ownerId];
	NSString *categoryId = [[NSString alloc] initWithFormat:@"%d",products.categoryId];
	//NSLog(@"categoryId:%@",categoryId);

	sqlite3_bind_text(insert_product_statement, 1, [orderConfirmationNo UTF8String], -1,SQLITE_TRANSIENT);
	sqlite3_bind_int(insert_product_statement, 2, customerId);
	sqlite3_bind_int(insert_product_statement, 3, 1);
	sqlite3_bind_text(insert_product_statement, 4, [ownerId UTF8String], -1,SQLITE_TRANSIENT);
	sqlite3_bind_text(insert_product_statement, 5, [categoryId UTF8String], -1,SQLITE_TRANSIENT);	
	sqlite3_bind_text(insert_product_statement, 6, [products.prodCode UTF8String], -1,SQLITE_TRANSIENT);
	sqlite3_bind_text(insert_product_statement, 7, [products.prodId UTF8String], -1,SQLITE_TRANSIENT);
	sqlite3_bind_text(insert_product_statement, 8, [products.TITLE UTF8String], -1,SQLITE_TRANSIENT);
	sqlite3_bind_text(insert_product_statement, 9, [products.shortDiscription UTF8String], -1,SQLITE_TRANSIENT);
	
	if (![products.color isEqualToString:@"Color"])
		sqlite3_bind_text(insert_product_statement, 10, [products.color UTF8String], -1,SQLITE_TRANSIENT);
	else
		sqlite3_bind_text(insert_product_statement, 10, "", -1,SQLITE_TRANSIENT);
	if (![products.size isEqualToString:@"Size"])
		sqlite3_bind_text(insert_product_statement, 11, [products.size UTF8String], -1,SQLITE_TRANSIENT);
	else
		sqlite3_bind_text(insert_product_statement, 11, "", -1,SQLITE_TRANSIENT);
	sqlite3_bind_text(insert_product_statement, 12, [products.Price UTF8String], -1,SQLITE_TRANSIENT);
	if(![products.inStorePrice isEqualToString:@"-"])
	   sqlite3_bind_text(insert_product_statement, 13, [products.inStorePrice UTF8String], -1,SQLITE_TRANSIENT);
	else
	    sqlite3_bind_text(insert_product_statement, 13, [@"0.00" UTF8String], -1,SQLITE_TRANSIENT);
	   
	sqlite3_bind_text(insert_product_statement, 14, [@"1" UTF8String], -1,SQLITE_TRANSIENT);
	if (products.travelDate!=nil)
		sqlite3_bind_text(insert_product_statement, 15, [products.travelDate UTF8String], -1,SQLITE_TRANSIENT);
	else
		sqlite3_bind_text(insert_product_statement, 15, [@"" UTF8String], -1,SQLITE_TRANSIENT);
	//sqlite3_bind_text(insert_product_statement, 16, [@"FLIGHTNUMREPLACE" UTF8String], -1,SQLITE_TRANSIENT);
	sqlite3_bind_text(insert_product_statement, 16, [tailNumber UTF8String], -1,SQLITE_TRANSIENT);
	sqlite3_bind_text(insert_product_statement, 17, [@"R0" UTF8String], -1,SQLITE_TRANSIENT);
	sqlite3_bind_text(insert_product_statement, 18, [@"" UTF8String], -1,SQLITE_TRANSIENT);
	sqlite3_bind_text(insert_product_statement,19, [[[[NSDate date]description]substringToIndex: 19] UTF8String], -1,SQLITE_TRANSIENT);
	sqlite3_bind_text(insert_product_statement, 20, [@"Admin" UTF8String], -1,SQLITE_TRANSIENT);
	sqlite3_bind_text(insert_product_statement, 21, [[[[NSDate date]description]substringToIndex: 19] UTF8String], -1,SQLITE_TRANSIENT);
	sqlite3_bind_text(insert_product_statement, 22, [@"Admin" UTF8String], -1,SQLITE_TRANSIENT);
	
	int success = sqlite3_step(insert_product_statement);
	
	if(success!=SQLITE_ERROR){
		NSInteger rowid = sqlite3_last_insert_rowid(database);
		//NSLog(@"Inserted prod Id : %d",rowid);
	}	
	sqlite3_reset(insert_product_statement);
	
	//sqlite3_close(database);
	//[ownerId release];
	//[categoryId release];
}


- (void)insertAirPakcageDetails:(CustomerDetails *)customerAirPackageDetails  productDetails:(Products *)products deviceRegDetails:(DeviceDetails *)deviceDetails
{
	[self getDatabaseConnection];
	
	//if(insert_product_statement == nil){
	const char *sql="INSERT INTO sbh_air_order(vend_id,catg_id,prod_code,prod_id,prod_name,cust_name,cust_contact_phone,cust_contact_email,deliver_attempt,deliver_status,device_key,device_id,device_code,flight_no,order_ref_tag,air_id,air_name,email_deliver_status,sbh_create_dt,sbh_create_id ,sbh_update_dt,sbh_update_id)  VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	
	if(sqlite3_prepare_v2(database, sql, -1, &insert_airpackage_statement, NULL)!=SQLITE_OK){
		NSAssert1(0,@"Error: failed to prepare statement with messgae '%s'.",sqlite3_errmsg(database));
	}
	//}
	
	NSString *ownerId = [[NSString alloc] initWithFormat:@"%d",products.ownerId];
	NSString *categoryId = [[NSString alloc] initWithFormat:@"%d",products.categoryId];
	//NSLog(@"categoryId:%@",categoryId);
	
	sqlite3_bind_text(insert_airpackage_statement, 1, [ownerId UTF8String], -1,SQLITE_TRANSIENT);
	sqlite3_bind_text(insert_airpackage_statement, 2, [categoryId UTF8String], -1,SQLITE_TRANSIENT);
	sqlite3_bind_text(insert_airpackage_statement, 3, [products.prodCode UTF8String], -1,SQLITE_TRANSIENT);
	sqlite3_bind_text(insert_airpackage_statement, 4, [products.prodId UTF8String], -1,SQLITE_TRANSIENT);
	sqlite3_bind_text(insert_airpackage_statement, 5, [products.TITLE UTF8String], -1,SQLITE_TRANSIENT);	
	sqlite3_bind_text(insert_airpackage_statement, 6, [customerAirPackageDetails.custFirstName UTF8String], -1,SQLITE_TRANSIENT);
	sqlite3_bind_text(insert_airpackage_statement, 7, [customerAirPackageDetails.phone UTF8String], -1,SQLITE_TRANSIENT);
	sqlite3_bind_text(insert_airpackage_statement, 8, [customerAirPackageDetails.email UTF8String], -1,SQLITE_TRANSIENT);
	sqlite3_bind_text(insert_airpackage_statement, 9, [@"R0" UTF8String], -1,SQLITE_TRANSIENT);
	sqlite3_bind_text(insert_airpackage_statement, 10, [@"" UTF8String], -1,SQLITE_TRANSIENT);
	sqlite3_bind_text(insert_airpackage_statement, 11, [deviceDetails.productKey UTF8String], -1,SQLITE_TRANSIENT);
	sqlite3_bind_int(insert_airpackage_statement, 12,  deviceDetails.deviceId);
	sqlite3_bind_text(insert_airpackage_statement, 13, [deviceDetails.deviceCode UTF8String], -1,SQLITE_TRANSIENT);
	//sqlite3_bind_text(insert_airpackage_statement, 14, [@"FLIGHTNUMREPLACE" UTF8String], -1,SQLITE_TRANSIENT);
	sqlite3_bind_text(insert_airpackage_statement, 14, [customerAirPackageDetails.tailNumber UTF8String], -1,SQLITE_TRANSIENT);
	sqlite3_bind_text(insert_airpackage_statement, 15, [@"" UTF8String], -1,SQLITE_TRANSIENT);
	sqlite3_bind_int(insert_airpackage_statement, 16,  deviceDetails.airCode);
	sqlite3_bind_text(insert_airpackage_statement, 17, [deviceDetails.airName UTF8String], -1,SQLITE_TRANSIENT);
	sqlite3_bind_text(insert_airpackage_statement, 18, [@"" UTF8String], -1,SQLITE_TRANSIENT);
	sqlite3_bind_text(insert_airpackage_statement,19,  [[[[NSDate date]description]substringToIndex: 19] UTF8String], -1,SQLITE_TRANSIENT);
	sqlite3_bind_text(insert_airpackage_statement, 20, [@"Admin" UTF8String], -1,SQLITE_TRANSIENT);
	sqlite3_bind_text(insert_airpackage_statement, 21, [[[[NSDate date]description]substringToIndex: 19] UTF8String], -1,SQLITE_TRANSIENT);
	sqlite3_bind_text(insert_airpackage_statement, 22, [@"Admin" UTF8String], -1,SQLITE_TRANSIENT);
	
	int success = sqlite3_step(insert_airpackage_statement);
	
	if(success!=SQLITE_ERROR){
		NSInteger rowid = sqlite3_last_insert_rowid(database);
		//NSLog(@"Inserted airline package order Id : %d",rowid);
		//NSLog(@"Product Title  = %@",products.TITLE);
	}	
	sqlite3_reset(insert_airpackage_statement);
	
	sqlite3_close(database);
	//[ownerId release];
	//[categoryId release];
}


- (NSInteger)getOrderItemsCount 
{
	[self getDatabaseConnection];
	
	//if(select_orderitemcount_statement == nil){
	const char *sql="SELECT COUNT(DISTINCT(order_confirmation_no)) FROM sbh_orderdetail;";
	
	if(sqlite3_prepare_v2(database, sql, -1, &select_orderitemcount_statement, NULL)!=SQLITE_OK){
		NSAssert1(0,@"Error: failed to prepare statement with messgae '%s'.",sqlite3_errmsg(database));
	}
	//}
	
	NSInteger orderItemsCount;
	if(sqlite3_step(select_orderitemcount_statement) == SQLITE_ROW){
		orderItemsCount = sqlite3_column_int(select_orderitemcount_statement, 0);	
	}else{
		orderItemsCount = 0;
	}
	
	//NSLog(@"Order Items Count:%d",orderItemsCount);
	
	sqlite3_reset(select_orderitemcount_statement);
	
	sqlite3_close(database);
	
	return orderItemsCount;
	
}


- (NSInteger)getAirPackageItemsCount 
{
	[self getDatabaseConnection];
	
	//if(select_orderitemcount_statement == nil){
	const char *sql="SELECT COUNT(order_id) FROM sbh_air_order;";
	
	if(sqlite3_prepare_v2(database, sql, -1, &select_airpackageitemcount_statement, NULL)!=SQLITE_OK){
		NSAssert1(0,@"Error: failed to prepare statement with messgae '%s'.",sqlite3_errmsg(database));
	}
	//}
	
	NSInteger airPackageItemsCount;
	
	if(sqlite3_step(select_airpackageitemcount_statement) == SQLITE_ROW){
		airPackageItemsCount = sqlite3_column_int(select_airpackageitemcount_statement, 0);	
	}else{
		airPackageItemsCount = 0;
	}
	
	//NSLog(@"Airline Package Items Count:%d",airPackageItemsCount);
	
	sqlite3_reset(select_airpackageitemcount_statement);
	
	sqlite3_close(database);
	
	return airPackageItemsCount;
	
}



-(void)deleteOrderDetails:(NSInteger)customerId
{
	
	[self getDatabaseConnection];
	
	//if(select_orderitemcount_statement == nil){
	const char *sql="DELETE FROM sbh_orderdetail WHERE cust_id=?;DELETE FROM sbh_customer WHERE cust_id=?;";
	
	if(sqlite3_prepare_v2(database, sql, -1, &delete_orderdetails_statement, NULL)!=SQLITE_OK){
		NSAssert1(0,@"Error: failed to prepare statement with messgae '%s'.",sqlite3_errmsg(database));
	}
	//}
	
	sqlite3_bind_int(delete_orderdetails_statement, 1, customerId);
	sqlite3_bind_int(delete_orderdetails_statement, 2, customerId);
	int success = sqlite3_step(delete_orderdetails_statement);
	if (success != SQLITE_DONE) {
		NSAssert1(0, @"Error:'%s'.", sqlite3_errmsg(database));
	}
	
	sqlite3_reset(delete_orderdetails_statement);
	
	sqlite3_close(database);
	
}	


-(void)deleteAirPackageDetails
{
	
	[self getDatabaseConnection];
	
	//if(select_orderitemcount_statement == nil){
	const char *sql="DELETE FROM sbh_air_order;";
	
	if(sqlite3_prepare_v2(database, sql, -1, &delete_airpackagedetails_statement, NULL)!=SQLITE_OK){
		NSAssert1(0,@"Error: failed to prepare statement with messgae '%s'.",sqlite3_errmsg(database));
	}
	//}
	
	int success = sqlite3_step(delete_airpackagedetails_statement);
	if (success != SQLITE_DONE) {
		NSAssert1(0, @"Error:'%s'.", sqlite3_errmsg(database));
	}
	
	sqlite3_reset(delete_airpackagedetails_statement);
	
	sqlite3_close(database);
	
}	



-(void)deleteAllOrderDetails
{
	
	[self getDatabaseConnection];
	
	//if(select_orderitemcount_statement == nil){
	const char *sql="DELETE FROM sbh_orderdetail;DELETE FROM sbh_customer;DELETE FROM sbh_air_order;";
	
	if(sqlite3_prepare_v2(database, sql, -1, &delete_orderdetails_statement, NULL)!=SQLITE_OK){
		NSAssert1(0,@"Error: failed to prepare statement with messgae '%s'.",sqlite3_errmsg(database));
	}
	//}
	
	int success = sqlite3_step(delete_orderdetails_statement);
	if (success != SQLITE_DONE) {
		NSAssert1(0, @"Error:'%s'.", sqlite3_errmsg(database));
	}
	
	sqlite3_reset(delete_orderdetails_statement);
	
	sqlite3_close(database);
	
}	

-(void)updateSuggestions:(NSString *)suggestions feedBack:(NSString *)feedBack orderConfirmationNumber:(NSString *)orderConfirmationNo
{
	
	[self getDatabaseConnection];
	
	//if(select_orderitemcount_statement == nil){
	const char *sql="UPDATE sbh_customer SET cust_newservices_feedback=?,cust_suggestion=? WHERE order_confirmation_no=?;";
	
	if(sqlite3_prepare_v2(database, sql, -1, &update_suggestions_statement, NULL)!=SQLITE_OK){
		NSAssert1(0,@"Error: failed to prepare statement with messgae '%s'.",sqlite3_errmsg(database));
	}
	//}
	
	sqlite3_bind_text(update_suggestions_statement, 1, [feedBack UTF8String], -1,SQLITE_TRANSIENT);
	sqlite3_bind_text(update_suggestions_statement, 2, [suggestions UTF8String], -1,SQLITE_TRANSIENT);
	sqlite3_bind_text(update_suggestions_statement, 3, [orderConfirmationNo UTF8String], -1,SQLITE_TRANSIENT);
	
	int success = sqlite3_step(update_suggestions_statement);
	if (success != SQLITE_DONE) {
		NSAssert1(0, @"Error:'%s'.", sqlite3_errmsg(database));
	}
	
	sqlite3_reset(update_suggestions_statement);
	
	sqlite3_close(database);	
}	

-(void)updateCatalogueDate:(NSInteger)deviceId macAddress:(NSString *)macAddr withCatalogueDate:(NSString *)catalogueDate
{
	
	[self getDatabaseConnection];
	
	//if(select_orderitemcount_statement == nil){
	const char *sql="UPDATE sbh_catalogue_date SET catalogue_date=? WHERE device_id=?;";
	
	if(sqlite3_prepare_v2(database, sql, -1, &update_cataloguedate_statement, NULL)!=SQLITE_OK){
		NSAssert1(0,@"Error: failed to prepare statement with messgae '%s'.",sqlite3_errmsg(database));
	}
	//}
	
	sqlite3_bind_text(update_cataloguedate_statement, 1, [catalogueDate UTF8String], -1,SQLITE_TRANSIENT);
	sqlite3_bind_int(update_cataloguedate_statement, 2, deviceId);
	//sqlite3_bind_text(update_cataloguedate_statement, 3, [macAddr UTF8String], -1,SQLITE_TRANSIENT);
	
	int success = sqlite3_step(update_cataloguedate_statement);
	if (success != SQLITE_DONE) {
		NSAssert1(0, @"Error:'%s'.", sqlite3_errmsg(database));
	}
	
	sqlite3_reset(update_cataloguedate_statement);
	
	sqlite3_close(database);	
}	

-(void)updateDeviceId:(NSInteger)deviceId macAddress:(NSString *)macAddr 
{
	
	[self getDatabaseConnection];
	
	//if(select_orderitemcount_statement == nil){
	const char *sql="UPDATE sbh_catalogue_date SET device_id=?,mac_addr=?";
	
	if(sqlite3_prepare_v2(database, sql, -1, &update_deviceid_statement, NULL)!=SQLITE_OK){
		NSAssert1(0,@"Error: failed to prepare statement with messgae '%s'.",sqlite3_errmsg(database));
	}
	//}
	
	sqlite3_bind_int(update_deviceid_statement, 1, deviceId);
	sqlite3_bind_text(update_deviceid_statement, 2, [macAddr UTF8String], -1,SQLITE_TRANSIENT);
	
	int success = sqlite3_step(update_deviceid_statement);
	if (success != SQLITE_DONE) {
		NSAssert1(0, @"Error:'%s'.", sqlite3_errmsg(database));
	} else {
		//NSLog(@"Successfully updated catalogue date");
	}
	
	sqlite3_reset(update_deviceid_statement);
	
	sqlite3_close(database);	
}	

-(void)updateDeviceDetails
{
	
	[self getDatabaseConnection];
	
	//if(select_orderitemcount_statement == nil){
	const char *sql="UPDATE sbh_catalogue_date SET device_id='142', mac_addr='e0f27122c09245dcd2622fb1d7a27d8eea436889', catalogue_date='SEP 11 2009 06:45:00 AM';";
	
	if(sqlite3_prepare_v2(database, sql, -1, &update_cataloguedate_statement, NULL)!=SQLITE_OK){
		NSAssert1(0,@"Error: failed to prepare statement with messgae '%s'.",sqlite3_errmsg(database));
	}
	//}
	
		
	int success = sqlite3_step(update_cataloguedate_statement);
	if (success != SQLITE_DONE) {
		NSAssert1(0, @"Error:'%s'.", sqlite3_errmsg(database));
	}
	
	sqlite3_reset(update_cataloguedate_statement);
	
	sqlite3_close(database);	
}	


-(void)updateServerAccountInfo:(NSString *)accountInfo
{
	
	[self getDatabaseConnection];
	
	//if(select_orderitemcount_statement == nil){
	const char *sql="UPDATE sbh_server_configuration SET account_userid=?,account_passcode =?;";
	
	if(sqlite3_prepare_v2(database, sql, -1, &update_accountInfo_statement, NULL)!=SQLITE_OK){
		NSAssert1(0,@"Error: failed to prepare statement with messgae '%s'.",sqlite3_errmsg(database));
	}
	//}
	
	NSArray *accountDetails = [accountInfo componentsSeparatedByString:@"|"];
	sqlite3_bind_text(update_accountInfo_statement, 1, [[accountDetails objectAtIndex:0] UTF8String], -1,SQLITE_TRANSIENT);
	sqlite3_bind_text(update_accountInfo_statement, 2, [[accountDetails objectAtIndex:1] UTF8String], -1,SQLITE_TRANSIENT);
	
	int success = sqlite3_step(update_accountInfo_statement);
	if (success != SQLITE_DONE) {
		NSAssert1(0, @"Error:'%s'.", sqlite3_errmsg(database));
	}
	//[accountDetails release];
	sqlite3_reset(update_accountInfo_statement);
	
	sqlite3_close(database);	
}

- (NSMutableArray *)getServerAccountInfo
{
	
	[self getDatabaseConnection];
	
	//if(select_orderitemcount_statement == nil){
	const char *sql="SELECT account_userid,account_passcode FROM sbh_server_configuration;";
	
	if(sqlite3_prepare_v2(database, sql, -1, &select_accountinfo_statement, NULL)!=SQLITE_OK){
		NSAssert1(0,@"Error: failed to prepare statement with messgae '%s'.",sqlite3_errmsg(database));
	}
	//}
	
	NSMutableArray *accountDetails=nil;
	if(sqlite3_step(select_accountinfo_statement) == SQLITE_ROW){
		accountDetails = [[NSMutableArray alloc] init];
		[accountDetails addObject:[NSString stringWithUTF8String:	(char *) sqlite3_column_text(select_accountinfo_statement, 0)]];
		[accountDetails addObject:[NSString stringWithUTF8String:	(char *) sqlite3_column_text(select_accountinfo_statement, 1)]];
		
	}
    //NSLog(@"UserId:%@",[accountDetails objectAtIndex:0]);
	//NSLog(@"Password:%@",[accountDetails objectAtIndex:1]);
	
	
	sqlite3_reset(select_accountinfo_statement);
	
	sqlite3_close(database);
	
	return accountDetails;
	
}


-(void)updateDeviceStatus
{
	[self getDatabaseConnection];	
	
	//if(select_orderitemcount_statement == nil){
	const char *sql="UPDATE sbh_devicereg SET device_status='D' WHERE mac_addr=? and device_status='A';";
	
	if(sqlite3_prepare_v2(database, sql, -1, &update_devicestatus_statement, NULL)!=SQLITE_OK){
		NSAssert1(0,@"Error: failed to prepare statement with messgae '%s'.",sqlite3_errmsg(database));
	}
	//}
	
	sqlite3_bind_text(update_devicestatus_statement, 1, [[[UIDevice currentDevice]	uniqueIdentifier] UTF8String], -1,SQLITE_TRANSIENT);
	
	int success = sqlite3_step(update_devicestatus_statement);
	if (success != SQLITE_DONE) {
		NSAssert1(0, @"Error:'%s'.", sqlite3_errmsg(database));
	} else {
		//NSLog(@"Successfully updated device status");
	}
	
	sqlite3_reset(update_devicestatus_statement);
	
	sqlite3_close(database);	
}

-(void)updateAirCode:(NSString *)airCode airName:(NSString *)airName
{
	[self getDatabaseConnection];	
	
	//if(select_orderitemcount_statement == nil){
	const char *sql="UPDATE sbh_devicereg SET air_id=?,air_name=? WHERE mac_addr=? and device_status='A';";
	
	if(sqlite3_prepare_v2(database, sql, -1, &update_aircode_statement, NULL)!=SQLITE_OK){
		NSAssert1(0,@"Error: failed to prepare statement with messgae '%s'.",sqlite3_errmsg(database));
	}
	//}
	
	sqlite3_bind_int(update_aircode_statement,  1, [airCode integerValue]);
	sqlite3_bind_text(update_aircode_statement, 2, [airName UTF8String], -1,SQLITE_TRANSIENT);
	sqlite3_bind_text(update_aircode_statement, 3, [[[UIDevice currentDevice]	uniqueIdentifier] UTF8String], -1,SQLITE_TRANSIENT);
	
	int success = sqlite3_step(update_aircode_statement);
	if (success != SQLITE_DONE) {
		NSAssert1(0, @"Error:'%s'.", sqlite3_errmsg(database));
	}	
	sqlite3_reset(update_aircode_statement);
	
	sqlite3_close(database);	
}



-(void)updateDeliverAttempt:(NSInteger)customerId
{
	
	[self getDatabaseConnection];
	
	//if(select_orderitemcount_statement == nil){
	const char *sql="UPDATE sbh_orderdetail SET deliver_status = 'F', deliver_attempt = (CASE deliver_attempt WHEN 'R0' THEN 'R1' WHEN 'R1' THEN 'R2' WHEN 'R2' THEN 'R3' ELSE 'R3' END) WHERE cust_id=?;";
	
	if(sqlite3_prepare_v2(database, sql, -1, &update_deliverattempt_statement, NULL)!=SQLITE_OK){
		NSAssert1(0,@"Error: failed to prepare statement with messgae '%s'.",sqlite3_errmsg(database));
	}
	//}
	
	sqlite3_bind_int(update_deliverattempt_statement, 1, customerId);
	
	int success = sqlite3_step(update_deliverattempt_statement);
	if (success != SQLITE_DONE) {
		NSAssert1(0, @"Error:'%s'.", sqlite3_errmsg(database));
	}
	
	sqlite3_reset(update_deliverattempt_statement);
	
	sqlite3_close(database);	
}	


-(void)updateAirPackageDeliverAttempt
{
	
	[self getDatabaseConnection];
	
	//if(select_orderitemcount_statement == nil){
	const char *sql="UPDATE sbh_air_order SET deliver_status = 'F', deliver_attempt = (CASE deliver_attempt WHEN 'R0' THEN 'R1' WHEN 'R1' THEN 'R2' WHEN 'R2' THEN 'R3' ELSE 'R3' END);";
	
	if(sqlite3_prepare_v2(database, sql, -1, &update_airpackage_deliverattempt_statement, NULL)!=SQLITE_OK){
		NSAssert1(0,@"Error: failed to prepare statement with messgae '%s'.",sqlite3_errmsg(database));
	}
	//}
	
	int success = sqlite3_step(update_airpackage_deliverattempt_statement);
	if (success != SQLITE_DONE) {
		NSAssert1(0, @"Error:'%s'.", sqlite3_errmsg(database));
	}
	
	sqlite3_reset(update_airpackage_deliverattempt_statement);
	
	sqlite3_close(database);	
}	



-(NSString *)getCatalogueDate:(NSInteger)deviceId macAddress:(NSString *)macAddr
{
	
	[self getDatabaseConnection];
	
	//if(select_orderitemcount_statement == nil){
	const char *sql="SELECT catalogue_date FROM sbh_catalogue_date WHERE device_id=? and mac_addr=?;";
	
	if(sqlite3_prepare_v2(database, sql, -1, &select_cataloguedate_statement, NULL)!=SQLITE_OK){
		NSAssert1(0,@"Error: failed to prepare statement with messgae '%s'.",sqlite3_errmsg(database));
	}
	//}
	
	sqlite3_bind_int(select_cataloguedate_statement, 0, deviceId);
	sqlite3_bind_text(select_cataloguedate_statement, 1, [macAddr UTF8String], -1,SQLITE_TRANSIENT);
	
	NSString *catalogueDate = nil;
	if(sqlite3_step(select_cataloguedate_statement) == SQLITE_ROW){
		catalogueDate = [NSString stringWithUTF8String:	(char *) sqlite3_column_text(select_cataloguedate_statement, 0)];
	}else{
		catalogueDate =@"";
	}
	
	//NSLog(@"Catalogue Date:%@",catalogueDate);
	
	sqlite3_reset(select_cataloguedate_statement);
	
	sqlite3_close(database);	
	return catalogueDate;
}

-(NSString *)getCatalogueDate
{
	
	[self getDatabaseConnection];
	
	//if(select_orderitemcount_statement == nil){
	const char *sql="SELECT catalogue_date FROM sbh_catalogue_date;";
	
	if(sqlite3_prepare_v2(database, sql, -1, &select_cataloguedate_statement, NULL)!=SQLITE_OK){
		NSAssert1(0,@"Error: failed to prepare statement with messgae '%s'.",sqlite3_errmsg(database));
	}
	//}
	
	NSString *catalogueDate = nil;
	if(sqlite3_step(select_cataloguedate_statement) == SQLITE_ROW){
		catalogueDate = [NSString stringWithUTF8String:	(char *) sqlite3_column_text(select_cataloguedate_statement, 0)];
	}else{
		catalogueDate =@"";
	}
	
	//NSLog(@"Catalogue Date:%@",catalogueDate);
	
	sqlite3_reset(select_cataloguedate_statement);
	
	sqlite3_close(database);	
	return [catalogueDate stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
}


-(NSString *)getCatalogueDetailsURL
{
	
	NSString *macAddr=nil,*processorId = nil,*catalogueDate=nil,*deviceKey=nil,*catalogueDetailsURL;
	NSUInteger deviceId=0,airId=0;
	[self getDatabaseConnection];
	
	//if(select_orderitemcount_statement == nil){
	const char *sql="SELECT a.mac_addr,a.processor_id,b.catalogue_date,a.device_key,a.device_id,a.air_id FROM sbh_devicereg a, sbh_catalogue_date b ON a.device_id =b.device_id AND a.mac_addr = b.mac_addr AND a.device_status = 'A';";
	
	if(sqlite3_prepare_v2(database, sql, -1, &select_cataloguedetails_statement, NULL)!=SQLITE_OK){
		NSAssert1(0,@"Error: failed to prepare statement with messgae '%s'.",sqlite3_errmsg(database));
	}
	//}
	if(sqlite3_step(select_cataloguedetails_statement) == SQLITE_ROW){
		macAddr = [NSString stringWithUTF8String:(char *) sqlite3_column_text(select_cataloguedetails_statement, 0)];
		processorId = [NSString stringWithUTF8String:(char *) sqlite3_column_text(select_cataloguedetails_statement, 1)];
		catalogueDate = [NSString stringWithUTF8String:(char *) sqlite3_column_text(select_cataloguedetails_statement, 2)];
		deviceKey= [NSString stringWithUTF8String:(char *) sqlite3_column_text(select_cataloguedetails_statement, 3)];
		deviceId = sqlite3_column_int(select_cataloguedetails_statement,4);
		airId = sqlite3_column_int(select_cataloguedetails_statement,5);
		
		NSDictionary *connections = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Connections" ofType:@"plist"]];
		if([(SkyBuyHighAppDelegate *)[[UIApplication sharedApplication] delegate] downloadAirPackage]) {
			catalogueDetailsURL = [[NSString alloc] initWithFormat:[connections valueForKey:@"CatalogueDownloadURL"],macAddr,processorId,catalogueDate,deviceKey,deviceId,airId];
			catalogueDetailsURL = [[NSString alloc] initWithFormat:@"%@%@",catalogueDetailsURL,@"&IsAirlinePackageDownload=YES"];
			//[(SkyBuyHighAppDelegate *)[[UIApplication sharedApplication] delegate] setDownloadAirPackage:NO];
 
		}
		else
			catalogueDetailsURL = [[NSString alloc] initWithFormat:[connections valueForKey:@"CatalogueDownloadURL"],macAddr,processorId,catalogueDate,deviceKey,deviceId,airId];
	} else {
		catalogueDetailsURL=@"";
	}	
	
	NSLog(@"Catalogue Details URL :%@",catalogueDetailsURL);
	sqlite3_reset(select_cataloguedetails_statement);
	
	sqlite3_close(database);	
	return [catalogueDetailsURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
}	

-(NSString *)getValidDeviceURL
{
	
	NSString *macAddr=nil,*deviceKey=nil,*validDeviceURL;
	
	[self getDatabaseConnection];
	
	macAddr = [[UIDevice currentDevice]	uniqueIdentifier];
	//if(select_orderitemcount_statement == nil){
	const char *sql="SELECT device_key FROM sbh_devicereg WHERE mac_addr=?;";
	
	if(sqlite3_prepare_v2(database, sql, -1, &select_devicekey_statement, NULL)!=SQLITE_OK){
		NSAssert1(0,@"Error: failed to prepare statement with messgae '%s'.",sqlite3_errmsg(database));
	}
	//}
	
	sqlite3_bind_text(select_devicekey_statement, 1, [macAddr UTF8String], -1,SQLITE_TRANSIENT);
	
	NSDictionary *connections = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Connections" ofType:@"plist"]];
	//NSLog(@"valid Device URL from plist= %@",[connections valueForKey:@"ValidDeviceURL"]);
	if(sqlite3_step(select_devicekey_statement) == SQLITE_ROW){
		NSArray *accountInfo = [self getServerAccountInfo];
		deviceKey= [NSString stringWithUTF8String:(char *) sqlite3_column_text(select_devicekey_statement, 0)];
		validDeviceURL = [[NSString alloc] initWithFormat:[connections valueForKey:@"ValidDeviceURL"],[accountInfo objectAtIndex:0], [accountInfo objectAtIndex:1], macAddr,deviceKey];
	}else{
		validDeviceURL=@"";
	}	
	
	//NSLog(@"Valid DeviceURL :%@",validDeviceURL);
	sqlite3_reset(select_devicekey_statement);
	
	sqlite3_close(database);	
	return validDeviceURL;
}	



- (void)insertDeviceRegDetails:(NSArray *)deviceRegDetails  withProductKey:(NSString *)productKey 
{
	NSString *macAddr=nil;
	macAddr = [[UIDevice currentDevice]	uniqueIdentifier];
	//macAddr=@"e0f27122c09245dcd2622fb1d7a27d8eea436889";
	
	[self getDatabaseConnection];
	
	//if(insert_product_statement == nil){
	const char *sql="INSERT INTO sbh_devicereg(air_id,air_name,air_email,processor_id,device_key,mac_addr,mac_desc,active_macaddr,active_macdesc,device_type,device_model,device_serial_no,device_id,device_code,device_status,sbh_create_dt,sbh_create_id,sbh_update_dt,sbh_update_id) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	
	if(sqlite3_prepare_v2(database, sql, -1, &insert_devicereg_statement, NULL)!=SQLITE_OK){
		NSAssert1(0,@"Error: failed to prepare statement with messgae '%s'.",sqlite3_errmsg(database));
	}
	//}
	
	sqlite3_bind_int(insert_devicereg_statement,  1, [[deviceRegDetails objectAtIndex:0] integerValue]);
	sqlite3_bind_text(insert_devicereg_statement, 2,  [[deviceRegDetails objectAtIndex:1] UTF8String], -1,SQLITE_TRANSIENT);
	sqlite3_bind_text(insert_devicereg_statement, 3, [[deviceRegDetails objectAtIndex:2] UTF8String], -1,SQLITE_TRANSIENT);
	sqlite3_bind_text(insert_devicereg_statement, 4, [@"" UTF8String], -1,SQLITE_TRANSIENT);
	sqlite3_bind_text(insert_devicereg_statement, 5, [productKey UTF8String], -1,SQLITE_TRANSIENT);
	sqlite3_bind_text(insert_devicereg_statement, 6, [macAddr UTF8String], -1,SQLITE_TRANSIENT);
	sqlite3_bind_text(insert_devicereg_statement, 7, [@"" UTF8String], -1,SQLITE_TRANSIENT);
	sqlite3_bind_text(insert_devicereg_statement, 8, [macAddr UTF8String], -1,SQLITE_TRANSIENT);
	sqlite3_bind_text(insert_devicereg_statement, 9, [@"" UTF8String], -1,SQLITE_TRANSIENT);
	sqlite3_bind_text(insert_devicereg_statement, 10, [[deviceRegDetails objectAtIndex:5] UTF8String], -1,SQLITE_TRANSIENT);
	sqlite3_bind_text(insert_devicereg_statement, 11, [[deviceRegDetails objectAtIndex:4] UTF8String], -1,SQLITE_TRANSIENT);
	sqlite3_bind_text(insert_devicereg_statement, 12, [[deviceRegDetails objectAtIndex:6] UTF8String], -1,SQLITE_TRANSIENT);
	sqlite3_bind_int(insert_devicereg_statement,  13, [[deviceRegDetails objectAtIndex:3] integerValue]);
	sqlite3_bind_text(insert_devicereg_statement, 14, [[deviceRegDetails objectAtIndex:7] UTF8String], -1,SQLITE_TRANSIENT);
	sqlite3_bind_text(insert_devicereg_statement, 15, [@"A" UTF8String], -1,SQLITE_TRANSIENT);	
	sqlite3_bind_text(insert_devicereg_statement, 16, [[[[NSDate date]description]substringToIndex: 19] UTF8String], -1,SQLITE_TRANSIENT);
	sqlite3_bind_text(insert_devicereg_statement, 17, [@"Admin" UTF8String], -1,SQLITE_TRANSIENT);
	sqlite3_bind_text(insert_devicereg_statement, 18, [[[[NSDate date]description]substringToIndex: 19] UTF8String], -1,SQLITE_TRANSIENT);
	sqlite3_bind_text(insert_devicereg_statement, 19, [@"Admin" UTF8String], -1,SQLITE_TRANSIENT);
	
	int success = sqlite3_step(insert_devicereg_statement);
	
	if(success!=SQLITE_ERROR){
		NSInteger rowid = sqlite3_last_insert_rowid(database);
		//NSLog(@"Inserted device Id : %d",rowid);
	}	
	sqlite3_reset(insert_devicereg_statement);
	
	sqlite3_close(database);
	
}


-(DeviceDetails *)getDeviceRegDetails
{
	NSString *macAddr=nil;
	macAddr = [[UIDevice currentDevice]	uniqueIdentifier];
	//macAddr=@"e0f27122c09245dcd2622fb1d7a27d8eea436889";
	
	[self getDatabaseConnection];
	
	//if(select_orderitemcount_statement == nil){
	const char *sql="SELECT air_id,air_name,air_email,processor_id,device_key,mac_addr,active_macaddr,device_type,device_model,device_serial_no,device_id,device_code,device_status FROM sbh_devicereg WHERE mac_addr=? and device_status='A';";
	
	if(sqlite3_prepare_v2(database, sql, -1, &select_devicereg_statement, NULL)!=SQLITE_OK){
		NSAssert1(0,@"Error: failed to prepare statement with messgae '%s'.",sqlite3_errmsg(database));
	}
	//}
	
	
	sqlite3_bind_text(select_devicereg_statement, 1, [macAddr UTF8String], -1,SQLITE_TRANSIENT);
	
	DeviceDetails *deviceDetails = nil;
	if(sqlite3_step(select_devicereg_statement) == SQLITE_ROW){
		deviceDetails = [[DeviceDetails alloc] init];
		deviceDetails.airCode = sqlite3_column_int(select_devicereg_statement, 0);
		deviceDetails.airName = [NSString stringWithUTF8String:	(char *) sqlite3_column_text(select_devicereg_statement, 1)];
		deviceDetails.airEmail = [NSString stringWithUTF8String:(char *) sqlite3_column_text(select_devicereg_statement,2)];
		deviceDetails.processorId = [NSString stringWithUTF8String:	(char *) sqlite3_column_text(select_devicereg_statement, 3)];
		deviceDetails.productKey = [NSString stringWithUTF8String:(char *) sqlite3_column_text(select_devicereg_statement, 4)];
		deviceDetails.macAddr = [NSString stringWithUTF8String:	(char *) sqlite3_column_text(select_devicereg_statement,5)];
		deviceDetails.activeMacAddr = [NSString stringWithUTF8String:(char *) sqlite3_column_text(select_devicereg_statement, 6)];
		deviceDetails.deviceType = [NSString stringWithUTF8String:(char *) sqlite3_column_text(select_devicereg_statement, 7)];
		deviceDetails.deviceModel = [NSString stringWithUTF8String:(char *) sqlite3_column_text(select_devicereg_statement, 8)];
		deviceDetails.deviceSerialNo = [NSString stringWithUTF8String:	(char *) sqlite3_column_text(select_devicereg_statement, 9)];
		deviceDetails.deviceId = sqlite3_column_int(select_devicereg_statement, 10);
		deviceDetails.deviceCode = [NSString stringWithUTF8String:(char *) sqlite3_column_text(select_devicereg_statement, 11)];
		deviceDetails.deviceStatus = [NSString stringWithUTF8String:(char *) sqlite3_column_text(select_devicereg_statement, 12)];
	}
	
	//NSLog(@"Device type from DB = %@",deviceDetails.deviceType);
	sqlite3_reset(select_devicereg_statement);
	
	sqlite3_close(database);	
	return deviceDetails;
}	


- (BOOL)getDeviceRegStatus 
{
	
	NSString *macAddr=nil;
	macAddr = [[UIDevice currentDevice]	uniqueIdentifier];
	//macAddr=@"e0f27122c09245dcd2622fb1d7a27d8eea436889";
	BOOL isDeviceRegistered=NO;
	
	[self getDatabaseConnection];
	
	//if(select_orderitemcount_statement == nil){
	const char *sql="SELECT COUNT(*)  FROM sbh_devicereg where mac_addr=? and device_status='A';";
	
	if(sqlite3_prepare_v2(database, sql, -1, &select_deviceregstatus_statement, NULL)!=SQLITE_OK){
		NSAssert1(0,@"Error: failed to prepare statement with messgae '%s'.",sqlite3_errmsg(database));
	}
	//}
	
	sqlite3_bind_text(select_deviceregstatus_statement, 1, [macAddr UTF8String], -1,SQLITE_TRANSIENT);
	
	NSInteger deviceCount=0;
	if(sqlite3_step(select_deviceregstatus_statement) == SQLITE_ROW){
		deviceCount = sqlite3_column_int(select_deviceregstatus_statement, 0);	
		if(deviceCount>0){
			isDeviceRegistered = YES;
		}	
	}
	
	//NSLog(@"Device Registered:%d",isDeviceRegistered);
	
	sqlite3_reset(select_deviceregstatus_statement);
	
	sqlite3_close(database);
	
	return isDeviceRegistered;
	
}



- (NSMutableArray *)generateOrderItemDetailsXML 
{
	NSUInteger customerId =0,previousCustomerId=-1,airId=0,deviceId=0,ownerId=0,categoryId=0,orderRefId=0,totalOrdersItems=0;//customerIdCount=0;
	
	NSString *orderConfirmationNo=nil,*custBillFirstName=nil,*custBillLastName=nil,*custBillAddr1=nil,*custBillAddr2=nil,*custBillCity=nil,*custBillState=nil,*custBillZip=nil,*custBillPhone=nil;
	NSString *custBillEmail=nil,*custShippingFirstName=nil,*custShippingLastName=nil,*custShippingAddr1=nil,*custShippingAddr2=nil,*custShippingCity=nil,*custShippingState=nil,*custShippingZip=nil;
	NSString *custShippingPhone=nil,*custShippingEmail=nil,*custSplInst=nil,*custPersonalMsg=nil,*paymentName=nil,*paymentType=nil,*paymentNumber=nil,*paymentCode=nil,*paymentExpDate=nil,*paymentLfd=nil;
	NSString *devicekey=nil,*airName=nil,*deviceCode=nil,*custFeedBack=nil,*custSuggestion=nil,*orderSeqNo=nil,*prodCode=nil,*prodId=nil,*prodName=nil;
	NSString *prodShortDesc=nil,*prodColor=nil,*prodSize=nil,*skybuyPrice=nil,*vendPrice=nil,*qty=nil,*travelDate=nil,*flightNO=nil,*deliverAttempt=nil,*deliverStatus=nil;
	NSString *orderCreatedDt=nil,*orderCreatedId=nil,*macAddr=nil,*processorId=nil;
	NSString *productdetailsXmlNode =nil,*customerdetailsXmlNode =nil;
	NSMutableArray *placeOrderDetails =nil;
	OrderDetails *orderDetails =nil;
	double dSkybuyTotalPrice=0.0;
	
	NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
	[formatter setNumberStyle:NSNumberFormatterCurrencyStyle];
	[formatter setMaximumIntegerDigits:19];
	[formatter setMaximumFractionDigits:2];
	
	[self getDatabaseConnection];
	
	//if(select_orderitem_statement == nil){
	const char *sql="SELECT a.cust_id,a.order_confirmation_no,a.cust_bill_fname,a.cust_bill_lname,a.cust_bill_addr1,a.cust_bill_addr2,a.cust_bill_city ,a.cust_bill_state,a.cust_bill_zip,a.cust_bill_phone,a.cust_bill_email,a.cust_ship_fname,a.cust_ship_lname,a.cust_ship_addr1,a.cust_ship_addr2,a.cust_ship_city,a.cust_ship_state,a.cust_ship_zip,a.cust_ship_phone,a.cust_ship_email,a.cust_spl_inst,a.cust_personal_msg,a.payment_name,a.payment_type,a.payment_number,a.payment_code,a.payment_exp_date,a.payment_lfd,a.device_key,a.air_id,a.air_name,a.device_id,a.device_code,a.cust_newservices_feedback,a.cust_suggestion,b.order_ref_id,b.order_seq_no,b.vend_id,b.catg_id,b.prod_code,b.prod_id,b.prod_name,b.prod_short_desc,b.prod_color,b.prod_size,b.skybuy_price,b.vend_price,b.shop_qty,b.travel_date,b.flight_no,b.deliver_attempt,b.deliver_status,b.sbh_create_dt,b.sbh_create_id,c.mac_addr,c.processor_id FROM sbh_customer a, sbh_orderdetail b,sbh_devicereg c ON a.cust_id=b.cust_id  and a.device_id=c.device_id ORDER BY a.cust_id,b.order_ref_id;";	
	if(sqlite3_prepare_v2(database, sql, -1, &select_orderitem_statement, NULL)!=SQLITE_OK){
		NSAssert1(0,@"Error: failed to prepare statement with messgae '%s'.",sqlite3_errmsg(database));
	}
	//}
	
	while(sqlite3_step(select_orderitem_statement) == SQLITE_ROW){	
		if(placeOrderDetails == nil)
			placeOrderDetails = [[NSMutableArray alloc] init];
		
		customerId = sqlite3_column_int(select_orderitem_statement, 0);	
		if(customerId != previousCustomerId){			
			
			//customerIdCount=customerIdCount+1;
			
			if(customerdetailsXmlNode!=nil && productdetailsXmlNode!=nil){
				orderDetails = [[OrderDetails alloc] init];
				
				//NSNumber *number = [[NSDecimalNumber alloc] initWithDecimal:[[NSDecimalNumber numberWithDouble:dSkybuyTotalPrice] decimalValue]];
				customerdetailsXmlNode = [[NSString alloc] initWithFormat:@"%@&lt;Items&gt;%@&lt;/Items&gt;&lt;TotalOrderItems&gt;%d&lt;/TotalOrderItems&gt;&lt;TotalPayment&gt;%f&lt;/TotalPayment&gt;&lt;/OrderRecordSet&gt;",customerdetailsXmlNode,productdetailsXmlNode,totalOrdersItems,dSkybuyTotalPrice];
				//NSLog(@"OrderXmlNode:%@",customerdetailsXmlNode);
				[orderDetails setCustomerId:previousCustomerId];
				[orderDetails setOrderDetailsXml:customerdetailsXmlNode];
				
				[placeOrderDetails addObject:orderDetails];
				[orderDetails release];
				orderDetails=nil;
				
		    }
			previousCustomerId = customerId;
			totalOrdersItems=1;
			dSkybuyTotalPrice=0.0;
			customerdetailsXmlNode=nil;
			productdetailsXmlNode =nil;
			
			orderConfirmationNo = [self getEncryptedData:[NSString stringWithUTF8String:(char *) sqlite3_column_text(select_orderitem_statement, 1)]];
			custBillFirstName = [NSString stringWithUTF8String:(char *) sqlite3_column_text(select_orderitem_statement, 2)];
			custBillLastName = [NSString stringWithUTF8String:(char *) sqlite3_column_text(select_orderitem_statement, 3)];
			custBillAddr1 = [NSString stringWithUTF8String:(char *) sqlite3_column_text(select_orderitem_statement, 4)];
			custBillAddr2 = [NSString stringWithUTF8String:(char *) sqlite3_column_text(select_orderitem_statement, 5)];
			custBillCity = [NSString stringWithUTF8String:(char *) sqlite3_column_text(select_orderitem_statement, 6)];
			custBillState = [NSString stringWithUTF8String:(char *) sqlite3_column_text(select_orderitem_statement, 7)];
			custBillZip = [NSString stringWithUTF8String:(char *) sqlite3_column_text(select_orderitem_statement, 8)];
			custBillPhone = [NSString stringWithUTF8String:(char *) sqlite3_column_text(select_orderitem_statement, 9)];
			custBillEmail = [NSString stringWithUTF8String:(char *) sqlite3_column_text(select_orderitem_statement, 10)];			
			
			
			custShippingFirstName = [NSString stringWithUTF8String:(char *) sqlite3_column_text(select_orderitem_statement, 11)];
			custShippingLastName = [NSString stringWithUTF8String:(char *) sqlite3_column_text(select_orderitem_statement, 12)];
			custShippingAddr1 = [NSString stringWithUTF8String:(char *) sqlite3_column_text(select_orderitem_statement, 13)];
			custShippingAddr2 = [NSString stringWithUTF8String:(char *) sqlite3_column_text(select_orderitem_statement, 14)];
			custShippingCity = [NSString stringWithUTF8String:(char *) sqlite3_column_text(select_orderitem_statement, 15)];
			custShippingState = [NSString stringWithUTF8String:(char *) sqlite3_column_text(select_orderitem_statement, 16)];
			custShippingZip = [NSString stringWithUTF8String:(char *) sqlite3_column_text(select_orderitem_statement, 17)];
			custShippingPhone = [NSString stringWithUTF8String:(char *) sqlite3_column_text(select_orderitem_statement, 18)];
			custShippingEmail = [NSString stringWithUTF8String:(char *) sqlite3_column_text(select_orderitem_statement, 19)];
			custSplInst = [NSString stringWithUTF8String:(char *) sqlite3_column_text(select_orderitem_statement, 20)];
			custPersonalMsg = [NSString stringWithUTF8String:(char *) sqlite3_column_text(select_orderitem_statement, 21)];
			
			
			paymentName = [NSString stringWithUTF8String:(char *) sqlite3_column_text(select_orderitem_statement, 22)];
			paymentType = [NSString stringWithUTF8String:(char *) sqlite3_column_text(select_orderitem_statement, 23)];
			paymentNumber = [NSString stringWithUTF8String:(char *) sqlite3_column_text(select_orderitem_statement, 24)];
			paymentCode = [NSString stringWithUTF8String:(char *) sqlite3_column_text(select_orderitem_statement, 25)];
			paymentExpDate = [NSString stringWithUTF8String:(char *) sqlite3_column_text(select_orderitem_statement, 26)];
			paymentLfd = [NSString stringWithUTF8String:(char *) sqlite3_column_text(select_orderitem_statement, 27)];
			
			
			devicekey = [NSString stringWithUTF8String:(char *) sqlite3_column_text(select_orderitem_statement, 28)];
			airId = sqlite3_column_int(select_orderitem_statement, 29);
			airName = [NSString stringWithUTF8String:(char *) sqlite3_column_text(select_orderitem_statement, 30)];
			deviceId = sqlite3_column_int(select_orderitem_statement, 31);
			deviceCode = [NSString stringWithUTF8String:(char *) sqlite3_column_text(select_orderitem_statement, 32)];
		
			custFeedBack = [NSString stringWithUTF8String:(char *) sqlite3_column_text(select_orderitem_statement, 33)];
			custSuggestion = [NSString stringWithUTF8String:(char *) sqlite3_column_text(select_orderitem_statement, 34)];
			orderRefId = sqlite3_column_int(select_orderitem_statement, 35);
			flightNO = [NSString stringWithUTF8String:(char *) sqlite3_column_text(select_orderitem_statement, 49)];
			deliverAttempt = [NSString stringWithUTF8String:(char *) sqlite3_column_text(select_orderitem_statement, 50)];
			deliverStatus = [NSString stringWithUTF8String:(char *) sqlite3_column_text(select_orderitem_statement, 51)];
			
			
			orderSeqNo = [NSString stringWithUTF8String:(char *) sqlite3_column_text(select_orderitem_statement, 36)];
			ownerId = sqlite3_column_int(select_orderitem_statement, 37);
			categoryId = sqlite3_column_int(select_orderitem_statement, 38);			
			prodCode = [NSString stringWithUTF8String:(char *) sqlite3_column_text(select_orderitem_statement, 39)];
			prodId = [NSString stringWithUTF8String:(char *) sqlite3_column_text(select_orderitem_statement, 40)];
			prodName = [NSString stringWithUTF8String:(char *) sqlite3_column_text(select_orderitem_statement, 41)];
			prodName=[prodName stringByReplacingOccurrencesOfString:@"&" withString:@"and"]; 
			prodShortDesc = [NSString stringWithUTF8String:(char *) sqlite3_column_text(select_orderitem_statement, 42)];
			prodColor = [NSString stringWithUTF8String:(char *) sqlite3_column_text(select_orderitem_statement, 43)];
			prodSize = [NSString stringWithUTF8String:(char *) sqlite3_column_text(select_orderitem_statement, 44)];
			skybuyPrice = [NSString stringWithUTF8String:(char *) sqlite3_column_text(select_orderitem_statement, 45)];
			vendPrice = [NSString stringWithUTF8String:(char *) sqlite3_column_text(select_orderitem_statement, 46)];			
			qty = [NSString stringWithUTF8String:(char *) sqlite3_column_text(select_orderitem_statement, 47)];
			travelDate = [NSString stringWithUTF8String:(char *) sqlite3_column_text(select_orderitem_statement, 48)];			
			orderCreatedDt = [NSString stringWithUTF8String:(char *) sqlite3_column_text(select_orderitem_statement, 52)];
			orderCreatedId = [NSString stringWithUTF8String:(char *) sqlite3_column_text(select_orderitem_statement, 53)];
			macAddr = [NSString stringWithUTF8String:(char *) sqlite3_column_text(select_orderitem_statement, 54)];
			processorId = [NSString stringWithUTF8String:(char *) sqlite3_column_text(select_orderitem_statement, 55)];
			
			if([deliverAttempt isEqualToString:@"R3"])
			{
				paymentNumber=@"";
				paymentCode=@"";
			}	
			
			vendPrice=[vendPrice stringByReplacingOccurrencesOfString:@"$" withString:@""];
			skybuyPrice=[skybuyPrice stringByReplacingOccurrencesOfString:@"$" withString:@""];
			
			
			//NSString *tempString = @"&lt;?xml version=1.0"" encoding=""UTF-8""?&gt;";
			customerdetailsXmlNode = [[NSString alloc] initWithFormat:@"&lt;OrderRecordSet originatedFrom=\"%@\"&gt;&lt;CheckSumRefID&gt;6&lt;/CheckSumRefID&gt;&lt;CustTransId&gt;&lt;![CDATA[%@]]&gt;&lt;/CustTransId&gt;&lt;CustBillFname&gt;&lt;![CDATA[%@]]&gt;&lt;/CustBillFname&gt;&lt;CustBillLname&gt;&lt;![CDATA[%@]]&gt;&lt;/CustBillLname&gt;&lt;CustBillAddr1&gt;&lt;![CDATA[%@]]&gt;&lt;/CustBillAddr1&gt;&lt;CustBillAddr2&gt;&lt;![CDATA[%@]]&gt;&lt;/CustBillAddr2&gt;&lt;CustBillCity&gt;&lt;![CDATA[%@]]&gt;&lt;/CustBillCity&gt;&lt;CustBillState&gt;&lt;![CDATA[%@]]&gt;&lt;/CustBillState&gt;&lt;CustBillZip&gt;&lt;![CDATA[%@]]&gt;&lt;/CustBillZip&gt;&lt;CustBillPhone&gt;&lt;![CDATA[%@]]&gt;&lt;/CustBillPhone&gt;&lt;CustBillEmail&gt;&lt;![CDATA[%@]]&gt;&lt;/CustBillEmail&gt;&lt;CustShipFname&gt;&lt;![CDATA[%@]]&gt;&lt;/CustShipFname&gt;&lt;CustShipLname&gt;&lt;![CDATA[%@]]&gt;&lt;/CustShipLname&gt;&lt;CustShipAddr1&gt;&lt;![CDATA[%@]]&gt;&lt;/CustShipAddr1&gt;&lt;CustShipAddr2&gt;&lt;![CDATA[%@]]&gt;&lt;/CustShipAddr2&gt;&lt;CustShipCity&gt;&lt;![CDATA[%@]]&gt;&lt;/CustShipCity&gt;&lt;CustShipState&gt;&lt;![CDATA[%@]]&gt;&lt;/CustShipState&gt;&lt;CustShipZip&gt;&lt;![CDATA[%@]]&gt;&lt;/CustShipZip&gt;&lt;CustShipPhone&gt;&lt;![CDATA[%@]]&gt;&lt;/CustShipPhone&gt;&lt;CustShipEmail&gt;&lt;![CDATA[%@]]&gt;&lt;/CustShipEmail&gt;&lt;SpecialInstruction&gt;&lt;![CDATA[%@]]&gt;&lt;/SpecialInstruction&gt;&lt;PersonalInformation&gt;&lt;![CDATA[%@]]&gt;&lt;/PersonalInformation&gt;&lt;PaymentName&gt;&lt;![CDATA[%@]]&gt;&lt;/PaymentName&gt;&lt;PaymentType&gt;&lt;![CDATA[%@]]&gt;&lt;/PaymentType&gt;&lt;PaymentNumber&gt;&lt;![CDATA[%@]]&gt;&lt;/PaymentNumber&gt;&lt;PaymentCode&gt;&lt;![CDATA[%@]]&gt;&lt;/PaymentCode&gt;&lt;LFD&gt;&lt;![CDATA[%@]]&gt;&lt;/LFD&gt;&lt;PaymentExpDt&gt;&lt;![CDATA[%@]]&gt;&lt;/PaymentExpDt&gt;&lt;NewServicesFeedback&gt;&lt;![CDATA[%@]]&gt;&lt;/NewServicesFeedback&gt;&lt;Suggestion&gt;&lt;![CDATA[%@]]&gt;&lt;/Suggestion&gt;&lt;AirId&gt;%d&lt;/AirId&gt;&lt;AirName&gt;%@&lt;/AirName&gt;&lt;DeviceId&gt;%d&lt;/DeviceId&gt;&lt;DeviceCode&gt;%@&lt;/DeviceCode&gt;&lt;DeviceKey&gt;%@&lt;/DeviceKey&gt;&lt;OrderDt&gt;%@&lt;/OrderDt&gt;&lt;CreatedBy&gt;%@&lt;/CreatedBy&gt;&gt;&lt;FlightNo&gt;%@&lt;/FlightNo&gt;&lt;Attempt&gt;%@&lt;/Attempt&gt;&lt;DeliveryStatus&gt;%@&lt;/DeliveryStatus&gt;&lt;OrderRefId&gt;%d&lt;/OrderRefId&gt;&lt;MacAddr&gt;%@&lt;/MacAddr&gt;&lt;ProcessorId&gt;%@&lt;/ProcessorId&gt;",[[UIDevice currentDevice] model],orderConfirmationNo,custBillFirstName,custBillLastName,custBillAddr1,custBillAddr2,custBillCity,custBillState,custBillZip,custBillPhone,custBillEmail,
									  custShippingFirstName,custShippingLastName,custShippingAddr1,custShippingAddr2,custShippingCity,custShippingState,custShippingZip,custShippingPhone,custShippingEmail,custSplInst,custPersonalMsg,
									  paymentName,paymentType,paymentNumber,paymentCode,paymentLfd,paymentExpDate,
									  custFeedBack,custSuggestion,airId,airName,deviceId,deviceCode,devicekey,orderCreatedDt,orderCreatedId,flightNO,deliverAttempt,deliverStatus,orderRefId,macAddr,processorId];
			
			productdetailsXmlNode =[[NSString alloc] initWithFormat:@"&lt;Item&gt;&lt;OrderSeqNo&gt;%@&lt;/OrderSeqNo&gt;&lt;OwnerId&gt;%d&lt;/OwnerId&gt;&lt;CateId&gt;%d&lt;/CateId&gt;&lt;ProdCode&gt;%@&lt;/ProdCode&gt;&lt;ProdId&gt;%@&lt;/ProdId&gt;&lt;ProdName&gt;%@&lt;/ProdName&gt;&lt;ProdColor&gt;%@&lt;/ProdColor&gt;&lt;ProdSize&gt;%@&lt;/ProdSize&gt;&lt;SbhPrice&gt;%@&lt;/SbhPrice&gt;&lt;VendPrice&gt;%@&lt;/VendPrice&gt;&lt;Qty&gt;%@&lt;/Qty&gt;&lt;TravelDate&gt;%@&lt;/TravelDate&gt;&lt;/Item&gt;",orderSeqNo,ownerId,categoryId,prodCode,prodId,prodName,prodColor,prodSize,skybuyPrice,vendPrice,qty,travelDate];
			//NSLog(@"customerdetailsXmlNode:%@",customerdetailsXmlNode);
			//NSLog(@"productdetailsXmlNode:%@",productdetailsXmlNode);
			double sbhPrice = [skybuyPrice doubleValue];		 
			dSkybuyTotalPrice =dSkybuyTotalPrice+sbhPrice;
			//NSLog(@"SkyBuy total price = %f",dSkybuyTotalPrice);
			
		}else{
			
			totalOrdersItems=totalOrdersItems+1;	
			orderSeqNo = [NSString stringWithUTF8String:(char *) sqlite3_column_text(select_orderitem_statement, 36)];
			ownerId = sqlite3_column_int(select_orderitem_statement, 37);
			categoryId = sqlite3_column_int(select_orderitem_statement, 38);			
			prodCode = [NSString stringWithUTF8String:(char *) sqlite3_column_text(select_orderitem_statement, 39)];
			prodId = [NSString stringWithUTF8String:(char *) sqlite3_column_text(select_orderitem_statement, 40)];
			prodName = [NSString stringWithUTF8String:(char *) sqlite3_column_text(select_orderitem_statement, 41)];
			prodShortDesc = [NSString stringWithUTF8String:(char *) sqlite3_column_text(select_orderitem_statement, 42)];
			prodColor = [NSString stringWithUTF8String:(char *) sqlite3_column_text(select_orderitem_statement, 43)];
			prodSize = [NSString stringWithUTF8String:(char *) sqlite3_column_text(select_orderitem_statement, 44)];
			skybuyPrice = [NSString stringWithUTF8String:(char *) sqlite3_column_text(select_orderitem_statement, 45)];
			vendPrice = [NSString stringWithUTF8String:(char *) sqlite3_column_text(select_orderitem_statement, 46)];			
			qty = [NSString stringWithUTF8String:(char *) sqlite3_column_text(select_orderitem_statement, 47)];
			travelDate = [NSString stringWithUTF8String:(char *) sqlite3_column_text(select_orderitem_statement, 48)];			
			
			vendPrice=[vendPrice stringByReplacingOccurrencesOfString:@"$" withString:@""];
			skybuyPrice=[skybuyPrice stringByReplacingOccurrencesOfString:@"$" withString:@""];
			double sbhPrice = [skybuyPrice doubleValue];		 
			dSkybuyTotalPrice =dSkybuyTotalPrice+sbhPrice;			
			//NSLog(@"SkyBuy total price = %f",dSkybuyTotalPrice);
			productdetailsXmlNode =[[NSString alloc] initWithFormat:@"%@&lt;Item&gt;&lt;OrderSeqNo&gt;%@&lt;/OrderSeqNo&gt;&lt;OwnerId&gt;%d&lt;/OwnerId&gt;&lt;CateId&gt;%d&lt;/CateId&gt;&lt;ProdCode&gt;%@&lt;/ProdCode&gt;&lt;ProdId&gt;%@&lt;/ProdId&gt;&lt;ProdName&gt;%@&lt;/ProdName&gt;&lt;ProdColor&gt;%@&lt;/ProdColor&gt;&lt;ProdSize&gt;%@&lt;/ProdSize&gt;&lt;SbhPrice&gt;%@&lt;/SbhPrice&gt;&lt;VendPrice&gt;%@&lt;/VendPrice&gt;&lt;Qty&gt;%@&lt;/Qty&gt;&lt;TravelDate&gt;%@&lt;/TravelDate&gt;&lt;/Item&gt;",productdetailsXmlNode,orderSeqNo,ownerId,categoryId,prodCode,prodId,prodName,prodColor,prodSize,skybuyPrice,vendPrice,qty,travelDate];
			//NSLog(@"totalOrdersItems:%d",totalOrdersItems);	
		}
		
		
	}	
	if(customerdetailsXmlNode!=nil && productdetailsXmlNode!=nil){
		orderDetails = [[OrderDetails alloc] init];
		//NSNumber *number = [[NSDecimalNumber alloc] initWithDecimal:[[NSDecimalNumber numberWithDouble:dSkybuyTotalPrice] decimalValue]];
		//NSLog(@"SkyBuy total price = %f",dSkybuyTotalPrice);
		customerdetailsXmlNode = [[NSString alloc] initWithFormat:@"%@&lt;Items&gt;%@&lt;/Items&gt;&lt;TotalOrderItems&gt;%d&lt;/TotalOrderItems&gt;&lt;TotalPayment&gt;%f&lt;/TotalPayment&gt;&lt;/OrderRecordSet&gt;",customerdetailsXmlNode,productdetailsXmlNode,totalOrdersItems,dSkybuyTotalPrice];
		//NSLog(@"OrderXmlNode:%@",customerdetailsXmlNode);
		[orderDetails setCustomerId:previousCustomerId];
		[orderDetails setOrderDetailsXml:customerdetailsXmlNode];
		
		[placeOrderDetails addObject:orderDetails];
		[orderDetails release];
		orderDetails=nil;
		
	}
	
	sqlite3_reset(select_orderitem_statement);
	
	sqlite3_close(database);
	
	orderConfirmationNo=nil,custBillFirstName=nil,custBillLastName=nil,custBillAddr1=nil,custBillAddr2=nil,custBillCity=nil,custBillState=nil,custBillZip=nil,custBillPhone=nil;
	custBillEmail=nil,custShippingFirstName=nil,custShippingLastName=nil,custShippingAddr1=nil,custShippingAddr2=nil,custShippingCity=nil,custShippingState=nil,custShippingZip=nil;
	custShippingPhone=nil,custShippingEmail=nil,custSplInst=nil,custPersonalMsg=nil,paymentName=nil,paymentType=nil,paymentNumber=nil,paymentCode=nil,paymentExpDate=nil,paymentLfd=nil;
	devicekey=nil,airName=nil,deviceCode=nil,custFeedBack=nil,custSuggestion=nil,orderSeqNo=nil,prodCode=nil,prodId=nil,prodName=nil;
	prodShortDesc=nil,prodColor=nil,prodSize=nil,skybuyPrice=nil,vendPrice=nil,qty=nil,travelDate=nil,flightNO=nil,deliverAttempt=nil,deliverStatus=nil;
	orderCreatedDt=nil,orderCreatedId=nil,macAddr=nil,processorId=nil;
	productdetailsXmlNode =nil,customerdetailsXmlNode =nil;
	//NSLog(@"XML - \n %@", placeOrderDetails);
	return placeOrderDetails;
	
}


- (NSString *)generateAirlinePackageDetailsXML 
{
	NSUInteger airId=0,deviceId=0,ownerId=0,categoryId=0;	
	NSString *custName=nil,*custPhone=nil,*custEmail=nil,*devicekey=nil,*airName=nil,*deviceCode=nil,*prodCode=nil,*prodId=nil,*prodName=nil,*flightNO=nil;
	NSString *orderCreatedDt=nil,*macAddr=nil;
	NSString *airPackageDetailsXmlNode =nil;		
	
	[self getDatabaseConnection];
	macAddr = [[UIDevice currentDevice]	uniqueIdentifier];
	//macAddr=@"e0f27122c09245dcd2622fb1d7a27d8eea436880";
	
	//if(select_orderitem_statement == nil){
	const char *sql="SELECT vend_id,catg_id,prod_code,prod_id,prod_name,cust_name,cust_contact_phone,cust_contact_email,device_key,device_id,device_code,flight_no,air_id,air_name,sbh_create_dt FROM sbh_air_order order by order_id";	
	if(sqlite3_prepare_v2(database, sql, -1, &select_airpackage_statement, NULL)!=SQLITE_OK){
		NSAssert1(0,@"Error: failed to prepare statement with messgae '%s'.",sqlite3_errmsg(database));
	}
	//}
	
	while(sqlite3_step(select_airpackage_statement) == SQLITE_ROW){	
		
		ownerId = sqlite3_column_int(select_airpackage_statement, 0);
		categoryId = sqlite3_column_int(select_airpackage_statement, 1);			
		prodCode = [NSString stringWithUTF8String:(char *) sqlite3_column_text(select_airpackage_statement, 2)];
		prodId = [NSString stringWithUTF8String:(char *) sqlite3_column_text(select_airpackage_statement, 3)];
		prodName = [NSString stringWithUTF8String:(char *) sqlite3_column_text(select_airpackage_statement, 4)];
		custName = [NSString stringWithUTF8String:(char *) sqlite3_column_text(select_airpackage_statement, 5)];
		custPhone = [NSString stringWithUTF8String:(char *) sqlite3_column_text(select_airpackage_statement, 6)];
		custEmail = [NSString stringWithUTF8String:(char *) sqlite3_column_text(select_airpackage_statement, 7)];
		
		devicekey = [NSString stringWithUTF8String:(char *) sqlite3_column_text(select_airpackage_statement, 8)];
		deviceId = sqlite3_column_int(select_airpackage_statement, 9);
		deviceCode = [NSString stringWithUTF8String:(char *) sqlite3_column_text(select_airpackage_statement, 10)];
		flightNO = [NSString stringWithUTF8String:(char *) sqlite3_column_text(select_airpackage_statement, 11)];
		airId = sqlite3_column_int(select_airpackage_statement, 12);
		airName = [NSString stringWithUTF8String:(char *) sqlite3_column_text(select_airpackage_statement, 13)];
		orderCreatedDt = [NSString stringWithUTF8String:(char *) sqlite3_column_text(select_airpackage_statement, 14)];
		if(airPackageDetailsXmlNode!=nil){
			airPackageDetailsXmlNode =[[NSString alloc] initWithFormat:@"%@&lt;Item id=\"4\"&gt;&lt;OwnerId&gt;%d&lt;/OwnerId&gt;&lt;CateId&gt;%d&lt;/CateId&gt;&lt;ProdCode&gt;%@&lt;/ProdCode&gt;&lt;ProdId&gt;%@&lt;/ProdId&gt;&lt;ProdName&gt;%@&lt;/ProdName&gt;&lt;CustomerName&gt;%@&lt;/CustomerName&gt;&lt;ContactPhone&gt;%@&lt;/ContactPhone&gt;&lt;ContactEmail&gt;%@&lt;/ContactEmail&gt;&lt;DeviceKey&gt;%@&lt;/DeviceKey&gt;&lt;DeviceId&gt;%d&lt;/DeviceId&gt;&lt;DeviceCode&gt;%@&lt;/DeviceCode&gt;&lt;AirId&gt;%d&lt;/AirId&gt;&lt;AirName&gt;%@&lt;/AirName&gt;&lt;FlightNo&gt;%@&lt;/FlightNo&gt;&lt;OrderDt&gt;%@&lt;/OrderDt&gt;&lt;MacAddr&gt;%@&lt;/MacAddr&gt;&lt;ProcessorId&gt;&lt;/ProcessorId&gt;&lt;/Item&gt;",airPackageDetailsXmlNode,ownerId,categoryId,prodCode,prodId,prodName,custName,custPhone,custEmail,devicekey,deviceId,deviceCode,airId,airName,flightNO,orderCreatedDt,macAddr];
		}else{
			airPackageDetailsXmlNode =[[NSString alloc] initWithFormat:@"&lt;Item id=\"4\"&gt;&lt;OwnerId&gt;%d&lt;/OwnerId&gt;&lt;CateId&gt;%d&lt;/CateId&gt;&lt;ProdCode&gt;%@&lt;/ProdCode&gt;&lt;ProdId&gt;%@&lt;/ProdId&gt;&lt;ProdName&gt;%@&lt;/ProdName&gt;&lt;CustomerName&gt;%@&lt;/CustomerName&gt;&lt;ContactPhone&gt;%@&lt;/ContactPhone&gt;&lt;ContactEmail&gt;%@&lt;/ContactEmail&gt;&lt;DeviceKey&gt;%@&lt;/DeviceKey&gt;&lt;DeviceId&gt;%d&lt;/DeviceId&gt;&lt;DeviceCode&gt;%@&lt;/DeviceCode&gt;&lt;AirId&gt;%d&lt;/AirId&gt;&lt;AirName&gt;%@&lt;/AirName&gt;&lt;FlightNo&gt;%@&lt;/FlightNo&gt;&lt;OrderDt&gt;%@&lt;/OrderDt&gt;&lt;MacAddr&gt;%@&lt;/MacAddr&gt;&lt;ProcessorId&gt;&lt;/ProcessorId&gt;&lt;/Item&gt;",ownerId,categoryId,prodCode,prodId,prodName,custName,custPhone,custEmail,devicekey,deviceId,deviceCode,airId,airName,flightNO,orderCreatedDt,macAddr];
			
		}
		//NSLog(@"productdetailsXmlNode:%@",productdetailsXmlNode);		
		
	}		
	
	if(airPackageDetailsXmlNode!=nil){
		airPackageDetailsXmlNode =[[NSString alloc] initWithFormat:@"&lt;AirlineRecordSet originatedFrom=\"%@\"&gt;%@&lt;/AirlineRecordSet&gt;",[[UIDevice currentDevice] model],airPackageDetailsXmlNode];
	}
	//NSLog(@"AirPackage DetailsXmlNode:%@",airPackageDetailsXmlNode);
	sqlite3_reset(select_airpackage_statement);
	
	sqlite3_close(database);
	
	airId=0,deviceId=0,ownerId=0,categoryId=0;	
	custName=nil,custPhone=nil,custEmail=nil,devicekey=nil,airName=nil,deviceCode=nil,prodCode=nil,prodId=nil,prodName=nil,flightNO=nil;
	orderCreatedDt=nil,macAddr=nil;
	
	return airPackageDetailsXmlNode;
	
}

- (NSString *)getEncryptedData:(NSString *)value
{
	// //NSLog(@"//NSLog");
	const size_t BUFFER_SIZE = [value length];
	const size_t CIPHER_BUFFER_SIZE = 256;
	size_t cipherBufferSize = CIPHER_BUFFER_SIZE;
	const uint8_t *plainBuffer = (uint8_t *)calloc(BUFFER_SIZE, sizeof(uint8_t));
	uint8_t *cipherBuffer = (uint8_t *)malloc(CIPHER_BUFFER_SIZE * sizeof(uint8_t));
	const char *inputString = [value cStringUsingEncoding:NSUTF8StringEncoding];
	strncpy( (char *)plainBuffer, inputString, strlen(inputString));
	memset((void *)cipherBuffer, 0x0, cipherBufferSize);
	
	OSStatus sanityCheck = SecKeyEncrypt([EncryptUtil getPublicKeyRef], kSecPaddingPKCS1, plainBuffer, strlen(inputString), &cipherBuffer[0], &cipherBufferSize);
	//NSLog(@"encryption result code: %d ", sanityCheck);
	//NSLog(@"encrypted text: %s", cipherBuffer);
	//NSLog(@"Encrypted text size = %d", strlen((char *)cipherBuffer));
	NSData *encryptedData = [NSData dataWithBytes:(const void *)cipherBuffer length:(NSUInteger)cipherBufferSize];
	//NSLog(@"Encrypted data to base 64 \n%@\n Size = %d",[EncryptUtil base64EncodeData:encryptedData],[[EncryptUtil base64EncodeData:encryptedData] length]);
	
	return [EncryptUtil base64EncodeData:encryptedData];	
}

+ (NSString *)getEData:(NSString *)value
{
	// //NSLog(@"//NSLog");
	const size_t BUFFER_SIZE = [value length];
	const size_t CIPHER_BUFFER_SIZE = 256;
	size_t cipherBufferSize = CIPHER_BUFFER_SIZE;
	const uint8_t *plainBuffer = (uint8_t *)calloc(BUFFER_SIZE, sizeof(uint8_t));
	uint8_t *cipherBuffer = (uint8_t *)malloc(CIPHER_BUFFER_SIZE * sizeof(uint8_t));
	const char *inputString = [value cStringUsingEncoding:NSUTF8StringEncoding];
	strncpy( (char *)plainBuffer, inputString, strlen(inputString));
	memset((void *)cipherBuffer, 0x0, cipherBufferSize);
	
	OSStatus sanityCheck = SecKeyEncrypt([EncryptUtil getPublicKeyRef], kSecPaddingPKCS1, plainBuffer, strlen(inputString), &cipherBuffer[0], &cipherBufferSize);
	//NSLog(@"encryption result code: %d ", sanityCheck);
	//NSLog(@"encrypted text: %s", cipherBuffer);
	//NSLog(@"Encrypted text size = %d", strlen((char *)cipherBuffer));
	NSData *encryptedData = [NSData dataWithBytes:(const void *)cipherBuffer length:(NSUInteger)cipherBufferSize];
	//NSLog(@"Encrypted data to base 64 \n%@\n Size = %d",[EncryptUtil base64EncodeData:encryptedData],[[EncryptUtil base64EncodeData:encryptedData] length]);
	
	return [EncryptUtil base64EncodeData:encryptedData];	
}


- (NSString *)getMaskCreditCardNo:(NSString *)creditCardNO
{
	//NSLog(@"getMaskCreditCardNo entered");
	NSString *nsMaskCCNo=@"";
	if([creditCardNO length]>4){
		
		for(int index=1;index<=[creditCardNO length]-4;index++){
			nsMaskCCNo = [[NSString alloc] initWithFormat:@"%@X",nsMaskCCNo];
			if(index%4==0 && index!=[creditCardNO length]-4){
				//nsMask=nsMask+"-";
				nsMaskCCNo = [[NSString alloc] initWithFormat:@"%@-",nsMaskCCNo];
			}
		}
		nsMaskCCNo = [[NSString alloc] initWithFormat:@"%@-%@",nsMaskCCNo,[creditCardNO substringWithRange:NSMakeRange([creditCardNO length]-4, 4)]];
		
	}else{
		nsMaskCCNo = creditCardNO;
	}
	
	//NSLog(@"Mask CC No:%@",nsMaskCCNo);
	
	return nsMaskCCNo;
}

@end


