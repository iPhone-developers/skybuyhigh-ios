//
//  CreditCardDetailsController.m
//  SkyBuyHigh
//
//  Created by Thapovan InfoSystems on 30/07/09.
//  Copyright 2009 Thapovan. All rights reserved.
//

#import "CustomerAirPackageController.h"
#import "SkyBuyHighAppDelegate.h"
#import "AirPackageConfirmationViewController.h"
#import "AirPackageTailNumberViewController.h"


@implementation CustomerAirPackageController
@synthesize webView;

- (void)viewWillAppear:(BOOL)animated {
	/*NSString *htmlFile = [[NSBundle mainBundle] pathForResource:@"CustomerAirPackage" ofType:@"html"];
	NSData *htmlData = [NSData dataWithContentsOfFile:htmlFile];
	webView.delegate = self;
	[webView loadData:htmlData MIMEType:@"text/html" textEncodingName:@"UTF-8" baseURL:[NSURL URLWithString:@""]];*/
	[super viewWillAppear:animated];
}


- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidLoad {
	NSString *htmlFile = [[NSBundle mainBundle] pathForResource:@"CustomerAirPackage" ofType:@"html"];
	NSData *htmlData = [NSData dataWithContentsOfFile:htmlFile];
	webView.delegate = self;
	[webView loadData:htmlData MIMEType:@"text/html" textEncodingName:@"UTF-8" baseURL:[NSURL URLWithString:@""]];
	[super viewDidLoad];
}
	

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request  navigationType:(UIWebViewNavigationType)navigationType {
	SkyBuyHighAppDelegate *delegate=(SkyBuyHighAppDelegate *)[[UIApplication sharedApplication]delegate];
	CustomerDetails *customerAirPackageDetails =(CustomerDetails *) delegate.customerAirPackageDetails;
	NSString *requestString = [[request URL] absoluteString];	
	NSArray *components = [requestString componentsSeparatedByString:@":"];
	
	if ([components count] > 1 && 
		[(NSString *)[components objectAtIndex:0] isEqualToString:@"http"]) {
		if([(NSString *)[components objectAtIndex:2] isEqualToString:@"Name"]) 
		{			
			[customerAirPackageDetails setCustFirstName:[[components objectAtIndex:3] stringByReplacingOccurrencesOfString:@"%20" withString:@" "]];
		}
		if([(NSString *)[components objectAtIndex:4] isEqualToString:@"Phone"]) 
		{		
			[customerAirPackageDetails setPhone:[[components objectAtIndex:5] stringByReplacingOccurrencesOfString:@"%20" withString:@" "]];
		}
		if([(NSString *)[components objectAtIndex:6] isEqualToString:@"Email"]) 
		{		
			[customerAirPackageDetails setEmail:[[components objectAtIndex:7] stringByReplacingOccurrencesOfString:@"%20" withString:@" "]];
		}
	}
	AirPackageTailNumberViewController *airPackageTailNumberViewController = [[AirPackageTailNumberViewController alloc] initWithNibName:@"AirlinePackageTailNumberView" bundle:nil];
	[self.navigationController pushViewController:airPackageTailNumberViewController animated:NO];
	
//	AirPackageConfirmationViewController *airPackageConfirmationViewController = [[AirPackageConfirmationViewController alloc] initWithNibName:@"AirPackageConfirmationView" bundle:nil];
//	[self.navigationController pushViewController:airPackageConfirmationViewController animated:NO];
	
	return YES; // Return YES to make sure regular navigation works as expected.
}


-(void)webViewDidFinishLoad:(UIWebView *)webView1

{	
	SkyBuyHighAppDelegate *delegate=(SkyBuyHighAppDelegate *)[[UIApplication sharedApplication]delegate];
	CustomerDetails *customerAirPackageDetails =(CustomerDetails *) delegate.customerAirPackageDetails;
	NSString *name= nil;
	NSString *phone = nil;
	NSString *email = nil;
	
	if([customerAirPackageDetails custFirstName] != nil) {
		name = [NSString stringWithFormat:@"%@",[customerAirPackageDetails custFirstName]];
	}
	else { 
		name =[[NSString alloc] initWithString:@""];
	}
	
	if([customerAirPackageDetails phone] != nil) {
		phone = [NSString stringWithFormat:@"%@",[customerAirPackageDetails phone]];
	}
	else {
		phone =[[NSString alloc] initWithString:@""];
	}
	if([customerAirPackageDetails email] != nil) {
		email = [NSString stringWithFormat:@"%@",[customerAirPackageDetails email]];
	}
	else {
		email =[[NSString alloc] initWithString:@""];
	}
	NSString *jsCommand = [NSString stringWithFormat:@"setFormValues('%@','%@','%@')",name,phone,email];
    [webView stringByEvaluatingJavaScriptFromString:jsCommand];
}

- (void)dealloc {
	[webView release], webView = nil;
    [super dealloc];
}


@end
