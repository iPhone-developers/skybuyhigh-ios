//
//  BoxView.m
//  SimpleBox
//
//  Created by Bill Dudney on 3/19/08.
//  Copyright 2008 Gala Factory. All rights reserved.
//

#import "BoxView.h"
#import <QuartzCore/QuartzCore.h>

@implementation BoxView

//START:code.BoxView.actionForLayer:forKey:
- (id<CAAction>)actionForLayer:(CALayer *)layer forKey:(NSString *)key {
	NSLog(@"actionForLayer entered")	;

  id<CAAction> animation = nil;
  if([key isEqualToString:@"position"]) {
    animation = [CABasicAnimation animation];
    ((CABasicAnimation*)animation).duration = 1.0f;
  } else {
    animation = [super actionForLayer:layer forKey:key];
  }
  return animation;
}
//END:code.BoxView.actionForLayer:forKey:

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
	NSLog(@" BoxView touchesEnded")	;

  UITouch *touch = touches.anyObject;
  CGPoint newCenter = [touch locationInView:self.superview];
  [UIView beginAnimations:@"center" context:nil];
  self.center = newCenter;
	
  [UIView commitAnimations];
}

@end

