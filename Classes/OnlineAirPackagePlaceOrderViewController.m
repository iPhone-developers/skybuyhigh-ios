//
//  OnlineAirPackagePlaceOrderViewController.m
//  SkyBuyHigh
//
//  Created by Thapovan Info Systems on 14/12/09.
//  Copyright 2009 Thapovan. All rights reserved.
//

#import "OnlineAirPackagePlaceOrderViewController.h"

#import "ProductDetailsDAO.h"
#import "SkyBuyHighAppDelegate.h"
#import "AirPackageConfirmationViewController.h"
#import "PlaceOrder.h"
#import "OrderDetails.h"


@implementation OnlineAirPackagePlaceOrderViewController
@synthesize activityIndicator,placeOrderStatus;

/*
 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
 - (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
 if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
 // Custom initialization
 }
 return self;
 }
 */

- (void)viewWillAppear:(BOOL)animated {
	[placeOrderStatus setText:@"Please wait while your order is being placed..."];
	
}	

- (void)viewDidAppear:(BOOL)animated{
	
	[self performSelector:@selector(placeOrder) withObject:nil afterDelay:0.0f];
	
}


-(void)placeOrder{
	
	/*SkyBuyHighAppDelegate *delegate=(SkyBuyHighAppDelegate *)[[UIApplication sharedApplication]delegate];
	 NSMutableArray *productInfomation=nil ;
	 id categoryViewController=nil;
	 categoryViewController = [[self.navigationController viewControllers] objectAtIndex:0];
	 productInfomation = [categoryViewController productInfo];
	 NSUInteger rowCount = [categoryViewController nsiCurrentRow];
	 Products *prod= [productInfomation objectAtIndex:rowCount];
	 DeviceDetails *deviceDetails=[[ProductDetailsDAO instance] getDeviceRegDetails];
	 [[ProductDetailsDAO instance] insertAirPakcageDetails:delegate.customerAirPackageDetails productDetails:prod deviceRegDetails:deviceDetails];
	 */
	if (!([[Reachability sharedReachability] internetConnectionStatus] == NotReachable)) {
		
		[activityIndicator startAnimating];
		[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
		NSString *orderResponse=nil;
		
		if([[ProductDetailsDAO instance] getAirPackageItemsCount]>0){
			//Place airline packages
			NSString *airlinePackageDetailsXML = [[ProductDetailsDAO instance] generateAirlinePackageDetailsXML];
			if(airlinePackageDetailsXML!=nil){
				orderResponse = [PlaceOrder sendRequestWithParams:airlinePackageDetailsXML];	
				if (orderResponse != nil && [orderResponse isEqualToString:@"OPS"]) {
					//NSLog(@"Airline Package has been placed successfully");
				}
			}
		}	
		if([[ProductDetailsDAO instance] getOrderItemsCount]>0){
			//Place product details
			NSMutableArray *ordersArray = [[ProductDetailsDAO instance] generateOrderItemDetailsXML];
			for (int i = 0; i < [ordersArray count]; i++) {
				NSString *orderXML=[[ordersArray objectAtIndex:i] orderDetailsXml];		
				orderResponse = [PlaceOrder sendRequestWithParams:orderXML customerId:[[ordersArray objectAtIndex:i] customerId]];
				if(orderResponse!=nil && [orderResponse isEqualToString:@"OPS"]) {
					//placeOrderSuccessCount=placeOrderSuccessCount+1;
					//NSLog(@"Order has been successfully placed.");
				}				
			}
		}
		[activityIndicator stopAnimating];
		[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
	}
	//delegate.customerAirPackageDetails = [[CustomerDetails alloc] init];	
	//[UIApplication sharedApplication].applicationIconBadgeNumber = [[ProductDetailsDAO instance] getOrderItemsCount]+[[ProductDetailsDAO instance] getAirPackageItemsCount];
	
	AirPackageConfirmationViewController *airPackageConfirmationViewController = [[AirPackageConfirmationViewController alloc] initWithNibName:@"AirPackageConfirmationView" bundle:nil];
	[self.navigationController pushViewController:airPackageConfirmationViewController animated:NO];
	
}


/*
 // Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
 - (void)viewDidLoad {
 [super viewDidLoad];
 }
 */

/*
 // Override to allow orientations other than the default portrait orientation.
 - (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
 // Return YES for supported orientations
 return (interfaceOrientation == UIInterfaceOrientationPortrait);
 }
 */

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [super dealloc];
}


@end
