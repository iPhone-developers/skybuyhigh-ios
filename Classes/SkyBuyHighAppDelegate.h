//
//  SkyBuyHighAppDelegate.h
//  SkyBuyHigh
//
//  Created by Thapovan InfoSystems on 02/07/09.
//  Copyright Thapovan 2009. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MenCategoryViewController.h"
#import "WomenCategoryViewController.h"
#import "GiftCategoryViewController.h"
#import "PrivateJetCategoryViewController.h"
#import "CreditCardDetails.h"
#import "CustomerDetails.h"
#import "Reachability.h"
#import "DeviceRegViewController.h"
#import "TwitterViewController.h"
#import "AboutUsViewController.h"
#import "ContactViewController.h"
#import "SupportViewController.h"
#import "AboutThisViewController.h"
#import "PartnersViewController.h"

@interface SkyBuyHighAppDelegate : NSObject <UIApplicationDelegate> {
    UIWindow *window;
	
	UITabBarController *tabBarController;
	MenCategoryViewController *menTableViewController;
	WomenCategoryViewController *womenTableViewController;
	GiftCategoryViewController *giftTableViewController;
	PrivateJetCategoryViewController *privatejetTableViewController;
	DeviceRegViewController *deviceRegViewController;
	TwitterViewController *twitterViewController;
	AboutUsViewController *aboutUsViewController;
	ContactViewController *contactViewController;
	SupportViewController *supportViewController;
	AboutThisViewController *aboutThisViewController;
	PartnersViewController *partnersViewController;
	
	UINavigationController *menNavController;
	UINavigationController *womenNavController;
	UINavigationController *giftNavController;
	UINavigationController *privatejetNavController;
	UINavigationController *deviceRegNavController;
	UINavigationController *twitterNavController;
	UINavigationController *aboutUsNavController;
	UINavigationController *contactNavController;
	UINavigationController *supportNavController;
	UINavigationController *aboutThisNavController;
	UINavigationController *partnersNavController;
	
	NSUInteger nsiCategoryId;
	NSMutableArray *addToBagProducts;
	CreditCardDetails *creditCardDetails;
	CustomerDetails *customerBillingDetails;
	CustomerDetails *customerShippingDetails;
	BOOL shouldShowTabBar;
	BOOL shouldShowDownloadCatalogue;
	BOOL callDownloadCatalogue;
	CustomerDetails *customerAirPackageDetails;
	BOOL deviceRegStatus;
	BOOL downloadAirPackage;
}

@property (nonatomic, retain) IBOutlet UIWindow *window;

@property (nonatomic, retain) UITabBarController *tabBarController;
@property (nonatomic, retain) MenCategoryViewController  *menTableViewController;
@property (nonatomic, retain) WomenCategoryViewController *womenTableViewController;
@property (nonatomic, retain) GiftCategoryViewController *giftTableViewController;
@property (nonatomic, retain) PrivateJetCategoryViewController *privatejetTableViewController;

@property (nonatomic, retain) UINavigationController *menNavController;
@property (nonatomic, retain) UINavigationController *womenNavController;
@property (nonatomic, retain) UINavigationController *giftNavController;
@property (nonatomic, retain) UINavigationController *privatejetNavController;

@property (nonatomic) NSUInteger nsiCategoryId;

@property (nonatomic, retain) NSMutableArray *addToBagProducts;
@property (nonatomic, retain) CreditCardDetails *creditCardDetails;
@property (nonatomic, retain) CustomerDetails *customerBillingDetails;
@property (nonatomic, retain) CustomerDetails *customerShippingDetails;
@property (nonatomic) BOOL shouldShowTabBar;
@property (nonatomic) BOOL shouldShowDownloadCatalogue;
@property (nonatomic) BOOL callDownloadCatalogue;
@property (nonatomic,retain) CustomerDetails *customerAirPackageDetails;
@property (nonatomic) BOOL deviceRegStatus;
@property (nonatomic) BOOL downloadAirPackage;

-(void)createEditableCopyOfdatabaseIfNeeded;
-(void)initializeTabBar;
-(void)downloadCatalogue:(BOOL)shouldDownload;
-(void)mainApplicationFlow;
-(void)showWelcomePage;


@end

