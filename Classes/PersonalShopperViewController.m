//
//  PersonalShopperViewController.m
//  SkyBuyHigh
//
//  Created by Thapovan Info Systems on 27/07/09.
//  Copyright 2009 Thapovan. All rights reserved.
//

#import "PersonalShopperViewController.h"


@implementation PersonalShopperViewController
@synthesize imageView;

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewWillAppear:(BOOL)animated{
	
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	NSMutableString *personalShopperPath = [[NSMutableString alloc] initWithString:[documentsDirectory stringByAppendingPathComponent:@"PersonalShopper.png"]];
   
	[self.imageView setImage:[UIImage imageWithContentsOfFile:personalShopperPath]];
	[super viewWillAppear:NO];
}


- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [super dealloc];
}


@end
