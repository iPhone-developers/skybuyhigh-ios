//
//  CustomerDetails.h
//  SkyBuyHigh
//
//  Created by Thapovan InfoSystems on 27/07/09.
//  Copyright 2009 Thapovan. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface CustomerDetails : NSObject {
	
	NSString  *custTitle;
	NSString  *custFirstName;
	NSString  *cusLastName;
	NSString  *address1;
	NSString  *address2;
	NSString  *city;
	NSString  *state;
	NSString  *country;
	NSString  *zip;
	NSString  *phone;	
	NSString  *email;	
	NSString  *sameAddress;
	NSString  *specialIns;
	NSString  *personalMsg;
	NSString  *feedback;
	NSString  *suggestions;
	NSString  *tailNumber;
	//BOOL isOn;
}

@property (nonatomic,retain) NSString  *custTitle;
@property (nonatomic,retain) NSString  *custFirstName;
@property (nonatomic,retain) NSString  *cusLastName;
@property (nonatomic,retain) NSString  *address1;
@property (nonatomic,retain) NSString  *address2;
@property (nonatomic,retain) NSString  *city;
@property (nonatomic,retain) NSString  *state;
@property (nonatomic,retain) NSString  *country;
@property (nonatomic,retain) NSString  *zip;
@property (nonatomic,retain) NSString  *phone;
@property (nonatomic,retain) NSString  *email;
@property (nonatomic,retain) NSString  *sameAddress;
@property (nonatomic,retain) NSString  *specialIns;
@property (nonatomic,retain) NSString  *personalMsg;
@property (nonatomic,retain) NSString  *feedback;
@property (nonatomic,retain) NSString  *suggestions;
@property (nonatomic,retain) NSString  *tailNumber;

-(id)initWithCustomerDetails:(CustomerDetails *) data;
//@property (nonatomic) BOOL isOn;
@end
