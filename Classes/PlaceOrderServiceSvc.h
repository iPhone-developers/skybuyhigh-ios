#import <Foundation/Foundation.h>
#import "USAdditions.h"
#import <libxml/tree.h>
#import "USGlobals.h"
@class PlaceOrderServiceSvc_placeOrder;
@class PlaceOrderServiceSvc_placeOrderResponse;
@interface PlaceOrderServiceSvc_placeOrder : NSObject {
	
/* elements */
	NSString * arg0;
	NSString * arg1;
/* attributes */
}
- (NSString *)nsPrefix;
- (NSString *)serializedFormUsingElementName:(NSString *)elName;
- (NSString *)serializedAttributeString;
- (NSString *)serializedElementString;
+ (PlaceOrderServiceSvc_placeOrder *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * arg0;
@property (retain) NSString * arg1;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface PlaceOrderServiceSvc_placeOrderResponse : NSObject {
	
/* elements */
	NSString * return_;
/* attributes */
}
- (NSString *)nsPrefix;
- (NSString *)serializedFormUsingElementName:(NSString *)elName;
- (NSString *)serializedAttributeString;
- (NSString *)serializedElementString;
+ (PlaceOrderServiceSvc_placeOrderResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * return_;
/* attributes */
- (NSDictionary *)attributes;
@end
/* Cookies handling provided by http://en.wikibooks.org/wiki/Programming:WebObjects/Web_Services/Web_Service_Provider */
#import <libxml/parser.h>
#import "xs.h"
#import "PlaceOrderServiceSvc.h"
@class PlaceOrderPortBinding;
@interface PlaceOrderServiceSvc : NSObject {
	
}
+ (PlaceOrderPortBinding *)PlaceOrderPortBinding;
@end
@class PlaceOrderPortBindingResponse;
@class PlaceOrderPortBindingOperation;
@protocol PlaceOrderPortBindingResponseDelegate <NSObject>
- (void) operation:(PlaceOrderPortBindingOperation *)operation completedWithResponse:(PlaceOrderPortBindingResponse *)response;
@end
@interface PlaceOrderPortBinding : NSObject <PlaceOrderPortBindingResponseDelegate> {
	NSURL *address;
	NSTimeInterval defaultTimeout;
	NSMutableArray *cookies;
	BOOL logXMLInOut;
	BOOL synchronousOperationComplete;
}
@property (copy) NSURL *address;
@property (assign) BOOL logXMLInOut;
@property (assign) NSTimeInterval defaultTimeout;
@property (nonatomic, retain) NSMutableArray *cookies;
- (id)initWithAddress:(NSString *)anAddress;
- (void)sendHTTPCallUsingBody:(NSString *)body soapAction:(NSString *)soapAction forOperation:(PlaceOrderPortBindingOperation *)operation;
- (void)addCookie:(NSHTTPCookie *)toAdd;
- (PlaceOrderPortBindingResponse *)placeOrderUsingParameters:(PlaceOrderServiceSvc_placeOrder *)aParameters ;
- (void)placeOrderAsyncUsingParameters:(PlaceOrderServiceSvc_placeOrder *)aParameters  delegate:(id<PlaceOrderPortBindingResponseDelegate>)responseDelegate;
@end
@interface PlaceOrderPortBindingOperation : NSOperation {
	PlaceOrderPortBinding *binding;
	PlaceOrderPortBindingResponse *response;
	id<PlaceOrderPortBindingResponseDelegate> delegate;
	NSMutableData *responseData;
	NSURLConnection *urlConnection;
}
@property (retain) PlaceOrderPortBinding *binding;
@property (readonly) PlaceOrderPortBindingResponse *response;
@property (nonatomic, retain) id<PlaceOrderPortBindingResponseDelegate> delegate;
@property (nonatomic, retain) NSMutableData *responseData;
@property (nonatomic, retain) NSURLConnection *urlConnection;
- (id)initWithBinding:(PlaceOrderPortBinding *)aBinding delegate:(id<PlaceOrderPortBindingResponseDelegate>)aDelegate;
@end
@interface PlaceOrderPortBinding_placeOrder : PlaceOrderPortBindingOperation {
	PlaceOrderServiceSvc_placeOrder * parameters;
}
@property (retain) PlaceOrderServiceSvc_placeOrder * parameters;
- (id)initWithBinding:(PlaceOrderPortBinding *)aBinding delegate:(id<PlaceOrderPortBindingResponseDelegate>)aDelegate
	parameters:(PlaceOrderServiceSvc_placeOrder *)aParameters
;
@end
@interface PlaceOrderPortBinding_envelope : NSObject {
}
+ (PlaceOrderPortBinding_envelope *)sharedInstance;
- (NSString *)serializedFormUsingHeaderElements:(NSDictionary *)headerElements bodyElements:(NSDictionary *)bodyElements;
@end
@interface PlaceOrderPortBindingResponse : NSObject {
	NSArray *headers;
	NSArray *bodyParts;
	NSError *error;
}
@property (retain) NSArray *headers;
@property (retain) NSArray *bodyParts;
@property (retain) NSError *error;
@end
