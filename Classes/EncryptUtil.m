//
//  EncryptUtil.m
//  SkyBuyHigh
//
//  Created by Thapovan Info Systems on 20/08/09.
//  Copyright 2009 Thapovan. All rights reserved.
//

#import "EncryptUtil.h"


@implementation EncryptUtil
static SecKeyRef publicKeyRef;

const uint8_t kKeyBytes[] = "abcdefgh0123456"; // Must be 16 bytes

static const char encodingTable[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

+ (SecKeyRef) getPublicKeyRef {
	if (publicKeyRef == NULL) {
		OSStatus sanityCheck;
		NSData *publicKeyData = [[NSData alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"PubKey-1024" ofType:@"cer"]];
		SecCertificateRef publicKeyCertificate = NULL;
		publicKeyCertificate = SecCertificateCreateWithData (kCFAllocatorMalloc, (CFDataRef)publicKeyData);
		if (publicKeyCertificate != NULL) {
			//NSLog(@"Certificate Summary \n%@",(NSString *)SecCertificateCopySubjectSummary(publicKeyCertificate));
			SecTrustRef publicKeyTrustRef = NULL;
			//NSLog(@"We have the certificate ref!");
			sanityCheck = SecTrustCreateWithCertificates((CFArrayRef) [NSArray arrayWithObject:(id)publicKeyCertificate],SecPolicyCreateBasicX509(),&publicKeyTrustRef);
			//NSLog(@"Trust create err = %d",sanityCheck);
			SecTrustResultType *result = NULL;
			sanityCheck = SecTrustEvaluate(publicKeyTrustRef, result);
			//NSLog(@"\nSecTrustEvaluate Error = %d and result = %d\n", sanityCheck, result);
			publicKeyRef = SecTrustCopyPublicKey(publicKeyTrustRef);
			//CFShow(publicKeyRef);
		}
	}
	return publicKeyRef;
}

#pragma mark -
#pragma mark Base64 Encode/Decoder
+ (NSString *)base64EncodeData:(NSData*)dataToConvert
{
	if ([dataToConvert length] == 0)
		return @"";
	
    char *characters = malloc((([dataToConvert length] + 2) / 3) * 4);
	if (characters == NULL)
		return nil;
	
	NSUInteger length = 0;
	
	NSUInteger i = 0;
	while (i < [dataToConvert length])
	{
		char buffer[3] = {0,0,0};
		short bufferLength = 0;
		while (bufferLength < 3 && i < [dataToConvert length])
			buffer[bufferLength++] = ((char *)[dataToConvert bytes])[i++];
		
		//  Encode the bytes in the buffer to four characters, including padding "=" characters if necessary.
		characters[length++] = encodingTable[(buffer[0] & 0xFC) >> 2];
		characters[length++] = encodingTable[((buffer[0] & 0x03) << 4) | ((buffer[1] & 0xF0) >> 4)];
		if (bufferLength > 1)
			characters[length++] = encodingTable[((buffer[1] & 0x0F) << 2) | ((buffer[2] & 0xC0) >> 6)];
		else characters[length++] = '=';
		if (bufferLength > 2)
			characters[length++] = encodingTable[buffer[2] & 0x3F];
		else characters[length++] = '=';	
	}
	
	return [[[NSString alloc] initWithBytesNoCopy:characters length:length encoding:NSASCIIStringEncoding freeWhenDone:YES] autorelease];
}

+ (NSData*)base64DecodeString:(NSString *)string
{
	if (string == nil)
		[NSException raise:NSInvalidArgumentException format:nil];
	if ([string length] == 0)
		return [NSData data];
	
	static char *decodingTable = NULL;
	if (decodingTable == NULL)
	{
		decodingTable = malloc(256);
		if (decodingTable == NULL)
			return nil;
		memset(decodingTable, CHAR_MAX, 256);
		NSUInteger i;
		for (i = 0; i < 64; i++)
			decodingTable[(short)encodingTable[i]] = i;
	}
	
	const char *characters = [string cStringUsingEncoding:NSASCIIStringEncoding];
	if (characters == NULL)     //  Not an ASCII string!
		return nil;
	char *bytes = malloc((([string length] + 3) / 4) * 3);
	if (bytes == NULL)
		return nil;
	NSUInteger length = 0;
	
	NSUInteger i = 0;
	while (YES)
	{
		char buffer[4];
		short bufferLength;
		for (bufferLength = 0; bufferLength < 4; i++)
		{
			if (characters[i] == '\0')
				break;
			if (isspace(characters[i]) || characters[i] == '=')
				continue;
			buffer[bufferLength] = decodingTable[(short)characters[i]];
			if (buffer[bufferLength++] == CHAR_MAX)      //  Illegal character!
			{
				free(bytes);
				return nil;
			}
		}
		
		if (bufferLength == 0)
			break;
		if (bufferLength == 1)      //  At least two characters are needed to produce one byte!
		{
			free(bytes);
			return nil;
		}
		
		//  Decode the characters in the buffer to bytes.
		bytes[length++] = (buffer[0] << 2) | (buffer[1] >> 4);
		if (bufferLength > 2)
			bytes[length++] = (buffer[1] << 4) | (buffer[2] >> 2);
		if (bufferLength > 3)
			bytes[length++] = (buffer[2] << 6) | buffer[3];
	}
	
	realloc(bytes, length);
	
	return [NSData dataWithBytesNoCopy:bytes length:length];
}


@end
