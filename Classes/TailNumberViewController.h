//
//  TailNumberViewController.h
//  SkyBuyHigh
//
//  Created by Thapovan Info Systems on 09/12/09.
//  Copyright 2009 Thapovan. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface TailNumberViewController : UIViewController <UIWebViewDelegate> {
	UIWebView *webView;
	NSString *orderConfirmationNo;
}

@property (nonatomic, retain) IBOutlet UIWebView *webView;
@property (nonatomic, retain) NSString *orderConfirmationNo;

-(void)saveProductsDetails;
@end
