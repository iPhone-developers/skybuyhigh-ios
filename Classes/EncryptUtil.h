//
//  EncryptUtil.h
//  SkyBuyHigh
//
//  Created by Thapovan Info Systems on 20/08/09.
//  Copyright 2009 Thapovan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EncryptUtil : NSObject {
	
}

+ (SecKeyRef) getPublicKeyRef;
+ (NSString *)base64EncodeData:(NSData*)dataToConvert;
+ (NSData*)base64DecodeString:(NSString *)string;

@end
