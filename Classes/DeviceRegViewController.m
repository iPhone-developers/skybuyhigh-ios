//
//  DeviceRegViewController.m
//  SkyBuyHigh
//
//  Created by Thapovan InfoSystems on 03/09/09.
//  Copyright 2009 Thapovan. All rights reserved.
//

#import "DeviceRegViewController.h"
#import "ProductDetailsDAO.h"
#import "SkyBuyHighAppDelegate.h"


@implementation DeviceRegViewController
@synthesize airCode,productKey;
@synthesize theConnection,receivedData;
@synthesize errorMsg;
@synthesize activityIndicator;
@synthesize submitButton, continueButton;
@synthesize deviceDetailsView;
@synthesize deviceRegistered;

- (void)viewDidAppear:(BOOL)animated {
	[errorMsg setText:@"Please enter the Air Charter Code and Product Key."];
}

- (void)viewWillAppear:(BOOL)animated {
	if([(SkyBuyHighAppDelegate *)[[UIApplication sharedApplication] delegate] deviceRegStatus]){
		[deviceDetailsView setHidden:NO];
		DeviceDetails *regDevice = [[ProductDetailsDAO instance] getDeviceRegDetails];
		[deviceRegistered setText:[NSString stringWithFormat:@"This device is registered with SkyBuyHigh as %@", regDevice.deviceCode]];
	}	
}

-(IBAction)continueShopping:(id)sender{

	[[self.view viewWithTag:10020] removeFromSuperview];	
	[(SkyBuyHighAppDelegate *)[[UIApplication sharedApplication] delegate] setDeviceRegStatus:[[ProductDetailsDAO instance] getDeviceRegStatus]];
	if([(SkyBuyHighAppDelegate *)[[UIApplication sharedApplication] delegate] deviceRegStatus]){			
		ProductViewController *productViewController = [[ProductViewController alloc] init];
		[[[[productViewController toolbar] items] objectAtIndex:6] setEnabled:YES];
	}	
	[(SkyBuyHighAppDelegate *)[[UIApplication sharedApplication] delegate] setShouldShowTabBar:YES];
	if([(SkyBuyHighAppDelegate *)[[UIApplication sharedApplication] delegate] deviceRegStatus]) {
		[(SkyBuyHighAppDelegate *)[[UIApplication sharedApplication] delegate] downloadCatalogue:NO];
	}else {
		[[(SkyBuyHighAppDelegate *)[[UIApplication sharedApplication] delegate] tabBarController] setSelectedViewController:[[[(SkyBuyHighAppDelegate *)[[UIApplication sharedApplication] delegate] tabBarController] viewControllers] objectAtIndex:0]];
	} 
} 


-(IBAction)textFieldDoneEditing:(id)sender {
	[sender resignFirstResponder];
}

-(IBAction)submitDeviceDetails:(id)sender {
	[airCode resignFirstResponder];
	[productKey resignFirstResponder];
	[submitButton setEnabled:NO];
	[continueButton setEnabled:NO];
	if([[[[airCode text] componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"1234567890"]] componentsJoinedByString:@""] length] == 0
	   && ![[[airCode text] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""] 
	   && ![[[productKey text] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""] ){
		NSString *macAddr=nil;
		[activityIndicator startAnimating];
		macAddr = [[UIDevice currentDevice]	uniqueIdentifier];
		[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
		//macAddr=@"e0f27122c09245dcd2622fb1d7a27d8eea436889";
		NSDictionary *connections = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Connections" ofType:@"plist"]];
		NSString *deviceRegURL =[[NSString alloc] initWithFormat:[connections valueForKey:@"DeviceRegURL"],macAddr,[airCode text],macAddr,[productKey text]]; 
		//NSLog(@"Device Reg URL = %@",deviceRegURL);
		NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[deviceRegURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:120.0];
		theConnection = [[NSURLConnection alloc] initWithRequest:theRequest delegate:self];
		if (theConnection) {
			receivedData = [[NSMutableData data] retain];
		}
		else {
			//NSLog(@"Connection could not be made");
		}
	}else{
		NSString *alertMsg=@"";
		if([[[airCode text] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""] 
		   && [[[productKey text] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""]){
			alertMsg=@"Please enter the Air Charter Code and Product Key." ;
		}	
		else if([[[airCode text] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""]){
			alertMsg=@"Please enter the Air Charter Code.";
		}	
		else if([[[productKey text] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""]){
			alertMsg=@"Please enter the Product Key." ;
		}	
		else if([[[[airCode text] componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"1234567890"]] componentsJoinedByString:@""] length] >0){
			alertMsg=@"Invalid characters in Air Charter Code.";	
		} 
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"SkyBuyHigh" message:alertMsg delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
		[alert show];
		[alert release];
		[submitButton setEnabled:YES];
		[continueButton setEnabled:YES];
	}
}	


-(void)connection:(NSURLConnection *)connection
didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge {
	 if ([challenge previousFailureCount] == 0) {		
		//NSLog(@"Inside DeviceRegView didReceiveAuthentication Challenge");
        NSURLCredential *newCredential;		
		NSArray *accountInfo = [[ProductDetailsDAO instance] getServerAccountInfo];
		if(accountInfo!=nil){
			newCredential=[NSURLCredential credentialWithUser:[accountInfo objectAtIndex:0]
													 password:[accountInfo objectAtIndex:1]
												  persistence:NSURLCredentialPersistenceNone];
		}
        [[challenge sender] useCredential:newCredential
               forAuthenticationChallenge:challenge];
		
		[accountInfo release];accountInfo = nil;
		
    } else {
		NSLog(@"Authentication failed");
        [[challenge sender] cancelAuthenticationChallenge:challenge];
        // inform the user that the user name and password
        // in the preferences are incorrect
        //[self showPreferencesCredentialsAreIncorrectPanel:self];
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
	if ([response isKindOfClass:[NSHTTPURLResponse class]]) {
		[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
		//NSLog(@"Response = %@", [(NSHTTPURLResponse *)response allHeaderFields]);
		NSString *airlinedetails = [[(NSHTTPURLResponse *)response allHeaderFields] objectForKey:@"Airlinedetails"];
		if(airlinedetails!=nil || [airlinedetails isEqualToString:@""]){
			NSArray *deviceRegDetails = [airlinedetails componentsSeparatedByString:@";"];			
			[[ProductDetailsDAO instance] insertDeviceRegDetails:deviceRegDetails withProductKey:[productKey text]];
			[[ProductDetailsDAO instance] updateDeviceId:[[deviceRegDetails objectAtIndex:3] integerValue] macAddress:[[UIDevice currentDevice] uniqueIdentifier]];
			[(SkyBuyHighAppDelegate *)[[UIApplication sharedApplication] delegate] setDeviceRegStatus:YES];
			[errorMsg setHidden:NO];
			errorMsg.text = @"Device registration successful. Touch Continue Shopping to proceed.";
			[(SkyBuyHighAppDelegate *)[[UIApplication sharedApplication] delegate] setDownloadAirPackage:YES];

		}
	}
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
	[receivedData appendData:data];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
	[submitButton setEnabled:YES];
	[continueButton setEnabled:YES];
	[errorMsg setHidden:NO];
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
	NSString *receivedValue =[[NSString alloc] initWithData:receivedData encoding:NSASCIIStringEncoding];
	//NSLog(@"Device reg: receivedValue = %@",receivedValue);
	if(receivedValue!=nil && [receivedValue isEqualToString:@"IAP\r\n"]){
		errorMsg.text = @"Invalid Air Charter Code/Product key. Contact SkyBuyHigh Admin.";
	}else if(receivedValue!=nil && [receivedValue isEqualToString:@"IAC\r\n"]){
		errorMsg.text = @"Invalid Air Charter Code. Contact SkyBuyHigh Admin.";
	}else if(receivedValue!=nil && [receivedValue isEqualToString:@"IPK\r\n"]){
		errorMsg.text = @"Invalid Product Key. Contact SkyBuyHigh Admin.";
	}else if(receivedValue!=nil && [receivedValue isEqualToString:@"DIN\r\n"]){
		errorMsg.text = @"Device is Inactive. Contact SkyBuyHigh Admin.";
	}else if(receivedValue!=nil && [receivedValue isEqualToString:@"DID\r\n"]){
		errorMsg.text = @"Device is Deleted. Contact SkyBuyHigh Admin.";
	}else if(receivedValue!=nil && [receivedValue isEqualToString:@"INA\r\n"]){
		errorMsg.text = @"Air Charter is Inactive. Contact SkyBuyHigh Admin.";
	}else if(receivedValue!=nil && [receivedValue isEqualToString:@"DNC\r\n"]){
		errorMsg.text = @"Device is not activated. Contact SkyBuyHigh Admin.";
	}else if(receivedValue!=nil && [receivedValue isEqualToString:@"DAR\r\n"]){
		errorMsg.text = @"Device is already Registered";
	}else if(receivedValue!=nil && [receivedValue isEqualToString:@"DRD\r\n"]){
		errorMsg.text = @"Device registered with this air charter is Deleted. Contact SkyBuyHigh Admin.";
	}else if(receivedValue!=nil && [receivedValue isEqualToString:@"DNA\r\n"]){
		errorMsg.text = @"Device is not allocated. Contact SkyBuyHigh Admin.";
	}else if(receivedValue!=nil && [receivedValue isEqualToString:@"DAA\r\n"]){
		errorMsg.text = @"Device is already activated";
	}
	[activityIndicator stopAnimating];
}

- (void)connection:(NSURLConnection *)connection
  didFailWithError:(NSError *)error {	
    // release the connection, and the data object	
    [theConnection release];	
		
    //NSLog(@"Connection failed! Error - %d - %@ %@",	[error code],	  
     //     [error localizedDescription],		  
         // [[error userInfo] objectForKey:NSErrorFailingURLStringKey]);	
	errorMsg.text = @"Please check your connectivity and try again later.";
	//[NSThread sleepForTimeInterval:2.0];
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
	[submitButton setEnabled:YES];
	[continueButton setEnabled:YES];
	[activityIndicator stopAnimating];
}


- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)dealloc {
	[airCode release], airCode = nil;
	[productKey release], productKey = nil;
	if (theConnection) {
		[theConnection release], theConnection = nil;
	}
	if (receivedData) {
		[receivedData release], receivedData = nil;
	}
	[errorMsg release], errorMsg = nil;
	[activityIndicator release],activityIndicator = nil;
    [super dealloc];
}


@end
