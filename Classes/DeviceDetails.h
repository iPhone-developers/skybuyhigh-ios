//
//  DeviceDetails.h
//  SkyBuyHigh
//
//  Created by Thapovan InfoSystems on 04/09/09.
//  Copyright 2009 Thapovan. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface DeviceDetails : NSObject {
	
	NSUInteger airCode;
	NSString *airName;
	NSString *airEmail;
	NSString *macAddr;
	NSString *activeMacAddr;
	NSString *productKey;
	NSString *processorId;
	NSString *deviceType;
	NSString *deviceModel;
	NSString *deviceSerialNo;
	NSUInteger deviceId;
	NSString *deviceCode;
	NSString *deviceStatus;
	

}


@property(nonatomic) NSUInteger airCode;
@property(nonatomic,retain) NSString *airName;
@property(nonatomic,retain) NSString *airEmail;
@property(nonatomic,retain) NSString *macAddr;
@property(nonatomic,retain) NSString *activeMacAddr;
@property(nonatomic,retain) NSString *productKey;
@property(nonatomic,retain) NSString *processorId;
@property(nonatomic,retain) NSString *deviceType;
@property(nonatomic,retain) NSString *deviceModel;
@property(nonatomic,retain) NSString *deviceSerialNo;
@property(nonatomic) NSUInteger deviceId;
@property(nonatomic,retain) NSString *deviceCode;
@property(nonatomic,retain) NSString *deviceStatus;

@end
