//
//  CustomerAirPackageController.h
//  SkyBuyHigh
//
//  Created by Thapovan InfoSystems on 08/09/09.
//  Copyright 2009 Thapovan. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface CustomerAirPackageController : UIViewController <UIWebViewDelegate> {
	UIWebView *webView;
}

@property (nonatomic, retain) IBOutlet UIWebView *webView;

@end