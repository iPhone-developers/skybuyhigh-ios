//
//  AirPackageTailNumberViewController.m
//  SkyBuyHigh
//
//  Created by Thapovan Info Systems on 10/12/09.
//  Copyright 2009 Thapovan. All rights reserved.
//

#import "AirPackageTailNumberViewController.h"

#import "ProductDetailsDAO.h"
#import "SkyBuyHighAppDelegate.h"
#import "OnlineAirPackagePlaceOrderViewController.h"
#import "AirPackageConfirmationViewController.h"


@implementation AirPackageTailNumberViewController
@synthesize webView;

- (void)viewWillAppear:(BOOL)animated {
	
    NSString *htmlFile = [[NSBundle mainBundle] pathForResource:@"AirPackageTailNumberDetails" ofType:@"html"];
	NSData *htmlData = [NSData dataWithContentsOfFile:htmlFile];
	webView.delegate = self;
	[webView loadData:htmlData MIMEType:@"text/html" textEncodingName:@"UTF-8" baseURL:[NSURL URLWithString:@""]];
	
	[super viewWillAppear:animated];
}


- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request  navigationType:(UIWebViewNavigationType)navigationType {
	
	SkyBuyHighAppDelegate *delegate=(SkyBuyHighAppDelegate *)[[UIApplication sharedApplication]delegate];
	CustomerDetails *customerAirPackageDetails =(CustomerDetails *) delegate.customerAirPackageDetails;
	NSString *requestString = [[request URL] absoluteString];
	
	//NSLog(@"Request String =%@",requestString);
	
	NSArray *components = [requestString componentsSeparatedByString:@":"];
	if ([components count] > 1 && [(NSString *)[components objectAtIndex:0] isEqualToString:@"http"]) {
		if ([(NSString *)[components objectAtIndex:2] isEqualToString:@"TailNumber"]) {
			[customerAirPackageDetails setTailNumber:[[components objectAtIndex:3] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
			[self savePackageDetails];
		}	
		
	}	
	return YES; // Return YES to make sure regular navigation works as expected.
}

-(void)webViewDidFinishLoad:(UIWebView *)webView1 {
	
	NSString *resourcePath = [[[[NSBundle mainBundle] resourcePath]
							   stringByReplacingOccurrencesOfString:@"/" withString:@"//"]
							  stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
	
	NSString *jsCommand = [NSString stringWithFormat:@"setFormValues('file:/%@//logo_black.png')",resourcePath];
	
    [webView stringByEvaluatingJavaScriptFromString:jsCommand];
	
	
}

-(void)savePackageDetails{
	
	SkyBuyHighAppDelegate *delegate=(SkyBuyHighAppDelegate *)[[UIApplication sharedApplication]delegate];
	NSMutableArray *productInfomation=nil ;
	id categoryViewController=nil;
	categoryViewController = [[self.navigationController viewControllers] objectAtIndex:0];
	productInfomation = [categoryViewController productInfo];
	NSUInteger rowCount = [categoryViewController nsiCurrentRow];
	Products *prod= [productInfomation objectAtIndex:rowCount];
	DeviceDetails *deviceDetails=[[ProductDetailsDAO instance] getDeviceRegDetails];
	[[ProductDetailsDAO instance] insertAirPakcageDetails:delegate.customerAirPackageDetails productDetails:prod deviceRegDetails:deviceDetails];
	delegate.customerAirPackageDetails = [[CustomerDetails alloc] init];	
	
	if (!([[Reachability sharedReachability] internetConnectionStatus] == NotReachable)) {
		OnlineAirPackagePlaceOrderViewController *onlineAirPackagePlaceOrderViewController = [[OnlineAirPackagePlaceOrderViewController alloc] initWithNibName:@"OnlineAirPackagePlaceOrderViewController" bundle:nil];
		[self.navigationController pushViewController:onlineAirPackagePlaceOrderViewController animated:NO];
	}else{
		AirPackageConfirmationViewController *airPackageConfirmationViewController = [[AirPackageConfirmationViewController alloc] initWithNibName:@"AirPackageConfirmationView" bundle:nil];
		[self.navigationController pushViewController:airPackageConfirmationViewController animated:NO];
	}	

}

- (void)dealloc {
	[webView release];
    [super dealloc];
}

@end