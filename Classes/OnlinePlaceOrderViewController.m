//
//  OnlinePlaceOrderViewController.m
//  SkyBuyHigh
//
//  Created by Thapovan Info Systems on 14/12/09.
//  Copyright 2009 Thapovan. All rights reserved.
//

#import "OnlinePlaceOrderViewController.h"
#import "SkyBuyHighAppDelegate.h"
#import "ProductDetailsDAO.h"
#import "OrderDetails.h"
#import "PlaceOrder.h"
#import "OrderConfirmationViewController.h"


@implementation OnlinePlaceOrderViewController
@synthesize activityIndicator,placeOrderStatus,orderConfirmationNo,emailTextMessage;

/*
 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
 - (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
 if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
 // Custom initialization
 }
 return self;
 }
 */

- (void)viewWillAppear:(BOOL)animated {
	[placeOrderStatus setText:@"Please wait while your order is being placed..."];
	
}	

- (void)viewDidAppear:(BOOL)animated{
	
	[self performSelector:@selector(placeOrder) withObject:nil afterDelay:0.0f];
	
}


-(void)placeOrder{
	/*SkyBuyHighAppDelegate *delegate=(SkyBuyHighAppDelegate *)[[UIApplication sharedApplication]delegate];
	 
	 if([delegate.addToBagProducts count]>0){
	 
	 DeviceDetails *deviceDetails=[[ProductDetailsDAO instance] getDeviceRegDetails];
	 [[ProductDetailsDAO instance] getDatabaseConnection];
	 NSDateFormatter *dateFormatter = [[[NSDateFormatter alloc] init] autorelease];
	 [dateFormatter setDateFormat:@"MMddyy-HHmmss"];
	 NSString *currentDatetime = [dateFormatter stringFromDate:[NSDate date]];
	 
	 orderConfirmationNo =[[NSString alloc] initWithFormat:@"%@-%@",delegate.customerBillingDetails.phone,currentDatetime];
	 NSInteger customerId=[[ProductDetailsDAO instance] insertCustomerBillingDetails:delegate.customerBillingDetails customerShippingDetails:delegate.customerShippingDetails customerCreditCatddetails:delegate.creditCardDetails deviceRegDetails:deviceDetails orderConfirmationNumber:orderConfirmationNo];
	 
	 
	 for(int i=0;i<[delegate.addToBagProducts count];i++){		
	 Products *prod = [delegate.addToBagProducts objectAtIndex:i];
	 [[ProductDetailsDAO instance] insertProductDetails:prod setCustomerId:customerId setOrderConfirmationNo:orderConfirmationNo setTailNumber:delegate.customerBillingDetails.tailNumber];
	 [prod release];		
	 }	
	 [[ProductDetailsDAO instance] closeDatabaseConnection];
	 
	 }
	 */
	
	if (!([[Reachability sharedReachability] internetConnectionStatus] == NotReachable)) {
		NSString *orderResponse=nil;
		[activityIndicator startAnimating];
		[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
		
		if([[ProductDetailsDAO instance] getOrderItemsCount]>0){
			//Place product details
			NSMutableArray *ordersArray = [[ProductDetailsDAO instance] generateOrderItemDetailsXML];
			for (int i = 0; i < [ordersArray count]; i++) {
				NSString *orderXML=[[ordersArray objectAtIndex:i] orderDetailsXml];		
				orderResponse = [PlaceOrder sendRequestWithParams:orderXML customerId:[[ordersArray objectAtIndex:i] customerId]];
				if(orderResponse!=nil && [orderResponse isEqualToString:@"OPS"]) {
					//placeOrderSuccessCount=placeOrderSuccessCount+1;
					//NSLog(@"Order has been successfully placed.");
				}				
			}
		}
		if([[ProductDetailsDAO instance] getAirPackageItemsCount]>0){
			//Place airline packages
			NSString *airlinePackageDetailsXML = [[ProductDetailsDAO instance] generateAirlinePackageDetailsXML];
			if(airlinePackageDetailsXML!=nil){
				orderResponse = [PlaceOrder sendRequestWithParams:airlinePackageDetailsXML];	
				if (orderResponse != nil && [orderResponse isEqualToString:@"OPS"]) {
					//NSLog(@"Airline Package has been placed successfully");
				}
			}
		}
		[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
		[activityIndicator stopAnimating];
	}
	
	/*	NSString *email = [delegate.customerBillingDetails	email];
	 delegate.creditCardDetails = [[CreditCardDetails alloc] init];	
	 delegate.customerBillingDetails = [[CustomerDetails alloc] init];	
	 delegate.customerShippingDetails = [[CustomerDetails alloc] init];
	 delegate.addToBagProducts = [[NSMutableArray alloc] init];*/
	
	
	OrderConfirmationViewController *orderConfirmationViewController = [[OrderConfirmationViewController alloc] initWithNibName:@"OrderConfirmationView" bundle:nil];
	[orderConfirmationViewController setOrderConfirmationNo:orderConfirmationNo];
	[orderConfirmationViewController setEmailTextMessage:emailTextMessage];
	[self.navigationController pushViewController:orderConfirmationViewController animated:NO];
	
	
}

/*
 // Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
 - (void)viewDidLoad {
 [super viewDidLoad];
 }
 */



/*
 // Override to allow orientations other than the default portrait orientation.
 - (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
 // Return YES for supported orientations
 return (interfaceOrientation == UIInterfaceOrientationPortrait);
 }
 */

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [super dealloc];
}


@end
