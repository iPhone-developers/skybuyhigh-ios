//
//  PlaceOrderController.h
//  SkyBuyHigh
//
//  Created by Thapovan InfoSystems on 17/08/09.
//  Copyright 2009 Thapovan. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface PlaceOrderController : NSObject {

}
+(void)PlaceOrderDetails;
-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response;
-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data;
-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error;
-(void)connectionDidFinishLoading:(NSURLConnection *)connection;
@end
