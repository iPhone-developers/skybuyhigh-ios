//
//  ControllerManager.m
//  SkyBuyHigh
//
//  Created by Thapovan Info Systems on 11/09/09.
//  Copyright 2009 Thapovan. All rights reserved.
//

#import "ControllerManager.h"


@implementation ControllerManager

static ControllerManager *singletonInstance;
+ (ControllerManager *) instance {
	
	@synchronized (self) {
		if (singletonInstance == NULL) {
			singletonInstance = [ControllerManager alloc];
		}
	}
	return singletonInstance;		
}


- (UIViewController *) controllerWithName:(NSString *) name andNib:(NSString *) nibName {
	if (controllers == nil) {
		controllers = [[NSMutableDictionary alloc] initWithCapacity:5];
	}
	UIViewController *ret = [controllers objectForKey:name];
	if (ret == nil) {
		Class cls = NSClassFromString(name);
		ret = [cls alloc];
		[ret initWithNibName:nibName bundle:nil];
		[controllers setObject:ret forKey:name];
	}
	return ret;	

}

@end
