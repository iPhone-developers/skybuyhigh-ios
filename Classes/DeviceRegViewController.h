//
//  DeviceRegViewController.h
//  SkyBuyHigh
//
//  Created by Thapovan InfoSystems on 03/09/09.
//  Copyright 2009 Thapovan. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface DeviceRegViewController : UIViewController {
	
	UITextField *airCode;
	UITextField *productKey;
	NSURLConnection *theConnection;
	NSMutableData *receivedData;
	UILabel *errorMsg;
	UIActivityIndicatorView *activityIndicator;
	UIButton *submitButton;
	UIButton *continueButton;
	UIView  *deviceDetailsView;
	UITextView *deviceRegistered;
}

@property(nonatomic,retain) IBOutlet UITextField *airCode;
@property(nonatomic,retain) IBOutlet UITextField *productKey;
@property(nonatomic,retain) NSURLConnection *theConnection;
@property(nonatomic,retain) NSMutableData *receivedData;
@property(nonatomic,retain) IBOutlet UILabel *errorMsg;
@property (nonatomic, retain) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (nonatomic, retain) IBOutlet UIButton *submitButton;
@property (nonatomic, retain) IBOutlet UIButton *continueButton;
@property (nonatomic, retain) IBOutlet UIView  *deviceDetailsView;
@property (nonatomic, retain) IBOutlet UITextView *deviceRegistered;

-(IBAction)submitDeviceDetails:(id)sender;
-(IBAction)textFieldDoneEditing:(id)sender;
-(IBAction)continueShopping:(id)sender;

@end
