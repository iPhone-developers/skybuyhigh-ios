//
//  ProductImageZoomController.h
//  SkyBuyHigh
//
//  Created by Thapovan InfoSystems on 08/07/09.
//  Copyright 2009 Thapovan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AirlinePackageViewController.h"
#import "AirlineProductViewController.h"


@class Products, ProductViewController, SkyBuyScrollView, AirlineProductViewController,AirlinePackageViewController;

@interface ProductImageZoomController : UIViewController<UIScrollViewDelegate> {
	NSString *productImage;
	UIImageView *imageView;
	UIScrollView *scrollView;
	UIToolbar *toolbar;
	Products *currentProduct;
	UIBarButtonItem *buyNowBarButton;
	UIImageView *addToBagImage;
	//NSString *buttonTitle;
	UILabel *lblTitle;
	UILabel *lblPrice;
	ProductViewController *productViewController;
	UIImage *image;
	UIButton *nextPic;
	UIButton *prevPic;
	NSUInteger numberOfPages;
	NSUInteger currentPage;
	AirlinePackageViewController *airlineViewController;
	AirlineProductViewController *airlineProductViewController;
	NSArray* toolbarItems;
	NSString *showAirlineDetailView;
	NSString *prodView;
}

@property (nonatomic, retain) NSString *productImage;
@property (nonatomic, retain) IBOutlet UIImageView *imageView;
@property (nonatomic, retain) IBOutlet UIScrollView *scrollView;
@property (nonatomic, retain) IBOutlet UIToolbar *toolbar;
@property (nonatomic, retain) Products *currentProduct;
@property (nonatomic, retain) UIBarButtonItem *buyNowBarButton;
@property (nonatomic, retain) UIImageView *addToBagImage;
//@property (nonatomic, retain) NSString *buttonTitle;
@property (nonatomic, retain) UILabel *lblPrice;
@property (nonatomic, retain) UILabel *lblTitle;
@property (nonatomic, retain) ProductViewController *productViewController;
@property (nonatomic, retain) UIImage *image;
@property (nonatomic, retain) IBOutlet UIButton *nextPic;
@property (nonatomic, retain) IBOutlet UIButton *prevPic;
@property (nonatomic) NSUInteger numberOfPages;
@property (nonatomic) NSUInteger currentPage;
@property (nonatomic, retain) AirlinePackageViewController *airlineViewController;
@property (nonatomic, retain) AirlineProductViewController *airlineProductViewController;
@property (nonatomic , retain) NSString *showAirlineDetailView;
@property (nonatomic , retain) NSString *prodView;

-(IBAction)previousImage:(id)sender;
-(IBAction)nextImage:(id)sender;
-(IBAction)detailView:(id)sender;
-(void)pageControlAction;
@end
