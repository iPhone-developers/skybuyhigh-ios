//
//  SkyBuyHighAppDelegate.m
//  SkyBuyHigh
//
//  Created by Thapovan InfoSystems on 02/07/09.
//  Copyright Thapovan 2009. All rights reserved.
//

#import "SkyBuyHighAppDelegate.h"
#import "CheckOutProductsController.h"
#import "XMLParser.h"
#import "ZipArchive.h"
#import "PlaceOrder.h"
#import "ProductDetailsDAO.h"
#import "OrderDetails.h"
#import "PlaceOrderViewController.h"
#import "CatalogueDownloadViewController.h"
#import "DeviceRegViewController.h"
#import "UnzipViewController.h"
#import "DataCache.h"
#import "WelcomePageViewController.h"

@implementation SkyBuyHighAppDelegate

@synthesize window;

@synthesize tabBarController;
@synthesize menNavController,womenNavController,giftNavController,privatejetNavController;
@synthesize menTableViewController,womenTableViewController,giftTableViewController,privatejetTableViewController;
@synthesize nsiCategoryId;
@synthesize addToBagProducts,creditCardDetails,customerBillingDetails,customerShippingDetails;
@synthesize shouldShowTabBar;
@synthesize shouldShowDownloadCatalogue;
@synthesize callDownloadCatalogue;
@synthesize customerAirPackageDetails;
@synthesize deviceRegStatus;
@synthesize downloadAirPackage;

-(void)createEditableCopyOfdatabaseIfNeeded{
	BOOL success ;//fileCopySuccess,welcomeTextCopySuccess,personalShopperCopySuccess;
	NSFileManager *fileManager = [NSFileManager defaultManager];
	NSError *error;
	NSArray *paths= NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentDirectory = [paths objectAtIndex:0];
	NSString *writableDBPath = [documentDirectory stringByAppendingPathComponent:@"skybuyhigh.sqlite"];
	/*NSString *welcomePagePath = [documentDirectory stringByAppendingPathComponent:@"WelcomePage.png"];
	NSString *welcomeTextPath = [documentDirectory stringByAppendingPathComponent:@"WelcomeText.png"];
	NSString *personalShopperPath = [documentDirectory stringByAppendingPathComponent:@"PersonalShopper.png"];
	*/
	//NSString *writableZipPath = [documentDirectory stringByAppendingPathComponent:@"SkyBuy.zip"];
	success = [fileManager fileExistsAtPath:writableDBPath];
	
	if(success) {
		deviceRegStatus = [[ProductDetailsDAO instance] getDeviceRegStatus];
		return;
	}
	NSString *defaultDBPath =[[NSBundle mainBundle] pathForResource:@"skybuyhigh" ofType:@"sqlite"];
	/*NSString *defaultWelcomePagePath =[[NSBundle mainBundle] pathForResource:@"WelcomePage" ofType:@"png"];
	NSString *defaultWelcomeTextPath =[[NSBundle mainBundle] pathForResource:@"WelcomeText" ofType:@"png"];
	NSString *defaultPersonalShopperPath =[[NSBundle mainBundle] pathForResource:@"PersonalShopper" ofType:@"png"];*/
	
	success = [fileManager copyItemAtPath:defaultDBPath toPath:writableDBPath error:&error];
	/*fileCopySuccess = [fileManager copyItemAtPath:defaultWelcomePagePath toPath:welcomePagePath error:&error];
	welcomeTextCopySuccess = [fileManager copyItemAtPath:defaultWelcomeTextPath toPath:welcomeTextPath error:&error];
	personalShopperCopySuccess = [fileManager copyItemAtPath:defaultPersonalShopperPath toPath:personalShopperPath error:&error];
	*/
	
	NSMutableString *ZipPath = [[NSMutableString alloc] initWithString:[[NSBundle mainBundle] pathForResource:@"Images" ofType:@"zip"]];
	[ZipPath replaceOccurrencesOfString:@" " withString:@"\\ " options:NSNumericSearch range:NSMakeRange(0, [ZipPath length])];
	BOOL unzipSuccess = YES;
    ZipArchive* za = [[ZipArchive alloc] init];
		if( [za UnzipOpenFile:ZipPath] )
		{
			BOOL ret = [za UnzipFileTo:documentDirectory overWrite:YES];
			if( NO==ret )
			{
				unzipSuccess = NO;
				NSLog(@"Unzip failed");
			}
			//NSLog(@"Unzip : Success");
			[za UnzipCloseFile];
		}else{
			unzipSuccess = NO;
		}
	[za release];
	[ZipPath release];
	
	
	if(success && unzipSuccess){
		if (!([[Reachability sharedReachability] internetConnectionStatus] == NotReachable)) {
			shouldShowTabBar = NO;
			shouldShowDownloadCatalogue = NO;
			
			/*DeviceRegViewController *deviceRegViewController = [[DeviceRegViewController alloc] initWithNibName:@"DeviceRegView" bundle:nil];
			deviceRegViewController.view.tag = 10020;
			[window addSubview:deviceRegViewController.view];
			[window makeKeyAndVisible];*/
		}	
	}	
	else {
		if(error){
		  NSLog(@"Failed to create writable database file with message '%@'.",[error localizedDescription]);
		}	
	}
}

-(void)initializeTabBar {
	[[window viewWithTag:1000] removeFromSuperview];
	[[window viewWithTag:10010] removeFromSuperview];
	
	addToBagProducts =[[NSMutableArray alloc] init];
	creditCardDetails = [[CreditCardDetails alloc] init];
	customerBillingDetails = [[CustomerDetails alloc] init];
	customerShippingDetails = [[CustomerDetails alloc] init];
	customerAirPackageDetails = [[CustomerDetails alloc] init];
	tabBarController = [[UITabBarController alloc] init];

	menTableViewController = [[MenCategoryViewController alloc] initWithStyle:UITableViewStylePlain];
	[menTableViewController setTitle:@"For Him"];
	menNavController = [[[UINavigationController alloc] initWithRootViewController:menTableViewController] autorelease];
	[menNavController setTitle:@"Him"];
	menNavController.navigationBar.barStyle = UIBarStyleBlack;
	menNavController.tabBarItem.image = [UIImage imageNamed:@"men-icon.png"];
	[menTableViewController release];
	
	womenTableViewController = [[WomenCategoryViewController alloc] initWithStyle:UITableViewStylePlain];
	[womenTableViewController setTitle:@"For Her"];
	womenNavController = [[[UINavigationController alloc] initWithRootViewController:womenTableViewController] autorelease];
	[womenNavController setTitle:@"Her"];
	womenNavController.navigationBar.barStyle = UIBarStyleBlack;
	womenNavController.tabBarItem.image = [UIImage imageNamed:@"women-icon.png"];
	[womenTableViewController release];
	
	giftTableViewController = [[GiftCategoryViewController alloc] initWithStyle:UITableViewStylePlain];
	[giftTableViewController setTitle:@"Gifts"];
	giftNavController = [[[UINavigationController alloc] initWithRootViewController:giftTableViewController] autorelease];
	[giftNavController setTitle:@"Gifts"];
	giftNavController.navigationBar.barStyle = UIBarStyleBlack;
	giftNavController.tabBarItem.image = [UIImage imageNamed:@"gift-icon.png"];
	[giftTableViewController release];
	
	/*privatejetTableViewController = [[PrivateJetCategoryViewController alloc] initWithStyle:UITableViewStylePlain];
	[privatejetTableViewController setTitle:@"Partners"];
	privatejetNavController = [[[UINavigationController alloc] initWithRootViewController:privatejetTableViewController] autorelease];
	[privatejetNavController setTitle:@"Partners"];
	privatejetNavController.navigationBar.barStyle = UIBarStyleBlack;
	privatejetNavController.tabBarItem.image = [UIImage imageNamed:@"jet-icon.png"];
	[privatejetTableViewController release];*/
	
	partnersViewController = [[PartnersViewController alloc] initWithNibName:@"PartnersViewController" bundle:nil];
	//[twitterViewController setTitle:@"Test"];
	partnersNavController = [[[UINavigationController alloc] initWithRootViewController:partnersViewController] autorelease];
	[partnersNavController setTitle:@"Partners"];
	partnersNavController.navigationBar.barStyle = UIBarStyleBlack;
	partnersNavController.tabBarItem.image = [UIImage imageNamed:@"jet-icon.png"];
	[partnersViewController release];
	
	//UITableViewController *moreTableViewController = [[UITableViewController alloc] initWithStyle:UITableViewStylePlain];
	deviceRegViewController = [[DeviceRegViewController alloc] init];
	//[moreTableViewController setTitle:@"Device Registration"];
	deviceRegNavController = [[[UINavigationController alloc] initWithRootViewController:deviceRegViewController] autorelease];
	[deviceRegNavController setTitle:@"Registration"];
	deviceRegNavController.navigationBar.barStyle = UIBarStyleBlack;
	deviceRegNavController.tabBarItem.image = [UIImage imageNamed:@"icon-reg.png"];
	[deviceRegViewController release];
	
	twitterViewController = [[TwitterViewController alloc] initWithNibName:@"TwitterView" bundle:nil];
	//[twitterViewController setTitle:@"Test"];
	twitterNavController = [[[UINavigationController alloc] initWithRootViewController:twitterViewController] autorelease];
	[twitterNavController setTitle:@"Twitter"];
	twitterNavController.navigationBar.barStyle = UIBarStyleBlack;
	twitterNavController.tabBarItem.image = [UIImage imageNamed:@"icon-twitter.png"];
	[twitterViewController release];
	
	
	aboutUsViewController = [[AboutUsViewController alloc] initWithNibName:@"AboutUsViewController" bundle:nil];
	//[twitterViewController setTitle:@"Test"];
	aboutUsNavController = [[[UINavigationController alloc] initWithRootViewController:aboutUsViewController] autorelease];
	[aboutUsNavController setTitle:@"About Us"];
	aboutUsNavController.navigationBar.barStyle = UIBarStyleBlack;
	aboutUsNavController.tabBarItem.image = [UIImage imageNamed:@"icon-aboutus.png"];
	[aboutUsViewController release];
	
	contactViewController = [[ContactViewController alloc] initWithNibName:@"ContactViewController" bundle:nil];
	//[twitterViewController setTitle:@"Test"];
	contactNavController = [[[UINavigationController alloc] initWithRootViewController:contactViewController] autorelease];
	[contactNavController setTitle:@"Contact Us"];
	contactNavController.navigationBar.barStyle = UIBarStyleBlack;
	contactNavController.tabBarItem.image = [UIImage imageNamed:@"icon-contactus.png"];
	[contactViewController release];
	
	supportViewController = [[SupportViewController alloc] initWithNibName:@"SupportViewController" bundle:nil];
	//[twitterViewController setTitle:@"Test"];
	supportNavController = [[[UINavigationController alloc] initWithRootViewController:supportViewController] autorelease];
	[supportNavController setTitle:@"Support"];
	supportNavController.navigationBar.barStyle = UIBarStyleBlack;
	supportNavController.tabBarItem.image = [UIImage imageNamed:@"icon-support.png"];
	[supportViewController release];
	
	aboutThisViewController = [[AboutThisViewController alloc] initWithNibName:@"AboutThisViewController" bundle:nil];
	//[twitterViewController setTitle:@"Test"];
	aboutThisNavController = [[[UINavigationController alloc] initWithRootViewController:aboutThisViewController] autorelease];
	[aboutThisNavController setTitle:@"About This App"];
	aboutThisNavController.navigationBar.barStyle = UIBarStyleBlack;
	aboutThisNavController.tabBarItem.image = [UIImage imageNamed:@"icon-aboutthis.png"];
	[aboutThisViewController release];
	
	
	tabBarController.viewControllers = [NSArray arrayWithObjects: womenNavController,menNavController,giftNavController,partnersNavController,deviceRegNavController,twitterNavController,supportNavController,contactNavController, aboutUsNavController,aboutThisNavController, nil];
	[tabBarController setCustomizableViewControllers:nil];
	tabBarController.moreNavigationController.navigationBar.barStyle = UIBarStyleBlack;
	//tabBarController.selectedIndex = 1;
	tabBarController.view.tag = 1000;
	[window addSubview:tabBarController.view];
	
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	NSMutableString *nsiXmlPath = [[NSMutableString alloc] initWithString:[documentsDirectory stringByAppendingPathComponent:@"eCatalogue.xml"]];
	NSURL *xmlurl =  [NSURL fileURLWithPath:nsiXmlPath];
	NSXMLParser *xmlParser = [[NSXMLParser alloc] initWithData:[[NSData alloc] initWithContentsOfURL:xmlurl]];
	[nsiXmlPath release];
	
	//Initialize the delegate.
	XMLParser *parser = [[XMLParser alloc] initXMLParser];
	
	//Set delegate
	[xmlParser setDelegate:parser];
	
	//Start parsing the XML file.
	BOOL success = [xmlParser parse];
	
	if(success) {
		//NSLog(@"No Errors");
	}
	else {
		NSLog(@"Error Error Error!!!");
	}

	//Place Order
	if (!([[Reachability sharedReachability] internetConnectionStatus] == NotReachable)) {
		NSUInteger  orderItemsCount=[[ProductDetailsDAO instance] getOrderItemsCount];
		NSUInteger airPackageItemsCount = [[ProductDetailsDAO instance] getAirPackageItemsCount];
		if((orderItemsCount+airPackageItemsCount)> 0) {				
			PlaceOrderViewController *placeOrderViewController = [[PlaceOrderViewController alloc] initWithNibName:@"PlaceOrderView" bundle:nil];
			placeOrderViewController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
			[self.womenNavController presentModalViewController:placeOrderViewController animated:YES];
		}
	}
	[window makeKeyAndVisible];
}


-(void)downloadCatalogue:(BOOL)shouldDownload
{
	[[window viewWithTag:10010] removeFromSuperview];
	[[window viewWithTag:10020] removeFromSuperview];
	if (!shouldDownload) {
		UnzipViewController *unzip = [[UnzipViewController alloc] initWithNibName:@"UnzipViewController" bundle:[NSBundle mainBundle]];
		unzip.view.tag = 10010;
		[window addSubview:unzip.view];
		[window makeKeyAndVisible];
		
		
		
	}
	else {
		/* 
		 * Code for catalogue update
		 * If network connection is available, check if device is registered, check for catalogue update
		 * Display Catalogue Download View as main view, switch to tabBarController View on completion/cancellation
		 * If no network is available, catalogue will load instead
		 */
		if (!([[Reachability sharedReachability] internetConnectionStatus] == NotReachable)) {
			/*NSError *error = nil;
			NSString *deviceReg = [NSString stringWithContentsOfURL:[NSURL URLWithString:[[ProductDetailsDAO instance] getValidDeviceURL]] encoding:NSUTF8StringEncoding error:&error];
			if (!error) {
				
				if ([deviceReg isEqualToString:@"DID\r\n"]) { // Device has been deleted, update device status in local DB so that Buy option will be disabled.
					[[ProductDetailsDAO instance] updateDeviceStatus];
					[self initializeTabBar];
				}
				//if ([deviceReg isEqualToString:@"DAA\r\n"]) 
				else { // Device is allocated and active - Proceed to Download Catalogue*/
			//if(deviceRegStatus){
					shouldShowTabBar = NO;
					CatalogueDownloadViewController *catalogueDL = [[CatalogueDownloadViewController alloc] initWithNibName:@"CatalogueDownloadViewController" bundle:nil];
					catalogueDL.view.tag = 10010;
					[window addSubview:catalogueDL.view];
					[window makeKeyAndVisible];
			//}else {
			//	[self initializeTabBar];
			//}
			
			/*	}
			}
			else { 
				NSLog(@"Error while checking for valid device - %@", [error localizedDescription]);
				shouldShowTabBar = YES;
				[self initializeTabBar];
			}*/
		} 
		else {
			[self initializeTabBar];
		}
	}
}

-(void)mainApplicationFlow {

	[[window viewWithTag:10001] removeFromSuperview];
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	
	if(![[NSFileManager defaultManager] fileExistsAtPath:[documentsDirectory stringByAppendingString:@"/ZipFileExtracted"]] && !deviceRegStatus) {
		[self downloadCatalogue:NO];
	}
	/*else if(deviceRegStatus && ![[NSFileManager defaultManager] fileExistsAtPath:[documentsDirectory stringByAppendingString:@"/ZipFileExtracted"]]){
	 [self downloadCatalogue:NO];		
	 }*/
	else if (shouldShowDownloadCatalogue) {
		
		[self downloadCatalogue:YES];
	}
	else if (shouldShowTabBar) {
		[self initializeTabBar];		
	}
}




- (void)applicationDidFinishLaunching:(UIApplication *)application {
	shouldShowTabBar = YES;
	shouldShowDownloadCatalogue =YES;
	callDownloadCatalogue =YES;
	downloadAirPackage = NO;

	// NSLog(@"Device Model = %@",[[UIDevice currentDevice] model]);
	//[[ProductDetailsDAO instance] deleteAllOrderDetails];
	//[[ProductDetailsDAO instance] deleteAirPackageDetails];

	
	[self createEditableCopyOfdatabaseIfNeeded];
	// Uncomment this and comment next line to jump right into the application
	//[self mainApplicationFlow];
	
	// Use this to show welcome page and then jump to the application
	[self showWelcomePage];
	
}


-(void)showWelcomePage {
	WelcomePageViewController *welcome = [[WelcomePageViewController alloc] initWithNibName:@"WelcomePageViewController" bundle:nil];
	welcome.view.tag = 10001;
	[window addSubview:welcome.view];
	[window makeKeyAndVisible];
}

- (void)applicationDidReceiveMemoryWarning:(UIApplication *)application {
	[DataCache clearThumbnailCache];
	[DataCache clear];
}

- (void)dealloc {
	[addToBagProducts release];
	/*[menNavController release];
	[menTableViewController release];
	[womenNavController release];
	[womenTableViewController release];
	[giftNavController release];
	[giftTableViewController release];
	[privatejetNavController release];
	[privatejetTableViewController release];
	[creditCardDetails release];
	[customerBillingDetails release];
	[customerShippingDetails release];*/
    
	[window release];
	
    [super dealloc];
}


@end
