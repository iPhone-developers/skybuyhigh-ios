//
//  PlaceOrder.h
//  SkyBuyHigh
//
//  Created by Thapovan Info Systems on 17/08/09.
//  Copyright 2009 Thapovan. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface PlaceOrder : NSObject {
	
}

+(NSString *) sendRequestWithParams:(NSString *)orderXMLString customerId:(NSInteger)customerId;
+(NSString *) sendRequestWithParams:(NSString *)airlinePackageXMLString;

@end
