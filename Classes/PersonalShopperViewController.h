//
//  PersonalShopperViewController.h
//  SkyBuyHigh
//
//  Created by Thapovan Info Systems on 27/07/09.
//  Copyright 2009 Thapovan. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface PersonalShopperViewController : UIViewController {
	
	UIImageView *imageView;

}

@property (nonatomic, retain) IBOutlet UIImageView *imageView;

@end
