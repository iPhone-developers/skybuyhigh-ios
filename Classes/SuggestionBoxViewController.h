//
//  SuggestionBoxViewController.h
//  SkyBuyHigh
//
//  Created by Thapovan InfoSystems on 23/09/09.
//  Copyright 2009 Thapovan. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface SuggestionBoxViewController : UIViewController <UIWebViewDelegate> {
	UIWebView *webView;
	NSString *orderConfirmationNo;
}

@property (nonatomic, retain) IBOutlet UIWebView *webView;
@property (nonatomic, retain) NSString *orderConfirmationNo;
@end
