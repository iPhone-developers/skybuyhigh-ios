//
//  CreditCardDetailsController.m
//  SkyBuyHigh
//
//  Created by Thapovan InfoSystems on 30/07/09.
//  Copyright 2009 Thapovan. All rights reserved.
//

#import "CreditCardDetailsController.h"
#import "CustomerBillingDetailsController.h"
#import "SkyBuyHighAppDelegate.h"
#import "CreditCardDetails.h"


@implementation CreditCardDetailsController
@synthesize webView;

- (void)viewWillAppear:(BOOL)animated {
	
	/*NSString *htmlFile = [[NSBundle mainBundle] pathForResource:@"CreditCardDetails" ofType:@"html"];
	NSData *htmlData = [NSData dataWithContentsOfFile:htmlFile];	
	webView.delegate = self;
    [webView loadData:htmlData MIMEType:@"text/html" textEncodingName:@"UTF-8" baseURL:[NSURL URLWithString:@""]];*/
	
	[super viewWillAppear:animated];
}


- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidLoad {
	
	NSString *htmlFile = [[NSBundle mainBundle] pathForResource:@"CreditCardDetails" ofType:@"html"];
	NSData *htmlData = [NSData dataWithContentsOfFile:htmlFile];	
	webView.delegate = self;
    [webView loadData:htmlData MIMEType:@"text/html" textEncodingName:@"UTF-8" baseURL:[NSURL URLWithString:@""]];
	[super viewDidLoad];
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request  navigationType:(UIWebViewNavigationType)navigationType {

	SkyBuyHighAppDelegate *delegate=(SkyBuyHighAppDelegate *)[[UIApplication sharedApplication]delegate];
	CreditCardDetails *creditCardDetails =(CreditCardDetails *) delegate.creditCardDetails;	
	NSString *requestString = [[request URL] absoluteString];
	
	NSArray *components = [requestString componentsSeparatedByString:@":"];
	
	if ([components count] > 1 && [(NSString *)[components objectAtIndex:0] isEqualToString:@"http"]) {
		if ([(NSString *)[components objectAtIndex:2] isEqualToString:@"CardType"]) {			
			[creditCardDetails setCreditCardType:[[components objectAtIndex:3] stringByReplacingOccurrencesOfString:@"%20" withString:@" "]];
		}
		if ([(NSString *)[components objectAtIndex:4] isEqualToString:@"CardNo"]) {		
			[creditCardDetails setCreditcardNo:[[components objectAtIndex:5] stringByReplacingOccurrencesOfString:@"%20" withString:@" "]];
		}
		if ([(NSString *)[components objectAtIndex:6] isEqualToString:@"CardHolderName"]) {		
			[creditCardDetails setCardHolderName:[[components objectAtIndex:7] stringByReplacingOccurrencesOfString:@"%20" withString:@" "]];
		}
		if ([(NSString *)[components objectAtIndex:8] isEqualToString:@"SecurityNo"]) {		
			[creditCardDetails setSecurityNo:[[components objectAtIndex:9] stringByReplacingOccurrencesOfString:@"%20" withString:@" "]];
		}
		if([(NSString *)[components objectAtIndex:10] isEqualToString:@"ExpiryDate"]) {		
			[creditCardDetails setExpiryDate:[[components objectAtIndex:11] stringByReplacingOccurrencesOfString:@"%20" withString:@" "]];
		}
	}
	
	CustomerBillingDetailsController *customerBillingDetailsController = [[CustomerBillingDetailsController alloc] initWithNibName:@"CustomerBillingDetailsView" bundle:nil];
	[self.navigationController pushViewController:customerBillingDetailsController animated:NO];
	
	return YES; // Return YES to make sure regular navigation works as expected.
}


-(void)webViewDidFinishLoad:(UIWebView *)webView1 {

	SkyBuyHighAppDelegate *delegate=(SkyBuyHighAppDelegate *)[[UIApplication sharedApplication]delegate];
	CreditCardDetails *creditCardDetails =(CreditCardDetails *) delegate.creditCardDetails;	
	NSString *cardType = nil;
	NSString *cardNo = nil;
	NSString *cardHolderName = nil;
	NSString *securityNo = nil;
	NSString *expDate = nil;
	if ([creditCardDetails creditCardType] != nil) {
		cardType = [NSString stringWithFormat:@"%@",[creditCardDetails creditCardType]];
	}
	else {
		cardType =[[NSString alloc] initWithString:@""];
	}
	
	if ([creditCardDetails creditcardNo] != nil) {
		cardNo = [NSString stringWithFormat:@"%@",[creditCardDetails creditcardNo]];
	}
	else {
		cardNo =[[NSString alloc] initWithString:@""];
	}
	if ([creditCardDetails cardHolderName] != nil) {
		cardHolderName = [NSString stringWithFormat:@"%@",[creditCardDetails cardHolderName]];
	}
	else {
		cardHolderName =[[NSString alloc] initWithString:@""];
	}
	
	if ([creditCardDetails securityNo] != nil) {
		securityNo = [NSString stringWithFormat:@"%@",[creditCardDetails securityNo]];
	}
	else {
		securityNo =[[NSString alloc] initWithString:@""];
	}
	
	if ([creditCardDetails expiryDate] != nil) {
		expDate = [NSString stringWithFormat:@"%@",[creditCardDetails expiryDate]];
	}
	else {
		expDate =[[NSString alloc] initWithString:@""];
	}
	NSString *resourcePath = [[[[NSBundle mainBundle] resourcePath]
							   stringByReplacingOccurrencesOfString:@"/" withString:@"//"]
							  stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
	
	NSString *jsCommand = [NSString stringWithFormat:@"setFormValues('%@','%@','%@','%@','%@','file:/%@//ccicons.png')",cardType,cardNo,cardHolderName,securityNo,expDate,resourcePath];
	
    [webView stringByEvaluatingJavaScriptFromString:jsCommand];
}

- (void)dealloc {
	[webView release];
    [super dealloc];
}


@end
