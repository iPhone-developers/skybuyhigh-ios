//
//  OrderDetails.h
//  SkyBuyHigh
//
//  Created by Thapovan InfoSystems on 14/08/09.
//  Copyright 2009 Thapovan. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface OrderDetails : NSObject {
	
	NSString *orderDetailsXml;
	NSUInteger customerId;

}

@property(nonatomic,retain) NSString *orderDetailsXml;
@property(nonatomic) NSUInteger customerId;

@end
