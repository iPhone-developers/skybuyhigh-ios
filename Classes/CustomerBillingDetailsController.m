//
//  CustomerBillingDetailsController.m
//  SkyBuyHigh
//
//  Created by Thapovan InfoSystems on 30/07/09.
//  Copyright 2009 Thapovan. All rights reserved.
//

#import "CustomerBillingDetailsController.h"
#import "CustomerShippingDetailsController.h"
#import "SkyBuyHighAppDelegate.h"
#import "OrderConfirmationViewController.h"
#import "TailNumberViewController.h"

@implementation CustomerBillingDetailsController
@synthesize webView;

- (void)viewWillAppear:(BOOL)animated {
/*	NSString *htmlFile = [[NSBundle mainBundle] pathForResource:@"CustomerBillingDetails" ofType:@"html"];
	NSData *htmlData = [NSData dataWithContentsOfFile:htmlFile];
	webView.delegate = self;
	[webView loadData:htmlData MIMEType:@"text/html" textEncodingName:@"UTF-8" baseURL:[NSURL URLWithString:@""]];*/
	
 }

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidLoad {
	NSString *htmlFile = [[NSBundle mainBundle] pathForResource:@"CustomerBillingDetails" ofType:@"html"];
	NSData *htmlData = [NSData dataWithContentsOfFile:htmlFile];
	webView.delegate = self;
	[webView loadData:htmlData MIMEType:@"text/html" textEncodingName:@"UTF-8" baseURL:[NSURL URLWithString:@""]];
	[super viewDidLoad];
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request  navigationType:(UIWebViewNavigationType)navigationType {
		SkyBuyHighAppDelegate *delegate=(SkyBuyHighAppDelegate *)[[UIApplication sharedApplication]delegate];
		CustomerDetails *customerBillingDetails =(CustomerDetails *) delegate.customerBillingDetails;
		NSString *requestString = [[request URL] absoluteString];
		//NSLog(@"Request String from billing = %@",requestString);
		NSArray *components = [requestString componentsSeparatedByString:@":"];
		if ([components count] > 1 && [(NSString *)[components objectAtIndex:0] isEqualToString:@"http"]) {
			if ([(NSString *)[components objectAtIndex:2] isEqualToString:@"FirstName"]) {			
				[customerBillingDetails setCustFirstName:[[components objectAtIndex:3] stringByReplacingOccurrencesOfString:@"%20" withString:@" "]];
			}
			if ([(NSString *)[components objectAtIndex:4] isEqualToString:@"LastName"])	{		
				[customerBillingDetails setCusLastName:[[components objectAtIndex:5] stringByReplacingOccurrencesOfString:@"%20" withString:@" "]];
			}
			if ([(NSString *)[components objectAtIndex:6] isEqualToString:@"Address1"]) {		
				[customerBillingDetails setAddress1:[[components objectAtIndex:7] stringByReplacingOccurrencesOfString:@"%20" withString:@" "]];
			}
			if([(NSString *)[components objectAtIndex:8] isEqualToString:@"Address2"]) {		
				[customerBillingDetails setAddress2:[[components objectAtIndex:9] stringByReplacingOccurrencesOfString:@"%20" withString:@" "]];
			}
			if([(NSString *)[components objectAtIndex:10] isEqualToString:@"City"]) {
				[customerBillingDetails setCity:[[components objectAtIndex:11] stringByReplacingOccurrencesOfString:@"%20" withString:@" "]];
			}
			if([(NSString *)[components objectAtIndex:12] isEqualToString:@"State"]) {		
				[customerBillingDetails setState:[[components objectAtIndex:13] stringByReplacingOccurrencesOfString:@"%20" withString:@" "]];
			}
			if([(NSString *)[components objectAtIndex:14] isEqualToString:@"Country"]) {		
				[customerBillingDetails setCountry:[[components objectAtIndex:15] stringByReplacingOccurrencesOfString:@"%20" withString:@" "]];
			}
			if([(NSString *)[components objectAtIndex:16] isEqualToString:@"Zip"]) {		
				[customerBillingDetails setZip:[[components objectAtIndex:17] stringByReplacingOccurrencesOfString:@"%20" withString:@" "]];
			}
			if([(NSString *)[components objectAtIndex:18] isEqualToString:@"Phone"]) {		
				[customerBillingDetails setPhone:[[components objectAtIndex:19] stringByReplacingOccurrencesOfString:@"%20" withString:@" "]];
			}
			if([(NSString *)[components objectAtIndex:20] isEqualToString:@"Email"]) {		
				[customerBillingDetails setEmail:[[components objectAtIndex:21] stringByReplacingOccurrencesOfString:@"%20" withString:@" "]];
			}
			if([(NSString *)[components objectAtIndex:22] isEqualToString:@"SameAddress"]) {		
				[customerBillingDetails setSameAddress:[components objectAtIndex:23]];
			}
			if([(NSString *)[components objectAtIndex:24] isEqualToString:@"SpecialIns"]) {		
				[customerBillingDetails setSpecialIns:[[components objectAtIndex:25] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
			}
			if([(NSString *)[components objectAtIndex:26] isEqualToString:@"PersonalMsg"]) {
				[customerBillingDetails setPersonalMsg:[[components objectAtIndex:27] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
			}			
		}
		
		if ([[customerBillingDetails sameAddress] isEqualToString:@"true"]) {
			SkyBuyHighAppDelegate *delegate=(SkyBuyHighAppDelegate *)[[UIApplication sharedApplication]delegate];
			delegate.customerShippingDetails = [[CustomerDetails alloc] initWithCustomerDetails:delegate.customerBillingDetails];
			/*OrderConfirmationViewController *orderConfirmationViewController = [[OrderConfirmationViewController alloc] initWithNibName:@"OrderConfirmationView" bundle:nil];
			[self.navigationController pushViewController:orderConfirmationViewController animated:NO];*/
			
			TailNumberViewController *tailNumberViewController = [[TailNumberViewController alloc] initWithNibName:@"TailNumberView" bundle:nil];
			[self.navigationController pushViewController:tailNumberViewController animated:NO];
			
		} else {
			CustomerShippingDetailsController *customerShippingDetailsController = [[CustomerShippingDetailsController alloc] initWithNibName:@"CustomerShippingDetailsView" bundle:nil];
			[self.navigationController pushViewController:customerShippingDetailsController animated:NO];
		}	
		
		return YES; // Return YES to make sure regular navigation works as expected.
	}

-(void)webViewDidFinishLoad:(UIWebView *)webView1 {

	SkyBuyHighAppDelegate *delegate=(SkyBuyHighAppDelegate *)[[UIApplication sharedApplication]delegate];
	CustomerDetails *customerBillingDetails =(CustomerDetails *) delegate.customerBillingDetails;
	NSString *firstName = nil;
	NSString *lastName = nil;
	NSString *address1 = nil;
	NSString *address2 = nil;
	NSString *city = nil;
	NSString *state = nil;
	NSString *country= nil;
	NSString *zip = nil;
	NSString *phone = nil;
	NSString *email = nil;
	NSString *sameAddress = nil;
	NSString *personalMsg = nil;
	NSString *specialIns = nil;

	if ([customerBillingDetails custFirstName] != nil) {
		firstName = [NSString stringWithFormat:@"%@",[customerBillingDetails custFirstName]];
	}
	else {
		firstName =[[NSString alloc] initWithString:@""];
	}
	
	if ([customerBillingDetails cusLastName] != nil) {
		lastName = [NSString stringWithFormat:@"%@",[customerBillingDetails cusLastName]];
	}
	else {
		lastName =[[NSString alloc] initWithString:@""];
	}
	
	if ([customerBillingDetails address1] != nil) {
		address1 = [NSString stringWithFormat:@"%@",[customerBillingDetails address1]];
	}
	else {
		address1 =[[NSString alloc] initWithString:@""];
	}
	
	if ([customerBillingDetails address2] != nil) {
		address2 = [NSString stringWithFormat:@"%@",[customerBillingDetails address2]];
	}
	else {
		address2 =[[NSString alloc] initWithString:@""];
	}
	
	if ([customerBillingDetails city] != nil) {
		city = [NSString stringWithFormat:@"%@",[customerBillingDetails city]];
	}
	else {
		city =[[NSString alloc] initWithString:@""];
	}
	
	if ([customerBillingDetails state] != nil) {
		state = [NSString stringWithFormat:@"%@",[customerBillingDetails state]];
	}
	else {
		state =[[NSString alloc] initWithString:@""];
	}
	
	if ([customerBillingDetails country] != nil) {
		country = [NSString stringWithFormat:@"%@",[customerBillingDetails country]];
	}
	else {
		country =[[NSString alloc] initWithString:@"US"];
	}
	
	if ([customerBillingDetails zip] != nil) {
		zip = [NSString stringWithFormat:@"%@",[customerBillingDetails zip]];
	}
	else {
		zip =[[NSString alloc] initWithString:@""];
	}
	
	if ([customerBillingDetails phone] != nil) {
		phone = [NSString stringWithFormat:@"%@",[customerBillingDetails phone]];
	}
	else {
		phone =[[NSString alloc] initWithString:@""];
	}
	if ([customerBillingDetails email] != nil) {
		email = [NSString stringWithFormat:@"%@",[customerBillingDetails email]];
	}
	else {
		email =[[NSString alloc] initWithString:@""];
	}
	
	if ([customerBillingDetails sameAddress] != nil) {
		sameAddress = [NSString stringWithFormat:@"%@",[customerBillingDetails sameAddress]];
	}
	else {
		sameAddress =[[NSString alloc] initWithString:@"true"];
	}	
	
	if ([customerBillingDetails specialIns] != nil) {
		specialIns = [NSString stringWithFormat:@"%@",[customerBillingDetails specialIns]];
	}
	else {
		specialIns =[[NSString alloc] initWithString:@""];
	}
	
	if ([customerBillingDetails personalMsg] != nil) {
		personalMsg = [NSString stringWithFormat:@"%@",[customerBillingDetails personalMsg]];
	}
	else {
		personalMsg =[[NSString alloc] initWithString:@""];
	}
	

	
	NSString *jsCommand = [NSString stringWithFormat:@"setFormValues('%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@')",firstName,lastName,address1,address2,city,state,country,zip,phone,email,sameAddress,specialIns,personalMsg];
	
	

    [webView stringByEvaluatingJavaScriptFromString:jsCommand];
}

- (void)dealloc {
	[webView release], webView =nil;
	[super dealloc];
}

@end
