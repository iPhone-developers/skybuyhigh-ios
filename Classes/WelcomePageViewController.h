//
//  WelcomePageViewController.h
//  SkyBuyHigh
//
//  Created by SkyBuyHigh on 02/11/09.
//  Copyright 2009 Thapovan Info Systems. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface WelcomePageViewController : UIViewController {
	UIButton *skipButton;
	UIScrollView *scrollView;
	UIImageView *imageView;
	UIActivityIndicatorView *activityIndicator;
	UILabel *loading;
}

@property (nonatomic, retain) IBOutlet UIButton *skipButton;
@property (nonatomic, retain) IBOutlet UIScrollView  *scrollView;
@property (nonatomic, retain) IBOutlet UIImageView *imageView;
@property (nonatomic, retain) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (nonatomic, retain) IBOutlet UILabel *loading;

-(IBAction) skipIntro;
-(void) showDesignersList;
-(void) callApplication;
@end
