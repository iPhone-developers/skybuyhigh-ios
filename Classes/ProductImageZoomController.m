//
//  ProductImageZoomController.m
//  SkyBuyHigh
//
//  Created by Thapovan InfoSystems on 08/07/09.
//  Copyright 2009 Thapovan. All rights reserved.
//

#import "ProductImageZoomController.h"
#import "ProductViewController.h"
#import "SkyBuyHighAppDelegate.h"
#import "CheckOutProductsController.h"
#import <QuartzCore/QuartzCore.h>
#import "DataCache.h"
#import "ProductDetailsDAO.h"
#import "ControllerManager.h"

@implementation ProductImageZoomController

@synthesize productImage,imageView,scrollView,toolbar, currentProduct,buyNowBarButton, addToBagImage;
@synthesize lblTitle, lblPrice, productViewController, image;
@synthesize nextPic,prevPic;
@synthesize airlineViewController;
@synthesize numberOfPages,currentPage;
@synthesize airlineProductViewController;
@synthesize showAirlineDetailView;
@synthesize prodView;

-(void) showImage {
	[self.scrollView setZoomScale:1.0];
	[self.scrollView setContentSize:CGSizeMake(280,350)];
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	NSString *fileName = [documentsDirectory stringByAppendingPathComponent:self.productImage];
	
	if ( [[DataCache instance] objectForKey:fileName] == nil) {
		UIImage *tempimage= [[UIImage alloc] initWithContentsOfFile:fileName];
		[[DataCache instance] setObject:tempimage forKey:fileName];
		[tempimage release];
	}
	[imageView setImage:[[DataCache instance] objectForKey:fileName]];
	imageView.tag = 999;
	self.scrollView.delegate = self;
	self.scrollView.clipsToBounds = YES;
	[self.scrollView addSubview:imageView];
}

-(void)viewWillAppear:(BOOL)animated {
	[[self.view viewWithTag:999] removeFromSuperview];
	if([showAirlineDetailView isEqualToString:@"ProductView"]){
		if (self.airlineProductViewController == nil) {
			self.airlineProductViewController = (AirlineProductViewController *)[[ControllerManager instance] controllerWithName:@"AirlineProductViewController" andNib:@"AirlineProductViewController"];
			self.airlineProductViewController.hidesBottomBarWhenPushed = YES;
		}
		airlineProductViewController.productData = currentProduct;
		
		CATransition *animation = [CATransition animation];
		//animation.type = @"oglFlip";
		animation.type = kCATransitionReveal;
		if([prodView isEqualToString:@"next"])
			animation.subtype = kCATransitionFromRight;
		else
			animation.subtype = kCATransitionFromLeft;
		animation.duration = 0.75;  //Or whatever
		//animation.speed = 0.75;
		animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionDefault]; //Or whatever
		//animation.startProgress = 0.25;  //Set this as needed
		//animation.endProgress = 0.75;  //Set this as needed
		animation.fillMode = kCAFillModeForwards;
		[[[self.navigationController view] layer] addAnimation:animation forKey:kCATransition];
		
		[self.navigationController pushViewController:airlineProductViewController animated:NO];
		showAirlineDetailView=@"";
		prodView =@"";
		
	}else if([showAirlineDetailView isEqualToString:@"PackageView"]){
		if (self.airlineViewController == nil) {
			
			self.airlineViewController = (AirlinePackageViewController *)[[ControllerManager instance] controllerWithName:@"AirlinePackageViewController" andNib:@"AirlinePackageViewController"];
			self.airlineViewController.hidesBottomBarWhenPushed = YES;
		}
		airlineViewController.productData = currentProduct;
		
		CATransition *animation = [CATransition animation];
		//animation.type = @"oglFlip";
		animation.type = kCATransitionReveal;
		if([prodView isEqualToString:@"next"])
			animation.subtype = kCATransitionFromRight;
		else
			animation.subtype = kCATransitionFromLeft;
		animation.duration = 0.75;  //Or whatever
		//animation.speed = 0.75;
		animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionDefault]; //Or whatever
		//animation.startProgress = 0.25;  //Set this as needed
		//animation.endProgress = 0.75;  //Set this as needed
		animation.fillMode = kCAFillModeForwards;
		[[[self.navigationController view] layer] addAnimation:animation forKey:kCATransition];
		
		[self.navigationController pushViewController:airlineViewController animated:NO];
		
		showAirlineDetailView=@"";
		prodView =@"";
	}
	
	[super viewWillAppear:NO];
}


-(void)viewWillDisappear:(BOOL)animated {
	//NSLog(@"Inside ViewWillDisappear");
	[DataCache clear];
	[super viewWillDisappear:NO];
}

- (void)viewDidAppear:(BOOL)animated {
	numberOfPages = [self.currentProduct totalImagesCount];
	if (numberOfPages == 1) {
		[self.nextPic setHidden:YES];
		[self.prevPic setHidden:YES];
	}
	else {
		[self.nextPic setHidden:NO];
		[self.prevPic setHidden:NO];
	}
	currentPage = 0;
	[self.prevPic setHidden:YES];
	
	if (toolbar == nil) {
		toolbar =[[UIToolbar alloc] initWithFrame:CGRectMake(0,372,320,44)];
	}
	
	UIButton *info = [UIButton buttonWithType:UIButtonTypeInfoLight];
    [info addTarget:self action:@selector(detailView:) forControlEvents:UIControlEventTouchUpInside];
	
	if (toolbarItems == nil) {
		toolbarItems = [NSArray arrayWithObjects:
						[[UIBarButtonItem alloc] initWithCustomView:info],
						[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:NULL action:NULL],
						[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"prev-icon.png"] style:UIBarButtonItemStylePlain target:self action:@selector(previousProduct:)],
						[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:NULL action:NULL],
						[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"next-icon.png"] style:UIBarButtonItemStylePlain target:self action:@selector(nextProduct:)],
						[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:NULL action:NULL],
						[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"cart-icon.png"] style:UIBarButtonItemStylePlain target:self action:@selector(checkoutAction:)],	
						nil];	
		toolbar.barStyle =UIBarStyleBlack;
		toolbar.items = toolbarItems;
	}
	
	// Since only single instance of ProductImageZoomController is created, enable all the buttons when view appears
	// so that if it is set to disabled in one category, it is not disabled in other categories as well.
	for (UIBarButtonItem *item in toolbarItems) {
		[item setEnabled:YES];
	}
	
	self.navigationItem.hidesBackButton = YES;
	self.navigationItem.leftBarButtonItem =	[[UIBarButtonItem alloc] initWithTitle:@"More Products"
																			 style:UIBarButtonItemStyleBordered
																			target:self
																			action:@selector(handleBack:)];	
	if (self.navigationItem.titleView == nil) {
		self.navigationItem.titleView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
	}
	if (lblTitle == nil) {
		lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 220, 20)];
	}
	lblTitle.backgroundColor = [UIColor clearColor];
	
	lblTitle.textColor = [UIColor whiteColor];
	[lblTitle setFont:[UIFont boldSystemFontOfSize:18.0]];
	if (lblPrice == nil) {
		lblPrice = [[UILabel alloc] initWithFrame:CGRectMake(0, 20, 100, 20)];
	}
	lblPrice.backgroundColor = [UIColor clearColor];
	
	lblPrice.textColor = [UIColor whiteColor];
	[lblPrice setFont:[UIFont systemFontOfSize:14.0]];
	if ([(SkyBuyHighAppDelegate *)[[UIApplication sharedApplication] delegate] tabBarController].selectedIndex != 3) {
		lblTitle.text = self.currentProduct.BRAND;
		lblPrice.text = self.currentProduct.Price;
	}
	else {
		lblTitle.text = self.currentProduct.TITLE;
		lblPrice.text = @"";
	}
	[self.navigationItem.titleView addSubview:lblTitle];	
	[self.navigationItem.titleView addSubview:lblPrice];
	
	[self performSelector:@selector(showImage) withObject:nil afterDelay:0.0];
	
	if ([[(SkyBuyHighAppDelegate *)[[UIApplication sharedApplication] delegate] addToBagProducts] count] > 0) {
		[[[toolbar items] objectAtIndex:6] setTitle:[NSString stringWithFormat:@"%d",[[(SkyBuyHighAppDelegate *)[[UIApplication sharedApplication] delegate] addToBagProducts] count]]];
	}else{
		[[[toolbar items] objectAtIndex:6] setTitle:@""];
	}	
	
	if(![(SkyBuyHighAppDelegate *)[[UIApplication sharedApplication] delegate] deviceRegStatus]){	
		[[[toolbar items] objectAtIndex:6] setEnabled:NO];
	}	
	
	id categoryViewController ;
	categoryViewController = [[self.navigationController viewControllers] objectAtIndex:0];
	NSMutableArray *productInformation = [categoryViewController productInfo];
	NSUInteger rowCount = [categoryViewController nsiCurrentRow];
	if(rowCount ==0){
		[[[toolbar items] objectAtIndex:2] setEnabled:NO];		
	}else if((rowCount+1) == productInformation.count){
		[[[toolbar items] objectAtIndex:4] setEnabled:NO];
	}
	
	[self.view addSubview:toolbar];
	[super viewDidAppear:NO];
}


- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
	return [self imageView];
}


-(IBAction)nextImage:(id)sender
{
	
	if(currentPage<numberOfPages-1){
		currentPage=currentPage+1;		
		[self pageControlAction];
	}	
	
	
}	

-(IBAction)previousImage:(id)sender
{
	if(currentPage>0){
		currentPage=currentPage-1;		
		[self pageControlAction];
	}	
	
}	

-(void)pageControlAction{
	
	if (numberOfPages > 2) {
		if (currentPage == numberOfPages - 1) {
			[self.nextPic setHidden:YES];
		}
		else if (currentPage == 0) {
			[self.prevPic setHidden:YES];
		}
		else {
			[self.nextPic setHidden:NO];
			[self.prevPic setHidden:NO];
		}
	}
	else if (numberOfPages == 2) {
		if (currentPage == 0) {
			[self.nextPic setHidden:NO];
			[self.prevPic setHidden:YES];
		}
		else if (currentPage == 1){
			[self.nextPic setHidden:YES];
			[self.prevPic setHidden:NO];
			
		}
	}
	
	[self.scrollView setZoomScale:1.0];
	[[self.view viewWithTag:999] removeFromSuperview];
	
	if (currentPage < [self.currentProduct totalImagesCount]) {
		NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
		NSString *documentsDirectory = [paths objectAtIndex:0];
		NSString *fileName = [documentsDirectory stringByAppendingPathComponent:[[self.currentProduct prodImagesURL] objectAtIndex:currentPage]];
		if ( [[DataCache instance] objectForKey:fileName] == nil) {
			[[DataCache instance] setObject:[[[UIImage alloc] initWithContentsOfFile:fileName] autorelease] forKey:fileName];
		}
		[imageView setImage:[[DataCache instance] objectForKey:fileName]];
		imageView.tag = 999;
	}
	[self.scrollView addSubview:imageView];
	
	CATransition *animation = [CATransition animation];
	animation.type = kCATransitionFade;
	animation.duration = 0.50; 
	animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionDefault]; 
	animation.fillMode = kCAFillModeForwards;
	[[self.scrollView layer] addAnimation:animation forKey:kCATransition];
	
}


- (void) handleBack:(id)sender {
	[self.navigationController popToRootViewControllerAnimated:YES];
	//[self.scrollView setZoomScale:1.0];
	//[self.scrollView setContentSize:CGSizeMake(280,350)];
}


-(IBAction)detailView:(id)sender
{
	id categoryViewController ;
	NSMutableArray *productInfomation ;
	categoryViewController = [[self.navigationController viewControllers] objectAtIndex:0];
	productInfomation = [categoryViewController productInfo];
	NSUInteger rowCount = [categoryViewController nsiCurrentRow];
	Products *product= [productInfomation objectAtIndex:rowCount];	
	
	if ([(SkyBuyHighAppDelegate *)[[UIApplication sharedApplication] delegate] tabBarController].selectedIndex != 3) {
		if (self.productViewController == nil) {
			self.productViewController = [[ProductViewController alloc] initWithNibName:@"ProductViewController" bundle:nil];
			self.productViewController.hidesBottomBarWhenPushed = YES;
		}
		productViewController.productData = product;
	}
	else {
		if ([product isAdv]) {
			if (self.airlineViewController == nil) {
				self.airlineViewController = (AirlinePackageViewController *)[[ControllerManager instance] controllerWithName:@"AirlinePackageViewController" andNib:@"AirlinePackageViewController"];
				self.airlineViewController.hidesBottomBarWhenPushed = YES;
			}
			airlineViewController.productData = product;
		}else{
			
			if (self.airlineProductViewController == nil) {
				self.airlineProductViewController = (AirlineProductViewController *)[[ControllerManager instance] controllerWithName:@"AirlineProductViewController" andNib:@"AirlineProductViewController"];
				self.airlineProductViewController.hidesBottomBarWhenPushed = YES;
			}
			airlineProductViewController.productData = product;
		}
	}
	
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationDuration: 0.65];
	[UIView setAnimationTransition:UIViewAnimationTransitionFlipFromLeft forView:self.navigationController.view cache:YES];
	if ([(SkyBuyHighAppDelegate *)[[UIApplication sharedApplication] delegate] tabBarController].selectedIndex != 3) {
		[self.navigationController pushViewController:productViewController animated:NO];
	}
	else {
		if ([product isAdv]) 
			[self.navigationController pushViewController:airlineViewController animated:NO];
		else
			[self.navigationController pushViewController:airlineProductViewController animated:NO];
	}
	[[[toolbar items] objectAtIndex:0] setEnabled:NO];
	[UIView commitAnimations];
}

- (void)checkoutAction:(id)sender {
	SkyBuyHighAppDelegate *delegate = (SkyBuyHighAppDelegate *)[[UIApplication sharedApplication]delegate];
	if([delegate.addToBagProducts count]>0){
		CheckOutProductsController	*checkoutViewController = [[CheckOutProductsController alloc]init];
		[self.navigationController pushViewController:checkoutViewController animated:YES];
	}
	else {
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"SkyBuyHigh" message:@"You have no items to checkout" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
		[alert show];
		[alert release];
	}
}




- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
	//[DataCache clear];
    [super didReceiveMemoryWarning];
}


- (void)dealloc {
	[scrollView release], scrollView = nil;
	[toolbar release], toolbar = nil;
	[buyNowBarButton release], buyNowBarButton = nil;
	[addToBagImage release], addToBagImage = nil;
	[imageView release];
	[productImage release];
	[lblPrice release];
	[lblTitle release];
	[currentProduct release];
	[productViewController release], productViewController = nil;
	[image release];
	[nextPic release];
	[prevPic release];
	[airlineViewController release],airlineViewController = nil;
    [super dealloc];
}


#pragma mark -
#pragma mark Methods copied over from ProductViewController

-(void)previousProduct:(id)sender
{
	[DataCache clear];
	id categoryViewController;
	NSMutableArray *productInformation ;
	categoryViewController = [[self.navigationController viewControllers] objectAtIndex:0];
	productInformation = [categoryViewController productInfo];
	NSUInteger rowCount = [categoryViewController nsiCurrentRow];
	
	if(rowCount == 0){
		[[[toolbar items] objectAtIndex:2] setEnabled:NO];
	}		
	else if(rowCount + 1 == productInformation.count) {
		[[[toolbar items] objectAtIndex:4] setEnabled:YES];
	}
	if ([[[toolbar items] objectAtIndex:2] isEnabled]) {
		
		Products *prod= [productInformation objectAtIndex:rowCount-1];	
		self.currentProduct  = prod;
		numberOfPages = [self.currentProduct totalImagesCount];
		if (numberOfPages == 1) {
			[self.nextPic setHidden:YES];
			[self.prevPic setHidden:YES];
		}
		else {
			[self.nextPic setHidden:NO];
			[self.prevPic setHidden:NO];
		}
		currentPage = 0;
		[self.prevPic setHidden:YES];
		[categoryViewController setNsiCurrentRow:rowCount-1];
		self.productImage = prod.MainImageURL;
		if(prod != nil) { 		
			if (prod.MainImageURL != nil) { 
				if ([(SkyBuyHighAppDelegate *)[[UIApplication sharedApplication] delegate] tabBarController].selectedIndex != 3) {
					lblTitle.text = self.currentProduct.BRAND;
					lblPrice.text = self.currentProduct.Price;
				}
				else {
					lblTitle.text = self.currentProduct.TITLE;
					lblPrice.text = @"";
				}
				[[self.view viewWithTag:999] removeFromSuperview];
				NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
				NSString *documentsDirectory = [paths objectAtIndex:0];
				NSString *fileName = [documentsDirectory stringByAppendingPathComponent:prod.MainImageURL];
				imageView.tag = 999;
				[self.scrollView setZoomScale:1.0];
				[self.scrollView setContentSize:CGSizeMake(280,350)];
				if ( [[DataCache instance] objectForKey:fileName] == nil) {
					[[DataCache instance] setObject:[[[UIImage alloc] initWithContentsOfFile:fileName] autorelease] forKey:fileName];
					
				}
				[imageView setImage:[[DataCache instance] objectForKey:fileName]];	    
				[self.scrollView addSubview:imageView];
			}
		}
		
		CATransition *animation = [CATransition animation];
		animation.type = kCATransitionReveal;
		animation.subtype = kCATransitionFromLeft;
		animation.duration = 0.75;  
		animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionDefault];
		animation.fillMode = kCAFillModeForwards;
		[[[self imageView] layer] addAnimation:animation forKey:kCATransition];
	}
	
	if(rowCount == 1){
		[[[toolbar items] objectAtIndex:2] setEnabled:NO];
	}
}

-(void)nextProduct:(id)sender
{
	[DataCache clear];
	id categoryViewController ;
	NSMutableArray *productInformation ;
	categoryViewController = [[self.navigationController viewControllers] objectAtIndex:0];
	productInformation = [categoryViewController productInfo];
	NSUInteger rowCount = [categoryViewController nsiCurrentRow];
	if(rowCount + 1 == productInformation.count){
		[[[toolbar items] objectAtIndex:4] setEnabled:NO];
	}
	else if (rowCount == 0) {
		[[[toolbar items] objectAtIndex:2] setEnabled:YES];
	}
	
	if ([[[toolbar items] objectAtIndex:4] isEnabled]) {
		
		Products *prod= [productInformation objectAtIndex:rowCount+1];	
		self.currentProduct  = prod;
		numberOfPages = [self.currentProduct totalImagesCount];
		if (numberOfPages == 1) {
			[self.nextPic setHidden:YES];
			[self.prevPic setHidden:YES];
		}
		else {
			[self.nextPic setHidden:NO];
			[self.prevPic setHidden:NO];
		}
		currentPage = 0;
		[self.prevPic setHidden:YES];
		[categoryViewController setNsiCurrentRow:rowCount+1];
		self.productImage = prod.MainImageURL;
		if(prod != nil) { 		
			if (prod.MainImageURL != nil) { 
				if ([(SkyBuyHighAppDelegate *)[[UIApplication sharedApplication] delegate] tabBarController].selectedIndex != 3) {
					lblTitle.text = self.currentProduct.BRAND;
					lblPrice.text = self.currentProduct.Price;
				}
				else {
					lblTitle.text = self.currentProduct.TITLE;
					lblPrice.text = @"";
				}
				[[self.view viewWithTag:999] removeFromSuperview];
				NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
				NSString *documentsDirectory = [paths objectAtIndex:0];
				NSString *fileName = [documentsDirectory stringByAppendingPathComponent:prod.MainImageURL];
				imageView.tag = 999;
				[self.scrollView setZoomScale:1.0];
				[self.scrollView setContentSize:CGSizeMake(280,350)];
				if ( [[DataCache instance] objectForKey:fileName] == nil) {
					[[DataCache instance] setObject:[[[UIImage alloc] initWithContentsOfFile:fileName] autorelease] forKey:fileName];
					
				}
				[imageView setImage:[[DataCache instance] objectForKey:fileName]];
				[self.scrollView addSubview:imageView];
			}
			
		}
		CATransition *animation = [CATransition animation];
		animation.type = kCATransitionReveal;
		animation.subtype = kCATransitionFromRight;
		animation.duration = 0.75;  //Or whatever
		animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionDefault]; //Or whatever
		animation.fillMode = kCAFillModeForwards;
		[[[self imageView] layer] addAnimation:animation forKey:kCATransition];
	}
	
	if((rowCount+2) == productInformation.count){
		[[[toolbar items] objectAtIndex:4] setEnabled:NO];
	}
}


-(void)addToBagProducts:(id)sender
{	
	NSMutableArray *productInfomation=nil ;
	id categoryViewController=nil;
	SkyBuyHighAppDelegate *delegate=(SkyBuyHighAppDelegate *)[[UIApplication sharedApplication]delegate];
	categoryViewController = [[self.navigationController viewControllers] objectAtIndex:0];
	productInfomation = [categoryViewController productInfo];
	NSUInteger rowCount = [categoryViewController nsiCurrentRow];
	Products *prod= [productInfomation objectAtIndex:rowCount];
	self.currentProduct = prod;
	[delegate.addToBagProducts addObject:self.currentProduct];
	
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	NSString *fileName = [documentsDirectory stringByAppendingPathComponent:[self.currentProduct thumbnailURL]];
	
	UIImage *img=[UIImage imageWithContentsOfFile:fileName];
	addToBagImage=[[UIImageView alloc]init];
	addToBagImage.frame=CGRectMake(25, 20, 200, 200);
	addToBagImage.image=img;
	[self.view addSubview:addToBagImage];
	
	CGPoint checkOutLocation = CGPointMake(800,800);
	[UIView beginAnimations:@"center" context:nil];
	[UIView setAnimationDuration:1.4];
	addToBagImage.center=checkOutLocation;
	addToBagImage.alpha=0;	
	[UIView commitAnimations];
	[img release];
	
}


// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
	// return (interfaceOrientation == UIInterfaceOrientationPortrait);
	return YES;
}

/*
 #pragma mark -
 #pragma mark Single tap event
 
 #define HORIZ_SWIPE_DRAG_MIN  12
 
 #define VERT_SWIPE_DRAG_MAX    4
 
 - (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
 {	
 //NSLog(@"Inside Image Zoom touchesBegan");
 UITouch *touch = [touches anyObject];
 startTouchPosition = [touch locationInView:self.view];
 //NSLog(@"Touch Position = %",startTouchPosition);
 }
 
 - (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
 {
 //NSLog(@"Inside Image Zoom touchesMoved");
 UITouch *touch = [touches anyObject];
 CGPoint currentTouchPosition = [touch locationInView:self.view];
 // If the swipe tracks correctly.
 if (fabsf(startTouchPosition.x - currentTouchPosition.x) >= HORIZ_SWIPE_DRAG_MIN &&
 fabsf(startTouchPosition.y - currentTouchPosition.y) <= VERT_SWIPE_DRAG_MAX)
 {
 // It appears to be a swipe.
 if (startTouchPosition.x < currentTouchPosition.x) {
 //[self myProcessRightSwipe:touches withEvent:event];
 pageControl.currentPage = pageControl.currentPage + 1;
 
 }
 else {
 //[self myProcessLeftSwipe:touches withEvent:event];
 pageControl.currentPage = pageControl.currentPage - 1;
 }
 [self pageControlAction:nil];
 }	
 else
 {
 // Process a non-swipe event.
 }
 }
 */ 

@end
