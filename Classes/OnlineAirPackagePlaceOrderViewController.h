//
//  OnlineAirPackagePlaceOrderViewController.h
//  SkyBuyHigh
//
//  Created by Thapovan Info Systems on 14/12/09.
//  Copyright 2009 Thapovan. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface OnlineAirPackagePlaceOrderViewController : UIViewController {
	
	UIActivityIndicatorView *activityIndicator;
	UITextView *placeOrderStatus;

}

@property (nonatomic, retain) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (nonatomic,retain ) IBOutlet UITextView *placeOrderStatus;

-(void)placeOrder;

@end
