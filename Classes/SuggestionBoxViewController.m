//
//  SuggestionBoxViewController.m
//  SkyBuyHigh
//
//  Created by Thapovan InfoSystems on 30/07/09.
//  Copyright 2009 Thapovan. All rights reserved.
//

#import "SuggestionBoxViewController.h"
#import "ProductDetailsDAO.h"
#import "SkyBuyHighAppDelegate.h"


@implementation SuggestionBoxViewController
@synthesize webView,orderConfirmationNo;

- (void)viewWillAppear:(BOOL)animated {
	NSString *htmlFile = [[NSBundle mainBundle] pathForResource:@"SuggestionBox" ofType:@"html"];
	NSData *htmlData = [NSData dataWithContentsOfFile:htmlFile];
	webView.delegate = self;
	[webView loadData:htmlData MIMEType:@"text/html" textEncodingName:@"UTF-8" baseURL:[NSURL URLWithString:@""]];
	
	[super viewWillAppear:animated];
}


- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request  navigationType:(UIWebViewNavigationType)navigationType {
	
	SkyBuyHighAppDelegate *delegate=(SkyBuyHighAppDelegate *)[[UIApplication sharedApplication]delegate];
	CustomerDetails *customerBillingDetails =(CustomerDetails *) delegate.customerBillingDetails;

	NSString *requestString = [[request URL] absoluteString];
	
	NSArray *components = [requestString componentsSeparatedByString:@":"];
	NSString *feedback = nil;
	NSString *suggestions = nil;
	
	if ([components count] > 1 && [(NSString *)[components objectAtIndex:0] isEqualToString:@"http"]) {
		if ([(NSString *)[components objectAtIndex:2] isEqualToString:@"Feedback"]) {			
			//NSLog(@"Feedback = %@",[[[components objectAtIndex:3] stringByReplacingOccurrencesOfString:@"%20" withString:@" "] stringByReplacingOccurrencesOfString:@"%0A" withString:@"<br/>"]);
			 feedback = [[components objectAtIndex:3] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
			if(![[feedback stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""])
				[customerBillingDetails setFeedback:feedback];
			else
				[customerBillingDetails setFeedback:@""];
		}
		if ([(NSString *)[components objectAtIndex:4] isEqualToString:@"Suggestions"]) {		
			//NSLog(@"Suggestions = %@",[[[components objectAtIndex:5] stringByReplacingOccurrencesOfString:@"%20" withString:@" "] stringByReplacingOccurrencesOfString:@"%0A" withString:@"<br/>"]);
			suggestions=[[components objectAtIndex:5] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
			if(![[suggestions stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""])
				[customerBillingDetails setSuggestions:suggestions];
			else
				[customerBillingDetails setSuggestions:@""];
		}
	}
	
	/*if(![[feedback stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""] || ![[suggestions stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""]){
		[[ProductDetailsDAO instance] updateSuggestions:suggestions feedBack:feedback orderConfirmationNumber:orderConfirmationNo];		
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"SkyBuyHigh" message:@"Thank you for your valuable feedback" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[alert show];
		[alert release];
	}*/
	//[self.navigationController popToRootViewControllerAnimated:YES];
	[self.navigationController popViewControllerAnimated:NO];
	return YES; // Return YES to make sure regular navigation works as expected.
}

-(void)webViewDidFinishLoad:(UIWebView *)webView1 {
	
	SkyBuyHighAppDelegate *delegate=(SkyBuyHighAppDelegate *)[[UIApplication sharedApplication]delegate];
	CustomerDetails *customerBillingDetails =(CustomerDetails *) delegate.customerBillingDetails;
	NSString *feedback = nil;
	NSString *suggestions = nil;
	
	if ([customerBillingDetails feedback] != nil) {
		feedback = [NSString stringWithFormat:@"%@",[customerBillingDetails feedback]];
	}
	else {
		feedback =[[NSString alloc] initWithString:@""];
	}
	
	if ([customerBillingDetails suggestions] != nil) {
		suggestions = [NSString stringWithFormat:@"%@",[customerBillingDetails suggestions]];
	}
	else {
		suggestions =[[NSString alloc] initWithString:@""];
	}
	//NSLog(@"Feedback = %@",feedback);
	//NSLog(@"Suggestions = %@",suggestions);
	
	NSString *jsCommand = [NSString stringWithFormat:@"setFormValues('%@','%@')",feedback,suggestions];
	
    [webView stringByEvaluatingJavaScriptFromString:jsCommand];
}


/*
-(void)webViewDidFinishLoad:(UIWebView *)webView1 {
	

}
 */

- (void)dealloc {
	[webView release];
    [super dealloc];
}


@end
