//
//  TailNumberViewController.m
//  SkyBuyHigh
//
//  Created by Thapovan Info Systems on 09/12/09.
//  Copyright 2009 Thapovan. All rights reserved.
//

#import "TailNumberViewController.h"
#import "ProductDetailsDAO.h"
#import "SuggestionBoxViewController.h" 
#import "SkyBuyHighAppDelegate.h"
#import "OnlinePlaceOrderViewController.h"
#import "OrderConfirmationViewController.h"


@implementation TailNumberViewController
@synthesize webView,orderConfirmationNo;

- (void)viewWillAppear:(BOOL)animated {
	
    NSString *htmlFile = [[NSBundle mainBundle] pathForResource:@"TailNumberDetails" ofType:@"html"];
	NSData *htmlData = [NSData dataWithContentsOfFile:htmlFile];
	webView.delegate = self;
	[webView loadData:htmlData MIMEType:@"text/html" textEncodingName:@"UTF-8" baseURL:[NSURL URLWithString:@""]];
	
	[super viewWillAppear:animated];
}


- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request  navigationType:(UIWebViewNavigationType)navigationType {
	
	SkyBuyHighAppDelegate *delegate=(SkyBuyHighAppDelegate *)[[UIApplication sharedApplication]delegate];
	CustomerDetails *customerBillingDetails =(CustomerDetails *) delegate.customerBillingDetails;
	NSString *requestString = [[request URL] absoluteString];
	
	//NSLog(@"Request String =%@",requestString);
	
	NSArray *components = [requestString componentsSeparatedByString:@":"];
	if ([components count] > 1 && [(NSString *)[components objectAtIndex:0] isEqualToString:@"http"]) {
		if ([(NSString *)[components objectAtIndex:2] isEqualToString:@"CallSuggestionBox"]) {			
			SuggestionBoxViewController *suggestionBoxViewController=[[SuggestionBoxViewController alloc] initWithNibName:@"SuggestionBoxView" bundle:nil];
			[self.navigationController pushViewController:suggestionBoxViewController animated:NO];
		}else{		   
			if ([(NSString *)[components objectAtIndex:2] isEqualToString:@"TailNumber"]) {
			//	NSLog(@"Tail Number = %@",[[components objectAtIndex:3] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding]);
				[customerBillingDetails setTailNumber:[[components objectAtIndex:3] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
			    [self saveProductsDetails];
			}	
			
			
		}
	}	
		//[self.navigationController popToRootViewControllerAnimated:YES];
	return YES; // Return YES to make sure regular navigation works as expected.
}

-(void)webViewDidFinishLoad:(UIWebView *)webView1 {
	
	NSString *resourcePath = [[[[NSBundle mainBundle] resourcePath]
							   stringByReplacingOccurrencesOfString:@"/" withString:@"//"]
							  stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
	
	NSString *jsCommand = [NSString stringWithFormat:@"setFormValues('file:/%@//logo_black.png')",resourcePath];
	
    [webView stringByEvaluatingJavaScriptFromString:jsCommand];
	

}

-(void)saveProductsDetails{
	SkyBuyHighAppDelegate *delegate=(SkyBuyHighAppDelegate *)[[UIApplication sharedApplication]delegate];
	
	if([delegate.addToBagProducts count]>0){
		
		DeviceDetails *deviceDetails=[[ProductDetailsDAO instance] getDeviceRegDetails];
		[[ProductDetailsDAO instance] getDatabaseConnection];
		NSDateFormatter *dateFormatter = [[[NSDateFormatter alloc] init] autorelease];
		[dateFormatter setDateFormat:@"MMddyy-HHmmss"];
		NSString *currentDatetime = [dateFormatter stringFromDate:[NSDate date]];
		
		orderConfirmationNo =[[NSString alloc] initWithFormat:@"%@-%@",delegate.customerBillingDetails.phone,currentDatetime];
		NSInteger customerId=[[ProductDetailsDAO instance] insertCustomerBillingDetails:delegate.customerBillingDetails customerShippingDetails:delegate.customerShippingDetails customerCreditCatddetails:delegate.creditCardDetails deviceRegDetails:deviceDetails orderConfirmationNumber:orderConfirmationNo];
		
		
		for(int i=0;i<[delegate.addToBagProducts count];i++){		
			Products *prod = [delegate.addToBagProducts objectAtIndex:i];
			[[ProductDetailsDAO instance] insertProductDetails:prod setCustomerId:customerId setOrderConfirmationNo:orderConfirmationNo setTailNumber:delegate.customerBillingDetails.tailNumber];
			[prod release];		
		}	
		[[ProductDetailsDAO instance] closeDatabaseConnection];
		
	}
	NSString *email = [delegate.customerBillingDetails	email];
	delegate.creditCardDetails = [[CreditCardDetails alloc] init];	
	delegate.customerBillingDetails = [[CustomerDetails alloc] init];	
	delegate.customerShippingDetails = [[CustomerDetails alloc] init];
	delegate.addToBagProducts = [[NSMutableArray alloc] init];
	
	if (!([[Reachability sharedReachability] internetConnectionStatus] == NotReachable)) {
		OnlinePlaceOrderViewController *onlinePlaceOrderViewController = [[OnlinePlaceOrderViewController alloc] initWithNibName:@"OnlinePlaceOrderViewController" bundle:nil];
		[onlinePlaceOrderViewController setOrderConfirmationNo:orderConfirmationNo];
		[onlinePlaceOrderViewController setEmailTextMessage:[[NSString alloc] initWithFormat:@"Please note the order confirmation number which will also be e-mailed to %@",email]];
		[self.navigationController pushViewController:onlinePlaceOrderViewController animated:NO];
	}else{
		OrderConfirmationViewController *orderConfirmationViewController = [[OrderConfirmationViewController alloc] initWithNibName:@"OrderConfirmationView" bundle:nil];
		[orderConfirmationViewController setOrderConfirmationNo:orderConfirmationNo];
		[orderConfirmationViewController setEmailTextMessage:[[NSString alloc] initWithFormat:@"Please note the order confirmation number which will also be e-mailed to %@",email]];
		[self.navigationController pushViewController:orderConfirmationViewController animated:NO];
	}	
	
}

- (void)dealloc {
	[webView release];
    [super dealloc];
}

@end
