//
//  CheckOutProductsController.m
//  SkyBuyHigh
//
//  Created by Thapovan InfoSystems on 09/07/09.
//  Copyright 2009 Thapovan. All rights reserved.
//

#import "CheckOutProductsController.h"
#import "SkyBuyHighAppDelegate.h"
#import <QuartzCore/QuartzCore.h>
#import "CreditCardDetailsController.h"
#import "ImageManipulations.h"
#import "CreditCardDetailsController.h"
#import "DataCache.h"


@implementation CheckOutProductsController
@synthesize productInfo;
@synthesize nsiCurrentRow;
//@synthesize nsuGrandTotal;
@synthesize buyNowBarButton;
@synthesize dGrandTotal;
@synthesize myCustomCell;

- (void)viewDidLoad {
    [super viewDidLoad];
}


- (void)viewWillAppear:(BOOL)animated {
	dGrandTotal = 0;
	SkyBuyHighAppDelegate *delegate=(SkyBuyHighAppDelegate *)[[UIApplication sharedApplication]delegate];
	self.productInfo=delegate.addToBagProducts;
	
	for(int i=0;i<productInfo.count;i++)
	{
		Products *rowData = [self.productInfo objectAtIndex:i];
		NSString *skybuyPrice=[rowData.Price stringByReplacingOccurrencesOfString:@"$" withString:@""];
		double sbhPrice = [skybuyPrice doubleValue];		 
		dGrandTotal =dGrandTotal+sbhPrice;
		
	}
	
	buyNowBarButton = [[UIBarButtonItem alloc] initWithTitle:@"Checkout"
													   style:UIBarButtonItemStyleBordered
													  target:self
													  action:@selector(buyNowAction:)];	
	[self.navigationItem setRightBarButtonItem:buyNowBarButton animated:NO];
	[buyNowBarButton release];
	[super viewWillAppear:animated];
}



- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}


#pragma mark Table view methods


// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	
	dGrandTotal = 0;
	NSUInteger row = productInfo.count;
	for(int i=0;i<row;i++)
	{
		Products *rowData = [self.productInfo objectAtIndex:i];
		NSString *skybuyPrice=[rowData.Price stringByReplacingOccurrencesOfString:@"$" withString:@""];
		double sbhPrice = [skybuyPrice doubleValue];		 
		dGrandTotal =dGrandTotal+sbhPrice;
		
	}
	if( productInfo.count==0)
		row=0;
	else	
		row = row+1;
	
	return (row);
	
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
  	if(productInfo.count==0)
		return 0;
	else	
		return 1;
	
}

- (CGFloat)tableView:(UITableView *)tableView
heightForRowAtIndexPath:(NSIndexPath *) indexPath
{
	return 100;
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	if([indexPath row] >=self.productInfo.count) {
		return UITableViewCellEditingStyleNone;
	}
	else 
		return UITableViewCellEditingStyleDelete;
}


- (UITableViewCell *)tableView:(UITableView *)tableView
		 cellForRowAtIndexPath:(NSIndexPath *) indexPath {
	

	static NSString *CellIdentifier = @"CheckoutCellIdentifier";
	NSUInteger row = [indexPath row];
	if(row<self.productInfo.count) {
		UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];	
		
		if (cell == nil) {
			//cell = [[[CheckOutProductTableView alloc] initWithFrame:CGRectZero reuseIdentifier:CellIdentifier] autorelease];
			[[NSBundle mainBundle] loadNibNamed:@"CheckoutViewCell" owner:self options:nil];
			cell = myCustomCell;
			myCustomCell = nil;
			NSString *backgroundImagePath = [[NSBundle mainBundle] pathForResource:@"checkout-rowbg-gray" ofType:@"png"];
			UIImage *backgroundImage = [[UIImage alloc] initWithContentsOfFile:backgroundImagePath];
			cell.backgroundView = [[[UIImageView alloc] initWithImage:backgroundImage] autorelease];
			[backgroundImage release];
			[cell setEditing:YES animated:YES];	
		}	
		
		Products *rowData = [self.productInfo objectAtIndex:row];
		
		if(![rowData isAirlineProduct]){
			[(UILabel *)[cell viewWithTag:2] setText:rowData.BRAND];
			[(UILabel *)[cell viewWithTag:3] setText:rowData.TITLE];
			[(UILabel *)[cell viewWithTag:4] setText:rowData.Price];
			
			if([rowData.size isEqualToString:@"Size"]) {
				[(UILabel *)[cell viewWithTag:5] setText:@"Size        :  -"];
			}											 
			else {
				[(UILabel *)[cell viewWithTag:5] setText:[@"Size        : " stringByAppendingString:rowData.size]];
			}											 		
			
			if([rowData.color isEqualToString:@"Color"]) {
				[(UILabel *) [cell viewWithTag:6] setText:@"Color	      :  -"];
			}											 
			else {
				[(UILabel *) [cell viewWithTag:6] setText:[@"Color	      : " stringByAppendingString:rowData.color]];
			}											   	
		}else{
			[(UILabel *)[cell viewWithTag:2] setText:rowData.TITLE];
			[(UILabel *)[cell viewWithTag:3] setText:@""];
			
			[(UILabel *)[cell viewWithTag:4] setText:rowData.Price];
			
			if(rowData.travelDate==nil ||[rowData.travelDate isEqualToString:@""]) {
				[(UILabel *)[cell viewWithTag:5] setText:@"Travel Date :  -"];
			} 
			else {
				[(UILabel *)[cell viewWithTag:5] setText:[@"Travel Date : " stringByAppendingString:rowData.travelDate]];
			}			
			[(UILabel *) [cell viewWithTag:6] setText:@""];
		}	
		
		NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
		NSString *documentsDirectory = [paths objectAtIndex:0];
		NSString *fileName = [documentsDirectory stringByAppendingPathComponent:rowData.thumbnailURL];
		//UIImageView* imageView = [[UIImageView alloc] initWithFrame:CGRectMake(10, 7, 70, 70)];
		
		if ([[DataCache tableViewImageCache] objectForKey:fileName] == nil) {
			UIImage *fileImage = [[UIImage alloc] initWithContentsOfFile:fileName];
			[ImageManipulations roundedCornersOfImage:fileImage withClipRadius:20 forKey:fileName];
			[fileImage release];
		}
		[(UIImageView *) [cell viewWithTag:1] setImage:[UIImage imageWithCGImage:(CGImageRef)[[DataCache tableViewImageCache] objectForKey:fileName]]];
		//[cell.contentView addSubview:imageView];
		//[imageView release];
		return cell;
		
	}	
	
	
	if(row>=self.productInfo.count){
		
		static NSString *CellIdentifier = @"TotalCell";		 
		UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
		if (cell == nil) {
			cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
			//cell.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"total-cell.png"]];
			cell.backgroundColor = [UIColor colorWithRed:107.0/255.0 green:107.0/255.0 blue:112.0/255.0 alpha:1.0];
			cell.contentView.backgroundColor = [UIColor colorWithRed:107.0/255.0 green:107.0/255.0 blue:112.0/255.0 alpha:1.0];
			cell.selectionStyle=UITableViewCellSelectionStyleNone; 
			
		}
		[[cell.contentView viewWithTag:1026] removeFromSuperview];
		[[cell.contentView viewWithTag:1027] removeFromSuperview];
		[cell setEditing:NO animated:NO];	
		cell.editing = 0;
		//NSLog(@"Last Row entered");
		UILabel* total = [[UILabel alloc] initWithFrame:CGRectMake(10, 40, 120, 25)];
		total.text=@"Grand Total :";
		total.tag = 1027;
		total.backgroundColor = [UIColor clearColor];
		total.opaque = YES;
		total.textColor = [UIColor whiteColor] ;
		//totalAmount.textAlignment = UITextAlignmentCenter; 
		total.font = [UIFont boldSystemFontOfSize:18.0];		
		
		
		UILabel* totalAmount = [[UILabel alloc] initWithFrame:CGRectMake(140, 40, 170, 25)];
		//totalAmount.text=@"";
		totalAmount.tag = 1026;
		totalAmount.backgroundColor = [UIColor clearColor];
		totalAmount.opaque = YES;
		totalAmount.textColor = [UIColor whiteColor] ;
		totalAmount.textAlignment = UITextAlignmentRight; 
		totalAmount.font = [UIFont boldSystemFontOfSize:18.0];
		//[totalAmount setAdjustsFontSizeToFitWidth:YES];
		
		NSNumber *number = [[NSDecimalNumber alloc] initWithDecimal:[[NSDecimalNumber numberWithDouble:dGrandTotal] decimalValue]];
		NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
		[formatter setLocale:[[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"] autorelease]]; 
		[formatter setNumberStyle:NSNumberFormatterCurrencyStyle];
		[formatter setMaximumIntegerDigits:19];
		[formatter setMaximumFractionDigits:2];
		//NSString *sGrandTotal=[formatter stringFromNumber:number];
		//NSLog(@"GrandTotal:%@",[formatter stringFromNumber:number]);	
		
		totalAmount.text =[formatter stringFromNumber:number];
		[number release];
		[formatter release];
		[cell.contentView addSubview:total];
		[cell.contentView addSubview:totalAmount];
		[total release];
		[totalAmount release];
		return cell;
	}
	return nil;
}



-(void)buyNowAction:(id)sender{
	
	if ([self.productInfo count] > 0) {
		CreditCardDetailsController *creditCardDetailsController = [[CreditCardDetailsController alloc] initWithNibName:@"CreditCardDetailsView" bundle:nil];
		[self.navigationController pushViewController:creditCardDetailsController animated:YES];	
	}
	else {
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"SkyBuyHigh" message:@"You have no items to checkout" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
		[alert show];
		[alert release];
	}
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle 
forRowAtIndexPath:(NSIndexPath *)indexPath 
{
	nsiCurrentRow = [indexPath row];
	/*UIAlertView *baseAlert = [[UIAlertView alloc] 
	 initWithTitle:@"SkyBuyHigh" message:@"Do you want to delete this product?" 
	 delegate:self cancelButtonTitle:nil
	 otherButtonTitles:@"OK", @"Cancel", nil];
	 [baseAlert show];*/
	
	if (self.nsiCurrentRow < [self.productInfo count])
		[self.productInfo removeObjectAtIndex:self.nsiCurrentRow];
	
	[tableView beginUpdates];
	
	// Either delete some rows within a section (leaving at least one) or the entire section.
	if ([self.productInfo count] > 0)
	{
		// Section is not yet empty, so delete only the current row.
		[tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
						 withRowAnimation:UITableViewRowAnimationFade];
	}
	else
	{
		// Section is now completely empty, so delete the entire section.
		[tableView deleteSections:[NSIndexSet indexSetWithIndex:indexPath.section] 
				 withRowAnimation:UITableViewRowAnimationFade];
	}
	[self.tableView reloadData];
	[tableView endUpdates];
	
}	 

/*
 - (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
 {
 if(buttonIndex  == 0){
 if (self.nsiCurrentRow < [self.productInfo count])
 [self.productInfo removeObjectAtIndex:self.nsiCurrentRow];
 
 [self.tableView reloadData];
 }	
 [alertView release];
 }*/

- (void)dealloc {
	
    [super dealloc];
}


@end

