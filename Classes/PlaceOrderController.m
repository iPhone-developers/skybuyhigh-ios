//
//  PlaceOrderController.m
//  SkyBuyHigh
//
//  Created by Thapovan InfoSystems on 17/08/09.
//  Copyright 2009 Thapovan. All rights reserved.
//

#import "PlaceOrderController.h"


@implementation PlaceOrderController

+(void)PlaceOrderDetails
{
	//recordResults = FALSE;
	
	
	NSString *soapMessage =@"<?xml version=1.0 encoding=UTF-8?><OrderRecordSet><CustTransId>12324-23433-343423</CustTransId></OrderRecordSet>";

	//NSLog(soapMessage);
	
	NSURL *url = [NSURL URLWithString:@"http://qa.skybuyhigh.com:80/service/PlaceOrderPort"];
	NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:url];
	NSString *msgLength = [NSString stringWithFormat:@"%d", [soapMessage length]];
	
	[theRequest addValue: @"text/xml; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
	[theRequest addValue: @"placeOrder" forHTTPHeaderField:@"SOAPAction"];
	[theRequest addValue: msgLength forHTTPHeaderField:@"Content-Length"];
	[theRequest setHTTPMethod:@"POST"];
	[theRequest setHTTPBody: [soapMessage dataUsingEncoding:NSUTF8StringEncoding]];
	
	NSURLConnection *theConnection = [[NSURLConnection alloc] initWithRequest:theRequest delegate:self];
	
	if( theConnection )
	{
		//NSLog(@"theConnection is NOT NULL");
		//webData = [[NSMutableData data] retain];
	}
	else
	{
		//NSLog(@"theConnection is NULL");
	}
	
		
}

-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
	//NSLog(@"didReceiveResponse entered");
	//[webData setLength: 0];
}
-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
	//NSLog(@"didReceiveData entered");	
	//[webData appendData:data];
}
-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
	//NSLog(@"ERROR with theConenction");
	[connection release];
	//[webData release];
}
-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
	//NSLog(@"connectionDidFinishLoading entered");		
	//NSLog(@"DONE. Received Bytes: %d", [webData length]);
	
}

@end
