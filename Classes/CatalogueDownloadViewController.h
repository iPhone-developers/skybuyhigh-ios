//
//  CatalogueDownloadViewController.h
//  CatalogueDownload
//
//  Created by Thapovan Info Systems on 27/08/09.
//  Copyright Thapovan 2009. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CatalogueDownloadViewController : UIViewController {
	UIProgressView *progressView;
	UILabel *totalBytes;
	UILabel *completedBytes;
	UILabel *downloadedLabel;
	UILabel *totalSizeLabel;
	UILabel *totalBytesLabel;
	NSUInteger fileSize;
	NSURLConnection *theConnection;
	UIButton *cancelButton;
	NSFileHandle *zipHandle;
	NSUInteger completedFileSize;
	UIActivityIndicatorView *activityIndicator;
	UILabel *activityLabel;
	NSString *catalogueDate;
	UIButton *continueButton;
	NSString *airCode;
	NSString *airName;
	NSString *errorMsg;
	BOOL downloadAirPackage;
}

@property (nonatomic, retain) IBOutlet UIProgressView *progressView;
@property (nonatomic, retain) IBOutlet UILabel *totalBytes;
@property (nonatomic, retain) IBOutlet UILabel *completedBytes;
@property (nonatomic, retain) IBOutlet UILabel *downloadedLabel;
@property (nonatomic, retain) IBOutlet UILabel *totalSizeLabel;
@property (nonatomic, retain) IBOutlet UILabel *totalBytesLabel;
@property (nonatomic) NSUInteger fileSize;
@property (nonatomic, retain) NSURLConnection *theConnection;
@property (nonatomic, retain) IBOutlet UIButton *cancelButton;
@property (nonatomic) NSUInteger completedFileSize;
@property (nonatomic, retain) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (nonatomic, retain) IBOutlet UILabel *activityLabel;
@property (nonatomic, retain) NSString *catalogueDate;
@property (nonatomic, retain) IBOutlet UIButton *continueButton;
@property (nonatomic, retain) NSString *airCode;
@property (nonatomic, retain) NSString *airName;
@property (nonatomic, retain) NSString *errorMsg;

-(void)showCatalogue;
-(IBAction)cancelUpdate:(id)sender;
-(IBAction)continueShopping:(id)sender;
-(NSString *) documentsPath;
-(void)waitForContinue;

@end

