//
//  Products.m
//  SkyBuyHigh
//
//  Created by Thapovan InfoSystems on 02/07/09.
//  Copyright 2009 Thapovan. All rights reserved.
//

#import "Products.h"


@implementation Products
@synthesize BRAND,TITLE,MainImageURL,LongDiscription,Price;
@synthesize thumbnailURL,inStorePrice,size,color;
@synthesize prodSize,prodColor,prodCode,prodId,ownerId,categoryId,shortDiscription;
@synthesize prodImagesURL,totalImagesCount,isAdv,travelDate,isAirlineProduct;

/*
-(id)initWithProductBrand:(NSString *)brand prodctTitle:(NSString *)title productImage:(NSString *)image producDescription:(NSString *)desc productPrice:(NSString *)price productSize:(NSString *)Size productColor:(NSString *)color prodThumbnailURL:(NSString *)thumbnailURL
{
	self.BRAND = brand;
	self.TITLE = title;
	self.MainImageURL = image;
	self.LongDiscription = desc;
	self.Price = price;
	self.size = Size;	
	self.thumbnailURL = thumbnailURL;
	self.color = color;
	return self;
};*/

-(id)initWithProductData:(Products *)products
{

	self.BRAND = products.BRAND;
	self.TITLE = products.TITLE;
	self.MainImageURL = products.MainImageURL;
	self.LongDiscription = products.LongDiscription;
	self.Price = products.Price;
	self.inStorePrice=products.inStorePrice;
	self.size = products.size;	
	self.thumbnailURL = products.thumbnailURL;
	self.color = products.color;
	self.prodId=products.prodId;
	self.prodCode=products.prodCode;
	self.ownerId=products.ownerId;
	self.categoryId=products.categoryId;
	self.shortDiscription=products.shortDiscription;
	self.travelDate = products.travelDate;
	self.isAirlineProduct = products.isAirlineProduct;
	return self;

}

@end
