//
//  ProductViewController.h
//  SampleProject
//
//  Created by Thapovan Info Systems on 16/06/09.
//  Copyright 2009 Thapovan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Products.h"
#import "ProductImageZoomController.h"
#import "PersonalShopperViewController.h"
#import "CheckOutProductsController.h"
#import "AirlinePackageViewController.h"

@class AirlinePackageViewController,ProductImageZoomController;

@interface AirlineProductViewController : UIViewController<UIPickerViewDelegate> {
	Products *productData;
	UITextView *productDesc;
	UILabel *productTitle;
	UILabel *productBrand;
	UILabel *productPrice;
	UILabel *productInStorePrice;
	UIImageView *productImage;
	UIImageView *addToBagImage;
	
	UIBarButtonItem *checkoutBarButton;
	UIToolbar *toolbar;
	UIWindow *window;
	UIBarButtonItem *buyNowBarButton;
	ProductImageZoomController *imageController;
	UIButton *personalShopper;
	PersonalShopperViewController *personalShopperViewController;
	
	UIDatePicker *pickerView;
	Products *addtoBagProductData;
	NSArray* toolbarItems;
	CheckOutProductsController	*checkoutViewController;
	AirlinePackageViewController *airlineViewController;
	UIButton *travelDateButton;
	UILabel *travelDate;
	UIToolbar *pickerToolbar;
	NSArray* pickeToolbarItems;
	NSDate *selectedTravelDate;
}
@property(nonatomic,retain) IBOutlet UITextView *productDesc;
@property(nonatomic,retain) IBOutlet UILabel *productTitle;
@property(nonatomic,retain) IBOutlet UILabel *productBrand;
@property(nonatomic,retain) IBOutlet UILabel *productPrice;
@property(nonatomic,retain) IBOutlet UIImageView *productImage;
@property(nonatomic,retain) IBOutlet UIImageView *addToBagImage;
@property(nonatomic,retain) Products *productData;
@property(nonatomic,retain) UIBarButtonItem *checkoutBarButton;
@property(nonatomic,retain) UIToolbar *toolbar;
@property(nonatomic,retain) UIWindow *window;
@property(nonatomic, retain) UIBarButtonItem *buyNowBarButton;
@property (nonatomic, retain) IBOutlet UILabel *productInStorePrice;
//@property (nonatomic, retain) NSString *buttonTitle;
@property (nonatomic, retain) ProductImageZoomController *imageController;
@property (nonatomic, retain) IBOutlet UIButton *personalShopper;
@property (nonatomic, retain) PersonalShopperViewController *personalShopperViewController;
@property(nonatomic,retain) UIDatePicker *pickerView;
@property(nonatomic,retain) Products *addtoBagProductData;
@property (nonatomic, retain) NSArray *toolbarItems;
@property (nonatomic, retain) CheckOutProductsController *checkoutViewController;
@property (nonatomic, retain) AirlinePackageViewController *airlineViewController;
@property(nonatomic,retain) UIButton *travelDateButton;
@property(nonatomic,retain) IBOutlet UILabel *travelDate;
@property(nonatomic,retain) UIToolbar *pickerToolbar;
@property(nonatomic,retain) NSArray* pickeToolbarItems;
@property(nonatomic,retain) NSDate *selectedTravelDate;


-(IBAction) showPersonalShopper:(id)sender;

-(void)nextProduct:(id)sender;
-(void)previousProduct:(id)sender;
-(void)addToBagProducts:(id)sender;
- (void)showTravelDate;
-(void)showTraveldatePicker:(id)sender;
-(void)hidePickerView:(id)sender;
- (void)selectedTravelDate:(id) sender;

@end
