//
//  CheckOutProductsController.h
//  SkyBuyHigh
//
//  Created by Thapovan InfoSystems on 09/07/09.
//  Copyright 2009 Thapovan. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface CheckOutProductsController : UITableViewController 
<UITableViewDelegate, UITableViewDataSource> {
	NSMutableArray *productInfo;
	NSUInteger nsiCurrentRow;
	//NSUInteger nsuGrandTotal;
	double dGrandTotal;

	UIBarButtonItem *buyNowBarButton;
	UITableViewCell *myCustomCell;
}
@property(nonatomic,retain) NSMutableArray *productInfo;
@property(nonatomic) NSUInteger nsiCurrentRow;
//@property(nonatomic) NSUInteger nsuGrandTotal;
@property(nonatomic,retain) UIBarButtonItem *buyNowBarButton;
@property (nonatomic) double dGrandTotal;
@property (nonatomic, retain) IBOutlet UITableViewCell *myCustomCell;

-(void)buyNowAction:(id)sender;

@end
