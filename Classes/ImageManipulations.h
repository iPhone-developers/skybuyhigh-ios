//
//  ImageManipulations.h
//  PalTalk
//
//  Created by Thapovan Info Systems on 30/06/09.
//  Copyright 2009 Thapovan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreGraphics/CoreGraphics.h>

@interface ImageManipulations : NSObject {

}

+ (UIImage *)roundCornersOfImage:(UIImage *)source withClipRadius:(int) clipRadius;
+ (UIImage *)reflectedImage:(UIImageView *)fromImage withHeight:(NSUInteger)height;
+ (UIImageView *)getStarsOfType:(NSString *)starType withPosition:(int)i;
+ (void)roundedCornersOfImage:(UIImage *)source withClipRadius:(int) clipRadius forKey:(NSString *)key;
@end
