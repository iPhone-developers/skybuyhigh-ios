//
//  AboutThisViewController.m
//  SkyBuyHigh
//
//  Created by SkyBuyHigh on 21/12/09.
//  Copyright 2009 Thapovan Info Systems. All rights reserved.
//

#import "AboutThisViewController.h"
#import "DeviceDetails.h"
#import "ProductDetailsDAO.h"
#import "SkyBuyHighAppDelegate.h"

@implementation AboutThisViewController

@synthesize textView;

/*
 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        // Custom initialization
    }
    return self;
}
*/

- (void)viewWillAppear:(BOOL)animated {
	NSDictionary *infoPlist = [[NSBundle mainBundle] infoDictionary]; 
	NSMutableString *aboutThisText = [[NSMutableString alloc] initWithString:@"Version "];
	[aboutThisText appendString:[infoPlist valueForKey:@"CFBundleShortVersionString"]];
	
	NSString *catDate = [[ProductDetailsDAO instance] getCatalogueDate];
	if (catDate != nil && ![catDate isEqualToString:@""]) {
		[aboutThisText appendString:@"\n\nCatalogue Date\n"];
		[aboutThisText appendString:[catDate stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
	}
	/*if ([(SkyBuyHighAppDelegate *) [[UIApplication sharedApplication] delegate] deviceRegStatus]) {
		DeviceDetails *thisDevice = [[ProductDetailsDAO instance] getDeviceRegDetails];
		if (thisDevice != nil) {
			[aboutThisText appendString:[NSString stringWithFormat:@"\n\nDevice ID\n%@\n\nAir Charter Name\n%@\n", [thisDevice deviceCode], [thisDevice airName]]];
		}
	}*/
	[textView setText:aboutThisText];
	[aboutThisText release];
	[super viewWillAppear:NO];
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
	
    [super viewDidLoad];
}


/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [super dealloc];
}


@end
