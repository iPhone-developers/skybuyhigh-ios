//
//  MenCategoryViewController.m
//  SkyBuyHigh
//
//  Created by Thapovan InfoSystems on 02/07/09.
//  Copyright 2009 Thapovan. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "MenCategoryViewController.h"
#import "Products.h"
#import "SkyBuyHighAppDelegate.h"
#import "WomenCategoryViewController.h"
#import "GiftCategoryViewController.h"
#import "PrivateJetCategoryViewController.h"
#import "CheckOutProductsController.h"
#import "ImageManipulations.h"
#import "DataCache.h"
#import "ControllerManager.h"

@implementation MenCategoryViewController

@synthesize productInfo;
@synthesize productViewController;
@synthesize nsiCurrentRow, imageController;
@synthesize myCustomCell;

- (void)viewDidLoad { 
	
    [super viewDidLoad];
}


- (void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
}


- (void)viewDidAppear:(BOOL)animated {
	[DataCache clearThumbnailCache];
	[DataCache clear];
	[super viewDidAppear:animated];
}


- (void)viewWillDisappear:(BOOL)animated {
	//NSLog(@"Men viewWillDisappear");
	[super viewWillDisappear:animated];
}


- (void)viewDidDisappear:(BOOL)animated {
	//NSLog(@"Men viewDidDisappear");

	[super viewDidDisappear:animated];
}



// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
  //  return (interfaceOrientation == UIDeviceOrientationLandscapeRight);
	return YES;
}

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}


#pragma mark Table view methods

// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

	return productInfo.count;

}

- (CGFloat)tableView:(UITableView *)tableView
heightForRowAtIndexPath:(NSIndexPath *) indexPath
{
	return 85;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
		 cellForRowAtIndexPath:(NSIndexPath *) indexPath {
	
	static NSString *CellIdentifier = @"CellIdentifier";
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];	
	if (cell == nil) {
		//cell = [[[ProductTableView alloc] initWithFrame:CGRectZero reuseIdentifier:CellIdentifier] autorelease];
		[[NSBundle mainBundle] loadNibNamed:@"MenProductViewCell" owner:self options:nil];
		cell = myCustomCell;
		myCustomCell = nil;
		NSString *backgroundImagePath = [[NSBundle mainBundle] pathForResource:@"rowbg-85-gray" ofType:@"png"];
		UIImage *backgroundImage = [[UIImage alloc] initWithContentsOfFile:backgroundImagePath];
		cell.backgroundView = [[[UIImageView alloc] initWithImage:backgroundImage] autorelease];
		[backgroundImage release];
		//cell.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"rowbg.png"]];
		cell.accessoryType = UITableViewCellAccessoryDetailDisclosureButton;
	}
	
	NSUInteger row = [indexPath row];
	Products *rowData = [self.productInfo objectAtIndex:row];
	//[cell setData:rowData];
	[(UILabel *)[cell viewWithTag:2] setText:[rowData BRAND]];
	[(UILabel *)[cell viewWithTag:3] setText:[rowData TITLE]];
	[(UILabel *)[cell viewWithTag:4] setText:[rowData Price]];
	
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	NSString *fileName = [documentsDirectory stringByAppendingPathComponent:rowData.thumbnailURL];
	//NSLog(@"Path to image = %@", fileName);
	[[cell.contentView viewWithTag:1010] removeFromSuperview];
	if ([[DataCache tableViewImageCache] objectForKey:fileName] == nil) {
		UIImage *fileImage = [[UIImage alloc] initWithContentsOfFile:fileName];
		//image = [[UIImage alloc] initWithCGImage:
		[ImageManipulations roundedCornersOfImage:fileImage withClipRadius:20 forKey:fileName];
		//[[DataCache tableViewImageCache] setObject:image forKey:fileName];
		//[image release];
		[fileImage release];
	}
	
	///UIImageView* imageView = [[UIImageView alloc] initWithFrame:CGRectMake(3, 3, 75, 75)];
	//imageView.image = image;
	[(UIImageView *) [cell viewWithTag:1] setImage:[UIImage imageWithCGImage:(CGImageRef)[[DataCache tableViewImageCache] objectForKey:fileName]]];
	
	//UIImageView *shadow = [[UIImageView alloc] initWithFrame:CGRectMake(0, 1, 83, 83)];
	//shadow.image = [UIImage imageNamed:@"glosyoverlay.png"];
	//shadow.tag = 1010;
	//[cell.contentView addSubview:imageView];
	//[cell.contentView addSubview:shadow];
	//[image release];
	//[shadow release];
	//[imageView release];
	
	return cell;
}


/*
- (UITableViewCellAccessoryType)tableView:(UITableView *)tableView
		 accessoryTypeForRowWithIndexPath: (NSIndexPath *)indexPath {
	return UITableViewCellAccessoryDetailDisclosureButton;
	
}
*/


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

	if (imageController == nil) {
		imageController = (ProductImageZoomController *)[[ControllerManager instance] controllerWithName:@"ProductImageZoomController" andNib:@"ProductImageView"];
	}
	imageController.hidesBottomBarWhenPushed = YES;
	NSUInteger row = [indexPath row];
	Products *product = [productInfo objectAtIndex:row];
	[productViewController.productTitle setText:[product TITLE]];
	imageController.productImage =  product.MainImageURL;
	imageController.currentProduct = product;
	//[imageController.scrollView setZoomScale:1.0];
	//[imageController.scrollView setContentSize:CGSizeMake(280,350)];

	//imageController.buttonTitle = @"For Him";
	SkyBuyHighAppDelegate *delegate = [[UIApplication sharedApplication] delegate];
	//[UIView beginAnimations:nil context:NULL];
	//[UIView setAnimationDuration:0.75];
	//[UIView setAnimationTransition:UIViewAnimationTransitionFlipFromLeft forView:delegate.menNavController.view cache:YES];
	[delegate.menNavController pushViewController:imageController animated:YES];
	//[UIView commitAnimations];
	
	nsiCurrentRow = row;
	
	//UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Test" message:@"nothing happens" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil];
	//[alert show];
	//[alert release];
}

- (void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath {
	[self tableView:tableView didSelectRowAtIndexPath:indexPath];
}

- (void)dealloc {
	[super dealloc];
}


@end

