//
//  ProductViewController.h
//  SampleProject
//
//  Created by Thapovan Info Systems on 16/06/09.
//  Copyright 2009 Thapovan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Products.h"
#import "ProductImageZoomController.h"
#import "PersonalShopperViewController.h"
#import "CheckOutProductsController.h"

@interface ProductViewController : UIViewController<UIPickerViewDelegate> {
	Products *productData;
	UITextView *productDesc;
	UILabel *productTitle;
	UILabel *productBrand;
	UILabel *productPrice;
	UILabel *productInStorePrice;
	UIImageView *productImage;
	UIImageView *addToBagImage;
	
	UIBarButtonItem *checkoutBarButton;
	UIToolbar *toolbar;
	UIWindow *window;
	UIBarButtonItem *buyNowBarButton;
	ProductImageZoomController *imageController;
	//NSString *buttonTitle;
	UIButton *personalShopper;
	PersonalShopperViewController *personalShopperViewController;
	
	UIPickerView *pickerView;
	UIToolbar *pickerToolbar;
	UILabel *productSize;
	UILabel *productColor;	
	Products *addtoBagProductData;
	NSString *buttonTitle;
	UIButton *sizeButton;
	UIButton *colorButton;
	BOOL  isSizeandColor;
	NSArray* toolbarItems;
	NSArray* pickeToolbarItems;
	CheckOutProductsController	*checkoutViewController;
	NSUInteger selectedPickerSize;
	NSUInteger selectedPickerColor;
}
@property(nonatomic,retain) IBOutlet UITextView *productDesc;
@property(nonatomic,retain) IBOutlet UILabel *productTitle;
@property(nonatomic,retain) IBOutlet UILabel *productBrand;
@property(nonatomic,retain) IBOutlet UILabel *productPrice;
@property(nonatomic,retain) IBOutlet UIImageView *productImage;
@property(nonatomic,retain) IBOutlet UIImageView *addToBagImage;
@property(nonatomic,retain) Products *productData;
@property(nonatomic,retain) UIBarButtonItem *checkoutBarButton;
@property(nonatomic,retain) UIToolbar *toolbar;
@property(nonatomic,retain) UIWindow *window;
@property(nonatomic, retain) UIBarButtonItem *buyNowBarButton;
@property (nonatomic, retain) IBOutlet UILabel *productInStorePrice;
//@property (nonatomic, retain) NSString *buttonTitle;
@property (nonatomic, retain) ProductImageZoomController *imageController;
@property (nonatomic, retain) IBOutlet UIButton *personalShopper;
@property (nonatomic, retain) PersonalShopperViewController *personalShopperViewController;
@property(nonatomic,retain) UIPickerView *pickerView;
@property(nonatomic,retain) UIToolbar *pickerToolbar;
@property(nonatomic,retain) IBOutlet UILabel *productSize;
@property(nonatomic,retain) IBOutlet UILabel *productColor;
@property(nonatomic,retain) Products *addtoBagProductData;
@property(nonatomic,retain) NSString *buttonTitle;
@property(nonatomic,retain) UIButton *sizeButton;
@property(nonatomic,retain) UIButton *colorButton;
@property(nonatomic) BOOL  isSizeandColor;
@property (nonatomic, retain) NSArray *toolbarItems;
@property (nonatomic, retain) NSArray *pickeToolbarItems;
@property (nonatomic, retain) CheckOutProductsController *checkoutViewController;
@property (nonatomic) NSUInteger selectedPickerSize;
@property (nonatomic) NSUInteger selectedPickerColor;

-(IBAction) showPersonalShopper:(id)sender;

-(void)nextProduct:(id)sender;
-(void)previousProduct:(id)sender;
-(void)addToBagProducts:(id)sender;
-(void)showProductSizeandColorPicker:(id)sender;
- (void)showSizeandColorLabel;
-(void)hidePickerView:(id)sender;


@end
