//
//  GiftCategoryViewController.m
//  SkyBuyHigh
//
//  Created by Thapovan InfoSystems on 02/07/09.
//  Copyright 2009 Thapovan. All rights reserved.
//

#import "GiftCategoryViewController.h"
#import "Products.h"
#import "SkyBuyHighAppDelegate.h"
#import "DataCache.h"
#import "MenCategoryViewController.h"
#import "WomenCategoryViewController.h"
#import "PrivateJetCategoryViewController.h"
#import "CheckOutProductsController.h"
#import "ProductImageZoomController.h"
#import "ImageManipulations.h"
#import "ControllerManager.h"

@implementation GiftCategoryViewController
@synthesize productInfo;
//@synthesize productViewController;
@synthesize nsiCurrentRow;
@synthesize imageController;
@synthesize myCustomCell;


- (id)initWithStyle:(UITableViewStyle)style {
    // Override initWithStyle: if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
    if (self = [super initWithStyle:style]) {
    }
    return self;
}

- (void)viewDidLoad { 
	
    [super viewDidLoad];
	
}





- (void)viewWillAppear:(BOOL)animated {

	[super viewWillAppear:animated];
}


- (void)viewDidAppear:(BOOL)animated {
	[DataCache clearThumbnailCache];
	[DataCache clear];
	[super viewDidAppear:animated];
}
/*
- (void)viewWillDisappear:(BOOL)animated {
	[super viewWillDisappear:animated];
}
*/
/*
- (void)viewDidDisappear:(BOOL)animated {
	[super viewDidDisappear:animated];
}
*/

/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}


#pragma mark Table view methods



// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return productInfo.count;
}

- (CGFloat)tableView:(UITableView *)tableView
heightForRowAtIndexPath:(NSIndexPath *) indexPath
{
	return 85;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
		 cellForRowAtIndexPath:(NSIndexPath *) indexPath {
	
	static NSString *CellIdentifier = @"CellIdentifier";
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];	
	
	if (cell == nil) {
		//cell = [[[ProductTableView alloc] initWithFrame:CGRectZero reuseIdentifier:CellIdentifier] autorelease];
		[[NSBundle mainBundle] loadNibNamed:@"GiftProductViewCell" owner:self options:nil];
		cell = myCustomCell;
		myCustomCell = nil;
		NSString *backgroundImagePath = [[NSBundle mainBundle] pathForResource:@"rowbg-85-gray" ofType:@"png"];
		UIImage *backgroundImage = [[UIImage alloc] initWithContentsOfFile:backgroundImagePath];
		cell.backgroundView = [[[UIImageView alloc] initWithImage:backgroundImage] autorelease];
		[backgroundImage release];
		//cell.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"rowbg.png"]];
		cell.accessoryType = UITableViewCellAccessoryDetailDisclosureButton;
	}
	
	NSUInteger row = [indexPath row];
	Products *rowData = [self.productInfo objectAtIndex:row];
	//[cell setData:rowData];
	[(UILabel *)[cell viewWithTag:2] setText:[rowData BRAND]];
	[(UILabel *)[cell viewWithTag:3] setText:[rowData TITLE]];
	[(UILabel *)[cell viewWithTag:4] setText:[rowData Price]];
	
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	NSString *fileName = [documentsDirectory stringByAppendingPathComponent:rowData.thumbnailURL];
	//NSLog(@"Path to image = %@", fileName);
	[[cell.contentView viewWithTag:1010] removeFromSuperview];
	if ([[DataCache tableViewImageCache] objectForKey:fileName] == nil) {
		UIImage *fileImage = [[UIImage alloc] initWithContentsOfFile:fileName];
		//image = [[UIImage alloc] initWithCGImage:
		[ImageManipulations roundedCornersOfImage:fileImage withClipRadius:20 forKey:fileName];
		//[[DataCache tableViewImageCache] setObject:image forKey:fileName];
		//[image release];
		[fileImage release];
	}
	
	///UIImageView* imageView = [[UIImageView alloc] initWithFrame:CGRectMake(3, 3, 75, 75)];
	//imageView.image = image;
	[(UIImageView *) [cell viewWithTag:1] setImage:[UIImage imageWithCGImage:(CGImageRef)[[DataCache tableViewImageCache] objectForKey:fileName]]];
	
	//UIImageView *shadow = [[UIImageView alloc] initWithFrame:CGRectMake(0, 1, 83, 83)];
	//shadow.image = [UIImage imageNamed:@"glosyoverlay.png"];
	//shadow.tag = 1010;
	//[cell.contentView addSubview:imageView];
	//[cell.contentView addSubview:shadow];
	//[image release];
	//[shadow release];
	//[imageView release];
	
	return cell;
}

/*
- (UITableViewCellAccessoryType)tableView:(UITableView *)tableView
		 accessoryTypeForRowWithIndexPath: (NSIndexPath *)indexPath {
	return UITableViewCellAccessoryDetailDisclosureButton;
}
*/

- (void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath {
	[self tableView:tableView didSelectRowAtIndexPath:indexPath];
}

/*
- (void)tableView:(UITableView *)tableView 
accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath {
	if (productViewController == nil) 
		productViewController = [[ProductViewController alloc] initWithNibName:@"ProductViewController" bundle:nil];
	NSUInteger row = [indexPath row];
	Products *product = [productInfo objectAtIndex:row];
	productViewController.productData = product;
	SkyBuyHighAppDelegate *delegate = [[UIApplication sharedApplication] delegate];
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationDuration: 0.75];
	[UIView setAnimationTransition:UIViewAnimationTransitionFlipFromLeft forView:delegate.giftNavController.view cache:NO];	
	[delegate.giftNavController pushViewController:productViewController animated:NO];
	[UIView commitAnimations];
	nsiCurrentRow = row;
}

*/



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	/*if (productViewController == nil) 
		productViewController = [[ProductViewController alloc] initWithNibName:@"ProductViewController" bundle:nil];
	NSUInteger row = [indexPath row];
	Products *product = [productInfo objectAtIndex:row];
	[productViewController.productTitle setText:[product TITLE]];
	//productViewController.productBrand setText:[product productBrand]];
	//UIImage *img=[[UIImage imageNamed:[product productImage]]retain];
	//[productViewController.productImage setImage:img];
	
	productViewController.productData = product;
	SkyBuyHighAppDelegate *delegate = [[UIApplication sharedApplication] delegate];
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationDuration: 0.75];
	[UIView setAnimationTransition:UIViewAnimationTransitionFlipFromLeft forView:delegate.giftNavController.view cache:NO];
	[delegate.giftNavController pushViewController:productViewController animated:NO];
	[UIView commitAnimations];
	
	nsiCurrentRow = row;*/
	
	if (imageController == nil) {
		imageController = (ProductImageZoomController *)[[ControllerManager instance] controllerWithName:@"ProductImageZoomController" andNib:@"ProductImageView"];
	}	
	imageController.hidesBottomBarWhenPushed = YES;
	NSUInteger row = [indexPath row];
	Products *product = [productInfo objectAtIndex:row];
	[productViewController.productTitle setText:[product TITLE]];
	imageController.productImage =  product.MainImageURL;
	imageController.currentProduct = product;
	//[imageController.scrollView setZoomScale:1.0];
	//[imageController.scrollView setContentSize:CGSizeMake(280,350)];

	SkyBuyHighAppDelegate *delegate = [[UIApplication sharedApplication] delegate];
	//[UIView beginAnimations:nil context:UIGraphicsGetCurrentContext()];
	//[UIView setAnimationDuration:0.75];
	//[UIView setAnimationTransition:UIViewAnimationTransitionFlipFromLeft forView:delegate.giftNavController.view cache:YES];
	
	[delegate.giftNavController pushViewController:imageController animated:YES];
	//[UIView commitAnimations];
	
	nsiCurrentRow = row;
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/


/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:YES];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/


/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/


/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


- (void)dealloc {
    [super dealloc];
}


@end

