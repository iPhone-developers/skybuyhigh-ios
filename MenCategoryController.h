//
//  MenCategoryController.h
//  SkyBuyHigh
//
//  Created by Thapovan InfoSystems on 02/07/09.
//  Copyright 2009 Thapovan. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface MenCategoryController : UIViewController {
	
	NSMutableArray	*productInfo;
}

@property(nonatomic,retain) NSMutableArray *productInfo;

@end
